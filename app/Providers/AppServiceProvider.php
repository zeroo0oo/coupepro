<?php

namespace App\Providers;

use App\Http\Controllers\HomeController;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        \View::share('connect', $this->connect());
    }

    public function connect()
    {
        return (new HomeController())->connect();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        Carbon::setLocale('fr');
        URL::forceScheme('https');
    }
}
