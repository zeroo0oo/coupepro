<?php

namespace App\Http\Controllers;

use Axlon\PostalCodeValidation\Rules\PostalCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use App\Mail;
use Slince\Shopify\Client;
use Slince\Shopify\PrivateAppCredential;
use Srmklive\PayPal\Services\ExpressCheckout;
use PragmaRX\Countries\Package\Countries;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Http;
class HomeController extends Controller
{
    //
    public function connect()
    {
        $credential = new PrivateAppCredential(
            'd2e8c9f7eca2e7eb27cdccee59bc88ff',
            '711c0a587e0b9cae97edc8f0a8187e94',
            'aaca8d8b75e68e04025717edd46a2cc7');

        $client = new Client($credential, 'thinkeditioncom.myshopify.com', [
            'metaCacheDir' => './tmp' // Metadata cache dir, required
        ]);

        return $client;
    }

    public function index()
    {

        return view('products.index');
    }

    public function product()
    {
        return view('product');
    }

    public function variants(Request $request)
    {
        $data       = [];
        $client     = $this->connect();

        $variants   = explode(',', $request->variants);
        $qte        = explode(',', $request->qty);
        $i          = 0;
        foreach ($variants as $variant){
            $var    = $client->getProductVariantManager()->find($variant);
            $image  = $client->getProductImageManager()->find($var->getProductId(), $var->getImageId());
            $data[] = [
                'title' => $var->getTitle(),
                'image' => $image->getSrc(),
                'price' => $var->getPrice(),
                'qty'   => $qte[$i]
            ];
            $i++;
        }

        return response()->json($data);
    }

    public function paypal(Request $request)
    {
        $provider   = new ExpressCheckout;      // To use express checkout.
        $items      = [];
        $p          = [];
        $variants   = explode(',', $request->items);
        $i          = 0;

        foreach ($variants as $variant) {
            $data['items'][]    = [
                'name'  => 'Tondeuse professionnelle Barbe et Cheveux BroStyle',
                'price' => 350.00,
                'qty'   => 2
            ];

            $i++;
        }

        $data['invoice_id']             = rand(1111, 9999);
        $data['invoice_description']    = 'Order ' . $data['invoice_id'] . ' Invoice';

        $data['total']                  = 350.00 * (int) $request->qty;
        $data['return_url']             = route('paypal.success', $data['invoice_id']);
        $data['cancel_url']             = route('paypal.cancel', $data['invoice_id']);

        $options                        = [
            'BRANDNAME' => config("app.name"),
            'LOGOIMG'   => asset('images/type-s-logo-01.svg'),
        ];

        $response                       = $provider->addOptions($options)->setExpressCheckout($data,true);

        return response()->json($response)
            ->withCookie(Cookie('variants', $request->items, 60 * 60 * 24))
            ->withCookie(Cookie('qty', $request->qty, 60 * 60 * 24));
    }

    public function success(Request $request)
    {
        $provider   = new ExpressCheckout;

        $response   = $provider->getExpressCheckoutDetails($request->token);
        $items      = [];
        $variants   = explode(',', $request->cookie('variants'));
        $qty        = explode(',', $request->cookie('qty'));
        $total      = 0;

        if (in_array(strtoupper(isset($response['ACK'])), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

            $i  = 0;
            foreach ($variants as $variant){

                $items[]    = [
                    'title'     => 'title',
                    'variantid' => 1,
                    'quantity'  => 2,
                    'price'     => 350.00
                ];
                $total      += 350.00 * 2;

                $i++;
            }

            $status = $response['PAYERSTATUS'] == 'verified' ? 'paid' : 'refunded';

            $dataOrder                      = [
                'line_items'        => $items,
                'email'             => 'foo@example.com',
                'financial_status'  => $status,
                'first_name'        => $response['FIRSTNAME'],
                'last_name'         => $response['LASTNAME'],
                'address1'          => 'fake address',
                'city'              => 'Fake city',
                'country'           => 'fake country',
                'zip'               => 'fake zip',
                'amount'            => $total,
                'ADDRESSSTATUS'     => $response['ADDRESSSTATUS'],
            ];

            return view('success');
        }

        return view('success');
    }

    public function users(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        Mail::Query()->create([
            'email' => $request->email
        ]);

        return response()->json('good');
    }

    public function cancel(Request $request)
    {
        return view('cancel');
    }

    public function orders($data)
    {
        $client = $this->connect();
        $client->getOrderManager()->create($data);
    }

    public function strip(Request $request)
    {
        $this->validate($request, [
            'fname'         => 'required',
            'lname'         => 'required',
            'email'         => 'required|email',
            'address'       => 'required',
            'city'          => 'required',
            'phone'         => 'required',
            'amount'        => 'required',
            'price'         => 'required',
            'cardNumber'    => 'required',
            'cc_exp'        => 'required',
            'cc_cvc'        => 'required',
        ]);

        $items      = [];

        $items []   = [
            'title'     => 'title',
            'variantid' => $request->variant,
            'quantity'  => 1,
            'price'     => $request->price
        ];
        $price  = $request->price;
        $total  = $price;

        $exp    = explode('/', $request->cc_exp);

        try{
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            $token  = \Stripe\Token::create([
                'card'  =>[
                    'number'    => $request->get('cardNumber'),
                    'exp_month' => $exp[0],
                    'exp_year'  => $exp[1],
                    'cvc'       => $request->get('cc_cvc'),
                ]
            ]);
        }catch(\Stripe\Exception\CardException $exception){
            return response()->json([
                'status'    => 1,
                'message'   => 'Votre carte a été refusée !'
            ]);
        }
        if(!isset($token['id'])) {
            \Session::flash('errors', 'error');
            return response()->json([
                'status'    => 1,
                'message'   => 'Votre carte a été refusée !'
            ]);
        }
        $charge = \Stripe\Charge::create ([
            'currency'      => 'eur',
            'amount'        => $total * 100,
            'card'          => $token['id'],
            'description'   => 'Tondeuse professionnelle Barbe et Cheveux CoupePro',
        ]);

        $status     = ($charge->paid) ? 'paid' : 'refunded';
        $message    = ($charge->paid) ? 'paid' : 'votre solde est Insuffisant';

        Cache::put('price', $total, now()->addDays(1));

        if($charge->paid){

            $cURLConnection = curl_init();

            $data   = [
                'Date'      => now()->day . '-' . now()->month . '-' . now()->year,
                'Heur'      => now()->hour . '-' . now()->minute,
                'ip'        => $request->ip(),
                'Prix'      => $request->price,
                'Nom'       => $request->lname . ' ' . $request->fname ,
                'Ville'     => $request->city,
                'tele'      => $request->phone,
                'product'   => 'Tondeuse professionnelle Barbe et Cheveux CoupePro™',
                'quantity'  => $request->amount,
                'Produit'   => 'Tondeuse professionnelle Barbe et Cheveux CoupePro™',
                'Adresse'   => $request->address,
                'url'       => $request->fullUrl(),
            ];

            $html   = '';

            foreach ($data as $key => $value){
                $html   .= $key . '=' . $value . '&';
            }
            $html = rtrim($html, '&');

            curl_setopt($cURLConnection, CURLOPT_URL, 'https://script.google.com/macros/s/AKfycbw--e3HIKp6V6ElJOS9bet-J3RB5LP63fadOq8Jl8W1PbzNqr_6/exec');
            curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cURLConnection, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($cURLConnection, CURLOPT_HTTPHEADER,[
                    'Content-Type: application/x-www-form-urlencoded',
                ]
            );

            curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $html);

            $phoneList = curl_exec($cURLConnection);
            curl_close($cURLConnection);
        }

        return response()->json([
            'status'    => $status,
            'message'   => $message
        ]);
    }
}
