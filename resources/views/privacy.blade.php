<html>
<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>

        /* Font Definitions */
        @font-face {
            font-family: Helvetica;
            panose-1: 2 11 6 4 2 2 2 2 2 4;
        }

        @font-face {
            font-family: Wingdings;
            panose-1: 5 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: inherit;
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 8.0pt;
            margin-left: 0in;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        h2 {
            mso-style-link: "Heading 2 Char";
            margin-right: 0in;
            margin-left: 0in;
            font-size: 18.0pt;
            font-family: "Times New Roman", serif;
        }

        h3 {
            mso-style-link: "Heading 3 Char";
            margin-right: 0in;
            margin-left: 0in;
            font-size: 13.5pt;
            font-family: "Times New Roman", serif;
        }

        a:link, span.MsoHyperlink {
            color: blue;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 8.0pt;
            margin-left: .5in;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .5in;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .5in;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 8.0pt;
            margin-left: .5in;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.Heading2Char {
            mso-style-name: "Heading 2 Char";
            mso-style-link: "Heading 2";
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }

        span.Heading3Char {
            mso-style-name: "Heading 3 Char";
            mso-style-link: "Heading 3";
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }

        .MsoChpDefault {
            font-family: "Calibri", sans-serif;
        }

        .MsoPapDefault {
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: 1.0in 1.0in 1.0in 1.0in;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0in;
        }

        ul {
            margin-bottom: 0in;
        }

        .privacy-switch {
            font-family: "Open Sans", sans-serif;
        }

    </style>
</head>
<body lang=EN-US link=blue vlink="#954F72">
<div class="WordSection1">
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">Politique de confidentialité</span>
        </b>
    </p>
    <p class="MsoNormal" style="line-height:21.0pt;background:white">
        <span style="font-size:14.0pt;font-family:'Helvetica',sans-serif;color:#1D2129">&nbsp;</span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Notre politique de protection de vos données personnelles permet d'instaurer une relation de confiance entre vous et nos services, afin de vous offrir une expérience positive, de manière totalement transparente.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Grâce à cette politique, nous pourrons prendre en compte vos exigences et être conformes à vos attentes.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Afin de respecter votre confiance, CoupePro™, nous garantissons de façon primordiale le respect de vos données personnelles, ainsi que la confidentialité de nos clients (ci-après « Vos données »).
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Pour une parfaite transparence envers vous, afin de vous garantir une utilisation sécurisée de notre site internet <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a> dans toutes ses versions disponibles et applications liées (ci-après le « Site CoupePro™»), nous mettons à disposition la manière dont nous traitons vos données, afin que nos services soient toujours en accord avec le respect de vos droits. De cette façon, nous assurons la sécurité ainsi que la confidentialité et la non altération de votre vie privée et de vos données, cela à travers l'ensemble de nos plateformes.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Notre politique et nous-mêmes garantissons que toutes les précautions nécessaires à la protection de l'ensemble de vos données et contre la divulgation, la perte ou l’altération de celles-ci sont prises. C'est pourquoi nous mettons à votre disposition tous les éléments vous permettant de comprendre facilement notre façon de traiter vos données. Ces données ne seront conservées que le temps nécessaire à la gestion et au traitement déterminés.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            À ces fins, nous nous efforçons de prendre toutes les dispositions nécessaires dans le but d'être en conformité avec le droit applicable en matière de protection des données.
            <br>
            <br>
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Ainsi, ici soussigné CoupePro™ s'engage par cette politique de protection de vos données personnelles à respecter les principes essentiels du règlement européen général et du droit français en matière de protection des données personnelles, en mettant à votre disposition les informations concernant l'existence et les modalités du traitement des données ici appliqué (paragraphe 3), les droits dont vous disposez à propos de vos données, ainsi qu'en appliquant ces droits nous-mêmes (paragraphe 6 et 10). Toute information relative à d'éventuels transferts vers un pays tiers ou des destinataires est également mise à disposition, ainsi que le temps de conservation des données collectées (paragraphe 5) et les mesures de sécurité (paragraphe 9).

        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:14.0pt;line-height:107%; color:#1D2129">
                1. QUI ÊTES-VOUS ?
            </span>
        </b>
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Lorsque nous évoquons "vous" dans cette Politique de protection des données personnelles, c'est tout simplement parce qu'elle vous est directement liée et vous concerne en tant que client de CoupePro™, si vous avez passé commande sur le site CoupePro™, client de CoupePro si vous avez navigué comme visiteur sur le site CoupePro sans pour autant y avoir créé de compte client ou avoir passé commande.
        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:14.0pt;line-height:107%; color:#1D2129">
                2. QUI SOMMES-NOUS ?
            </span>
        </b>
        <span style="font-size: 14.0pt;line-height:107%;color:#1D2129">
            CoupePro™ est une société par actions simplifiée au capital de 1000 euros, représentée par CoupePro™, en qualité de Président directeur général et dont le siège social est situé 7 Place Félix Baret, 13006 Marseille, France
CoupePro™ édite le Site <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a> et met en œuvre, à ce titre, différents traitements de vos Données en qualité de responsable de traitements.
        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:14.0pt;line-height:107%; color:#1D2129">
                3.  À QUELLES FINS TRAITONS-NOUS VOS DONNÉES ?
            </span>
        </b>
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
                3.1  Quand collectons-nous vos données personnelles ?
            </span>
        </b>
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Vos données personnelles sont susceptibles d'être collectées si vous visitez le site <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a> utilisant des cookies, que vous passez commande d'un de nos produits ou services ou accepté d'être membre de nos newsletters (SMS, e-mails).
        </span>
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
           Vos données personnelles nous servent à fluidifier votre navigation sur le site <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>, ainsi qu'à vous offrir une expérience plus personnalisée. Nous pouvons ainsi traiter au mieux vos commandes, mettre à disposition le paiement en plusieurs fois, éviter les fraudes, effectuer les remboursements nécessaires, et gérer vos avis clients.
        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
               3.2.  Votre navigation sur le Site <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>
            </span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size: 14.0pt;line-height:107%;color:#1D2129">
            Afin de vous permettre une navigation sur le site <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>, nous traitons vos données avec pour base juridique pour cela, votre consentement.
        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
              3.3. Le traitement de vos commandes
            </span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Pour pouvoir nous occuper de vos commandes et les traiter, nous utilisons vos données.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            L'utilisation que nous en faisons sert à gérer la médiation, la relation client (et celle à travers les réseaux sociaux), notre service après-vente et de vente à distance, nos actions tenant à la gestion du marketing et de la prospection commerciale pour le site <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>, ainsi que pour la gestion, les livraisons et transports des commandes.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
           L'exécution du contrat entre les deux parties (vous et nous) est la base juridique du traitement de ces données.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
           L'obligation légale de CoupePro™ est la base juridique du traitement, pour ce qui est la gestion du rappel des produits. Votre consentement ou notre intérêt légitime sont, selon les cas, la base juridique du traitement pour les actions marketing et de prospection commerciale. Votre consentement l'est pour la mise en œuvre du paiement "flash".
        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
              3.4. Paiement en plusieurs fois
            </span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Pour les commandes concernées par le paiement en plusieurs fois et pour certains clients, vos données sont traitées afin de pouvoir vous proposer ce type de paiement. L'application du contrat entre les deux parties est la base juridique du traitement des données. Cependant, votre consentement reste le fondement du traitement de vos données bancaires.

        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
                3.5. Avis clients
            </span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Pour pouvoir partager vos avis avec nos clients et visiteurs ainsi que vous permettent de laisser votre avis sur le site <a href="https://coupepro.hop.boutique"> https://coupepro.hop.boutique</a>  nous utilisons vos données sur la base juridique de votre consentement ou intérêt légitime.
        </span>
    </p>

    <p class="MsoNormal">
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
                3.6. Recouvrement des paiements et lutte contre la fraude
            </span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Afin de permettre le recouvrement des paiements et la lutte contre la fraude, nous utilisons vos données.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            L'application de ce contrat entre les deux parties ainsi que l'intérêt légitime de CoupePro™, en qualité de responsable du traitement, sont les bases juridiques de ce traitement.
        </span>
    </p>
    <p class="MsoNormal">
        <b>
            <span style="font-size:10pt;line-height:107%; color:#1D2129">
                3.7. Opérations de gestion de la régie publicitaire de CoupePro
            </span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Les opérations de régie publicitaire de CoupePro sont gérées grâce à l’utilisation de vos données.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Cela va permettre d’accroître nos données de clients et de prospects, de gérer la maintenance et activités techniques de prospects, les statistiques commerciales et les études de campagnes publicitaires, la mise à jour des fichiers de prospection de l’organisme en charge de la gestion des oppositions au démarchage par téléphone, les sollicitations, la mise en place de nos jeux-concours et loteries ou de toute autre opération à fin promotionnelle sauf jeux d’argent et de hasard en ligne.
        </span>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Les bases juridiques des mentions évoquées ci-dessus sont le consentement de l’utilisateur ou l’intérêt légitime de CoupePro™.
        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">4. OÙ VONT VOS DONNÉES ?</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Vos données sont transmises à plusieurs services internes
            de <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>
<br><br>
            Elles ne sont pas envoyées à des tiers, excepté dans les situations précisées ci-dessous :
<br><br>
            Afin de pouvoir traiter vos commandes, vos données personnelles peuvent être transmises à plusieurs prestataires dont les spécialisations sont les transactions bancaires, les relations clients, le service après-vente, la livraison, le développement informatique, la gestion de site ou encore la fourniture de garanties ou assurances.<br><br>
Pour la mise en place du paiement en x fois, vos données peuvent être partagées avec des prestataires comme des centres de paiement et transaction (banques…), ou encore des centres d’appels pour la gestion des processus métier ou de l’expérience des clients, ou encore, pour les avis clients, à un gestionnaire de recueil et traitement des avis clients.<br><br>
La régie publicitaire de <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a> est quant à elle gérée, grâce à vos données, à des clients de la régie et des annonceurs.
            <br><br>
        </span>

    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">5. CONSERVATION DES DONNÉES</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Les données que récolte <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a> sont conservées uniquement pour le temps et l’aide nécessaires à la mise en place et à l’accomplissement des opérations citées en paragraphe 3 de notre politique de données personnelles.<br><br>
Nous conservons pour un certain temps certaines données collectées par <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>:<br><br>
En archives courantes pour les prospects, durant 3 années à compter du dernier contact du client (elles sont donc consultables par les services de CoupePro™). Nous n’effectuons pas d’archivage intermédiaire de ces données (pour ce qui est des données représentant un intérêt administratif pour certains services, comme pour le contentieux, les délais de conservation sont fixés par les règles de prescription applicables).<br><br>
Concernant nos commandes, vos données seront archivées dans les archives courantes durant 5 années dès la fin de l’usage des commandes du client, et dans les archives intermédiaires durant 5 années dès la fin de la conservation en archives courantes. Il en est de même pour les clients.<br><br>
À propos des données bancaires, elles sont archivées dans les archives courantes durant toute la durée de validité de la carte bancaire (plus un jour). Il n’y a pas d’archivage intermédiaire d’effectué pour les données bancaires.<br><br>
Les cookies et leur utilisation et délai sont détaillés en paragraphe 7 de notre politique.

        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">6. EXERCICE DE VOS DROITS</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            6.1. Vous êtes en droit de réclamer l’accès, la modification et la rectification de vos Données.<br><br>
            6.2. Vous êtes en droit de réclamer la limitation du traitement de vos Données.<br><br>
Précision importante : pour cela vous devez faire contestation de l’exactitude de vos données personnelles durant le temps voulu nous permettant de vérifier la conformité de ces dernières. Ou encore, dans l’hypothèse où vous estimez que l’utilisation que nous faisons de vos données personnelles est illicite et que vous réclamez une limitation de leur utilisation et non pas un effacement. Nous n’avons plus la nécessité d’utiliser vos données vis-à-vis des buts cités en paragraphe 3 mais que vos données sont encore utiles pour la constatation, l’exercice ou la défense de vos droits en justice, dans l’hypothèse où vous décidez d’exercer votre droit d’opposition durant le temps voulu pour la vérification afin de déterminer si les motifs légitimes que nous poursuivons prévalent sur les vôtres.<br><br>
6.3. Vous êtes en droit de réclamer la suppression de vos Données.<br><br>
Si vous exigez la suppression de vos données personnelles aura tout de même la possibilité de les conserver au format d’archive intermédiaire durant le temps nécessaire pour satisfaire ses obligations légales, comptables et fiscales.<br><br>
6.4. Vous êtes en droit de réclamer l’exercice de votre droit d’opposition face aux traitements exploités à des fins de prospections commerciales.<br><br>
En cas de prospection par e-mail, vous êtes en droit de demander la modification ou le désabonnement des newsletters en effectuant un clic sur le lien hypertexte « me désabonner » disponible dans toutes les newsletters, ou encore en naviguant directement sur la page contact du site <a href=" https://coupepro.hop.boutique"> https://coupepro.hop.boutique</a>
<br><br>
En cas de prospection par SMS, il est possible d’effectuer un désabonnement en transmettant par SMS la mention « STOP SMS » au 36007, ou en naviguant sur la page contact du site <a href=" https://coupepro.hop.boutique"> https://coupepro.hop.boutique</a><br><br>
6.5. Vous êtes en droit de transmettre des prérogatives post-mortem concernant la conservation, l’effacement et la communication de vos données personnelles.
En cas d’absence de ce type de prérogative, vos successeurs et héritiers ont la possibilité de communiquer avec CoupePro™ pour pouvoir avoir accès aux usages de ces données et permettre une « organisation et règlement de la succession du défunt » et/ou de clore le compte sur le site et/ou demander la non-poursuite du traitement des données personnelles.<br><br>
Vous pouvez également faire la demande de non-communication de vos données à un tiers en cas de décès.<br><br>
6.6. Vous êtes en droit de réclamer votre droit à la portabilité.<br><br>

6.7. Vous êtes en droit de revenir sur votre consentement concernant la réalisation des traitements fondés sur cette base juridique.<br><br>
Précision importante : Si vous décidez de revenir sur votre consentement cela ne pourra pas avoir effet sur la licéité des utilisations réalisées avant votre retrait de consentement.<br><br>
6.8. Vous êtes en droit, quand vous le souhaitez, de porter une réclamation devant l’autorité de contrôle compétente (en France, la CNIL : www.cnil.fr).<br><br>
Afin d’exercer vos droits, merci d’adresser votre réclamation (accompagnée de votre e-mail, nom, prénom, copie de votre pièce d’identité et adresse postale) à la délégation de protection des données de CoupePro™ par e-mail à contact@coupepro.hop.boutique et/ou par voie postale à 7 Place Félix Baret, 13006 Marseille, France<br>
Dans un délai d’un (1) mois maximum après la date de réception de la réclamation, nous vous adresserons une réponse.

        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">7. COOKIES</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            7.1. Qu’est-ce qu’un cookie ?<br><br>
Lorsque vous naviguez sur un site internet comme le site de CoupePro™, celui-ci peut alors, selon votre choix, insérer sur votre récepteur (ordinateur, téléphone ou tablette), par le biais de votre navigateur, un fichier texte.<br><br>
Ce fichier texte est appelé COOKIE. Ce cookie permet alors au site internet comme <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>, durant le temps prescrit de validité ou d’enregistrement du cookie, d’identifier votre récepteur utilisé lorsque vous effectuez une autre visite.<br><br>
Seul l’émetteur d'un cookie est susceptible de lire ou de modifier les informations contenues dans ce cookie.<br><br>
7.2. À quoi servent les cookies sur <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>?<br><br>
On peut classer différents types de cookies par catégories. Ils sont pour certains émis directement par CoupePro et ses prestataires, mais certains proviennent parfois de sociétés tierces.<br><br>
7.2.1. Cookies émis par CoupePro et ses prestataires<br><br>
Il existe plusieurs catégories de cookies pouvant se retrouver sur votre émetteur lorsque vous effectuez une navigation sur notre site internet :<br><br>
7.2.1.1. Cookies « Essentiels »<br><br>
Afin d’avoir accès à notre site, les cookies « essentiels » sont nécessaires, ils servent par exemple à pouvoir effectuer la commande.<br><br>
S’ils n’étaient pas présents, vous pourriez rencontrer des problèmes de navigation sur le site et être dans l’incapacité de passer commande.<br><br>
Les cookies « essentiels » permettent aussi à coupepro.hop.boutique de suivre son activité.<br><br>
Ils peuvent être insérés sur votre émetteur par coupepro.hop.boutique ou par ses prestataires.<br><br>
7.2.1.2. Cookies « Analytiques et de Personnalisation »<br><br>
Les cookies « Analytiques et de personnalisation » ne sont pas obligatoires, ils vont nous permettre la facilitation de vos recherches, l’optimisation de votre expérience chez nous, nous pourrons grâce à eux effectuer un meilleur ciblage de vos attentes ainsi qu’adapter nos offres et maximiser l’organisation de notre site.<br><br>
7.2.1.3. Cookies « Publicitaires »<br><br>
Les cookies publicitaires s’affichent dans les espaces réservés à la publicité de notre site. L’intérêt pour vous est que votre temps navigation soit meilleur et optimisé grâce à la présentation d’offres et de publicités pertinentes pour vous.<br><br>
Pour cela, les cookies « publicitaires » vont cibler vos attentes en temps réel et vous proposer un contenu publicitaire adapté à vos envies et centres d’intérêt du moment, à travers votre historique de navigation récent sur d’autres sites.<br><br>
Cela permet d’éviter de vous présenter un contenu publicitaire sans intérêt pour vous. Par la même occasion, CoupePro™ préfère voir ses offres et publicités proposées à des utilisateurs qui seront intéressés par celles-ci.<br><br>
Les contenus publicitaires proposés peuvent contenir des cookies émis par <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a> ou par ses prestataires, ou par des tiers par le biais d’association d’un cookie au contenu de publicité d’un annonceur.<br><br>
7.2.2. Cookies émis par des sociétés tierces<br><br>
Les tiers utilisant des cookies sur notre site utilisent pour cela leur propre politique de protection de la vie privée. Ces cookies ne sont pas nécessaires à l’utilisation de notre site.<br><br>
7.2.3. Cookies émis par des applications tierces intégrées à notre site<br><br>
Lors de votre navigation sur notre site, nous pouvons inclure à celui-ci des applications informatiques provenant d’un tiers, afin de vous offrir la possibilité de partager du contenu et/ou votre avis depuis notre site avec d’autres personnes, par exemple lorsque vous cliquez sur les boutons « partager » ou « j’aime » qui proviennent des réseaux sociaux.<br><br>
Ces réseaux sociaux peuvent alors à travers ces boutons vous identifier même si vous ne les avez pas utilisés lors de votre navigation sur le site. Il leur est possible de faire ça si lors de votre dernière navigation sur le site vous étiez parallèlement connecté ou actif sur votre émetteur à votre réseau social. Nous n’avons pas de contrôle les utilisations qu’ils emploient, ni sur les données dont ils disposent.<br><br>
Afin d’en savoir plus sur l’utilisation de vos données et du contenu publicitaire, vous pouvez vous rendre sur vos réseaux sociaux et consulter leurs politiques de protection des données personnelles. Vous devriez alors pouvoir, grâce à ces politiques, gérer vos paramètres selon vos préférences sur les comptes d’utilisateur de chacun des réseaux sociaux sur lesquels vous êtes inscrit.<br><br>
  
Politique de protection de vie privée des réseaux sociaux précités, cliquez sur le réseau social de votre choix :<br><br>
 
Facebook : https://fr-fr.facebook.com/privacy/explanation   <br><br> 
Twitter  : https://twitter.com/fr/tos<br><br>
Google + : https://policies.google.com/terms?hl=fr<br><br>
Concernant notre régie publicitaire, nous vous rappelons, comme évoqué juste avant, que tous nos espaces publicitaires peuvent contenir des cookies provenant de tiers (publicitaire à l’origine de la publicité présentée, prestataires tiers du publicitaire…).<br><br>
Ils peuvent donc avec ces cookies et durant le temps prescrit de validité de ceux-ci, proposer des publicités aux endroits mis à disposition pour les publicités des tiers, recenser le nombre de contenus qu’ils proposent dans nos espaces, connaître l’audience de ces publicités et le nombre de clics ; grâce à cela ils pourront réclamer les sommes qui leur sont dues et établir leurs statistiques. Ils peuvent également savoir que votre émetteur est celui ayant visité précédemment un autre site contenant une de leurs publicités, et donc vous cibler et personnaliser leur contenu le cas échéant.<br><br>
7.3. Les options offertes par votre logiciel de navigation (Internet Explorer, Firefox, Google Chrome, etc.).<br><br>
Votre logiciel de navigation contient de nombreuses options dont vous disposez et que vous pouvez régler selon vos préférences. Par ce biais, vous pourrez alors accepter ou non les cookies sur votre émetteur.<br><br>
Cependant, si vous faites le choix d’accepter l’enregistrement de ces cookies sur votre émetteur, alors, lors de vos visites sur des sites ou contenus avec des cookies présents, ceux-ci seront automatiquement enregistrés sur votre émetteur.<br><br>
Selon vos préférences, vous pouvez faire le choix d’activer un rappel vous redemandant si vous acceptez ou refusez les cookies avant leur potentiel enregistrement, ou refuser à chaque fois cet enregistrement de cookie sur votre émetteur.<br><br>
Cependant, il est important de souligner que les choix que vous ferez lors de ce paramétrage pourront peut-être modifier ou altérer votre navigation sur Internet ou sur certains sites ou services qui nécessitent d’utiliser ces cookies (comme pour passer commande sur notre site par exemple).<br><br>
Dans l’hypothèse où vous préféreriez refuser ces cookies sur votre émetteur ou effacer ceux déjà enregistrés, nous déclinons toute responsabilité quant aux conséquences de l’altération du fonctionnement de nos services, qui proviendrait de l’incapacité pour nos services d’enregistrer ou d’avoir accès aux cookies qui servent à leur fonctionnement.<br><br>
7.3.1. Comment choisir vos options selon votre navigateur ?<br><br>
Vous avez différentes options et choix possibles disponibles selon votre navigateur. Afin d’en avoir plus, vous pouvez consulter le menu aide de celui-ci.<br><br>
Internet Explorer™ : http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies<br><br>
Safari™ : https://support.apple.com/kb/PH19214?locale=fr_FR&viewlocale=fr_FR<br><br>
Chrome™ : http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647<br><br>
Firefox™ : http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies<br><br>
Opera™ : http://help.opera.com/Windows/10.20/fr/cookies.html<br><br>

        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">8. TRANSFERTS HORS UNION EUROPÉENNE</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            La plupart du temps, vos données sont conservées au sein de l’Union européenne.<br><br>
Toutefois, lorsque nos prestataires se trouvent dans des pays hors Union européenne, nous partageons certaines de vos données dans des pays tiers, comme avec des pays tiers où la Commission européenne n’a pas effectué d’évaluation sur le niveau de conformité.<br><br>
Dans cette hypothèse, nous faisons le nécessaire pour que ce partage de données soit fait en conformité avec la réglementation attenante et qu’une protection de votre vie privée et de vos droits fondamentaux soit garantie (avec par exemple l’emploi de clauses contractuelles de la Commission européenne).<br><br>
La délégation à la protection des données peut, si vous en faites la demande, vous donner plus de renseignements au sujet du transfert de données.<br><br>

        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">9. LES MESURES DE SÉCURITÉ</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
           Grâce aux mesures techniques et d’organisation que nous prenons, nous pouvons garantir un niveau de sécurité conforme aux risques pour les droits et libertés des personnes physiques vis-à-vis des points cités en point 2. Pour cela, nous tenons compte de l’origine, de la portée, du contexte, des coûts et de l’état des connaissances, des finalités du traitement, mais aussi des risques identifiés.<br><br>
De plus, nous sommes à niveau avec la norme de sécurité de l’industrie des cartes de paiements PCI DSS, ce qui traduit notre engagement en termes de sécurité.<br><br>

        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">10. PROFILAGE ET DÉCISION AUTOMATISÉE</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            De par le traitement automatisé auquel nous avons recours (profilage par exemple), vous êtes assujetti à des effets juridiques qui vous affectent.
Tout cela est indispensable à la conclusion ou à l’exécution du contrat qui vous lie à nous.<br><br>
C’est comme cela que nous pouvons proposer et effectuer l’automatisation d’identification des clients et le « paiement en 4 x ». Les fondements de ce fonctionnement sont attenants à l’analyse de différentes variables concernant le type de produits, les services commandés, ou le profil client.<br><br>
Si le risque est évalué avec ces statistiques comme étant trop grand (fraude/impayé), alors cette modalité de paiement ne sera pas proposée.<br><br>
Cependant, si vous le désirez, vous pouvez obtenir une intervention humaine bien que les décisions soient automatisées, vous pourrez ainsi donner votre avis et/ou vous opposer à la décision automatique.<br><br>

        </span>
    </p>
    <p class="MsoNormal" align="center"
       style="margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white">
        <b>
            <span style="font-size:2em;color:#52c8f6">11. ACTUALISATION DE LA POLITIQUE ET REVISION</span>
        </b>
    </p>
    <p class="MsoNormal">
        <span style="font-size:14.0pt;line-height:107%;color:#1D2129">
            Notre politique concernant les données personnelles sera mise à jour à chaque fois qu’il sera nécessaire afin d’être toujours en accord avec la règlementation applicable à la protection de vos données (tous les trois (3) ans au minimum).<br><br>
Le 01 November 2020.<br><br>

        </span>
    </p>
</div>
<style type="text/css">
    /*------------------------------------------------*/

    /* privacy-switch SECTION START*/

    /*------------------------------------------------*/

    .privacy-switch {
        position: relative;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        font-size: 1.6em;
        font-weight: bold;
        color: #fff;
        height: 18px;
        padding: 6px 6px 5px 6px;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        border-radius: 4px;
        background: #ececec;
        box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.1), inset 0px 1px 3px 0px rgba(0, 0, 0, 0.1);
        cursor: pointer;
        font-size: 16px;
        margin-right: 20px;
        float: none;
        margin-top: 13px;
        margin-bottom: 10px;
    }

    body.IE7 .privacy-switch {
        width: 78px;
    }

    .privacy-switch span {
        display: inline-block;
        width: 35px;
    }

    .privacy-switch span.on {
        color: #fff;
    }

    .privacy-switch .toggle {
        position: absolute;
        top: 1px;
        width: 40px;
        height: 25px;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.3);
        border-radius: 4px;
        background: #fff;
        background: -moz-linear-gradient(top, #ececec 0%, #ffffff 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ececec), color-stop(100%, #ffffff));
        background: -webkit-linear-gradient(top, #ececec 0%, #ffffff 100%);
        background: -o-linear-gradient(top, #ececec 0%, #ffffff 100%);
        background: -ms-linear-gradient(top, #ececec 0%, #ffffff 100%);
        background: linear-gradient(top, #ececec 0%, #ffffff 100%);
        box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);
        z-index: 999;
        -webkit-transition: all 0.15s ease-in-out;
        -moz-transition: all 0.15s ease-in-out;
        -o-transition: all 0.15s ease-in-out;
        -ms-transition: all 0.15s ease-in-out;
    }

    .privacy-switch.off .toggle {
        left: 2%;
    }

    .privacy-switch.on .toggle {
        left: 50%;
    }

    .privacy-switch.off {

        background: #f4524d;

    }

    .privacy-switch.on {

        background: #6dab3c;

    }

    @media only screen and (max-width: 50em) {
        p b span {
            font-size: 1.8rem !important;
            display: inline-block;
            margin-bottom: 5px;
            line-height: 1em;
        }
    }

</style>
</body>
</html>
