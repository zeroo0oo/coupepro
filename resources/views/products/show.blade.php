@extends('layouts.app')

@section('content')
    <div id="header" class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div id="header-logo" class="wpb_single_image wpb_content_element vc_align_center logo">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img src="{!! asset('images/type-s-logo-01.svg') !!}" class="vc_single_image-img attachment-full" alt="" />
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="s1" class="vc_row wpb_row vc_row-fluid vc_custom_1540838357423 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element" id="atf-headline">
                        <div class="wpb_wrapper">
                            <h1 style="text-align: center;">Parking ANY Vehicle Is Now Safe and Easy!</h1>
                        </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element " id="atf-subheadline">
                        <div class="wpb_wrapper">
                            <h2 style="text-align: center;">Detect the unseen with this ⭐⭐⭐⭐⭐ wireless parking sensor!</h2>
                        </div>
                    </div>
                    <div id="pp-atf" class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div id="guGallery-1-wrapperContainer" class="wrapperContainer">
                                        <div id="guGallery-1-imageWrapper" class="imageWrapper" style="display: none;">
                                            <img class="vc_single_image-img" src="">
                                        </div>
                                        <div id="guGallery-1-wistiaVideoWrapper"
                                             class="galleryVideoWrapper videoWrapper" style="display: none;">
                                            <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                                                <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                                                    <div id="guGallery-1-wistiaPlaceholder" class="" style="height:100%;position:relative;width:100%">
                                                        <div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;">
                                                            <img id="guGallery-1-wistiaThumbnail" src="" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" onload="this.parentNode.style.opacity=1;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript" id="guGallery-1-wistiaIdScript"></script>
                                            <script type="text/javascript" src="https://fast.wistia.com/assets/external/E-v1.js"></script>
                                        </div>
                                        <div id="guGallery-1-sproutVideoWrapper" class="galleryVideoWrapper videoWrapper" style="display: none;">
                                            <script type="text/javascript" src="https://c.sproutvideo.com/player_api.js"></script>
                                        </div>
                                    </div>
                                    <div class="wpb_gallery wpb_content_element vc_clearfix guGallery">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_gallery_slides wpb_image_grid" data-interval="3">
                                                <ul class="wpb_image_grid_ul">
                                                    @foreach($product->getImages() as $image)
                                                        <li class="isotope-item">
                                                            <img width="75" height="75" src="{!! $image->getSrc() !!}" class="attachment-thumbnail" alt="{!! $image->getSrc() !!}"/>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner vc_custom_1564796484160">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565033156925 prodDesc" id="pp-description">
                                        <div class="wpb_wrapper">
                                            <p>
                                                Whether you’re pulling in, backing up, or parallel parking, you can now get realtime visual and audio alerts on your phone when
                                                <strong>
                                                    your vehicle approaches any object
                                                </strong>
                                                – kids, bikers, other cars, poles, and whatever else you might encounter!
                                            </p>
                                            <p>
                                                Easily detect the unseen and
                                                <strong>
                                                    avoid fender benders or accidents – save yourself time, money
                                                </strong>
                                                , and keep yourself &amp; others safe!
                                            </p>
                                            <p>
                                                One drive with Type S, and you’ll wonder how you ever parked without it!
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565186707525 prodDesc" id="pp-bullets-4">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <b>Instant Audio &amp; Visual Alerts!</b>
                                                    – Type S detects objects from 0 to 4 feet away and instantly alerts the FREE Type S App on your iOS or Android device – if you see red or hear the beeps… STOP!
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565186734763 prodDesc" id="pp-bullets-2">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <b>
                                                        Installs in Minutes on ANY Car or Truck!
                                                    </b>
                                                    – Just remove your license plate, attach the Type S sensor, put your license plate back on… DONE! No wiring required – ANYONE can do it!
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565037101882 prodDesc" id="pp-bullets-3">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <b>
                                                        Protect Your Loved Ones From Accidents!
                                                    </b>
                                                    – Type S alerts you to objects (or people) that you might have missed, keeping passengers and passersby protected by more than just your sight!
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565186793433 prodDesc" id="pp-bullets-5">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <b>Works in Rain, Shine, or Snow! </b>
                                                    – With a waterproof design and an operating temperature between 14ºF – 122ºF, you can always count on Type S alerts when parking.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565187702216 prodDesc" id="pp-bullets-6">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <b>Always-On Alerts! </b>
                                                    – Type S sensors have a long-lasting battery and automatically recharge in the daylight with solar power (even on cloudy days)! With it&#8217;s built-in G-Sensor Technology, Type S automatically turns on when the car starts to move and off after 30 minutes of inactivity.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1564783025683 prodDesc" id="pp-bullets-7">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <strong>
                                                        Protected by a 1-Year Warranty!
                                                    </strong>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1564783036362 prodDesc hidden" id="pp-bullets-8">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>
                                                    <b>
                                                        PP Bullet
                                                    </b>
                                                    – PP Bullet text line goes here
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  cta smooth-scroll center vc_btn3-left vc_custom_1564687810563" id="cta-pp">
                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" href="#offer" title="" onclick="fireButtonEvent(this);">Get Type S Now!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="s2" class="vc_row wpb_row vc_row-fluid vc_custom_1521747817076 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_single_image wpb_content_element vc_align_left e3">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="1" height="1" src="https://gettypes.io/wp-content/uploads/1-year-warranty-badge.svg" class="vc_single_image-img attachment-large" alt="" />
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  e4">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="padding: 0 !important; margin: 0 !important;">Comes with a Risk-Free 1-Year Warranty!</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="s7" class="vc_row wpb_row vc_row-fluid vc_custom_1506448419875 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element " id="testimonial-headline">
                                        <div class="wpb_wrapper">
                                            <h2 style="text-align: center; font-size: 43px;">Here’s Why Customers Love Type S…</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="testimonial-1" class="vc_row wpb_row vc_inner vc_row-fluid wrapper2 vc_custom_1564797703016">
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div id="testimonial-1-img" class="wpb_single_image wpb_content_element vc_align_center stars">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img src="https://gettypes.io/wp-content/uploads/five-stars-1.svg" class="vc_single_image-img attachment-full" alt="" />
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element e1" id="testimonial-1-name">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <strong>
                                                    Samantha J.
                                                </strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-10">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="vc_btn3-container testimonialButton vc_btn3-inline" id="testimonial-1-header">
                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" href="https://apps.apple.com/us/app/type-s-drive/id1363886509" title="" target="_blank">
                                            “I now receive alerts when I'm close to hitting an object!”
                                        </a>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1564785331615" id="testimonial-1-text">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: left;">
                                                User friendly! Talk about a seamless installation &#8211; I am not tech-savvy and this took me less than 5 minutes to install. The parking sensor is solar powered so you never have to worry about changing the battery.
                                            </p>
                                            <p>
                                                I now receive alerts when I’m close to hitting and object and it’s a breeze to parallel park! The app provides audio and visual displays for seamless safe driving!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="testimonial-2" class="vc_row wpb_row vc_inner vc_row-fluid wrapper2 vc_custom_1515800728348">
                        <div class="wpb_column vc_column_container vc_col-sm-10">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="vc_btn3-container testimonialButton vc_btn3-right" id="testimonial-2-header">
                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" href="https://play.google.com/store/apps/details?id=com.axiacomm.TypeS&hl=en_US&reviewId=gp%3AAOqpTOEgQgZ28eEXJgYabMs0AiRy7iPfO0__z8GBfbH3ANWtE-9gzVeWV5ng9LLD3P8K0PUecJMiDJKb5kdHkg" title="" target="_blank">
                                            “If I could give it 10 stars I would.”
                                        </a>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1564785606534" id="testimonial-2-text">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: right;">
                                                As a parent of a teenager this product is a lifesaver. Literally two weeks into driving (before we got Type S) he backed into a post. This has not only saved us money on purchasing a full install backup sensor, it saved us money on car insurance. I can&#8217;t express how easy it is to use and install either. If I could give it 10 stars I would.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-2">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div id="testimonial-2-img" class="wpb_single_image wpb_content_element vc_align_center stars">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img src="https://gettypes.io/wp-content/uploads/four-half-stars-1.svg" class="vc_single_image-img attachment-full" alt="" />
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element e1" id="testimonial-2-name">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <strong>
                                                    Larissa L.
                                                </strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="s8" class="vc_row wpb_row vc_row-fluid vc_custom_1564158811377 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1565040950498">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element" id="whatyouget-head">
                                        <div class="wpb_wrapper">
                                            <h2 style="text-align: center;">
                                                Here&#8217;s what you get with each Type S:
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1564159384259">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div id="whatyouget-img" class="wpb_single_image wpb_content_element vc_align_right  vc_custom_1565191421392">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="323" height="187" src="https://gettypes.io/wp-content/uploads/types-s4-01c.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://gettypes.io/wp-content/uploads/types-s4-01c.jpg 323w, https://gettypes.io/wp-content/uploads/types-s4-01c-300x174.jpg 300w" sizes="(max-width: 323px) 100vw, 323px" />
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565040881050" id="whatyouget-bullets">
                                        <div class="wpb_wrapper">
                                            <ul>
                                                <li>5-Star Solar-Powered, Wireless Parking Sensor</li>
                                                <li>Audio and Visual Alerts on the FREE Type S App</li>
                                                <li>Easy-To-Follow Instructions</li>
                                                <li>USB Charging Cable</li>
                                                <li>1-Year Warranty</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="offer" class="vc_row wpb_row vc_row-fluid vc_custom_1506448454757 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div id="offer-headline" class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1511272014542">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  title2" id="offer-headline">
                                        <div class="wpb_wrapper">
                                            <h2 style="text-align: center;">
                                                Make Parking Safer &amp; Easier… Guaranteed!
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  title2" id="offer-subheadline">
                                        <div class="wpb_wrapper">
                                            <h3 style="text-align: center; font-weight: normal; margin-top: 5px;">
                                                &#8220;I can’t express how easy it is to use and install either. If I could give it 10 stars I would.&#8221;
                                                <br />
                                                &#8211; Larissa L, ⭐⭐⭐⭐⭐
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="offer-3" class="vc_row wpb_row vc_inner vc_row-fluid wrapper vc_custom_1564778005731">
                        <div class="offer presell presell-popup-1 guproduct_types_2_79 wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill" id="offer-3-bundle-1">
                            <div class="vc_column-inner vc_custom_1565375711041">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565017717820 e1" id="offer-3-bundle-1-header">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <strong>1 &#8211; Parking Sensor</strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565017585703 e1" id="offer-3-bundle-1-subhead">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                Rear Protection
                                            </p>
                                        </div>
                                    </div>
                                    <div id="offer-3-bundle-1-img" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1564786295614">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="200" height="200" src="https://gettypes.io/wp-content/uploads/types-b1-01-d.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gettypes.io/wp-content/uploads/types-b1-01-d.png 200w, https://gettypes.io/wp-content/uploads/types-b1-01-d-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" />
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1521592123402 e3" id="offer-3-bundle-1-main">
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1523462391766 e2">
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1534442203216" id="offer-3-bundle-1-price">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <span style="font-size: xx-large;">
                                                    $$TP
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565115520771 e3" id="offer-3-bundle-1-savings">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #ff0000; text-decoration: line-through;">
                                                    $$Retail
                                                </span>
                                                <span style="font-weight: normal; color: #008000;">
                                                    $$Save
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1525364354910 e3" id="offer-3-bundle-1-shipping">
                                        <div class="wpb_wrapper">
                                            <p>+S&amp;H</p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1525363647046 e4" id="offer-3-bundle-1-savings">
                                        <div class="wpb_wrapper"></div>
                                    </div>
                                    <div class="vc_btn3-container cta vc_btn3-center" id="bundle1-cta">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey">Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offer most-popular selected presell guproduct_types_1fs guproduct_types_70 wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill" id="offer-3-bundle-2">
                            <div class="vc_column-inner vc_custom_1564788189648">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565017735307 e1" id="offer-3-bundle-2-header">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <strong>
                                                    2 &#8211; Parking Sensors
                                                </strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565017770962 e1" id="offer-3-bundle-2-subhead">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">Front &amp; Rear Protection</p>
                                        </div>
                                    </div>
                                    <div id="offer-3-bundle-2-img" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1564786384479">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="200" height="200" src="https://gettypes.io/wp-content/uploads/types-b2-01-d.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gettypes.io/wp-content/uploads/types-b2-01-d.png 200w, https://gettypes.io/wp-content/uploads/types-b2-01-d-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" />
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1564786425041 e2" id="offer-3-bundle-2-main">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">Buy 1, Get 1 70% OFF!</p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1525380292871" id="offer-3-bundle-2-price">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <span style="font-size: xx-large;">
                                                    $$TP
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1564188203591 e3" id="offer-3-bundle-2-savings">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #ff0000; text-decoration: line-through;">$$Retail</span>
                                                <span style="font-weight: normal; color: #008000;">$$Save</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1515715823576 e4" id="offer-3-bundle-2-shipping">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center; font-weight: bold;">
                                                <span style="display: inline-block; position: relative; top: -10px;">FREE USA Shipping</span>
                                                <img class="alignnone wp-image-569 size-medium" src="{!! asset('images/lp-free-shipping-icon-black.svg') !!}" alt="" width="50" height="27" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  cta vc_btn3-center" id="bundle2-cta">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey">Next</button>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1515716375923 ribbon" id="offer-3-bundle-2-ribbon">
                                        <div class="wpb_wrapper">
                                            <p>Most Popular</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offer best-deal presell guproduct_types_1fs guproduct_types_1fs guproduct_types_1f guproduct_types_1f wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill" id="offer-3-bundle-3">
                            <div class="vc_column-inner vc_custom_1564788234007">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565017792847 e1" id="offer-3-bundle-1-header">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;"><strong>4 &#8211; Parking Sensors</strong></p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565033561229 e1" id="offer-3-bundle-1-subhead">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">2x Front &amp; Rear Protection</p>
                                        </div>
                                    </div>
                                    <div id="offer-3-bundle-3-img" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1564786415272">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey"><img width="200" height="200" src="https://gettypes.io/wp-content/uploads/types-b3-01-d.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gettypes.io/wp-content/uploads/types-b3-01-d.png 200w, https://gettypes.io/wp-content/uploads/types-b3-01-d-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" /></div>
                                        </figure>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1565115729807 e2" id="offer-3-bundle-3-main">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">Buy 2, Get 2 FREE!</p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1525389925408" id="offer-3-bundle-3-price">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">
                                                <span style="font-size: xx-large;">$$TP</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1564188213536 e3" id="offer-3-bundle-3-savings">
                                        <div class="wpb_wrapper">
                                            <p><span style="color: #ff0000; text-decoration: line-through;">$$Retail</span> <span style="font-weight: normal; color: #008000;">$$Save</span></p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1515715865935 e4" id="offer-3-bundle-3-shipping">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center; font-weight: bold;">
                                                <span style="display: inline-block; position: relative; top: -10px;">
                                                    FREE USA Shipping
                                                </span>
                                                <img class="alignnone wp-image-569 size-medium" src="{!! asset('images/lp-free-shipping-icon-black.svg') !!}" alt="" width="50" height="27" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  cta vc_btn3-center" id="bundle3-cta">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey">
                                            Next
                                        </button>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1564786896466 ribbon" id="offer-3-bundle-3-ribbon">
                                        <div class="wpb_wrapper">
                                            <p>Best Deal</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_btn3-container vc_btn3-center" id="buy-now-next">
                        <button style="background-color:#25ae4e; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom">ORDER NOW</button>
                    </div>
                    <div class="vc_btn3-container vc_btn3-center" id="buy-now-checkout">
                        <button style="background-color:#25ae4e; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" onclick="gu_fire_event('&#039;Checkout&#039;', '&#039;Click&#039;', '&#039;CheckoutButton&#039;', '&#039;InitiateCheckout&#039;')">
                            Checkout
                        </button>
                    </div>
                    <div id="badges" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="colors e1 wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element ">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center; padding-bottom: 20px; font-style: italic;"><strong>All Orders Include a Risk-Free 1 Year Warranty!</strong></p>
                                        </div>
                                    </div>
                                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                                        <span class="vc_sep_holder vc_sep_holder_l">
                                            <span style="border-color:#b5b5b5;" class="vc_sep_line"></span>
                                        </span>
                                        <span class="vc_sep_holder vc_sep_holder_r">
                                            <span style="border-color:#b5b5b5;" class="vc_sep_line"></span>
                                        </span>
                                    </div>
                                    <div id="domestic-badges" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1525363744113">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                <img width="848" height="77" src="https://gettypes.io/wp-content/uploads/desktop-trust-badges-4-1.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gettypes.io/wp-content/uploads/desktop-trust-badges-4-1.png 848w, https://gettypes.io/wp-content/uploads/desktop-trust-badges-4-1-300x27.png 300w, https://gettypes.io/wp-content/uploads/desktop-trust-badges-4-1-768x70.png 768w" sizes="(max-width: 848px) 100vw, 848px" />
                                            </div>
                                        </figure>
                                    </div>
                                    <div id="intl-badges" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1525363249048  hidden">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="559" height="77" src="https://gettypes.io/wp-content/uploads/desktop-trust-badges-4.png" class="vc_single_image-img attachment-full" alt="" srcset="https://gettypes.io/wp-content/uploads/desktop-trust-badges-4.png 559w, https://gettypes.io/wp-content/uploads/desktop-trust-badges-4-300x41.png 300w" sizes="(max-width: 559px) 100vw, 559px" />
                                            </div>
                                        </figure>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1525364547290 hidden" id="intl-guarantee">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center; font-size: 20px;"><strong>30 Day Money Back Guarantee!</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="presell" class="vc_row wpb_row vc_inner vc_row-fluid hidden presell guproduct_types_carmount vc_custom_1564790698947 vc_row-has-fill">
                        <div class="s1 wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1564788357763 header" id="presell-header">
                                        <div class="wpb_wrapper">
                                            <h3 style="text-align: center; font-size: 36px;">Enjoy Hands-free, Distraction-free Driving!</h3>
                                        </div>
                                    </div>
                                    <div class="wpb_gallery wpb_content_element vc_clearfix  img2" id="presell-main-img">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_gallery_slides wpb_image_grid" data-interval="3">
                                                <ul class="wpb_image_grid_ul">
                                                    @foreach($product->getImages() as $image)
                                                        <li class="isotope-item">
                                                            <img width="470" height="313" src="{!! $image->getSrc() !!}" class="attachment-full" alt="" srcset="{!! $image->getSrc() !!} 470w, {!! $image->getSrc() !!} 300w" sizes="(max-width: 470px) 100vw, 470px" />
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="s2 wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <p style="font-size: 16px;color: #0a0808;line-height: 24px;text-align: center;font-family:Open Sans;font-weight:400;font-style:normal" class="vc_custom_heading e2 vc_custom_1564788902494" id="presell-product-name">
                                        Driving safer is easier when you’re not holding your phone! That’s why we’re offering 30% off our Type S phone holder with your current purchase.
                                    </p>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1564788766809 header" id="presell-product-bullets">
                                        <div class="wpb_wrapper">
                                            <p>
                                                &#8211; Adjusts to hold large phones (up to 3”)<br />
                                                &#8211; Air-vent anchoring to easily view phone<br />
                                                &#8211; Portable – use in any car, anytime
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element vc_custom_1525468519965 e3" id="presell-price">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center; color: #98908b; font-size: 16px;">
                                                <strong>
                                                    <del>$$Retail</del>
                                                </strong>
                                            </p>
                                            <p style="text-align: center; color: #0d8900;">
                                                <strong>$$OnlyPrice</strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  e4 cta vc_btn3-center" id="presell-cta">
                                        <button style="background-color:#f47300; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-custom" onclick="presellClick();">
                                            YES, GET IT FOR 30% OFF NOW!
                                        </button>
                                    </div>
                                    <div class="vc_btn3-container  e4 cta active hidden vc_btn3-center">
                                        <button style="background-color:#2dad57; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-custom">
                                            Added!
                                        </button>
                                    </div>
                                    <div class="vc_btn3-container  subtle vc_btn3-inline" id="presell-decline">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" onclick="presellDecline();">
                                            No Thank You
                                        </button>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  quantity hidden" id="presell-quantity">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center">
                                                <strong>
                                                    Quantity
                                                </strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="faq" class="vc_row wpb_row vc_row-fluid vc_custom_1514512362804 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper vc_custom_1499987972328">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  e1">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center; font-size: 22px;">
                                                <strong>
                                                    Have a Question?
                                                </strong>
                                                <strong>
                                                    <span style="color: #0073ff;">
                                                        <a style="color: #ffffff; letter-spacing: 1px; text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.8);" href="#faq">
                                                            See Our FAQs
                                                        </a>
                                                    </span>
                                                </strong>
                                            </p>
                                            <p style="text-align: center;">
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element " id="faq-text">
                                        <div class="wpb_wrapper">
                                            <h4>How exactly does Type S work?</h4>
                                            <p>It’s remarkably simple. Unlike other parking solutions that must be connected to the wiring in your car, the solar-powered Type S uses a wireless connection between your sensor and your smartphone to make parking and reversing simple. Once installed on your license plate, Type S tells the app on your phone when an approaching object is too close, alerting you with a series of loud, clear beeps.</p>
                                            <h4>How fast can I set this up on my car?</h4>
                                            <p>In less than five minutes! Its so easy, even Grandma could do it! It’s literally only three steps – attach the sensor to your license plate, download the app, and sync with your phone! That’s it! You now have a sensor that will help prevent costly accidents and fender benders.</p>
                                            <h4>Can I put Type S on the front AND back of my car? Will they both work with one app?</h4>
                                            <p>Can I put Type S on the front AND back of my car? Will they both work with one app?</p>
                                            <h4>Does Type S need to be charged?</h4>
                                            <p>Only once (prior to installation) and never again! After that, your sensor will automatically recharge via the solar panel – even in low light! How’s that for convenience?</p>
                                            <h4>How secure is Type S?</h4>
                                            <p>It’s very easy to install Type S, but it’s actually quite difficult for someone to take off your car because they would have to completely remove your license plate to do so.</p>
                                            <h4>What if my license plate is on the back left of my vehicle?</h4>
                                            <p>No problem at all! Although the preferred placement for a sensor is centered, placement anywhere on the back of your vehicle will give a measure of improved visibility.</p>
                                            <h4>Is it water resistant?</h4>
                                            <p>Is it ever! Type S sensors are IP67-rated – which means you could leave it submerged for 30 minutes in up to a meter of water and it would still work! In other words, no rainstorm or snow is a match for it!</p>
                                            <h4>Will Type S work on EVERY vehicle – even motorcycles and mopeds?</h4>
                                            <p>Yes! Type S works on any vehicle with a license plate and a phone mount – from cars and motorcylces, to trucks and trailers!</p>
                                            <h4>Will my Type S sensor cover my license plate number?</h4>
                                            <p>Definitely not. The sensor fits at the top of your license plate, and is far too slim to obstruct view of any numbering. If you’re concerned about Type S covering the state name on top of your plate, check your state’s license plate rules.</p>
                                            <h4>Does Type S work with Apple and Android devices?</h4>
                                            <p>Yes! The hands-free app is compatible with nearly all Apple or Android devices: iPhone 4s &amp; higher, iPad3, iPad Mini, iPad Air &amp; higher, iPod touch 5th Generation &amp; higher (requires iOS 7.0 or higher) and phones with Android™ 4.3 or higher.</p>
                                            <h4>Does this device come with a warranty?</h4>
                                            <p>Yes! All sensors come with a 1 year warranty.</p>
                                            <h4>Will I have to pay any additional customs, duties or VAT fees when I receive my order?</h4>
                                            <p>Depending on your country, it is possible that you will be charged one or more of these fees when you receive your order.</p>
                                            <h4>How long does delivery take to countries outside of the U.S.?</h4>
                                            <p>We ship orders out FAST, but we can’t control carrier transit times. For orders outside the USA, please allow 7-12 business days for your order to arrive. Delivery times will vary based on country.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer-wrap" class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                        <div class="wpb_wrapper">
                            <div id="footer">
                                <div class="giddy-footer-container">
                                    <ul class="giddy-links">
                                        <li id="footer-support-cta">
                                            <span onclick="giddybox('#support'); ">
                                                Support
                                            </span>
                                        </li>
                                        <li id="footer-returns-cta">
                                            <span onclick="giddybox('#returns'); ">
                                                Returns
                                            </span>
                                        </li>
                                        <li id="footer-impressum-cta" class="hidden">
                                            <span onclick="giddybox('#impressum'); ">
                                                Impressum
                                            </span>
                                        </li>
                                        <li id="footer-privacy-cta">
                                            <span onclick="giddybox('#privacy'); ">
                                                Privacy
                                            </span>
                                        </li>
                                        <li id="footer-terms-cta">
                                            <span onclick="giddybox('#terms'); ">
                                                Terms
                                            </span>
                                        </li>
                                    </ul>
                                    <div class="footer-logo">
                                        <a onclick="giddybox('#about');">
                                            <img src="{!! asset('images/navfooterlogo.jpg') !!}">
                                        </a>
                                    </div>
                                    <span class="copyright">Copyright &copy; 2019 GiddyUp. All Rights Reserved.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row wpb_row vc_row-fluid hidden">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div id="privacy" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element giddybox-text wrapper2" id="privacy-text">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <iframe style="border-style: hidden;" src="https://js.giddyup.io/v01/gu-privacy.htm" width="800" allowfullscreen="allowfullscreen"></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="terms" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element giddybox-text wrapper2" id="terms-text">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <iframe style="border-style: hidden;" src="https://js.giddyup.io/v01/gu-terms.htm" width="800" allowfullscreen="allowfullscreen"></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="impressum" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1529520564001 giddybox-text wrapper2" id="impressum-text">
                                        <div class="wpb_wrapper">
                                            <h2 style="color: #52c8f6; font-size: 2em;">Impressum</h2>
                                            <p style="text-align: center;">
                                                <b>
                                                    Company Name:<br />
                                                </b>The GiddyUp Group</p>
                                            <p>
                                                <b>
                                                    Address:<br />
                                                </b>
                                                16 N. Oak St.<br />
                                                Ventura, CA 93001<br />
                                                USA
                                            </p>
                                            <p>
                                                <b>
                                                    Contact:
                                                </b>
                                                <br />
                                                support@giddyup.io
                                            </p>
                                            <p>
                                                <strong>
                                                    CEO Team:
                                                    <br />
                                                </strong>
                                                Todd Armstrong, Justin Grant, Topher Grant, Steven Isaac, Alan Reinhart, Eric Schechter
                                            </p>
                                            <p>
                                                <strong>
                                                    Form of Company:
                                                    <br />
                                                </strong>
                                                LLC
                                            </p>
                                            <p>
                                                <strong>
                                                    Registration Number:
                                                    <br />
                                                </strong>
                                                46-3920047
                                                <br>
                                            </p>
                                            <p>
                                                <b>
                                                    VAT Number
                                                    <br />
                                                </b>
                                                Not Applicable
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="support" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565712543011 giddybox-text wrapper2">
                                        <div class="wpb_wrapper">
                                            <h2 style="color: #52c8f6; font-size: 2em;">
                                                Have a question about Type S?
                                            </h2>
                                            <ul>
                                                <li>Questions About Your Order</li>
                                                <li>Product Questions</li>
                                                <li>Technical Support</li>
                                                <li>Returns</li>
                                                <li>Anything else product or order related</li>
                                            </ul>
                                            <h3>Please contact WinPlus North America directly at:</h3>
                                            <ul>
                                                <li>info@wirelessparkingsensor.com</li>
                                                <li>820 South Wanamaker Ave. Ontario, CA 91761</li>
                                            </ul>
                                            <h3>Would you like to unsubscribe from marketing emails?</h3>
                                            <p>
                                                Please click the following
                                                <a href="http://www.optout-mbng.net/o-mbng-e61-0d31b4bf05da8c9c0fe0008f3fe8cdb4" target="_blank" rel="noopener">link</a>
                                                to be added to a global do not mail list
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="returns" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1565712429720 giddybox-text wrapper2">
                                        <div class="wpb_wrapper">
                                            <h2 style="text-align: center; color: #52c8f6; font-size: 2em;">
                                                Return Policy
                                            </h2>
                                            <p>
                                                Returns<br />
                                                Our policy lasts 30 days. If 30 days have gone by since your purchase, unfortunately we can’t offer you a refund or exchange.
                                            </p>
                                            <p>
                                                To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.
                                            </p>
                                            <p>
                                                Several types of goods are exempt from being returned. Perishable goods such as food, flowers, newspapers or magazines cannot be returned. We also do not accept products that are intimate or sanitary goods, hazardous materials, or flammable liquids or gases.
                                            </p>
                                            <p>
                                                Additional non-returnable items:<br />
                                                Gift cards<br />
                                                Downloadable software products<br />
                                                Some health and personal care items
                                            </p>
                                            <p>
                                                To complete your return, we require a receipt or proof of purchase.
                                            </p>
                                            <p>
                                                Please do not send your purchase back to the manufacturer.
                                            </p>
                                            <p>
                                                There are certain situations where only partial refunds are granted (if applicable)
                                                <br />
                                                Book with obvious signs of use
                                                <br />
                                                CD, DVD, VHS tape, software, video game, cassette tape, or vinyl record that has been opened<br />
                                                Any item not in its original condition, is damaged or missing parts for reasons not due to our error<br />
                                                Any item that is returned more than 30 days after delivery
                                            </p>
                                            <p>
                                                Refunds (if applicable)<br />
                                                Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.<br />
                                                If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.
                                            </p>
                                            <p>
                                                Late or missing refunds (if applicable)<br />
                                                If you haven’t received a refund yet, first check your bank account again.<br />
                                                Then contact your credit card company, it may take some time before your refund is officially posted.<br />
                                                Next contact your bank. There is often some processing time before a refund is posted.<br />
                                                If you’ve done all of this and you still have not received your refund yet, please contact us at info@wirelessparkingsensor.com.
                                            </p>
                                            <p>Sale items (if applicable)<br />
                                                Only regular priced items may be refunded, unfortunately sale items cannot be refunded.
                                            </p>
                                            <p>Exchanges (if applicable)<br />
                                                We only replace items if they are defective or damaged. If you need to exchange it for the same item, send us an email at info@wirelessparkingsensor.com and send your item to: 13330 NE 154th Dr. Woodinville, Wa 98072, United States.
                                            </p>
                                            <p>
                                                Gifts<br />
                                                If the item was marked as a gift when purchased and shipped directly to you, you’ll receive a gift credit for the value of your return. Once the returned item is received, a gift certificate will be mailed to you.
                                            </p>
                                            <p>
                                                If the item wasn’t marked as a gift when purchased, or the gift giver had the order shipped to themselves to give to you later, we will send a refund to the gift giver and he will find out about your return.
                                            </p>
                                            <p>
                                                Shipping<br />
                                                To return your product, you should mail your product to: 13330 NE 154th Dr. Woodinville, Wa 98072, United States
                                            </p>
                                            <p>
                                                You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.
                                            </p>
                                            <p>
                                                Depending on where you live, the time it may take for your exchanged product to reach you, may vary.
                                            </p>
                                            <p>
                                                If you are shipping an item over $75, you should consider using a trackable shipping service or purchasing shipping insurance. We don’t guarantee that we will receive your returned item.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="about" class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element vc_custom_1529697982093 giddybox-text wrapper2">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <iframe style="border-style: hidden;" src="https://js.giddyup.io/v01/gu-about.htm" width="800" allowfullscreen="allowfullscreen"></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="presell-popup-1" class="vc_row wpb_row vc_inner vc_row-fluid presell-popup guproduct_types_50">
                        <div class="s1 wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element e1" id="upsell-headline">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #666666">
                                                    Special Offer!
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element e2" id="presell-popup-discount-domestic">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #333333">
                                                    <span class="number">50</span>
                                                    <span class="percent">%</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  percentage hidden" id="presell-popup-discount-intl">
                                        <div class="wpb_wrapper">
                                            <p style="color: #333333;">50% Off</p>
                                        </div>
                                    </div>
                                    <div id="presell-popup-image" class="wpb_single_image wpb_content_element vc_align_center   image">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="240" height="109" src="{!! asset('images/types-pp-01b.jpg') !!}" class="vc_single_image-img attachment-full" alt="" />
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="s2 wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  e1" id="upsell-subtitle">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #4e4e4e;">Act Now and Get&#8230;</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element e2" id="upsell-description">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #ff6b01;">
                                                    One More for 50% OFF!
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  e3" id="upsell-price">
                                        <div class="wpb_wrapper">
                                            <p style="color: red;">
                                                <del>
                                                    $$ValuePrice
                                                </del>
                                            </p>
                                            <p style="color: #6d6d6d;">
                                                $$SpecialPrice
                                            </p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  checkout vc_btn3-inline" id="upsell-accept">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" onclick="presellPopupClick(true); gu_fire_event(&#039;UpsellAccept&#039;, &#039;Click&#039;, &#039;UpsellPopup&#039;)">
                                            Yes, I Want This!
                                        </button>
                                    </div>
                                    <div class="vc_btn3-container  cancel vc_btn3-inline">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" onclick="presellPopupClick(); gu_fire_event(&#039;UpsellReject&#039;, &#039;Click&#039;, &#039;UpsellPopup&#039;);">
                                            No, I Don't Want This Offer.
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="upsell-1" class="vc_row wpb_row vc_inner vc_row-fluid presell-popup guproduct_types_50">
                        <div class="s1 wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  e1">
                                        <div class="wpb_wrapper">
                                            <p><span style="color: #666666;">Special Offer!</span></p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  e2">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #333333;">
                                                    <span class="number">50</span>
                                                    <span class="percent">%</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  percentage hidden">
                                        <div class="wpb_wrapper">
                                            <p style="color: #333333;">50% Off</p>
                                        </div>
                                    </div>
                                    <div class="wpb_single_image wpb_content_element vc_align_center image">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="100" height="100" src="{!! asset('images/types-ct-01.jpg') !!}" class="vc_single_image-img attachment-full" alt="" />
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="s2 wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="wpb_text_column wpb_content_element  e1">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #4e4e4e;">
                                                    Act Now and Get&#8230;
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  e2">
                                        <div class="wpb_wrapper">
                                            <p>
                                                <span style="color: #ff6b01;">
                                                    One More for 50% OFF!
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpb_text_column wpb_content_element  e3">
                                        <div class="wpb_wrapper">
                                            <p style="color: red;">
                                                <del>
                                                    $$ValuePrice
                                                </del>
                                            </p>
                                            <p style="color: #6d6d6d;">
                                                $$SpecialPrice
                                            </p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  checkout vc_btn3-inline">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" onclick="upsellClick(true); gu_fire_event(&#039;UpsellAccept&#039;, &#039;Click&#039;, &#039;UpsellPopup&#039;);">
                                            Yes, I Want This!
                                        </button>
                                    </div>
                                    <div class="vc_btn3-container  cancel vc_btn3-inline">
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" onclick="upsellClick(); gu_fire_event(&#039;UpsellReject&#039;, &#039;Click&#039;, &#039;UpsellPopup&#039;);">
                                            No, I Don't Want This Offer.
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_text_column wpb_content_element  hidden">
                        <div class="wpb_wrapper">
                            <p>!!guCheckout Shortcode DON&#8217;T DELETE ME!!</p>
                            Could not find block specified! Please check out the Shortcode parameters.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
