@extends('layouts.app')

@section('content')
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">title</th>
            <th scope="col">images</th>
            <th scope="col">action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{!! $product->getId() !!}</th>
                <td>{!! $product->getTitle() !!}</td>
                <td><img src="{!! $product->getImage()->getSrc() !!}" width="60" height="60"></td>
                <td><a href="{!! route('find', $product->getID()) !!}" class="btn btn-success btn-sm">show</a> </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
