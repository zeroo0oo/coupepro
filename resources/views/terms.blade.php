<html>
<head>
    <style>
        /* Font Definitions */
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: "Segoe UI";
            panose-1: 2 11 5 2 4 2 4 2 2 3;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 8.0pt;
            margin-left: 0in;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        h2 {
            mso-style-link: "Heading 2 Char";
            margin-right: 0in;
            margin-left: 0in;
            font-size: 18.0pt;
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }

        a:link, span.MsoHyperlink {
            color: #0563C1;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }

        p {
            margin-right: 0in;
            margin-left: 0in;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-link: "Balloon Text Char";
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 9.0pt;
            font-family: "Segoe UI", sans-serif;
        }

        span.BalloonTextChar {
            mso-style-name: "Balloon Text Char";
            mso-style-link: "Balloon Text";
            font-family: "Segoe UI", sans-serif;
        }

        span.Heading2Char {
            mso-style-name: "Heading 2 Char";
            mso-style-link: "Heading 2";
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }

        .MsoPapDefault {
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: 1.0in 1.0in 1.0in 1.0in;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0in;
        }

        ul {
            margin-bottom: 0in;
        }


        .privacy-switch {
            font-family: "Open Sans", sans-serif;
        }
    </style>
</head>
<body>
<div class=WordSection1>
    <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt; text-align:center;line-height:36.0pt;background:white'>
        <b>
            <span style='font-size:2em;color:#52c8f6'>
                CONDITIONS GENERALES DE VENTE <br>
                CLIENT CONSOMMATEUR

            </span>
        </b>
    </p>
    <p class=MsoNormal>&nbsp;</p>
    <p class=MsoNormal>
        <b>
            <span style='font-size:14.0pt;line-height:107%'>
                Article 1 – DISPOSITIONS GENERALES – CHAMP D’APPLICATION
            </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            1.1 Les présentes Conditions Générales de Vente (« les CGV ») déterminent les droits et obligations des parties dans le cadre de la vente en ligne de Produits proposés par la société CoupePro™ (« la Société »).
        </span>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
           1.2 Tout Commande passée sur le site internet CoupePro™ suppose l’acceptation préalable et sans restriction des présentes Conditions générales de vente. Ces CGV font donc partie intégrante du Contrat entre le Client et la Société. Elles sont pleinement opposables au Client qui déclare en avoir pris connaissance et les avoir acceptées, sans restriction ni réserve, avant de passer la Commande. 
        </span>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
           1.3 Les présentes CGV s’appliquent à toute Commande passée par une personne physique majeure agissant en qualité de consommateur. Le Client atteste donc qu’il est une personne physique de plus de 18 ans, agissant à des fins qui n’entrent pas dans le cadre de son activité commerciale, industrielle, artisanale, libérale ou agricole. Il reconnait avoir la pleine capacité pour s’engager lorsqu’il passe Commande et s’engage à fournir des éléments véridiques quant à son identité. 
        </span>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            1.4 EXCLUSION : Sont expressément exclues du champ d’application des présentes CGV les personnes agissant en qualité de professionnel, c’est-à-dire les personnes physiques ou morales, publiques ou privées, qui agissent à des fins entrant dans le cadre de leur activité commerciale, industrielle, artisanale ; libérale ou agricole, y compris si elles agissent au nom ou pour le compte d’un autre professionnel. <br><br>
Les professionnels qui souhaitent passer une Commande auprès de la Société sont invités à nous contacter directement. 
        </span>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
          1.5 Les CGV applicables à chaque Commande sont celles en vigueur à la date du paiement ou du premier paiement en cas de paiements multiples) de la commande. La Société se réserve la possibilité de les modifier à tout moment, par la publication d’une nouvelle version sur son site Internet. Ces CGV sont consultables sur le site Internet de la Société à l'adresse suivante : <a href="https://coupepro.quality.boutique">https://coupepro.quality.boutique</a> et sont téléchargeables au format pdf.
        </span>
    </p>

    <p class=MsoNormal>
        <b>
            <span style='font-size:14.0pt;line-height:107%'>
                Article 2 – CONCLUSION DU CONTRAT EN LIGNE
            </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            2.1. Processus de passation de la Commande
        </span>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
          Pour acheter un ou plusieurs Produits sur le site, le Client sélectionne chaque Produit et l’ajoute à son panier. Une fois sa sélection terminée, il doit confirmer son panier pour passer à la Commande (1er clic)<br><br>
            A ce stade, le Client est redirigé vers une page contenant :<br><br>
Un récapitulatif des Produits sélectionnés, les prix correspondants, les modalités et les frais de livraison. Il lui revient alors de vérifier et éventuellement de corriger le contenu de son panier. <br><br>
Les présentes CGV. Il lui revient de les lire attentivement avant de valider l’ensemble de la Commande. <br><br>

Cette validation de la Commande après vérification du panier et lecture des CGV (2ème clic) vaut conclusion du Contrat et le Client reconnaît que le 2ème clic entraine une obligation de paiement de sa part. <br><br>

Le Client est alors redirigé vers la page de paiement. Il peut choisir entre les différents modes de paiement proposés et procède au paiement de sa Commande. <br><br>

Après avoir validé sa Commande et effectué son paiement, le Client reçoit, sur l’adresse email qu’il a communiquée pour créer son compte un message de confirmation de la part de la Société. Ce message contient, sous format pdf : <br>
Le récapitulatif de sa commande (Produit sélectionnés, prix, modalités et frais de livraison) ;<br>
L’identification précise CoupePro™ et de son activité ;<br>
Le numéro de la Commande ;<br>
Les modalités, les conditions et le formulaire de rétractation ;<br>
Les présentes CGV au format pdf. <br><br>

En cas de non réception de la confirmation de la Commande, il est recommandé au Client de contacter la Société via le formulaire de contact présent sur le site.<br><br> 

Le Client reçoit ensuite une facture d’achat transmise sous forme électronique, ce que le Client accepte expressément. <br><br>

Il est vivement conseillé au Client de conserver ce message de confirmation et la facture d’achat qui lui est également transmise sous format électronique <br><br>puisque ces documents peuvent être produits à titre de preuve du Contrat. <br><br>

2.2 Cas de refus de validation de la Commande par la Société<br><br>

La Société se réserve le droit de refuser votre Commande pour tout motif légitime, dont par exemple :<br><br>
Commande non conforme aux CGV ;<br><br>
Quantités commandées ne correspondant pas à un usage normal par un Client consommateur ;<br><br>
Non-paiement d’une Commande précédente ou litige en cours concernant une Commande précédente ;<br><br>
Soupçon de fraude sur la Commande (étayés par un faisceau d’indices concordants). <br><br>

        </span>
    </p>
    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 3 - SPECIFICATIONS ET DISPONIBILITE DES PRODUITS
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            3.1 Spécifications des Produits 
<br><br>
Les caractéristiques essentielles des biens et leurs prix respectifs sont mis à disposition du Client sur les sites Internet de la Société, de même, le cas échéant, que des informations sur l'utilisation du produit. <br><br>

Bien que la Société veille de manière raisonnable à ce que les Spécifications soient exactes, lesdites Spécifications, sous réserve de certaines exceptions, comme les informations tarifaires, sont données par les fournisseurs de la Société. En conséquence, la Société décline toute responsabilité quant aux erreurs éventuelles pouvant être contenues dans les Spécifications. 
<br><br>
Les Spécifications sont présentées en détail et en langue française. Les Parties conviennent que les illustrations, vidéos ou photos des Produits oﬀerts à la vente n’ont pas de valeur contractuelle. La durée de validité de l’oﬀre des Produits, ainsi que leurs prix, est précisée sur les sites Internet de la Société. 
<br><br>
3.2 Disponibilité des Produits
<br><br>
Les offres de Produits sont valables dans la limite des stocks disponibles chez nos fournisseurs. Cette disponibilité des Produits est normalement indiquée sur la page spécifique du Produit. 
<br><br>
Cependant, dans la mesure où la Société ne procède pas à la réservation de stock (sauf cas particuliers de Produits signalés en pré-commande sur la fiche Produit), la mise au panier d’un Produit ne permet pas de garantir absolument la disponibilité du Produit ainsi que son prix. 
<br><br>
Dans l’éventualité où un Produit deviendrait indisponible après la validation de la Commande du Client, la Société l’en informera immédiatement par email. La Commande sera automatiquement annulée et la Société procèdera au remboursement du prix du Produit initialement commandé, ainsi que de toute somme versée au titre de la Commande. 
<br><br>
Cependant, si la Commande contient d’autres Produits que celui devenu indisponible, ceux-ci seront livrés au Client et les frais de livraison ne seront pas remboursés. <br><br>

        </span>
    </p>
    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 4 – PRIX DES PRODUITS
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            4.1 Prix de référence indiqués sur les sites
<br><br>
Le prix de référence des Produits proposés sur le site est le prix conseillé par la marque, le fabricant ou son représentant officiel. À défaut, il s’agit d’un prix déterminé en fonction des prix auxquels le Produit est communément vendu dans un panel d’enseignes le distribuant. Ce prix est mis à jour dès que la marque, le fabricant ou son représentant officiel communique un nouveau prix conseillé pour le Produit ou dès que le prix pratiqué au sein du panel d’enseignes est modifié. 
<br><br>
4.2 Modification des prix indiqués sur les sites
<br><br>
Les prix des Produits sont indiqués sur les pages de descriptifs des Produits. Ils sont indiqués hors taxe, hors droits de douane et hors frais de port. La Société se réserve le droit de modifier les prix des Produits à tout moment, dans le respect de la législation applicable. 
<br><br>
Les Produits commandés seront facturés sur la base du prix en vigueur sur le site au moment de la validation de la Commande. 
<br><br>
4.3 Prix des produits
<br><br>
Dans la mesure où de nombreux Produits sont importés depuis l’étranger (hors Union Européenne) sur demande des Clients, les prix des Produits vendus au travers des sites Internet sont indiqués en Euros hors taxes (hors TVA et hors droits de douane) sauf indications contraires. Ils sont précisément déterminés sur les pages de descriptifs des Produits. Ils sont également indiqués en Euros hors taxe (hors TVA et hors droits de douane), sauf indications contraires, sur la page de Commande des produits, et hors frais spécifiques d'expédition. 
<br><br>
Les prix des Produits n’incluent pas la TVA à l’importation, taxes à l’importation ou droits de douane, qui devront être réglés en sus et seront intégralement à la charge du Client, qui est redevable de ces taxes en tant que destinataire du Produit. 
<br><br>
Les prix des Produit(s) n'incluent pas les frais d'emballage, de conditionnement, d'expédition, de transport, d'assurance et de livraison du/des Produits à l’adresse de livraison.
<br><br>
4.4 Paiement des taxes
<br><br>
Le Client est seul responsable du processus de déclaration et de paiement de la TVA à l’importation lors du dédouanement du Produit. Il pourra lui être demandé de s’acquitter de la TVA à l’importation. Dans la mesure où cette taxe n’est pas du ressort de la Société, elle ne pourra pas être tenue au remboursement de cette taxe. 
<br><br>
Pour tous les produits expédiés hors Union européenne et/ou DOM-TOM, le prix est calculé hors taxes automatiquement sur la facture. Des droits de douane ou autres taxes locales ou droits d'importation ou taxes d'état sont susceptibles d'être exigibles dans certains cas. La Société n’a aucun contrôle sur ces droits et sommes.
<br><br>
Ils seront à la charge du Client et relèvent de sa responsabilité (déclarations, paiement aux autorités compétentes, etc.). La Société invite à ce titre le Client à se renseigner sur ces aspects auprès des autorités locales correspondantes. 


        </span>
    </p>
    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 5 – PAIEMENT DU PRIX DES PRODUITS
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            5.1 Moment du paiement
<br>
Le paiement de la totalité du prix de la Commande doit être effectué juste après la validation de la Commande.
<br>
La Société peut, de manière exceptionnelle, accorder un paiement en plusieurs fois, notamment eu égard au montant à régler et à la connaissance qu’elle a du Client concerné. Toutefois, la Société n’a aucune obligation d’accorder de telles modalités de paiement. En cas de situation particulière, le Client pourra en faire la demande en s’adressant au Service Clients de la Société à l’adresse suivante info@coupepro.hop.boutique.
<br>
5.2 Modes de paiement
<br>
Pour régler sa Commande, le Client peut choisir entre différents modes de paiement : 
<br>
Paiement par carte bancaire : 
Seules sont acceptées les cartes bancaires rattachées à un établissement bancaire situé en EUR ou les cartes bancaires internationales (Visa, MasterCard, American Express et Maestro). Le Client garantit la Société qu’il dispose des autorisations nécessaires pour payer avec la carte bancaire utilisée. Il reconnaît expressément que l’engagement de payer donné par carte est irrévocable et que la communication de son numéro de carte bancaire vaut autorisation de débit de son compte à concurrence du montant total correspondant aux Produits commandés. Le montant sera débité au moment de validation de la Commande. <br>
Les paiements par carte bancaire sont réalisés via une plateforme de paiement sécurisée et les informations sur les cartes bancaires communiquées bénéficient du procédé de cryptage SSL. 
<br>
De façon générale, en cas de refus d’autorisation de paiement de la part des organismes officiellement accrédités ou en cas de non-paiement de la Commande, la Société se réserve le droit de suspendre et/ou d’annuler ladite Commande. 
<br>
La Société se réserve le droit de suspendre à tout moment l’un quelconque des modes de paiement, notamment dans le cas où un prestataire de services de paiement ne proposerait plus le service utilisé ou dans le cas d’un litige avec un Client concernant une Commande précédente. 
<br>
La Société se réserve la possibilité de mettre en place une procédure de vérification des Commandes destinée à assurer qu’aucune personne n’utilise les coordonnées bancaires d’une autre personne à son insu. Dans le cadre de cette vérification, il peut être demandé au Client d’adresser par email ou par courrier à la Société une copie d’une pièce d’identité, un justificatif de domicile ainsi que la copie de la carte bancaire ayant servi au paiement. Des indications précises sur le contenu exact des informations demandées (pour préserver la confidentialité de ses données) seront communiquées au Client en cas de vérification. La commande ne sera validée qu’après réception et vérification des pièces envoyées.

        </span>
    </p>
    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 6 - LIVRAISON – DEDOUANEMENT – RECEPTION
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            6.1 Livraison<br><br>
            Avant la validation de la Commande, la Société communique au Client des informations concernant les différentes modalités de livraison ainsi que leurs tarifs respectifs. A la suite du choix du mode de livraison par le Client, la Société lui communique une estimation du délai de livraison. 
<br><br>
La Société mettra tout en œuvre pour que le/les Produit(s) soi(en)t livré(s) au plus tard à la date prévue pour la livraison. En cas de difficulté, la Société s’engage à communiquer rapidement avec le Client pour l’en informer et rechercher avec lui une solution adéquate.
<br><br>
Nos délais de livraison sont indiqués lors du passage en caisse. Ce sont les délais pratiqués en temps normal. Ces délais peuvent cependant varier pendant les périodes de forte demande de nos produits (Soldes, promotions exceptionnelles, déstockage...)
<br><br>
Le transport aérien, l'expédition et/ou la livraison du/des Produit(s) seront intégralement pris en charge par le Client ("Frais de livraison"). Ces frais de livraison sont inclus dans le prix final facturé au Client lors de la Commande. Par contre, les frais éventuels de douane ne sont pas compris dans le prix facturé au Client par la Société. 
<br><br>
En fonction des modalités choisies par le Client, la livraison sera effectuée, soit à l’adresse mentionnée par le Client, soit le cas échéant, dans un point relais choisi par le Client parmi la liste des points disponibles. Il incombe donc au Client de bien vérifier les indications communiquées pour la livraison car il reste seul responsable en en cas de défaut de livraison dû à des informations incomplètes ou erronées. 
<br><br>
6.2 Dédouanement
<br><br>
Lors de son achat, le Client achète le produit hors taxes et devient importateur du produit acheté en sa qualité de destinataire du Produit. 
<br><br>
Il est donc en particulier responsable des processus d'import et dédouanement du produit auprès des bureaux locaux de douane. En fonction du prix de sa commande, il pourra lui être demandé de s'acquitter des droits de douane (pour les Produits d’une valeur en douane supérieure à 150 €).
<br><br>
Le Client est seul responsable du processus de déclaration des droits de douane lors du dédouanement du Produit. Ces droits de douane, qui ne sont pas facturés au Client par la Société, ne relèvent pas du ressort de la Société. Cette dernière ne pourra donc pas être tenue du remboursement de ces droits. 
<br><br>
6.3. Réception
<br><br>
Lors de la réception du Produit, le Client s’engage à vérifier que le Produit est complet et qu’il n’est pas endommagé. 
<br><br>
En cas de constat d’une anomalie, le Client devra prendre contact avec le Service Client de la Société dans les trois jours (hors jours fériés) suivant la date de réception du Produit. Toute réclamation déposée hors délai ne pourra être traitée. 
<br><br>
6.4 Livraison sans assurance
            <br><br>
En cas de colis marqué comme livré mais que vous affirmez ne pas l'avoir reçu, nous ne pouvons ni vous rembourser ni vous le renvoyer gratuitement.
Les colis sans assurance ne pourront pas faire l'objet d'une réclamation en cas de perte ou de livraison tardive. Nous vous aiderons toutefois à vous fournir le maximum de documents en cas de réclamation auprès du transporteur. Nous informons notre clientèle qu'un colis suivi dont La Poste indique un tracking à l'état livré est considéré comme tel d'un point de vue légal.
            <br><br>
Par conséquent, nous pourrons régler, à l'amiable, ce problème en nous contactant à l'adresse suivante : contact@coupepro.hop.boutique


        </span>
    </p>
    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 7 – RETRACTATION
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
           7.1 Principe et délai
    <br><br>
    Le Client dispose du droit de se rétracter, sans donner de motif, dans un délai de quatorze (14) jours calendaires à compter de la date de réception de sa Commande. En cas de Commande de plusieurs Produits, le délai court à compter de la réception du dernier Produit. 
    <br><br>
    En cas d’exercice du droit de rétractation dans le délai susmentionné, le prix du ou des Produits achetés et les frais d’envoi seront remboursés, les frais de retour restant à la charge du Client.
    <br><br>
    7.2 Exclusions 
    <br><br>
    Conformément aux dispositions des articles L. 221-18 à L. 221-28 du Code de la consommation, ce droit de rétractation ne peut être exercé pour les contrats suivants :
    <br><br>
    De fourniture de biens ou de services dont le prix dépend de fluctuations sur le marché financier échappant au contrôle du professionnel et susceptibles de se produire pendant le délai de rétractation ;<br><br>
    De fourniture de biens confectionnés selon les spécifications du consommateur ou nettement personnalisés ;<br><br>
    De fourniture de biens susceptibles de se détériorer ou de se périmer rapidement ;<br><br>
    De fourniture de biens qui ont été descellés par le consommateur après la livraison et qui ne peuvent être renvoyés pour des raisons d'hygiène ou de protection de la santé ;<br><br>
    De fourniture de biens qui, après avoir été livrés et de par leur nature, sont mélangés de manière indissociable avec d'autres articles ;<br><br>
    De fourniture de boissons alcoolisées dont la livraison est différée au-delà de trente jours et dont la valeur convenue à la conclusion du contrat dépend de fluctuations sur le marché échappant au contrôle du professionnel ;<br><br>
    De travaux d'entretien ou de réparation à réaliser en urgence au domicile du consommateur et expressément sollicités par lui, dans la limite des pièces de rechange et travaux strictement nécessaires pour répondre à l'urgence ;<br><br>
    De fourniture d'enregistrements audio ou vidéo ou de logiciels informatiques lorsqu'ils ont été descellés par le consommateur après la livraison ;<br><br>
    De fourniture d'un journal, d'un périodique ou d'un magazine, sauf pour les contrats d'abonnement à ces publications ;<br><br>
    Conclus lors d'une enchère publique ;<br><br>

    7.3 Modalités d’exercice du droit de rétractation<br><br>

    Pour exercer son droit de rétractation, le Client doit informer la Société de son intention de se rétracter soit en remplissant le formulaire type dont un modèle figure ci-après, soit en adressant une déclaration dénuée d’ambiguïté et exprimant son intention de se rétracter dans le délai de 14 jours mentionné plus haut.<br><br> 

    Modèle-type de formulaire de rétractation : <br><br>

    A l’attention de la Société CoupePro™, (nom du site concerné), Service Client, 7 Place Félix Baret, 13006 Marseille, France.<br><br>

    Je soussigné ______ vous notifie par la présente ma rétractation du contrat portant sur la vente du Produit ci-dessous :<br><br>
    Dénomination du Produit :<br><br>
    Date de la commande et de la réception : <br><br>
    Numéro de commande : <br><br>
    Numéro de suivi de la demande de rétractation : <br><br>
    Nom du Client :<br><br>
    Adresse du Client :<br><br>
    Signature du Client : (uniquement en cas de notification du présent formulaire sur papier)<br><br>
    Date : <br><br>

    Ce formulaire doit être envoyé à la Société à l’une des adresses suivantes :<br><br>
    Pour un envoi papier, à CoupePro™, 7 Place Félix Baret, 13006 Marseille, France.<br><br>
    Pour un envoi électronique, à l’adresse : info@coupepro.hop.boutique<br><br>

    Le Client dispose d’un délai de 14 jours à compter de l’envoi de la notification de sa rétractation pour renvoyer le Produit à la Société dans son emballage d’origine, les frais de retour étant à la charge exclusive du Client. <br><br>

    Les Produits doivent être retournés dans leur état d’origine et complets (emballage, accessoires, notice, etc.) pour permettre une remise en vente par la Société. En cas de réception de Produits ouverts, utilisés, incomplets, endommagés ou salis, la Société ne procédera à aucun remboursement et pourra même, si elle le juge nécessaire et adéquat, engager la responsabilité du Client pour dépréciation du Produit. <br><br>

En cas de retour du Produit dans conditions prévues par la loi et les présentes CGV, la Société procédera au remboursement de la totalité des sommes versées par le Client, frais de livraison inclus, dans les 14 jours de la notification de rétractation, à moins que le Produit ne soit renvoyé après. Dans ce cas, la Société ne procédera au remboursement qu’après la réception et la vérification de l’état du Produit retourné. <br><br>

Ce remboursement sera effectué en utilisant le même moyen de paiement que celui utilisé par le Client pour payer la Commande, sauf accord exprès du Client pour un autre moyen de paiement. En cas de paiement par chèque cadeau/code promotionnel, le Client sera remboursé soit par l’envoi de nouveaux chèques cadeau/codes promotionnels pour un montant identique à celui payé sous cette forme. <br><br>

        </span>
    </p>
    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 8 – GARANTIES
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>

8.1 Garanties légales<br><br>

La Société reste tenue des défauts de conformité du bien conformément aux dispositions des articles L.217-4 et suivants du Code de la Consommation ainsi que des vices cachés de la chose vendue conformément aux articles 1641 et suivants du Code civil. <br><br>

Lorsqu’il agit dans le cadre de la garantie légale de conformité (telle que prévue par les articles L.217-4 et suivants du Code de la Consommation), le Client Consommateur :<br><br>
Bénéficie d’un délai de 2 ans à compter de la délivrance pour agir
Peut choisir entre la réparation ou le remplacement du Produit, sous réserve des conditions de coût prévues par l’article L.217-9 du Code de la consommation
Est dispensé de rapporter la preuve de l’existence du défaut de conformité pendant les 24 mois suivant la délivrance du bien. 
<br><br>
Le Client peut également décider d’agir dans le cadre de la garantie légale contre les vices cachés au sens de l’article 1641 du Code civil, le Client peut choisir entre la résolution de la vente ou une réduction du prix, conformément à l’article 1644 du Code civil. 
<br><br>
Ces garanties légales s’appliquent indépendamment de toute garantie contractuelle. 
<br><br>

Reproduction des textes applicables
<br><br>
L.217-4 Code de la Consommation
« Le vendeur livre un bien conforme au contrat et répond des défauts de conformité existant lors de la délivrance. Il répond également des défauts de conformité résultant de l’emballage, des instructions de montage ou de l’installation lorsque celle-ci a été mise à sa charge par le contrat ou réalisée sous sa responsabilité. »
<br><br>
L.217-5 Code de la Consommation<br><br>
« Le bien est conforme au contrat :<br><br>
1° S’il est propre à l’usage habituellement attendu d’un bien semblable et, le cas échéant :<br><br>
- s’il correspond à la description donnée par le vendeur et possède les qualités que celui-ci a présentées à l’acheteur sous forme d’échantillon ou de modèle ;
- s’il présente les qualités qu’un acheteur peut légitimement attendre eu égard aux déclarations publiques faites par le vendeur, par le producteur ou par son représentant, notamment dans la publicité ou dans l’étiquetage ;<br><br>
2° Ou s’il présente les caractéristiques définies d’un commun accord par les parties ou est propre à tout usage spécial recherché par l’acheteur, porté à la connaissance du vendeur et que ce dernier a accepté.» 
<br><br>
L.217-9 Code de la Consommation
« En cas de défaut de conformité, l’acheteur choisit entre la réparation et le remplacement du bien. Toutefois, le vendeur peut ne pas procéder selon le choix de l’acheteur si ce choix entraine un coût manifestement disproportionné au regard de l’autre modalité, compte tenu de la valeur du bien ou de l’importance du défaut. Il est alors tenu de procéder, sauf impossibilité, selon la modalité non choisie par l’acheteur. »
<br><br>
L.217-12 Code de la Consommation
« L’action résultant du défaut de conformité se prescrit par deux ans à compter de la délivrance du bien. »<br><br>
<br><br>
1641 du Code civil<br><br>
« Le vendeur est tenu de la garantie à raison des défauts cachés de la chose vendue qui la rendent impropre à l’usage auquel on la destine, ou qui diminuent tellement cet usage que l’acheteur ne l’aurait pas acquise, ou n’en aurait donné qu’un moindre prix, s’il les avait connus. »<br><br>

1648 du Code civil
« L’action résultant des vices rédhibitoires doit être intentée par l’acquéreur dans un délai de deux ans à compter de la découverte du vice. […] »
<br><br>

8.2 Garantie Fabricant
<br><br>
Certains Produits en vente sur le Site bénéficient d’une garantie contractuelle accordée par le fournisseur ou le fabricant du Produit, à laquelle la Société n’est pas directement partie. 
<br><br>
L’existence de ce type de garanties est mentionnée, le cas échéant, sur la page spécifique du Produit. 
Si le Client souhaite faire jouer cette garantie, il convient qu’il le signale à la Société par un contact avec le Service Client et qu’il consulte lui-même les modalités d’application de la garantie, qui sont généralement insérées dans la boite concernant le Produit. 
<br><br>
Il est rappelé que le bénéficie de la Garantie Fabricant ne fait pas obstacle à l’application des dispositions légales concernant la garantie légale de conformité et la garantie légale des vices cachés. 

        </span>
    </p>

    <p class=MsoNormal>
        <b>
            <span style='font-size:14.0pt;line-height:107%'>
                Article 9 - PROTECTION DES DONNÉES PERSONNELLES
            </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            Dans le cadre de la relation commerciale, la Société, responsable de traitement, collecte un certain nombre de données nominatives obligatoires (dont notamment, nom, prénom, adresse de livraison, etc. qui mentionnées par un astérisque) qui sont absolument nécessaires au traitement de la Commande, à la gestion de la relation commerciale, à la réalisation de statistiques et au respect des obligations légales et réglementaires de la Société. Elles sont conservées pendant 5 ans à compter de la fin du Contrat. 
<br><br>
Le fait pour le Client de ne pas communiquer ces informations entrainerait l’impossibilité de traiter sa Commande. 
<br><br>
Ces données sont destinées à un usage interne par la Société mais peuvent être transmises aux sociétés qui contribuent à l’exécution de la prestation, dont notamment celles qui assurent les livraisons des Produits ou assurent le traitement des paiements. 
<br><br>
Concernant ces données personnelles, le Client dispose de plusieurs droits : <br><br>
Droit d’accès aux données personnelles le concernant ;<br><br>
Droit de rectification et de suppression si les données personnelles sont inexactes, incomplètes, équivoques, périmées ou si la collecte, l’utilisation, la communication et la conservation de certaines données est interdite ;<br><br>
Droit de limitation du traitement des données, à condition que cette demande soit dûment justifiée et n’empêche pas le respect, par la Société de ses obligations réglementaires et légales ;<br><br>
Droit d’opposition au traitement des données (notamment en cas de traitement pour de la prospection commerciale)<br><br>
Droit de formuler des directives post-mortem concernant la conservation, l’effacement et la communication de vos données personnelles<br><br>
Droit de retirer son consentement à la réalisation de certains traitements (les traitements réalisés avant le retrait du consentement restent licites)<br><br>
Droit de porter réclamation devant la CNIL. <br><br>

Pour exercer ses droits, le Client peut adresser une demande à la Société, par le biais :<br><br>
d’un courrier adressé à la Société CoupePro™, 7 Place Félix Baret, 13006 Marseille, France.<br><br>

La demande doit mentionner l’adresse électronique du Client, ses noms, prénoms, adresse postale et doit être accompagnée d’une copie de sa pièce d’identité recto-verso.<br><br>

Une réponse lui sera adressée dans un délai d’un mois à compter de la réception de la demande. 

        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 10 - DROITS DE PROPRIÉTÉ INTELLECTUELLE
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
           Sauf mention contraire spécifiquement énoncée sur la page d’un Produit, les ventes de Produits sur le site n’entrainent aucun transfert de propriété intellectuelle sur les Produits vendus. <br><br>

Les marques, noms de domaines, produits, logiciels, images, vidéos, textes ou plus généralement toute information objet de droits de propriété intellectuelle sont et restent la propriété exclusive de la Société ou de leur propriétaire initial. Aucune cession de droits de propriété intellectuelle n’est réalisée au travers des présentes CGV. 

        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
           Article 11 - FORCE MAJEURE
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
          La Société ne saurait être tenue pour responsable d’une inexécution de ses obligations en vertu des présentes en cas de survenance d’un cas fortuit ou de force majeure qui en empêcherait l’exécution. La Société avisera le client de la survenance d’un tel évènement dès que possible.
        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 12 – EXCLUSION DE RESPONSABILITE 
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
           Nonobstant toute disposition contraire énoncée aux présentes, la Société ne pourra en aucun cas être tenue responsable en cas de pertes ou de dommages dus à une utilisation inappropriée du/des Produit(s) par le Client, incluant notamment une modification ou une altération du/des Produit(s) non autorisée par la Société.
        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 13 – SUSPENSION – RÉSILIATION DE COMPTE

        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>

La Société se réserve le droit de suspendre ou de résilier le compte d’un Client qui contreviendrait aux dispositions des CGV, ou de manière générale aux dispositions légales applicables, sans préjudice de tous dommages et intérêts que pourrait solliciter la Société.
<br><br>
Toute personne dont le compte aurait été suspendu ou clôturé ne pourra commander ultérieurement ni créer de nouveau compte sur le Site, sans l’autorisation préalable de la Société.

        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 14 – ARCHIVAGE – PREUVE
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            Sauf preuve contraire, les informations enregistrées par la Société constituent la preuve de l’ensemble des transactions. 
<br><br>
Lors de chaque Commande, le récapitulatif de Commande est envoyé par email au Client et archivé sur le site web de la Société. 
<br><br>
L’archivage des communications entre la Société et le Client est effectué sur des registres informatisés qui sont conservés pendant 5 ans dans des conditions raisonnables de sécurité. Ces registres, sur lesquels sont enregistrés les échanges sur un support fiable et durable, sont considérés comme preuve des communications, commandes, paiements et transactions intervenues entre le Client et la Société. Ils peuvent être produits à titre de preuve du Contrat. 
<br><br>
L'archivage des communications, de la commande, des détails de la commande, ainsi que des factures est eﬀectué sur un support fiable et durable de manière constituer une copie fidèle et durable conformément aux dispositions de l'article 1360 du code civil. Ces informations peuvent être produites à titre de preuve du contrat.
<br><br>
Le Client aura accès aux éléments archivés sur simple demande à l’adresse contact@coupepro.hop.boutique.

        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 15 - NULLITÉ ET MODIFICATION DES CGV
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
            Si l’une quelconque des stipulations des présentes CGV est nulle, elle sera réputée non écrite, mais n’entraînera pas la nullité de l’ensemble des dispositions contractuelles.
<br><br>
Toute tolérance de la part de la Société, dans l’application de tout ou partie des engagements pris dans le cadre des présentes CGV, quelles qu’en aient pu être la fréquence et la durée, ne saurait valoir modification des CGV, ni générer un droit quelconque pour le Client. 

        </span>
    </p>

    <p class=MsoNormal>
        <b>
        <span style='font-size:14.0pt;line-height:107%'>
            Article 16 – DROIT APPLICABLE ET REGLEMENT DES LITIGES
        </span>
        </b>
    </p>
    <p class=MsoNormal>
        <span style='font-size:14.0pt;line-height:107%'>
En cas de difficulté quelconque, le Service Client est à votre disposition pour trouver une solution amiable. 
<br><br>
A défaut de solution trouvée directement avec le Service Client, la Commission Européenne a mis en place une plateforme de résolution des litiges destinée à recueillir les éventuelles réclamations de consommateurs à la suite d’un achat en ligne. La plateforme transmet ensuite ces réclamations à un médiateur national compétent. Vous pouvez accéder à cette plateforme en suivant le lien suivant : http://ec.europa.eu/consumers/odr/.

        </span>
    </p>
</div>
</body>
</html>
