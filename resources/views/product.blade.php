@extends('layouts.app')

@section('title')
    Tondeuse Professionnelle Barbe et Cheveux CoupePro™
@endsection

@section('content')
    <style>
        .button{
            font-weight: 700;
            margin: 15px auto 10px;
            font-size: 21px;
            color: #FFFFFF !important;
            text-decoration: none;
            text-align: center;
            display: inline-block;
            padding: 15px 10px;
            cursor:pointer;
            width: 100%;
            text-transform:uppercase;
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            min-width: 210px;
            -webkit-box-shadow: 0 3px 5px 0 rgba(50, 50, 50, 0.75);
            -moz-box-shadow:    0 3px 5px 0 rgba(50, 50, 50, 0.75);
            box-shadow:         0 3px 5px 0 rgba(50, 50, 50, 0.75);


            -webkit-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
            -moz-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
            -ms-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
            animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
        }

        @-webkit-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(37, 174, 78, 0);}}
        @-moz-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(37, 174, 78, 0);}}
        @-ms-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(37, 174, 78, 0);}}
        @keyframes pulse {to {box-shadow: 0 0 0 45px rgba(37, 174, 78, 0);}}
    </style>
<div id="header" class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div id="header-logo" class="wpb_single_image wpb_content_element vc_align_center logo">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                            <img src="{!! asset('images/logo.png') !!}" class="vc_single_image-img attachment-full" alt=""/>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="s1" class="vc_row wpb_row vc_row-fluid vc_custom_1540838357423 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element " id="atf-headline">
                    <div class="wpb_wrapper">
                        <h1 style="text-align: center;">Tondeuse professionnelle Barbe et Cheveux CoupePro™</h1>
                    </div>
                </div>
                <div id="pp-atf" class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_gallery wpb_content_element vc_clearfix  guGallery">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_gallery_slides wpb_image_grid" data-interval="3">
                                            <ul class="wpb_image_grid_ul">
                                                <li class="isotope-item">
                                                    <img width="75" height="75" src="{!! asset('images/SilverLedEdition.png') !!}" class="attachment-thumbnail" alt=""/>
                                                </li>
                                                <li class="isotope-item">
                                                    <img width="75" height="75" src="{!! asset('images/OriginalLedEdition.png') !!}" class="attachment-thumbnail" alt=""/>
                                                </li>
                                                <li class="isotope-item">
                                                    <img width="75" height="75" src="{!! asset('images/packbarberetoriginal.png') !!}" class="attachment-thumbnail" alt=""/>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <iframe src="https://player.vimeo.com/video/477267311" width="100%" height="410" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner vc_custom_1564796484160">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  vc_custom_1565033156925 prodDesc" id="pp-description">
                                    <div class="wpb_wrapper">
                                        <p>
                                            <b>➔ N°1 des ventes : </b>
                                            utilisé par de nombreux coiffeurs barbiers en France, la tondeuse
                                            <strong>CoupePro™</strong>
                                            est reconnue comme étant l'une des meilleures du marché.
                                        </p>
                                        <p>
                                            <b>➔ Matériel de qualité professionnelle&nbsp;:&nbsp;</b>
                                            une conception digne des tondeuses les plus onéreuses,
                                            <span>
                                                pour une coupe parfaite et une précision au millimètre près !&nbsp;
                                            </span>
                                        </p>
                                        <p>
                                            <b>➔ Adapté à tout type de cheveux&nbsp;:<span>&nbsp;</span></b>
                                            cheveux courts&nbsp;ou longs, épais&nbsp;ou fins, cette tondeuse est faite pour vous !
                                        </p>
                                        <p>
                                            <span><b>➔&nbsp;Précision inégalable</b><strong> :&nbsp;</strong>ne ratez plus aucune coupe avec sa&nbsp;conception&nbsp;unique et ses lames en acier céramique ultraprécises.</span></p>
                                        <p><span><b>➔&nbsp;Facilement transportable</b><strong>&nbsp;:&nbsp;</strong>emportez-le partout avec vous grâce à son design&nbsp;compact et sa batterie intégrée.</span></p>
                                        <p><span><b>➔&nbsp;Economique :&nbsp;</b>dites adieu aux séances&nbsp;de coiffeur pour de simples&nbsp;retouches, désormais vous pouvez le faire vous même facilement !</span></p>
                                    </div>
                                </div>
                                <div class="vc_btn3-container  cta smooth-scroll center vc_btn3-left vc_custom_1564687810563" id="cta-pp">
                                    <a class="vc_general button vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey"
                                       id="fireButtonEvent"
                                       href="#offer"
                                       title="Tondeuse professionnelle Barbe et Cheveux CoupePro™">
                                        Obtenir maintenant
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="s2" class="vc_row wpb_row vc_row-fluid vc_custom_1521747817076 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                    <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_left e3">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                            <img width="1" height="1" src="{!! asset('images/1-year-warranty-badge.png') !!}" class="vc_single_image-img attachment-large" alt="year warranty badge">
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  e4">
                                    <div class="wpb_wrapper txtmb">
                                        <p>
                                            <span style="padding: 0 !important; margin: 0 !important;">
                                                Livré avec une garantie d'un an sans risque!
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="s8" class="vc_row wpb_row vc_row-fluid vc_custom_1564158811377 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1565040950498">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element " id="whatyouget-head">
                                    <div class="wpb_wrapper">
                                        <h2 style="text-align: center;">
                                            CONTENU DU KIT COUPEPRO
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1564159384259">
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div id="whatyouget-img" class="wpb_single_image wpb_content_element vc_align_right vc_custom_1565191421392">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                            <img width="323" height="187"
                                                 src="{!! asset('images/BroStyleSabots.jpg') !!}"
                                                 class="vc_single_image-img attachment-full"
                                                 alt=""
                                                 srcset="{!! asset('images/BroStyleSabots.jpg') !!} 323w, {!! asset('images/BroStyleSabots.jpg') !!} 300w"
                                                 sizes="(max-width: 323px) 1420px, 323px"/>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element vc_custom_1565040881050" id="whatyouget-bullets">
                                    <div class="wpb_wrapper">
                                        <ul>
                                            <li>
                                                <span style="color: #25ae4e;" data-mce-style="color: #25ae4e;">
                                                    <b>✓</b>
                                                </span>
                                                <b data-mce-fragment="1">&nbsp;</b>
                                                Votre tondeuse&nbsp;
                                                <strong data-mce-fragment="1">CoupePro</strong>
                                                (3 styles différents disponibles)
                                                <em>.</em>
                                            </li>
                                            <li>
                                                <span style="color: #25ae4e;" data-mce-style="color: #25ae4e;">
                                                    <b>✓</b>
                                                </span>
                                                <b data-mce-fragment="1">&nbsp;</b>
                                                3x sabots
                                                <span style="color: #000000;" data-mce-style="color: #000000;">(1mm,</span> 2mm, 3mm) et 0mm sans sabot.
                                            </li>
                                            <li>
                                                <span style="color: #25ae4e;" data-mce-style="color: #25ae4e;">
                                                    <b>✓</b>
                                                </span>
                                                <b data-mce-fragment="1">&nbsp;</b>1x batterie grande autonomie rechargeable .
                                            </li>
                                            <li>
                                                <span style="color: #25ae4e;" data-mce-style="color: #25ae4e;">
                                                    <b>✓</b>
                                                </span><b data-mce-fragment="1">&nbsp;</b>1x Chargeur USB (Chargeur universel).
                                            </li>
                                            <li>
                                                <span style="color: #25ae4e;" data-mce-style="color: #25ae4e;">
                                                    <b>✓</b>
                                                </span><b data-mce-fragment="1">&nbsp;</b>1x Brosse de nettoyage&nbsp;<strong>CoupePro</strong>.
                                            </li>
                                            <li>
                                                <span style="color: #25ae4e;" data-mce-style="color: #25ae4e;">
                                                    <b>✓</b>
                                                </span>
                                                <b data-mce-fragment="1">&nbsp;</b>
                                                1x Notice d'utilisation
                                                <strong>CoupePro</strong>
                                                en Français pour apprendre à utiliser votre tondeuse comme un professionnel.
                                                <br>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="offer" class="vc_row wpb_row vc_row-fluid vc_custom_1506448454757 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div id="offer-headline" class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1511272014542">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  title2">
                                    <div class="wpb_wrapper">
                                        <h2 style="text-align: center;">
                                            Choisir mon pack
                                        </h2>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element  title2" id="offer-subheadline">
                                    <div class="wpb_wrapper">
                                        <div style="margin-bottom: 14px !important; margin-top: 6px; padding-bottom:20px; padding-top:6px; border-top:1px solid #cccccc;">
                                            <i class="im im-timer timer-color" style="font-size:inherit; font-weight:bold; color:#25ae4e; margin-right: 6px;"></i>
                                            Plus que
                                            <strong class="timer-color" style="font-weight:800; color:#25ae4e;">
                                                <div id="timer-text-hour" style="display:inline-block; color:#25ae4e">05</div>
                                                Heures
                                                <div id="timer-text-min" style="display:inline-block; color:#25ae4e">40</div>
                                                Minutes
                                                <div id="timer-text-sec" style="display:inline-block; color:#25ae4e">14</div>
                                                Secondes
                                            </strong> pour profiter de cette offre !
                                            <br><i class="im im-truck timer-text" style="font-size:inherit; font-weight:bold; margin-right: 6px;"></i> Livraison + Suivie <strong style="font-weight:800;"><b>OFFERTS<b></b></b></strong>
                                            <br><i class="im im-check-mark-circle-o timer-text" style="font-size:inherit; font-weight:bold; margin-right: 6px;"></i> <strong style="font-weight:800;"><b>Retours gratuits pendant 14 jours<b></b></b></strong>

                                            <script type="text/javascript">
                                                var countdownsch=document.getElementById("timer-text-hour");
                                                var countdownscm=document.getElementById("timer-text-min");
                                                var countdownscs=document.getElementById("timer-text-sec");

                                                setInterval(function(){
                                                    var toDate=new Date();
                                                    var tomorrow=new Date();
                                                    tomorrow.setHours(24,0,0,0);
                                                    var diffMS=tomorrow.getTime()/1000-toDate.getTime()/1000;
                                                    var diffHr=Math.floor(diffMS/3600);
                                                    diffMS=diffMS-diffHr*3600;
                                                    var diffMi=Math.floor(diffMS/60);
                                                    diffMS=diffMS-diffMi*60;
                                                    var diffS=Math.floor(diffMS);
                                                    var resultH=((diffHr<10)?"0"+diffHr:diffHr);
                                                    var resultM=((diffMi<10)?"0"+diffMi:diffMi);
                                                    var resultS=((diffS<10)?"0"+diffS:diffS);
                                                    countdownsch.innerHTML=resultH;
                                                    countdownscm.innerHTML=resultM;
                                                    countdownscs.innerHTML=resultS;

                                                },1000);
                                            </script>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="offer-3" class="vc_row wpb_row vc_inner vc_row-fluid wrapper vc_custom_1564778005731">
                    <div class="offer presell presell-popup-1 guproduct_types_2_79 wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill offers-csdfsdsq" data-qte="1" data-id="1" data-price="34.75" id="offer-3-bundle-1">
                        <div class="vc_column-inner vc_custom_1565375711041">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element vc_custom_1565017717820 e1" id="offer-3-bundle-1-header">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            <strong>
                                                L'original
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <div id="offer-3-bundle-1-img" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1564786295614">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                            <img width="200"
                                                 height="200"
                                                 src="{!! asset('images/offer-1.jpg') !!}"
                                                 class="vc_single_image-img attachment-full"
                                                 alt="" sizes="(max-width: 200px) 1420px, 200px">
                                        </div>
                                    </figure>
                                </div>
                                <div class="wpb_text_column wpb_content_element vc_custom_1534442203216"
                                     id="offer-3-bundle-1-price">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            <span style="font-size: xx-large;">
                                               34,75 €
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="whats-included">
                                    <ul style="list-style: none; text-align: left">
                                        <li>Pour cheveux et barbe</li>
                                    </ul>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1565115520771 e3" id="offer-3-bundle-1-savings">
                                    <div class="wpb_wrapper">
                                        <p>
                                            <span style="font-weight:300; color:#383838;">
                                                Reste {!! rand(6, 14) !!} en stock
                                                Déjà {!! rand(3000, 3500) !!} commandées ce mois<br>
                                            </span>
                                        </p>
                                        <p>
                                            <span class="discount_price_amount discount_price_amount_5809809031322" style="text-decoration: line-through; color: red">€60,00</span>
                                            (<span class="discount_price_percentage discount_price_percentage_5809809031322">50</span> %)
                                        </p>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element vc_custom_1525363647046 e4">
                                    <div class="wpb_wrapper"></div>
                                </div>
                                <div class="wpb_text_column wpb_content_element vc_custom_1525363647046 e4">
                                    <div class="wpb_wrapper"></div>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1515715823576 e4" id="offer-3-bundle-2-shipping">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center; font-weight: bold;">
                                            <span style="display: inline-block; position: relative; top: -10px;">Livraison gratuite</span>
                                            <img class="alignnone wp-image-569 size-medium" src="{!! asset('images/lp-free-shipping-icon-black.svg') !!}" alt="Fee Shipping Icon Black" width="50" height="27">
                                        </p>
                                    </div>
                                </div>
                                <div class="vc_btn3-container cta vc_btn3-center" id="bundle1-cta">
                                    <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey">
                                        Commander
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="offer most-popular selected presell guproduct_types_1fs guproduct_types_70 wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill offers-csdfsdsq" data-qte="1" data-id="2" data-price="89.95" id="offer-3-bundle-2">
                        <div class="vc_column-inner vc_custom_1564788189648">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element vc_custom_1565017735307 e1" id="offer-3-bundle-2-header">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            <strong>
                                                Le Pack Barber
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1565017770962 e1" id="offer-3-bundle-2-subhead">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            Gold + Pack Barber
                                        </p>
                                    </div>
                                </div>
                                <div id="offer-3-bundle-2-img" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1564786384479">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                            <img width="200"
                                                 height="200"
                                                 src="{!! asset('images/pack.jpg') !!}"
                                                 alt="Tondeuse professionnelle Barbe et Cheveux CoupePro™" class="vc_single_image-img attachment-full"
                                                 srcset="{!! asset('images/pack.jpg') !!}" sizes="(max-width: 200px) 1420px, 200px"/>
                                        </div>
                                    </figure>
                                </div>
                                <div class="wpb_text_column wpb_content_element vc_custom_1564786425041 e2" id="offer-3-bundle-2-main">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">  Aujourd'hui seulement </p>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1525380292871" id="offer-3-bundle-2-price">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            <span style="font-size: xx-large;">
                                                89,90 €
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="whats-included">
                                    <ul style="list-style: none; text-align: left">
                                        <li>Garantie d'un an</li>
                                        <li>Pour cheveux et barbe</li>
                                    </ul>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1564188203591 e3" id="offer-3-bundle-2-savings">
                                    <div class="price">
                                        <span style="font-weight:300; color:#383838;">
                                              Reste {!! rand(4, 10) !!} en stock
                                                Déjà {!! rand(5000, 5500) !!} commandées ce mois<br>
                                            <span class="discount_price_amount discount_price_amount_5809809031322" style="text-decoration: line-through; color: red">€180,00</span>
                                            (<span class="discount_price_percentage discount_price_percentage_5809809031322">50</span> %)
                                        </span>
                                    </div>
                                </div>

                                <div class="wpb_text_column wpb_content_element  vc_custom_1515715823576 e4" id="offer-3-bundle-2-shipping">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center; font-weight: bold;">
                                            <span style="display: inline-block; position: relative; top: -10px;">Livraison gratuite</span>
                                            <img class="alignnone wp-image-569 size-medium" src="{!! asset('images/lp-free-shipping-icon-black.svg') !!}" alt="Fee Shipping Icon Black" width="50" height="27">
                                        </p>
                                    </div>
                                </div>
                                <div class="vc_btn3-container cta vc_btn3-center" id="bundle2-cta">
                                    <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey">
                                        Commander
                                    </button>
                                </div>
                                <div class="wpb_text_column wpb_content_element ribbon" id="offer-3-bundle-2-ribbon">
                                    <div class="corner-ribbon top-right sticky">
                                        <p>Le plus populaire</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="offer best-deal presell guproduct_types_1fs guproduct_types_1fs guproduct_types_1f guproduct_types_1f wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill offers-csdfsdsq" data-qte="1" data-id="3" data-price="34.75" id="offer-3-bundle-3">
                        <div class="vc_column-inner vc_custom_1564788234007">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  vc_custom_1565017792847 e1">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            <strong>
                                                La dorée
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <div id="offer-3-bundle-3-img" class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1564786415272">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper vc_box_border_grey">

                                            <img width="200"
                                                 height="200"
                                                 src="{!! asset('images/gold.jpg') !!}"
                                                 class="vc_single_image-img attachment-full"
                                                 alt="" srcset="{!! asset('images/gold.jpg') !!} 200w, {!! asset('images/gold.jpg') !!} 150w" sizes="(max-width: 200px) 1420px, 200px"/>
                                        </div>
                                    </figure>
                                </div>
                                <div class="wpb_text_column wpb_content_element vc_custom_1525389925408" id="offer-3-bundle-3-price">

                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">
                                            <span style="font-size: xx-large;">34,75 €</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="whats-included">
                                    <ul style="list-style: none; text-align: left">
                                        <li>Pour cheveux et barbe</li>
                                    </ul>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1565115520771 e3" id="offer-3-bundle-1-savings">
                                    <div class="wpb_wrapper">
                                        <p>
                                            <span style="font-weight:300; color:#383838;">
                                                Reste {!! rand(6, 14) !!} en stock
                                                Déjà {!! rand(4000, 4500) !!} commandées ce mois<br>
                                                <span class="discount_price_amount discount_price_amount_5809809031322" style="text-decoration: line-through; color: red">€60,00</span>
                                                (<span class="discount_price_percentage discount_price_percentage_5809809031322">50</span> %)
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1515715865935 e4" id="offer-3-bundle-3-shipping">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center; font-weight: bold;">
                                    <span style="display: inline-block; position: relative; top: -10px;">
                                        Livraison gratuite
                                    </span>
                                            <img class="alignnone wp-image-569 size-medium" src="{!! asset('images/lp-free-shipping-icon-black.svg') !!}" alt="Free Shipping Icon Black" width="50" height="27"/>
                                        </p>
                                    </div>
                                </div>
                                <div class="vc_btn3-container  cta vc_btn3-center" id="bundle3-cta">
                                    <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey">
                                        Commander
                                    </button>
                                </div>
                                <div class="wpb_text_column wpb_content_element ribbon" id="offer-3-bundle-3-ribbon">
                                    <div class="corner-ribbon top-right sticky" style="background-color: rgb(45, 173, 87)">
                                        <p>Meilleure affaire</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="price-tab" class="checkout" style="display: none;">
                    <h2 class="total">
                        Total:
                        <span class="price" data-price=""></span>
                    </h2>
                    <button onclick="prepCheckout();" id="cout" style="background-color: rgb(37, 174, 78); color: rgb(255, 255, 255);">
                        Commander
                    </button>
                </div>
                <div class="vc_btn3-container vc_btn3-center" id="buy-now-next">
                    <button style="background-color:#25ae4e; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom">
                        COMMANDEZ MAINTENANT
                    </button>
                </div>
                <div class="vc_btn3-container vc_btn3-center" id="buy-now-checkout">
                    <button style="background-color:#25ae4e; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" onclick="gu_fire_event('&#039;Checkout&#039;', '&#039;Click&#039;', '&#039;CheckoutButton&#039;', '&#039;InitiateCheckout&#039;')">
                        Check-out
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<main id="checkout" @if(Session::has('card') OR $errors->any()) style="display:block" @endif>
    <style>
        .d-none{
            display:block;
        }
        .alert{
            padding: 20px;
            background-color: #f44336;
            color: white;
            margin: 15px auto
        }
    </style>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert">{!! $error !!}</div>
        @endforeach
    @endif
    @if(Session::has('card'))
        <script>
            jQuery(document).ready(function(){
                jQuery('html,body').animate({
                    scrollTop: jQuery("#checkout").offset().top
                }, 'slow');
            })
        </script>
    @endif
    <!-- Data -->
    <div class="alert d-none"></div>
    <section id="data">
        <div class="wrapper">
            <div class="wrapper">
                <h1 class="headline">Terminer votre commande</h1>
            </div>
            <form id="main-form" novalidate method="post" action="{!! route('strip') !!}">
                @csrf
                <input type="text" id="amount" name="amount" style="display: none;">
                <input type="text" id="fprice" name="fprice" style="display: none;">
                <div class="divider">
                    <p>Ou payer par carte de crédit</p>
                </div>
                <!-- Shipping -->
                <section id="shipping-section" class="shipping">
                    <h2 class="title ship">Informations sur la livraison</h2>
                    <span class="inp email fullWidth">
                        <input name="email" id="email" autocomplete="email" spellcheck="false" autocapitalize="off" value="{!! old('email') !!}" placeholder="E-mail" type="email" required>
                        <label for="email" class="error">E-mail</label>

                        <label for=""></label>
                    </span>
                    <span class="inp fname formSpace">
                        <input name="fname" id="fname" autocomplete="given-name" required placeholder="Prénom(s)" type="text" value="{!! old('fname') !!}">
                        <label for="fname" class="error">Prénom(s)</label>
                    </span>
                    <span class="inp lname">
                        <input name="lname" id="lname" autocomplete="family-name" required placeholder="Nom" type="text" value="{!! old('lname') !!}">
                        <label for="lname" class="error">Nom</label>
                    </span>
                    <span class="inp fullwidth">
                        <input name="address1" id="address1" placeholder="Addresse" type="text" value="{!! old('address1') !!}">
                        <label for="address1" class="error">Adresse</label>
                    </span>
                    <span class="inp city fullWidth">
                        <input name="city" id="city" autocomplete="address-level2" required placeholder="Ville" type="text" value="{!! old('city') !!}">
                        <label for="city" class="error">Ville</label>
                    </span>
                    <span class="inp fname good filled formSpace">
                        <select name="country" id="country" autocomplete="country" required>
                            <option value="fr">France</option>
                        </select>
                        <label for="country" >Pays</label>
                    </span>
                    <span class="inp phone lname">
                        <input name="phone" id="phone" autocomplete="tel" required spellcheck="false" autocapitalize="off" placeholder="0033 1 33 33 33 33" type="tel" maxlength="13" value="{!! old('phone') !!}">
                        <label for="phone">Téléphone</label>
                        <a class="question" data-title="In case we need to contact you about your order">
                            <img width="100%" src="{!! asset('images/checkout-question.svg') !!}" alt="?">
                        </a>
                    </span>
                </section>

                <section class="payment" id="payment-method">
                    <h2 class="title">Informations de paiement</h2>
                    <p class="subtitle">Toutes les transactions sont sécurisées et cryptées.</p>
                    <div class="card-container">
                        <!--- ".card-type" is a sprite used as a background image with associated classes for the major card types, providing x-y coordinates for the sprite -->
                        <div id="crdTp" class="card-type"></div>
                        <!-- The checkmark ".card-valid" used is a custom font from icomoon.io -->
                    </div>
                    <div class="clear"></div>
                    <p class="error-msg"></p>
                    <span class="inp cc-number">
                        <input type="text" name="cardNumber" id="cardNumber" autocomplete="off" maxlength="19" placeholder="0000 0000 0000 0000" onkeyup="$cc.val(event)" value="{!! old('cardNumber') !!}">
                        <label for="cc-number">Numéro de carte</label>
                    </span>
                    <span class="inp cc-exp">
                        <input type="text" name="cc_exp" id="cc_exp" autocomplete="off" autocapitalize="off" onkeyup="$cc.expiry.call(this,event)" maxlength="7" placeholder="mm/yy" value="{!! old('cc_exp') !!}">
                        <label for="cc-exp">MM / YY</label>
                    </span>
                    <span class="inp cc-cvc">
                        <input type="text" name="cc_cvc" id="cc_cvc" autocomplete="off" autocapitalize="off" maxlength="3" placeholder="XXX" value="{!! old('cc_cvc') !!}">
                        <label for="cc-cvc">CVC</label>
                        <a class="question" data-title="3-digit security code usually found on the back of your card. American Express cards have a 4-digit code located on the front.">
                            <img width="100%" src="{!! asset('images/checkout-question.svg') !!}" alt="?">
                        </a>
                    </span>
                    <div id="card-errors"></div>
                </section>
                <!-- Order Now -->
                <button type="submit" class="submit next" data-next="Next">Complétez la commande</button>
            </form>
        </div>
    </section>
    <!-- Summary -->
    <section id="summary" style="display: block;">
        <div id="load" style="display: none">
            <img src="{!! asset('images/checkout-loading.gif') !!}" alt="checkout loading">
        </div>
        <section class="title">
            <div class="wrapper">
                <a>Récapitulatif de la commande</a>
                <p class="price"></p>
                <div class="clear"></div>
            </div>
        </section>
        <div class="toggle">
            <section class="products">
                <div class="wrapper" id="wr-response">

                </div>
            </section>
            <section class="extras">
            </section>
            <section id="tot" class="total">
                <div class="wrapper">
                    <p class="label">Total</p>
                    <p class="price float-right"></p>
                </div>
            </section>
        </div>
    </section>
    <div class="clear"></div>
    <!-- Checkout overlay -->
    <main id="checkout_overlay">
        <p class="loading_bar">Votre commande est en cours de traitement, veuillez patienter ...</p>
        <p class="no_internet_bar">Pas de connexion Internet</p>
        <p class="no_server_bar">Pas de connexion serveur</p>
        <div class="hidden"></div>
        <div class="center">
            <div class="center-anchor"></div>
        </div>
        <div class="loading">
            <h1>En traitement...</h1>
            <img width="40" src="{!! asset('images/checkout-loading.gif') !!}" alt="load">
        </div>
        <div class="no_internet">
            <h1>Votre Internet est déconnecté. Réessayer ...</h1>
            <img width="40" src="{!! asset('images/checkout-loading.gif') !!}" alt="no internet">
        </div>
        <div class="no_server">
            <h1>Le serveur ne répond pas. Réessayer ...</h1>
            <img width="40" src="{!! asset('images/checkout-loading.gif') !!}" alt="no server">
        </div>
    </main>
    <!-- Added to order -->
    <section id="conf">
        <div class="center">
            <div class="center-anchor">
                <div class="wrapper">
                    <img width="60" height="auto" src="{!! asset('images/checkout-shipping-green-check.svg') !!}" alt="checkout shipping green check">
                    <p>Article ajouté à la commande</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Alert -->
    <section id="alert">
        <div class="center">
            <div class="center-anchor">
                <div class="wrapper">
                    <p class="txt"></p>
                    <button class="accept">OK</button>
                </div>
            </div>
        </div>
    </section>
</main>

<div id="faq" class="vc_row wpb_row vc_row-fluid vc_custom_1514512362804 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper vc_custom_1499987972328">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  e1">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center; font-size: 22px;">
                                            <strong>QUESTIONS FRÉQUEMMENT POSÉES</strong>
                                            <br>
                                            <strong>
                                                <span style="color: #0073ff;">
                                                    <a style="color: #ffffff; letter-spacing: 1px; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.8);" href="#faq">
                                                        Question
                                                    </a>
                                                </span>
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element " id="faq-text">
                                    <div class="wpb_wrapper">
                                        <h4>L'article est-il durable ?</h4>
                                        <p>
                                            Oui, l'article est durable car il est fabriqué à partir de matériaux de qualité supérieure contre l'usure et la déformation. Un savoir-faire de qualité pour une durabilité accrue.
                                        </p>
                                        <h4>
                                            Où êtes-vous situé et d'où expédiez-vous ?
                                        </h4>
                                        <p>
                                            Notre entreprise et nos produits sont tous conçus à Marseille.
                                            Nous expédions à partir de différents Entrepôts,
                                            selon votre adresse de livraison

                                        </p>
                                        <h4>
                                            Quels sont les jours d'expédition ?
                                        </h4>
                                        <p>
                                            Nous expédions les commandes du lundi au vendredi.
                                            En raison de la situation actuelle,
                                            veuillez prévoir un délai de traitement de 3 à 5 jours.
                                        </p>
                                        <h4>
                                            Peut on utiliser la tondeuse avec et sans fil ?
                                        </h4>
                                        <p>
                                            Vous pouvez utiliser la tondeuse sans fil après l'avoir chargé ,
                                            ou bien directement branchée sur le chargeur.
                                            Le fil électrique s'adapte  bien sur la tondeuse, c'est plutôt pratique.
                                        </p>
                                        <h4>
                                            Combien de temps faudra-t-il pour que ma commande arrive ?
                                        </h4>
                                        <p>
                                            Pour les clients en France, votre commande devrait arriver dans les 4 à 6 jours ouvrables après avoir été traitée par les services de la poste.
                                            <br>
                                            Nous expédions les commandes internationales par DHL. Le délai de livraison moyen est de 7 à 9 jours ouvrables.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_raw_code wpb_raw_js hidden">
                    <div class="wpb_wrapper">
                        <script>
                            jQuery(function () {

                                // Wrap FAQ
                                if (!jQuery('#faq .wpb_wrapper .faq').length) {
                                    jQuery('#faq .wpb_wrapper h4').each(function () {
                                        jQuery(this).addClass('question');
                                        jQuery(this).nextUntil('h4').andSelf().wrapAll('<div class="faq"></div>');
                                    });
                                    jQuery('#faq .wpb_wrapper .faq').each(function () {
                                        jQuery(this).find('p, ul').wrapAll('<div class="answer"></div>');
                                    });
                                    jQuery('#faq .wpb_wrapper .faq').wrapAll('<div class="faq-wrapper"></div>');
                                    if (!jQuery('#faq').hasClass('expanded')) {
                                        jQuery('#faq .wpb_wrapper .faq-wrapper').hide();
                                    }
                                    // Hide answers
                                    jQuery('#faq .wpb_wrapper .faq .answer').hide();
                                }

                                // Show the FAQs when user clicks "See Our FAQs"
                                if (!jQuery('#faq').hasClass('expanded')) {
                                    jQuery('#faq .e1 a').on('click', function (e) {
                                        e.preventDefault();
                                        jQuery('#faq .wpb_wrapper .faq-wrapper').slideToggle();
                                        gu_fire_event('ShowFAQs', 'Click', 'FAQLink');

                                        jQuery('html, body').animate({scrollTop: jQuery("#faq").offset().top}, 1400);
                                    });
                                }

                                // Add onclick to FAQs
                                jQuery('#faq .wpb_wrapper .faq').on('click', function () {
                                    jQuery(this).toggleClass('active');
                                    jQuery(this).find('.answer').slideToggle();

                                    var question = jQuery('.question', this).text();
                                    if (jQuery(this).hasClass('active')) {
                                        gu_fire_event('OpenFAQ', 'Click', question);
                                    } else {
                                        gu_fire_event('CloseFAQ', 'Click', question);
                                    }
                                });

                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="footer-wrap" class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_text_column wpb_content_element  disclaimer">
        <div class="wpb_wrapper">
            <p>
                Les déclarations concernant l'efficacité et la sécurité de CoupePro n'ont pas été évaluées par l'Agence nationale de sécurité sanitaire de l’alimentation, de l’environnement et du travail. La ANSES évalue uniquement les aliments et les médicaments, pas les suppléments comme ces produits. Ces produits ne sont pas destinés à diagnostiquer, prévenir, traiter ou guérir une maladie. Ces informations ne constituent pas un avis médical et ne doivent pas être invoquées en tant que telles. Consultez votre médecin avant de modifier votre régime médical habituel.
            </p>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                        <div id="footer">
                            <div class="giddy-footer-container">
                                <ul class="giddy-links">
                                    <li id="footer-support-cta">
                                        <span onclick="giddybox('#support')">
                                            Support
                                        </span>
                                    </li>
                                    <li id="footer-returns-cta">
                                        <span onclick="giddybox('#returns')">
                                            Politique de remboursement
                                        </span>
                                    </li>
                                    <li id="footer-privacy-cta">
                                        <span onclick="giddybox('#privacy')">
                                            Politique de confidentialité
                                        </span>
                                    </li>
                                    <li id="footer-terms-cta">
                                        <span onclick="giddybox('#terms')">
                                            CGV
                                        </span>
                                    </li>
                                </ul>
                                <span class="copyright">
                                    Copyright &copy; 2020 CoupePro™. All Rights Reserved.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_raw_code wpb_content_element wpb_raw_html">
        <div class="wpb_wrapper">
            <div id="footer-disclaimer" style="display: block;">
                <em>
                    <strong>
                        À propos de ce site
                    </strong>
                </em>
                <p>
                    CoupePro™ est un fournisseur de produits novateurs et un revendeur officiel de la Tondeuse "CoupePro". En partenariat avec les inventeurs de CoupePro, nous vous présentons une offre spéciale que vous ne trouverez nulle part ailleurs. Afin de soutenir ces inventeurs, achetez votre CoupePro ici, plutôt qu'auprès d'un revendeur tiers.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid hidden">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="wpb_raw_code wpb_raw_js">
                    <div class="wpb_wrapper">
                        <script>
                            jQuery(function () {
                                // YouTube video auto resize
                                jQuery('.youtube-video iframe').css({
                                    width: jQuery('.youtube-video').innerWidth() + 'px !important',
                                    height: jQuery('.youtube-video').innerHeight() + 'px !important'
                                });

                                // Add the before-the-fold bobbing arrow
                                jQuery('#s1').append('<a class="bobbing_arrow animated bounce infinite"><img src="/images/dropdown-arrow.svg" alt="dropdown arrow"></a>');
                                jQuery('.bobbing_arrow').on('click', function () {
                                    jQuery('html, body').animate({scrollTop: jQuery("#s2").offset().top}, 2000);
                                    gu_fire_event('BobbingArrow', 'Click', 's1');
                                });

                                // Hide the bobbing arrow
                                jQuery(document).scroll(function () {
                                    if (jQuery(document).scrollTop() > jQuery("#s1").offset().top - 300) {
                                        jQuery('.bobbing_arrow').fadeOut();
                                    }
                                });
                            });
                        </script>
                    </div>
                </div>
                <div id="privacy" class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  giddybox-text wrapper2"
                                     id="privacy-text">
                                    <div class="wpb_wrapper">
                                        <p>
                                            <iframe style="border-style: hidden;" src="{!! url('privacy.htm') !!}" width="800" allowfullscreen="allowfullscreen"></iframe>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="terms" class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  giddybox-text wrapper2"
                                     id="terms-text">
                                    <div class="wpb_wrapper">
                                        <p>
                                            <iframe style="border-style: hidden;" src="{!! url('terms.htm') !!}" width="800" allowfullscreen="allowfullscreen"></iframe>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="support" class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div
                                    class="wpb_text_column wpb_content_element  vc_custom_1565712543011 giddybox-text wrapper2">
                                    <div class="wpb_wrapper">
                                        <h2 style="color: #52c8f6; font-size: 2em;">Avez-vous une question sur Coupe Pro ?</h2>
                                        <ul>
                                            <li>
                                                Questions à propos de votre ordre
                                            </li>
                                            <li>
                                                Questions sur le produit
                                            </li>
                                            <li>
                                                Support & Service Clients
                                            </li>
                                            <li>
                                                Retours et Remboursement
                                            </li>
                                            <li>
                                                Autres questions sur le produit ou lié à la commande
                                            </li>
                                        </ul>
                                        <h3>Veuillez contactez CoupePro™ directement sur:</h3>
                                        <ul>
                                            <li>info@coupepro.hop.boutique</li>
                                            <li>7 Place Félix Baret, 13006 Marseille, France</li>
                                        </ul>
                                        <h3>
                                            Souhaitez-vous vous désabonner de notre newsletter?
                                        </h3>
                                        <p>
                                            Veuillez cliquer sur
                                            <a href="#" target="_blank" rel="noopener">le lien</a>
                                            suivant pour être ajouté à la liste globale de numéros exclus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="returns" class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element  vc_custom_1565712429720 giddybox-text wrapper2">
                                    <div class="wpb_wrapper">
                                        <h1 style="text-align: center; color: #52c8f6; font-size: 3em;">
                                            Politique de remboursement
                                        </h1>
                                        <h2 style="text-align: center; color: #52c8f6; font-size: 2em;">
                                            1. Politique de livraison
                                        </h2>
                                        <p>
                                            <b>CoupePro™</b> ne pourra être tenu responsable des colis perdus ou mal acheminés si vous n'avez pas souscris à l'assurance anti-vol/casse lors de votre commande. Merci dans ce cas de vous rapprocher du transporteur de votre colis muni de votre N° de suivi (La Poste).

                                        </p>
                                        <p>
                                            Veuillez vous assurer que toutes les informations que vous avez fournies sont correctes avant de soumettre votre commande pour éviter que des pertes de colis ou d’autres incidents ne surviennent. Un client international doit fournir un numéro de téléphone.

                                        </p>
                                        <p>
                                            Si vous avez d'autres préoccupations, veuillez contacter notre service clientèle en envoyant un ticket via la rubrique contactez-nous de notre site Web <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>
                                            <br>
                                            <b>Les clients sont invités à saisir soigneusement les adresses correctes, sinon le colis ne sera pas livré.</b>
                                        </p>
                                        <p>
                                            Si vous pensez que vous avez entré une mauvaise adresse, vous devez nous contacter immédiatement afin de corriger l'erreur.
                                        </p>
                                        <h2 style="text-align: center; color: #52c8f6; font-size: 2em;">
                                            2. Politique d'annulation
                                        </h2>
                                        <p>
                                            Q: Puis-je annuler ma commande?
                                        </p>
                                        <p>
                                            R: Oui, nous acceptons les annulations de commande.
                                        </p>
                                        <p>
                                            Attention car toutefois, les annulations doivent être effectuées le jour même où la commande a été passée sous réserve que notre service logistique n'ai pas encore traité votre commande.
                                        </p>
                                        <p>
                                            Pour annuler votre commande, envoyez-nous un email le jour même avant minuit et faites-nous savoir la raison pour laquelle vous souhaitez annuler votre commande. Nous n'acceptons pas l'annulation de commandes en dehors de la de cette fenêtre / ou en raison de la raison d'expédition standard.
                                        </p>

                                        <h2 style="text-align: center; color: #52c8f6; font-size: 2em;">
                                            3. Politique de remboursement
                                        </h2>
                                        <p>
                                            Nous remboursons ou échangeons sans aucun frais supplémentaire votre article après avoir reçu de votre part un email dans un délai de 48 heures pour un problème technique ou pour un produit endommagé et dans un délai de 14 jours après réception de votre part.
                                        </p>
                                        <p>Notez dans le cadre d'une rétractation, <b>CoupePro™</b> ne reprend en aucun cas les produits qui ne sont plus emballés sous blister pour des questions d'hygiènes comme le souligne l'Art. L. 121-21-8 n°5 de la LOI n° 2014-344 du 17 mars 2014 relative à la consommation.</p>
                                        <p>
                                            <b>CoupePro™</b> se réserve le droit de refuser tout remplacement et remboursement s’il ne répond pas aux critères suivants :
                                        </p>
                                        <ol>
                                            <li>
                                                Retour tardifs (délais de 14 jours dépassé)
                                            </li>
                                            <li>
                                                La raison du retour n'est pas valable
                                            </li>
                                        </ol>
                                        <p>
                                            Au retour de votre colis, s'il est dans l'état que vous nous avez indiqué et que rien n'est manquant nous procéderons au remboursement sur votre moyen de paiement. Si vous n'avez pas encore reçu de remboursement, vérifiez à nouveau votre compte bancaire et contactez votre société émettrice de carte de crédit, cela peut prendre un certain temps avant que votre remboursement ne soit officiellement publié. Si c'est un remboursement, il apparaîtra immédiatement dans votre compte.
                                        </p>
                                        <p>
                                            Si vous avez fait tout cela et n’avez toujours pas reçu votre remboursement, veuillez nous contacter en nous envoyant un mail sur notre site web <a href="https://coupepro.hop.boutique">https://coupepro.hop.boutique</a>
                                        </p>

                                        <h2 style="text-align: center; color: #52c8f6; font-size: 2em;">
                                            4. Garantie
                                        </h2>
                                        <p>
                                            Après l'achat d'une ou de plusieurs tondeuse CoupePro, vous profitez d'une garantie de 1 an. La garantie par défaut de votre tondeuse comprend uniquement sa fonctionnalité à la date de réception.<br>
                                            Nous ne garantissons pas que la qualité de tous les produits, services, informations, ou toute autre marchandise que vous avez obtenue ou achetée répondra à vos attentes, ni que toute erreur dans le Service sera corrigée.

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <style>
        @import url("//fonts.googleapis.com/css?family=Open+Sans:400,600");

        .custom-social-proof {
            direction: ltr;
            text-align: left;
            position: fixed;
            bottom: 20px;
            left: 20px;
            z-index: 9999999999999 !important;
            font-family: 'Open Sans', sans-serif
        }
        .custom-social-proof .custom-notification {
            width: 300px;
            border: 0;
            text-align: right;
            z-index: 99999;
            box-sizing: border-box;
            font-weight: 400;
            border-radius: 6px;
            box-shadow: 2px 2px 10px 2px rgba(11, 10, 10, 0.2);
            background-color: #fff;
            position: relative;
            cursor: pointer
        }
        .custom-social-proof .custom-notification .custom-notification-container {
            display: flex !important;
            align-items: center;
            height: 80px
        }
        .custom-social-proof .custom-notification .custom-notification-container .custom-notification-image-wrapper img {
            max-height: 75px;
            width: 90px;
            overflow: hidden;
            border-radius: 6px 0 0 6px;
            object-fit: cover
        }
        .custom-social-proof .custom-notification .custom-notification-container .custom-notification-content-wrapper {
            margin: 0;
            height: 100%;
            color: gray;
            padding-left: 20px;
            padding-right: 20px;
            border-radius: 0 6px 6px 0;
            flex: 1;
            display: flex !important;
            flex-direction: column;
            justify-content: center
        }
        .custom-social-proof .custom-notification .custom-notification-container .custom-notification-content-wrapper .custom-notification-content {
            font-family: inherit !important;
            margin: 0 !important;
            padding: 0 !important;
            font-size: 14px;
            line-height: 16px;
            text-align: left
        }
        .custom-social-proof .custom-notification .custom-notification-container .custom-notification-content-wrapper .custom-notification-content small {
            margin-top: 3px !important;
            display: block !important;
            font-size: 12px !important;
            opacity: .8
        }
        .custom-social-proof .custom-notification .custom-close {
            position: absolute;
            top: 8px;
            right: 8px;
            height: 12px;
            width: 12px;
            cursor: pointer;
            transition: .2s ease-in-out;
            transform: rotate(45deg);
            opacity: 0
        }
        .custom-social-proof .custom-notification .custom-close::before {
            content: "";
            display: block;
            width: 100%;
            height: 2px;
            background-color: gray;
            position: absolute;
            left: 0;
            top: 5px
        }
        .custom-social-proof .custom-notification .custom-close::after {
            content: "";
            display: block;
            height: 100%;
            width: 2px;
            background-color: gray;
            position: absolute;
            left: 5px;
            top: 0
        }
        .custom-social-proof .custom-notification:hover .custom-close {
            opacity: 1
        }

        .poweredby {
            font-weight: 600;
            text-decoration: none;
            margin-left: 3px;
            color: #3498db
        }

        #footer-wrap {     max-width: 1200px;
            margin: 0 auto;
            color: #919b9a;
            padding: 20px 0 10px;
            text-align: center;
            font: 14px "Open Sans", sans-serif;
            background: #333333
        }
        #footer-wrap .e1 { background:white; width:800px; font-size:13px; padding:20px !important; margin:0 auto !important; }
        #footer-wrap .right { display:inline-block; float:right; }
        #footer-wrap .left { display:inline-block; float:left; }
        #footer-wrap a,#footer-wrap button { background:none; border:none; font-size:12px; letter-spacing:1px; color:white; text-decoration:none; padding-left:20px !important;  }
        #footer-wrap a:hover,#footer-wrap button:hover { color:#499fdc; transition: .3s; text-decoration:underline; }
        #footer-wrap .vc_btn3-container { margin:0 !important; }
        #footer-wrap .disclaimer {font-size: 11px; color: #dedede; padding-bottom: 40px;}
        #footer-wrap .disclaimer p { padding-bottom: 10px !important; font-size: 15px }
    </style>
    <section class="custom-social-proof">
        <div class="custom-notification">
            <div class="custom-notification-container">
                <div class="custom-notification-image-wrapper">
                    <img id="map1" src="{!! asset('images/SilverLedEdition.png') !!}" style="width: 50px; height: 50px">
                </div>
                <div class="custom-notification-content-wrapper">
                    <p class="custom-notification-content">
                        <span id="productss">Peuvel LUC</span> de <span id="countryN">Paris</span> a acheté ce produit
                        <small>
                            il y a <span id="time">1</span> Heures &nbsp;
                            <i class="fa fa-check-circle"></i>
                        </small>
                    </p>
                </div>
            </div>
            <div class="custom-close"></div>
        </div>
    </section>

    <script>

        jQuery(document).ready(function() {

            jQuery('.wptwa-toggle').on('click', function (e) {
                jQuery('.wptwa-container').toggleClass('wptwa-round-toggle-on-desktop').toggleClass('wptwa-show');
            });

            jQuery('.wptwa-close').on('click', function (e) {
                jQuery('.wptwa-container').toggleClass('wptwa-round-toggle-on-desktop').toggleClass('wptwa-show');
            });

            jQuery.getJSON("https://api.ipify.org?format=json",
                function(data) {
                    jQuery('#ip').val(data.ip);
                });
        });

        var r_text = new Array ();
        r_text[0] = "Paris";
        r_text[1] = "Nice";
        r_text[2] = "Marseille";
        r_text[3] = "Tours";
        r_text[5] = "Lyon";
        r_text[4] = "Toulon";
        r_text[6] = "Rouen";
        r_text[7] = "Roubaix";
        r_text[8] = "Argenteuil";
        r_text[9] = "Rennes";

        var r_map = new Array ();
        r_map[0] = "{!! asset('images/SilverLedEdition.png') !!}";

        var r_product = new Array ();
        r_product[0] = "Peuvel LUC";
        r_product[1] = "PONSIN FABIEN";
        r_product[2] = "PONSIN BENOIT";
        r_product[3] = "SAPPIN FABIEN";
        r_product[4] = "VISSIO ANTONY";
        r_product[5] = "RICARD FLORENT";
        r_product[6] = "VITTY JULLIEN";
        r_product[7] = "TOINON ROCCO";

        setInterval(function(){
            jQuery(".custom-social-proof").stop().slideToggle('slow')
        }, 4000);
        jQuery(".custom-close").click(function() {
            jQuery(".custom-social-proof").stop().slideToggle('slow');
        });

        setInterval(function(){
            var myNumber = Math.floor(7*Math.random());
            jQuery("#map1").attr("src",r_map[0]);
            jQuery('#countryN').text(r_text[myNumber]);

            jQuery('#productss').text(r_product[Math.floor(7*Math.random())]);
            var timeVal = Math.floor(7*Math.random());

            jQuery('#time').text(timeVal + 1);
        }, 6000);
    </script>

    <style>
        .d-none{
            display: none;
        }
        .wptwa-container, .wptwa-button {
            font-family: sans-serif;
            font-size: 14px;
            line-height: 1.5em;
        }
        .wptwa-container .wptwa-box {
            background-color: rgba(255, 255, 255, 1);
        }
        @media screen and (min-width: 783px){
            .wptwa-round-toggle-on-desktop .wptwa-box {
                bottom: 100px;
            }
        }
        .wptwa-left-side .wptwa-toggle, .wptwa-left-side .wptwa-box {
            left: auto;
            right: 20px;
        }
        .wptwa-box.wptwa-js-ready {
            display: block;
        }
        .wptwa-box {
            position: fixed;
            bottom: 80px;
            right: 20px;
            max-width: 360px;
            width: 100%;
            border-radius: 6px;
            box-shadow: 0 8px 25px -5px rgba(45, 62, 79, .15);
            visibility: hidden;
            opacity: 0;
            transform: translateY(50px) scale(0.9);
            background-color: white;
            transition: all .3s;
        }
        .wptwa-container.wptwa-left-side .wptwa-box:before, .wptwa-container.wptwa-left-side .wptwa-box:after {
            right: 22px;
            left: auto;
        }
        .wptwa-container .wptwa-box:before, .wptwa-container .wptwa-box:after {
            background-color: rgba(255, 255, 255, 1);
            border-color: rgba(255, 255, 255, 1);
        }
        .wptwa-show .wptwa-box {
            visibility: visible;
            opacity: 1;
            transform: translateY(0) scale(1);
        }
        .wptwa-container .wptwa-box:before {
            box-shadow: none;
            background: white;
            transform: none;
            bottom: 0;
            width: 19px;
            right: 17px;
            border-color: white;
        }
        .wptwa-container .wptwa-box:before, .wptwa-container .wptwa-box:after {
            content: '';
            position: absolute;
            bottom: -7px;
            right: 22px;
            width: 13px;
            height: 13px;
            background: white;
            transform: rotate(45deg);
            z-index: -1;
            border: 1px solid #f5f5f5;
            box-shadow: 3px 3px 3px rgba(0, 0, 0, .02);
        }
        .wptwa-box .wptwa-people {
            overflow: auto;
            padding: 10px;
        }
        .wptwa-box .wptwa-account:first-child {
            border-top: 0;
        }
        .wptwa-box .wptwa-account, .wptwa-container .wptwa-account.wptwa-offline:hover {
            border-color: #f5f5f5;
        }
        .wptwa-container .wptwa-gdpr, .wptwa-container .wptwa-account {
            color: rgba(85, 85, 85, 1);
        }
        .wptwa-box .wptwa-account {
            padding: 10px;
            border-top: 1px solid #f5f5f5;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: hidden;
            position: relative;
        }
        .wptwa-container a, .wptwa-button {
            color: #333;
            text-decoration: none;
        }
        .wptwa-clearfix:before, .wptwa-clearfix:after {
            content: '';
            display: block;
            clear: both;
        }
        .wptwa-box .wptwa-no-image .wptwa-face {
            background: transparent url({!! asset('images/SilverLedEdition.png') !!}) center center no-repeat;
            background-size: cover;
        }

        @media screen and (min-width: 783px){
            .wptwa-round-toggle-on-desktop .wptwa-box {
                bottom: 100px;
            }
        }

        .wptwa-left-side .wptwa-toggle, .wptwa-left-side .wptwa-box {
            right: 20px;
            left: auto;
        }



        .wptwa-box .wptwa-face {
            float: left;
            width: 60px;
            height: 60px;
            position: relative;
        }

        .wptwa-box .wptwa-face img {
            position: absolute;
            top: 50%;
            left: 50%;
            border-radius: 50px;
            transform: translateY(-50%) translateX(-50%);
        }

        .wptwa-container img, .wptwa-button img {
            max-width: 100%;
            max-height: 100%;
            vertical-align: top;
        }

        .wptwa-box .wptwa-title, .wptwa-box .wptwa-offline-text {
            font-size: 12px;
            line-height: 1.5em;
            opacity: .6;
            display: block;
        }

        .wptwa-box .wptwa-name {
            display: block;
            font-weight: bold;
        }

        .wptwa-container .wptwa-toggle, .wptwa-container .wptwa-mobile-close, .wptwa-container .wptwa-description, .wptwa-container .wptwa-description a {
            background-color: #0dc152;
            color: rgba(255, 255, 255, 1);
        }
        @media screen and (max-width: 783px){
            .custom-social-proof{
                display: none;
                visibility: hidden;
            }
        }

        @media screen and (min-width: 783px){
            .wptwa-round-toggle-on-desktop .wptwa-toggle {
                border-radius: 50px;
                width: 60px;
                height: 60px;
            }

        }

        .wptwa-toggle {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 40px;
            white-space: nowrap;
            line-height: 100%;
            padding: 12px 20px;
            border-radius: 50px;
            position: fixed;
            bottom: 20px;
            right: 20px;
            cursor: pointer;
            box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16);
            transition: box-shadow 80ms ease-in-out,width .4s ease .2s;
        }

        .wptwa-toggle, .wptwa-box {
            z-index: 9999999;
        }

        .wptwa-toggle, .wptwa-box .wptwa-description, .wptwa-button, .wptwa-mobile-close {
            background: #0DC152;
            color: white;
        }

        .wptwa-container .wptwa-toggle svg {
            fill: rgba(255, 255, 255, 1);
        }

        @media screen and (min-width: 783px){
            .wptwa-round-toggle-on-desktop .wptwa-toggle svg {
                margin: 0;
                width: 30px;
                height: 30px;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translateY(-50%) translateX(-50%);
            }
        }

        svg:not(:root) {
            overflow: hidden;
        }

        .wptwa-toggle svg {
            fill: white;
            display: inline-block;
            margin-right: 5px;
            margin-left: 10px;
            -moz-user-select: none;
        }

        @media screen and (min-width: 783px){
            .wptwa-round-toggle-on-desktop .wptwa-toggle .wptwa-text {
                display: none;
            }

        }

        .wptwa-container .wptwa-toggle, .wptwa-container .wptwa-mobile-close, .wptwa-container .wptwa-description, .wptwa-container .wptwa-description a {
            background-color: #0dc152;
            color: rgba(255, 255, 255, 1);
        }

        .wptwa-toggle, .wptwa-box .wptwa-description, .wptwa-button, .wptwa-mobile-close {
            background: #0DC152;
            color: white;
        }

        .wptwa-mobile-close {
            display: none;
        }
        .wptwa-container .wptwa-toggle, .wptwa-container .wptwa-mobile-close, .wptwa-container .wptwa-description, .wptwa-container .wptwa-description a {
            background-color: #0dc152;
            color: rgba(255, 255, 255, 1);
        }

        .wptwa-box .wptwa-description {
            padding: 15px 40px 15px 20px;
            border-radius: 6px 6px 0 0;
        }

        .wptwa-toggle, .wptwa-box .wptwa-description, .wptwa-button, .wptwa-mobile-close {
            background: #0DC152;
            color: white;
        }

        .wptwa-container .wptwa-description p {
            color: rgba(255, 255, 255, 1);
        }

        .wptwa-container p:last-child {
            margin-bottom: 0;
        }

        .wptwa-container p:first-child {
            margin-top: 0;
        }

        .wptwa-box .wptwa-description + .wptwa-close {
            visibility: visible;
        }

        .wptwa-box .wptwa-close {
            display: block;
            width: 15px;
            height: 15px;
            position: absolute;
            top: 7px;
            right: 7px;
            opacity: .5;
            visibility: hidden;
            transition: opacity .3s;
        }

        .wptwa-container .wptwa-close:before, .wptwa-container .wptwa-close:after {
            background-color: rgba(255, 255, 255, 1);
        }

        .wptwa-box .wptwa-close:before, .wptwa-box .wptwa-close:after {
            content: '';
            display: block;
            background: white;
            width: 15px;
            height: 3px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translateY(-50%) translateX(-50%) rotate(45deg);
        }

        .wptwa-box .wptwa-close:after {
            transform: translateY(-50%) translateX(-50%) rotate(-45deg);
        }

        .wptwa-box .wptwa-people {
            overflow: auto;
            padding: 10px;
        }


        .wptwa-toggle, .wptwa-box {
            z-index: 9999999;
        }
        .wptwa-container *, .wptwa-button * {
            -webkit-box-sizing: border-box;
            -khtml-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
        }
    </style>

    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="wptwa-logo">
            <path id="WhatsApp" d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z"/>
        </symbol>
    </svg>
@endsection
