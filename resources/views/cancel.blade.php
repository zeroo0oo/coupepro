@extends('layouts.app')

@section('content')

    <div id="header" class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid wrapper">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="header-logo" class="wpb_single_image wpb_content_element vc_align_center logo">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                <img src="{!! asset('images/type-s-logo-01.svg') !!}" class="vc_single_image-img attachment-full"/>
                                            </div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #topbar {
            background: #4f4a41;
            padding: 10px 0 10px 0;
            text-align: center;
        }

        #topbar a {
            color: #fff;
            font-size:1.3em;
            line-height: 1.25em;
            text-decoration: none;
            opacity: 0.5;
            font-weight: bold;
        }

        #topbar a:hover {
            opacity: 1;
        }

        /** typography **/
        h1 {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 2.5em;
            line-height: 1.5em;
            letter-spacing: -0.05em;
            margin-bottom: 20px;
            padding: .1em 0;
            color: #444;
            position: relative;
            overflow: hidden;
            white-space: nowrap;
            text-align: center;
        }
        h1:before,
        h1:after {
            content: "";
            position: relative;
            display: inline-block;
            width: 50%;
            height: 1px;
            vertical-align: middle;
            background: #f0f0f0;
        }
        h1:before {
            left: -.5em;
            margin: 0 0 0 -50%;
        }
        h1:after {
            left: .5em;
            margin: 0 -50% 0 0;
        }
        h1 > span {
            display: inline-block;
            vertical-align: middle;
            white-space: normal;
        }

        p {
            display: block;
            font-size: 1.35em;
            line-height: 1.5em;
            margin-bottom: 22px;
        }


        /** page structure **/
        #w {
            display: block;
            width: 750px;
            margin: 0 auto;
            padding-top: 30px;
        }

        #content {
            display: block;
            background: #fff;
            padding: 25px 20px;
            padding-bottom: 35px;
            -webkit-box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;
            -moz-box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;
            box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;
        }


        .flatbtn {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            display: inline-block;
            outline: 0;
            border: 0;
            color: #f9f8ed;
            text-decoration: none;
            background-color: #b6a742;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            font-size: 1.2em;
            font-weight: bold;
            padding: 12px 22px 12px 22px;
            line-height: normal;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            text-transform: uppercase;
            text-shadow: 0 1px 0 rgba(0,0,0,0.3);
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            -webkit-box-shadow: 0 1px 0 rgba(15, 15, 15, 0.3);
            -moz-box-shadow: 0 1px 0 rgba(15, 15, 15, 0.3);
            box-shadow: 0 1px 0 rgba(15, 15, 15, 0.3);
        }
        .flatbtn:hover {
            color: #fff;
            background-color: #c4b237;
        }
        .flatbtn:active {
            -webkit-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.1);
            -moz-box-shadow:inset 0 1px 5px rgba(0, 0, 0, 0.1);
            box-shadow:inset 0 1px 5px rgba(0, 0, 0, 0.1);
        }

        /** notifications **/
        .notify {
            display: block;
            background: #fff;
            padding: 12px 18px;
            max-width: 400px;
            margin: 0 auto;
            cursor: pointer;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            margin-bottom: 20px;
            box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 2px 0px;
        }

        .notify h1 { margin-bottom: 6px; }

        .successbox h1 { color: #678361; }
        .errorbox h1 { color: #6f423b; }

        .successbox h1:before, .successbox h1:after { background: #cad8a9; }
        .errorbox h1:before, .errorbox h1:after { background: #d6b8b7; }

        .notify .alerticon {
            display: block;
            text-align: center;
            margin-bottom: 10px;
        }
    </style>
    <main style="padding: 30px 15px">
        <div id="content">
            <!-- Icons source
            -->
            <div class="notify errorbox">
                <h1>Warning!</h1>
                <span class="alerticon">
                    <img src="{!! asset('images/type-s-logo-01.svg') !!}" alt="error">
                </span>
                <p>You did not set the proper return e-mail address. Please fill out the fields and then submit the form.</p>
            </div>
        </div><!-- @end #content -->
    </main>
@endsection
