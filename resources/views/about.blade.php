
<!DOCTYPE html>
<html>
<head>
    <title>About GiddyUp</title>
    <style type="text/css">
        .aboutuscontainer{
            font-family:"Open Sans", sans-serif;
        }
    </style>
</head>
<body>
<div class="aboutuscontainer">
    <h2 style="text-align: center; color: #52c8f6; font-size: 2em;">About GiddyUp</h2>
    <p style="text-align: left;">At GiddyUp, we're extremely passionate about connecting innovators with our global community &ndash; to ensure the best solutions rise to the top.</p>
    <p style="text-align: left;">When you purchase a product from GiddyUp, you can feel good knowing it's high quality, does what it says, and includes a deep discount that allows everyone to enjoy and gift them.</p>
    <p style="text-align: left;">If you love discovering innovative solutions to everyday problems (and helping drive innovation across the world), then we'd love for you to join our fast-growing community.</p>
    <p>
    <p style="text-align: left;">You can always contact us at: <a href="mailto:support@giddyup.io">support@giddyup.io</a></p>
    <p>
    <p style="text-align: left;">Or mail us at:<br>The GiddyUp Group, LLC<br>16 N. Oak St<br>Ventura, CA 93001</p>
    <p>
    <p style="text-align: left;">Or call us at: +1 510 399 2921</p>
    <p>
    <p style="text-align: left;">Our US EIN number is: 46-3920047</p>
</div>
</body>
</html>
