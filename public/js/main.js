// Polyfills v1.1
if (typeof Object.assign != 'function') {
    // Must be writable: true, enumerable: false, configurable: true
    Object.defineProperty(Object, "assign", {
        value: function assign(target, varArgs) { // .length of function is 2
            'use strict';
            if (target == null) { // TypeError if undefined or null
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var to = Object(target);

            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];

                if (nextSource != null) { // Skip over if undefined or null
                    for (var nextKey in nextSource) {
                        // Avoid bugs when hasOwnProperty is shadowed
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                            to[nextKey] = nextSource[nextKey];
                        }
                    }
                }
            }
            return to;
        },
        writable: true,
        configurable: true
    });
}

if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
        'use strict';
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

// mutationobserver-shim v0.3.2 (github.com/megawac/MutationObserver.js)
// Authors: Graeme Yeates (github.com/megawac)
window.MutationObserver = window.MutationObserver || function(w) {
    function v(a) {
        this.i = [];
        this.m = a
    }

    function I(a) {
        (function c() {
            var d = a.takeRecords();
            d.length && a.m(d, a);
            a.h = setTimeout(c, v._period)
        })()
    }

    function p(a) {
        var b = {
                type: null,
                target: null,
                addedNodes: [],
                removedNodes: [],
                previousSibling: null,
                nextSibling: null,
                attributeName: null,
                attributeNamespace: null,
                oldValue: null
            },
            c;
        for (c in a) b[c] !== w && a[c] !== w && (b[c] = a[c]);
        return b
    }

    function J(a, b) {
        var c = C(a, b);
        return function(d) {
            var f = d.length,
                n;
            b.a && 3 === a.nodeType &&
            a.nodeValue !== c.a && d.push(new p({
                type: "characterData",
                target: a,
                oldValue: c.a
            }));
            b.b && c.b && A(d, a, c.b, b.f);
            if (b.c || b.g) n = K(d, a, c, b);
            if (n || d.length !== f) c = C(a, b)
        }
    }

    function L(a, b) {
        return b.value
    }

    function M(a, b) {
        return "style" !== b.name ? b.value : a.style.cssText
    }

    function A(a, b, c, d) {
        for (var f = {}, n = b.attributes, k, g, x = n.length; x--;) k = n[x], g = k.name, d && d[g] === w || (D(b, k) !== c[g] && a.push(p({
            type: "attributes",
            target: b,
            attributeName: g,
            oldValue: c[g],
            attributeNamespace: k.namespaceURI
        })), f[g] = !0);
        for (g in c) f[g] || a.push(p({
            target: b,
            type: "attributes",
            attributeName: g,
            oldValue: c[g]
        }))
    }

    function K(a, b, c, d) {
        function f(b, c, f, k, y) {
            var g = b.length - 1;
            y = -~((g - y) / 2);
            for (var h, l, e; e = b.pop();) h = f[e.j], l = k[e.l], d.c && y && Math.abs(e.j - e.l) >= g && (a.push(p({
                type: "childList",
                target: c,
                addedNodes: [h],
                removedNodes: [h],
                nextSibling: h.nextSibling,
                previousSibling: h.previousSibling
            })), y--), d.b && l.b && A(a, h, l.b, d.f), d.a && 3 === h.nodeType && h.nodeValue !== l.a && a.push(p({
                type: "characterData",
                target: h,
                oldValue: l.a
            })), d.g && n(h, l)
        }

        function n(b, c) {
            for (var g = b.childNodes,
                     q = c.c, x = g.length, v = q ? q.length : 0, h, l, e, m, t, z = 0, u = 0, r = 0; u < x || r < v;) m = g[u], t = (e = q[r]) && e.node, m === t ? (d.b && e.b && A(a, m, e.b, d.f), d.a && e.a !== w && m.nodeValue !== e.a && a.push(p({
                type: "characterData",
                target: m,
                oldValue: e.a
            })), l && f(l, b, g, q, z), d.g && (m.childNodes.length || e.c && e.c.length) && n(m, e), u++, r++) : (k = !0, h || (h = {}, l = []), m && (h[e = E(m)] || (h[e] = !0, -1 === (e = F(q, m, r, "node")) ? d.c && (a.push(p({
                type: "childList",
                target: b,
                addedNodes: [m],
                nextSibling: m.nextSibling,
                previousSibling: m.previousSibling
            })), z++) : l.push({
                j: u,
                l: e
            })),
                u++), t && t !== g[u] && (h[e = E(t)] || (h[e] = !0, -1 === (e = F(g, t, u)) ? d.c && (a.push(p({
                type: "childList",
                target: c.node,
                removedNodes: [t],
                nextSibling: q[r + 1],
                previousSibling: q[r - 1]
            })), z--) : l.push({
                j: e,
                l: r
            })), r++));
            l && f(l, b, g, q, z)
        }
        var k;
        n(b, c);
        return k
    }

    function C(a, b) {
        var c = !0;
        return function f(a) {
            var k = {
                node: a
            };
            !b.a || 3 !== a.nodeType && 8 !== a.nodeType ? (b.b && c && 1 === a.nodeType && (k.b = G(a.attributes, function(c, f) {
                if (!b.f || b.f[f.name]) c[f.name] = D(a, f);
                return c
            })), c && (b.c || b.a || b.b && b.g) && (k.c = N(a.childNodes, f)), c = b.g) : k.a =
                a.nodeValue;
            return k
        }(a)
    }

    function E(a) {
        try {
            return a.id || (a.mo_id = a.mo_id || H++)
        } catch (b) {
            try {
                return a.nodeValue
            } catch (c) {
                return H++
            }
        }
    }

    function N(a, b) {
        for (var c = [], d = 0; d < a.length; d++) c[d] = b(a[d], d, a);
        return c
    }

    function G(a, b) {
        for (var c = {}, d = 0; d < a.length; d++) c = b(c, a[d], d, a);
        return c
    }

    function F(a, b, c, d) {
        for (; c < a.length; c++)
            if ((d ? a[c][d] : a[c]) === b) return c;
        return -1
    }
    v._period = 30;
    v.prototype = {
        observe: function(a, b) {
            for (var c = {
                b: !!(b.attributes || b.attributeFilter || b.attributeOldValue),
                c: !!b.childList,
                g: !!b.subtree,
                a: !(!b.characterData && !b.characterDataOldValue)
            }, d = this.i, f = 0; f < d.length; f++) d[f].s === a && d.splice(f, 1);
            b.attributeFilter && (c.f = G(b.attributeFilter, function(a, b) {
                a[b] = !0;
                return a
            }));
            d.push({
                s: a,
                o: J(a, c)
            });
            this.h || I(this)
        },
        takeRecords: function() {
            for (var a = [], b = this.i, c = 0; c < b.length; c++) b[c].o(a);
            return a
        },
        disconnect: function() {
            this.i = [];
            clearTimeout(this.h);
            this.h = null
        }
    };
    var B = document.createElement("i");
    B.style.top = 0;
    var D = (B = "null" != B.attributes.style.value) ? L : M,
        H = 1;
    return v
}(void 0);
//# sourceMappingURL=mutationobserver.map
// Global Querystring Parser V1.1
//inserting my common stuff
var requiredData = ['aff_id', 'sub_id', 'req_id', 'user_id'];
var global_key_prefix = 'giddy-';
var checkout_token_name = 'tkn';
var localData = Object.create(null);


//NOTE: it was expressed to not use global_key_prefix for url params.
var localToURLNameKeys = {
    user_id: 'guu'
};


function copyDataTo_gu_qs(gu_qs, data) {
    for (var i = 0; i < requiredData.length; i++) {
        var key = requiredData[i];

        var qsKey = (localToURLNameKeys[key] ? localToURLNameKeys[key] : key);
        //NOTE: dont override what is already in gu_qs. and dont add undefined
        if ((!gu_qs[qsKey] || gu_qs[qsKey] == 'undefined') && data[key]) {
            gu_qs[qsKey] = data[key];
        }

        //NOTE: user_id from other sources wins.  if user_id is undefined then bug somewhere
        if (key === 'user_id' && data[key]) {
            gu_qs[qsKey] = data[key];
        }
    }
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        return this.substr(position || 0, searchString.length) === searchString;
    };
}


function gu_assign(dest, source) {
    for (var k in source) {
        if (k && source[k] && source[k] != 'undefined') {
            dest[k] = source[k];
        }
    }
    return dest;
};


function getDomain() {
    var parts = document.location.hostname.toLowerCase().split('.');
    var domain = parts.slice(-2).join('.');

    return domain;
}

//copied from mdn
var docCookies = {
    getItem: function(sKey) {
        if (!sKey) {
            return null;
        }
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },
    setItem: function(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
        if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
            return false;
        }

        if (!vEnd) {
            vEnd = Infinity;
        }

        var sExpires = "";
        if (vEnd) {
            switch (vEnd.constructor) {
                case Number:
                    sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                    break;
                case String:
                    sExpires = "; expires=" + vEnd;
                    break;
                case Date:
                    sExpires = "; expires=" + vEnd.toUTCString();
                    break;
            }
        }
        document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
        return true;
    },
    removeItem: function(sKey, sPath, sDomain) {
        if (!this.hasItem(sKey)) {
            return false;
        }
        document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
        return true;
    },
    hasItem: function(sKey) {
        if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
            return false;
        }
        return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
    },
    keys: function() {
        var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
        for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) {
            aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]);
        }
        return aKeys;
    }
};

function getCookieData() {
    //NOTE: try catch prevents optimization but not going to have an effect
    var hld = Object.create(null);
    try {
        var cData = docCookies.keys();

        for (var i = 0; i < cData.length; i++) {
            if (cData[i].startsWith(global_key_prefix)) {
                //NOTE: we know its our data and not wild stuff in shoppify.com
                hld[cData[i].replace(global_key_prefix, '')] = docCookies.getItem(cData[i]);
            }
        }
        return hld;
    } catch (e) {
        return hld;
    }
}

function getLocalStorageData() {
    var hldItms = Object.create(null);
    try {
        if (typeof(Storage) !== "undefined") {
            for (var i = 0; i < localStorage.length; i++) {
                var cKey = localStorage.key(i);
                if (cKey.startsWith(global_key_prefix)) {
                    //NOTE: we know its our data and not wild stuff in shoppify.com
                    hldItms[cKey.replace(global_key_prefix, '')] = localStorage.getItem(cKey);
                }
            }
        }
    } catch (e) {} finally {
        return hldItms;
    }
}

function getLocalData() {
    var cData = getCookieData();

    var sData = getLocalStorageData();

    return gu_assign(sData, cData);
}

function saveDataLocal(data) {

    //NOTE: assumes data already merged with priority
    //NOTE: tmp for now may make array of excluded props
    delete data.referrer;

    var oKeys = Object.keys(data);

    for (var i = 0; i < oKeys.length; i++) {
        try {
            var key = oKeys[i];

            if (key && typeof(data[key]) !== 'undefined' && data[key] !== 'undefined') {

                //save to cookie
                //docCookies.setItem(global_key_prefix + key, data[key]);

                //save to local storage
                if (typeof(Storage) !== "undefined") {
                    localStorage[global_key_prefix + key] = data[key];
                }
            }
        } catch (e) {
            var huh = true;
        }
    }
}

function getCheckoutTokenFromURL() {
    try {
        var parts = document.location.pathname.split('/');

        for (var i = 0; i < parts.length; i++) {
            if (parts[i].toLowerCase() == 'checkouts') {
                return parts[i + 1];
            }
        }
    } catch (e) {

    }
    return null;
}



//uuid v4
! function(n) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = n();
    else if ("function" == typeof define && define.amd) define([], n);
    else {
        var e;
        e = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, e.uuidv4 = n()
    }
}(function() {
    return function n(e, r, o) {
        function t(f, u) {
            if (!r[f]) {
                if (!e[f]) {
                    var a = "function" == typeof require && require;
                    if (!u && a) return a(f, !0);
                    if (i) return i(f, !0);
                    var d = new Error("Cannot find module '" + f + "'");
                    throw d.code = "MODULE_NOT_FOUND", d
                }
                var l = r[f] = {
                    exports: {}
                };
                e[f][0].call(l.exports, function(n) {
                    var r = e[f][1][n];
                    return t(r ? r : n)
                }, l, l.exports, n, e, r, o)
            }
            return r[f].exports
        }
        for (var i = "function" == typeof require && require, f = 0; f < o.length; f++) t(o[f]);
        return t
    }({
        1: [function(n, e, r) {
            function o(n, e) {
                var r = e || 0,
                    o = t;
                return o[n[r++]] + o[n[r++]] + o[n[r++]] + o[n[r++]] + "-" + o[n[r++]] + o[n[r++]] + "-" + o[n[r++]] + o[n[r++]] + "-" + o[n[r++]] + o[n[r++]] + "-" + o[n[r++]] + o[n[r++]] + o[n[r++]] + o[n[r++]] + o[n[r++]] + o[n[r++]]
            }
            for (var t = [], i = 0; i < 256; ++i) t[i] = (i + 256).toString(16).substr(1);
            e.exports = o
        }, {}],
        2: [function(n, e, r) {
            (function(n) {
                var r, o = n.crypto || n.msCrypto;
                if (o && o.getRandomValues) {
                    var t = new Uint8Array(16);
                    r = function() {
                        return o.getRandomValues(t), t
                    }
                }
                if (!r) {
                    var i = new Array(16);
                    r = function() {
                        for (var n, e = 0; e < 16; e++) 0 === (3 & e) && (n = 4294967296 * Math.random()), i[e] = n >>> ((3 & e) << 3) & 255;
                        return i
                    }
                }
                e.exports = r
            }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
        }, {}],
        3: [function(n, e, r) {
            function o(n, e, r) {
                var o = e && r || 0;
                "string" == typeof n && (e = "binary" == n ? new Array(16) : null, n = null), n = n || {};
                var f = n.random || (n.rng || t)();
                if (f[6] = 15 & f[6] | 64, f[8] = 63 & f[8] | 128, e)
                    for (var u = 0; u < 16; ++u) e[o + u] = f[u];
                return e || i(f)
            }
            var t = n("./lib/rng"),
                i = n("./lib/bytesToUuid");
            e.exports = o
        }, {
            "./lib/bytesToUuid": 1,
            "./lib/rng": 2
        }]
    }, {}, [3])(3)
});


// Convert a name/value querystring array to string version
function gu_qs_to_str(qs) {
    var e = encodeURIComponent;
    var s = "";
    for (var k in qs) {
        if (s != "") {
            s += '&';
        }
        s += e(k) + '=' + e(qs[k]);

    }

    if (s != "") {
        s = '?' + s;
    }

    return s;
}

// Parse a query string into key/value pairs
gu_deparam = function gu_deparam(querystring) {
    var params = [];
    if (querystring == "" || querystring == "?") {
        return params;
    }
    if (querystring[0] == '?') {
        querystring = querystring.substr(1);
    }
    querystring = querystring.split('&');
    var params = {},
        pair, d = decodeURIComponent;
    for (var i = querystring.length - 1; i >= 0; i--) {
        pair = querystring[i].split('=');
        params[d(pair[0])] = d(pair[1]);
    }
    return params;
}; //--  fn  gu_deparam

// Create global query string key/value pair array
var gu_qs = gu_deparam(document.location.search);

localData = getLocalData();
copyDataTo_gu_qs(gu_qs, localData);

//  International Data v2.1
// const dynamicallyLoadScriptHead  = require('../common/helpers.js').dynamicallyLoadScriptHead;

// // <!-- International Data V2.1 -->
// dynamicallyLoadScriptHead("https://js.giddyup.io/v01/gu-intl-common-content.js", "intdatav2.1.");

// <!-- Load common Internationalizion content -->
var gu_language = "en";
var gu_country = "us";
var gu_currency = "usd";
var gu_languageAndCountry = "en-us";
var gu_translationLanguage = "en";
var gu_eu_country = false;
var gu_latam_country = false;
var gu_dollar_currency = false;

// Given a language, what is the default country we should use for that language?
var gu_default_country = {
    "af": "za",
    "ar": "sa",
    "be": "by",
    "bg": "bg",
    "ca": "es",
    "cs": "cz",
    "da": "dk",
    "de": "de",
    "div": "mv",
    "el": "gr",
    "en": "us",
    "es": "es",
    "et": "ee",
    "eu": "es",
    "fa": "ir",
    "fi": "fi",
    "fil": "ph",
    "fo": "fo",
    "fr": "fr",
    "gl": "es",
    "gu": "in",
    "he": "il",
    "hi": "in",
    "hr": "hr",
    "hu": "hu",
    "hy": "am",
    "id": "id",
    "is": "is",
    "it": "it",
    "ja": "jp",
    "ka": "ge",
    "kk": "kz",
    "kn": "in",
    "kok": "in",
    "ko": "kr",
    "ky": "kz",
    "lt": "lt",
    "lv": "lv",
    "mk": "mk",
    "mn": "mn",
    "mr": "in",
    "ms": "my",
    "nb": "no",
    "nl": "nl",
    "nn": "no",
    "no": "no",
    "pa": "in",
    "pl": "pl",
    "pt": "pt",
    "ro": "ro",
    "ru": "ru",
    "sa": "in",
    "sk": "sk",
    "sl": "si",
    "sq": "al",
    "sv": "se",
    "sw": "ke",
    "syr": "sy",
    "ta": "in",
    "te": "in",
    "th": "th",
    "tr": "tr",
    "tt": "ru",
    "uk": "ua",
    "ur": "pk",
    "vi": "vn",
    "zh": "hk",
};

var gu_currencies = {
    "af": "afn",
    "ax": "eur",
    "al": "all",
    "dz": "dzd",
    "ad": "eur",
    "ao": "aoa",
    "ai": "xcd",
    "ag": "xcd",
    "ar": "ars",
    "am": "amd",
    "aw": "awg",
    "au": "aud",
    "at": "eur",
    "az": "azn",
    "bs": "bsd",
    "bh": "bhd",
    "bd": "bdt",
    "bb": "bbd",
    "by": "byn",
    "be": "eur",
    "bz": "bzd",
    "bj": "xof",
    "bm": "bmd",
    "bt": "inr",
    "bo": "bob",
    "ba": "bam",
    "bw": "bwp",
    "bv": "nok",
    "br": "brl",
    "io": "usd",
    "bn": "bnd",
    "bg": "bgn",
    "bf": "xof",
    "bi": "bif",
    "kh": "khr",
    "ca": "cad",
    "cv": "cve",
    "ky": "kyd",
    "cf": "xaf",
    "td": "xaf",
    "cl": "clp",
    "cn": "cny",
    "cx": "aud",
    "cc": "aud",
    "co": "cop",
    "km": "kmf",
    "cg": "xaf",
    "cd": "cdf",
    "ck": "nzd",
    "cr": "crc",
    "ci": "xof",
    "hr": "hrk",
    "cu": "cup",
    "cw": "ang",
    "cy": "eur",
    "cz": "czk",
    "dk": "dkk",
    "dj": "djf",
    "dm": "xcd",
    "do": "dop",
    "ec": "usd",
    "eg": "egp",
    "sv": "svc",
    "gq": "xaf",
    "er": "ern",
    "ee": "eur",
    "et": "etb",
    "fk": "fkp",
    "fo": "dkk",
    "fj": "fjd",
    "fi": "eur",
    "fr": "eur",
    "gf": "eur",
    "pf": "xpf",
    "tf": "eur",
    "ga": "xaf",
    "gm": "gmd",
    "ge": "gel",
    "de": "eur",
    "gh": "ghs",
    "gi": "gip",
    "gr": "eur",
    "gl": "dkk",
    "gd": "xcd",
    "gp": "eur",
    "gt": "gtq",
    "gg": "gbp",
    "gn": "gnf",
    "gw": "xof",
    "gy": "gyd",
    "ht": "usd",
    "hm": "aud",
    "va": "eur",
    "hn": "hnl",
    "hk": "hkd",
    "hu": "huf",
    "is": "isk",
    "in": "inr",
    "id": "idr",
    "ir": "irr",
    "iq": "iqd",
    "ie": "eur",
    "im": "gbp",
    "il": "ils",
    "it": "eur",
    "jm": "jmd",
    "jp": "jpy",
    "je": "gbp",
    "jo": "jod",
    "kz": "kzt",
    "ke": "kes",
    "ki": "aud",
    "kp": "kpw",
    "xk": "eur",
    "kw": "kwd",
    "kg": "kgs",
    "la": "lak",
    "lv": "eur",
    "lb": "lbp",
    "ls": "lsl",
    "lr": "lrd",
    "ly": "lyd",
    "li": "chf",
    "lt": "eur",
    "lu": "eur",
    "mo": "mop",
    "mk": "mkd",
    "mg": "mga",
    "mw": "mwk",
    "my": "myr",
    "mv": "mvr",
    "ml": "xof",
    "mt": "eur",
    "mq": "eur",
    "mr": "mro",
    "mu": "mur",
    "yt": "eur",
    "mx": "mxn",
    "md": "mdl",
    "mc": "eur",
    "mn": "mnt",
    "me": "eur",
    "ms": "xcd",
    "ma": "mad",
    "mz": "mzn",
    "mm": "mmk",
    "na": "nad",
    "nr": "aud",
    "np": "npr",
    "nl": "eur",
    "an": "ang",
    "nc": "xpf",
    "nz": "nzd",
    "ni": "nio",
    "ne": "xof",
    "ng": "ngn",
    "nu": "nzd",
    "nf": "aud",
    "no": "nok",
    "om": "omr",
    "pk": "pkr",
    "ps": "egp",
    "pa": "pab",
    "pg": "pgk",
    "py": "pyg",
    "pe": "pen",
    "ph": "php",
    "pn": "nzd",
    "pl": "pln",
    "pt": "eur",
    "qa": "qar",
    "cm": "xaf",
    "re": "eur",
    "ro": "ron",
    "ru": "rub",
    "rw": "rwf",
    "bl": "eur",
    "sh": "shp",
    "kn": "xcd",
    "lc": "xcd",
    "mf": "eur",
    "pm": "eur",
    "ws": "wst",
    "sm": "eur",
    "st": "std",
    "sa": "sar",
    "sn": "xof",
    "rs": "rsd",
    "sc": "scr",
    "sl": "sll",
    "sg": "sgd",
    "sx": "ang",
    "sk": "eur",
    "si": "eur",
    "sb": "sbd",
    "so": "sos",
    "za": "zar",
    "gs": "fkp",
    "kr": "krw",
    "ss": "ssp",
    "es": "eur",
    "lk": "lkr",
    "vc": "xcd",
    "sd": "sdg",
    "sr": "srd",
    "sj": "nok",
    "sz": "zar",
    "se": "sek",
    "ch": "chf",
    "sy": "syp",
    "tw": "twd",
    "tj": "tjs",
    "tz": "tzs",
    "th": "thb",
    "tl": "usd",
    "tg": "xof",
    "tk": "nzd",
    "to": "top",
    "tt": "ttd",
    "tn": "tnd",
    "tr": "try",
    "tm": "tmt",
    "tc": "usd",
    "tv": "aud",
    "ug": "ugx",
    "ua": "uah",
    "ae": "aed",
    "gb": "gbp",
    "us": "usd",
    "um": "usd",
    "uy": "uyu",
    "uz": "uzs",
    "vu": "vuv",
    "ve": "vef",
    "vn": "vnd",
    "vg": "usd",
    "wf": "xpf",
    "eh": "mad",
    "ye": "yer",
    "zm": "zmw",
    "zw": "zwl",
};

function guSplitLangDescriptor(descriptor) {
    var parts = descriptor.replace(new RegExp("\\*", 'g'), "*").split("-");
    if (parts.length == 1) {
        return {
            "language": parts[0] == "" ? null : parts[0],
            "country": null,
            "currency": null
        };
    } else if (parts.length == 2) {
        return {
            "language": parts[0] == "" ? null : parts[0],
            "country": parts[1] == "" ? null : parts[1],
            "currency": null
        };
    } else if (parts.length == 3) {
        return {
            "language": parts[0] == "" ? null : parts[0],
            "country": parts[1] == "" ? null : parts[1],
            "currency": parts[2] == "" ? null : parts[2]
        };
    } else {
        // Invalid descriptor. Log the error and keep going by defaulting to en-us
        console.log("guSplitLangDescriptor: Invalid descriptor: " + descriptor);
        return {
            "language": "en",
            "country": "us",
            "currency": "usd"
        }
    }
}

function guMakeLangDescriptor(language, country) {
    return language + "-" + country;
}

function guValidateLanguage(language) {
    if (typeof gu_default_country[language] == "undefined") {
        return "en";
    } else {
        return language;
    }
}

function guValidateCountry(country) {
    if (typeof gu_currencies[country] == "undefined") {
        return "us";
    } else {
        return country;
    }
}

function guValidateCurrency(currency) {
    for (var key in gu_currencies) {
        if (currency == gu_currencies[key]) {
            return gu_currencies[key];
        }
    }
    return "usd";
}

function guSetIntl() {
    // Get the browser's language descriptor and set country if necessary
    var browserLang = (navigator.language || navigator.userLanguage || navigator.browserLanguage || navigator.systemLanguage || 'en-us').toLowerCase();
    if (browserLang == "es-es_tradnl" || browserLang == "es-419" || browserLang == "es-xl") {
        browserLang = "es-la";
    }
    var browserLangParts = guSplitLangDescriptor(browserLang);
    if (browserLangParts.country === null) {
        browserLangParts.country = gu_default_country[browserLangParts.language];
    }
    browserLangParts.language = guValidateLanguage(browserLangParts.language);
    browserLangParts.country = guValidateCountry(browserLangParts.country);
    browserLangParts.currency = gu_currencies[browserLangParts.country];

    // Is there a gulang querystring parameter?
    if (typeof gu_qs["gulang"] == "undefined") {

        // No gulang parameter. Use the browser's language/country/currency.
        gu_language = browserLangParts.language;
        gu_country = browserLangParts.country;
        gu_currency = browserLangParts.currency;

    } else {

        // gulang parameter exists.
        var gulangParts = guSplitLangDescriptor(gu_qs["gulang"].toLowerCase());
        if (gulangParts.language === null || gulangParts.language == "*") {
            gu_language = browserLangParts.language;
        } else {
            gu_language = guValidateLanguage(gulangParts.language);
        }

        if (gulangParts.country === null) {
            if (gu_language == browserLangParts.language) {
                gu_country = browserLangParts.country;
            } else {
                gu_country = gu_default_country[gu_language];
            }
        } else {
            if (gulangParts.country == "*") {
                gu_country = browserLangParts.country;
            } else {
                gu_country = guValidateCountry(gulangParts.country);
            }
        }

        if (gulangParts.currency === null) {
            gu_currency = gu_currencies[gu_country];
        } else {
            if (gulangParts.currency == "*") {
                gu_currency = browserLangParts.currency;
            } else {
                gu_currency = guValidateCurrency(gulangParts.currency);
            }
        }
    }

    if (gu_language == "es" && gu_country == "la") {
        if (browserLang == "es-la") {
            gu_country = "mx";
        } else {
            gu_country = browserLangParts.country;
        }
        //gu_currency = gu_currencies[gu_country];
    }

    gu_languageAndCountry = guMakeLangDescriptor(gu_language, gu_country);

    gu_translationLanguage = (typeof gulangParts != "undefined") ? guMakeLangDescriptor(gulangParts.language, gulangParts.country) : guMakeLangDescriptor(gu_language, gu_country);

    gu_qs["gulanguage"] = gu_language;
    gu_qs["gucountry"] = gu_country;
    gu_qs["gucurrency"] = gu_currency;

    var euCountries = ["at", "be", "bg", "hr", "cy", "cz", "dk", "ee", "fi", "fr", "de", "gr", "hu", "ie", "it", "lv", "lt", "lu", "mt", "nl", "pl", "pt", "ro", "sk", "si", "es", "se", "gb"];
    gu_eu_country = euCountries.indexOf(gu_country) > -1;

    var dollarCurrencies = ["aud", "cad", "nzd", "sgd", "twd", "hkd"];
    gu_dollar_currency = dollarCurrencies.indexOf(gu_currency) > -1;

    var latamCountries = ["mx", "co", "ar", "pe", "ve", "cl", "ce", "gt", "ec", "cu", "bo", "ht", "do", "hn", "py", "pa", "sv", "ni", "cr", "uy", "jm", "tt", "gy", "sr", "bs", "bz", "bb", "lc", "vc", "gd", "ag", "dm", "kn"];
    gu_latam_country = (latamCountries.indexOf(gu_country) > -1);

    console.log("browserLang: " + browserLang + " gulang: " + gu_qs["gulang"] + " gu_language: " + gu_language + " gu_country: " + gu_country + " gu_currency: " + gu_currency + " latAm: " + gu_latam_country);
}

guSetIntl();
// Fetch Remote Data v1.2
// <!-- Fetch Remote Data 1.2 -->
var gu_remote_data = {
    productData: null,
    currencyData: null,
    shippingCountries: null,
    allCountries: null,
    loaded: false,
    callbacks: [],
}

var guRemoteDataRetries = 0;

// Register a callback function that will be invoked when all remote data is loaded
function guRemoteDataRegisterCallback(callback, first) {
    if (typeof first == "undefined") first = false;
    if (gu_remote_data.loaded) {
        callback();
    } else {
        if (first) {
            gu_remote_data.callbacks.unshift(callback);
        } else {
            gu_remote_data.callbacks.push(callback);
        }
    }
}

function guWaitForRemoteData() {
    if (gu_remote_data.productData === null || gu_remote_data.currencyData === null || gu_remote_data.shippingCountries === null) {

        // Remote data hasn't loaded yet; wait for 100ms and check again
        var myVar = setTimeout(guWaitForRemoteData, 100);

    } else {

        // Remote data has loaded!
        gu_remote_data.loaded = true;

        // Run through all the registered callbacks
        for (var i = 0; i < gu_remote_data.callbacks.length; i++) {
            gu_remote_data.callbacks[i]();
        }
    }
}

function gu_ajax_get(url, success) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            try {
                var data = JSON.parse(xmlhttp.responseText);
            } catch (err) {
                console.log('Fetch Remote Data: JSON.parse error. url: ' + url + ' error message: ' + err.message + ' xmlhttp.responseText: ' + xmlhttp.responseText);
            }
            success(data);

        } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
            console.log('Fetch Remote Data: xmlhttp error. url: ' + url + ' xmlhttp.status: ' + xmlhttp.status + ' xmlhttp.responseText: ' + xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

// Fetch the remote data
// gu_ajax_get("https://ntf775zht8.execute-api.us-west-2.amazonaws.com/production/external-data/" + gu_shopify_domain + "/" + gu_language + "/",
//     function(data) {
//         gu_remote_data.productData = data.products;
//         gu_remote_data.currencyData = data.rates;
//         gu_remote_data.shippingCountries = data.shippingCountries;
//         gu_remote_data.allCountries = data.allCountries;
//     }
// );

// Now wait for remote data to load
guWaitForRemoteData();
// <!-- Shopify Prod API V1.9 -->

/* big.js v5.0.3 https://github.com/MikeMcl/big.js/LICENCE */
! function(e) {
    "use strict";

    function r() {
        function e(n) {
            var i = this;
            return i instanceof e ? (n instanceof e ? (i.s = n.s, i.e = n.e, i.c = n.c.slice()) : t(i, n), void(i.constructor = e)) : n === m ? r() : new e(n)
        }
        return e.prototype = v, e.DP = s, e.RM = f, e.NE = h, e.PE = l, e.version = "5.0.2", e
    }

    function t(e, r) {
        var t, n, i;
        if (0 === r && 0 > 1 / r) r = "-0";
        else if (!E.test(r += "")) throw Error(g + "number");
        for (e.s = "-" == r.charAt(0) ? (r = r.slice(1), -1) : 1, (t = r.indexOf(".")) > -1 && (r = r.replace(".", "")), (n = r.search(/e/i)) > 0 ? (0 > t && (t = n), t += +r.slice(n + 1), r = r.substring(0, n)) : 0 > t && (t = r.length), i = r.length, n = 0; i > n && "0" == r.charAt(n);) ++n;
        if (n == i) e.c = [e.e = 0];
        else {
            for (; i > 0 && "0" == r.charAt(--i););
            for (e.e = t - n - 1, e.c = [], t = 0; i >= n;) e.c[t++] = +r.charAt(n++)
        }
        return e
    }

    function n(e, r, t, n) {
        var i = e.c,
            o = e.e + r + 1;
        if (o < i.length) {
            if (1 === t) n = i[o] >= 5;
            else if (2 === t) n = i[o] > 5 || 5 == i[o] && (n || 0 > o || i[o + 1] !== m || 1 & i[o - 1]);
            else if (3 === t) n = n || i[o] !== m || 0 > o;
            else if (n = !1, 0 !== t) throw Error(w);
            if (1 > o) i.length = 1, n ? (e.e = -r, i[0] = 1) : i[0] = e.e = 0;
            else {
                if (i.length = o--, n)
                    for (; ++i[o] > 9;) i[o] = 0, o-- || (++e.e, i.unshift(1));
                for (o = i.length; !i[--o];) i.pop()
            }
        } else if (0 > t || t > 3 || t !== ~~t) throw Error(w);
        return e
    }

    function i(e, r, t, i) {
        var o, s, f = e.constructor,
            u = !e.c[0];
        if (t !== m) {
            if (t !== ~~t || (3 == r) > t || t > c) throw Error(3 == r ? g + "precision" : p);
            for (e = new f(e), t = i - e.e, e.c.length > ++i && n(e, t, f.RM), 2 == r && (i = e.e + t + 1); e.c.length < i;) e.c.push(0)
        }
        if (o = e.e, s = e.c.join(""), t = s.length, 2 != r && (1 == r || 3 == r && o >= i || o <= f.NE || o >= f.PE)) s = s.charAt(0) + (t > 1 ? "." + s.slice(1) : "") + (0 > o ? "e" : "e+") + o;
        else if (0 > o) {
            for (; ++o;) s = "0" + s;
            s = "0." + s
        } else if (o > 0)
            if (++o > t)
                for (o -= t; o--;) s += "0";
            else t > o && (s = s.slice(0, o) + "." + s.slice(o));
        else t > 1 && (s = s.charAt(0) + "." + s.slice(1));
        return e.s < 0 && (!u || 4 == r) ? "-" + s : s
    }
    var o, s = 20,
        f = 1,
        c = 1e6,
        u = 1e6,
        h = -7,
        l = 21,
        a = "[big.js] ",
        g = a + "Invalid ",
        p = g + "decimal places",
        w = g + "rounding mode",
        d = a + "Division by zero",
        v = {},
        m = void 0,
        E = /^-?(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i;
    v.abs = function() {
        var e = new this.constructor(this);
        return e.s = 1, e
    }, v.cmp = function(e) {
        var r, t = this,
            n = t.c,
            i = (e = new t.constructor(e)).c,
            o = t.s,
            s = e.s,
            f = t.e,
            c = e.e;
        if (!n[0] || !i[0]) return n[0] ? o : i[0] ? -s : 0;
        if (o != s) return o;
        if (r = 0 > o, f != c) return f > c ^ r ? 1 : -1;
        for (s = (f = n.length) < (c = i.length) ? f : c, o = -1; ++o < s;)
            if (n[o] != i[o]) return n[o] > i[o] ^ r ? 1 : -1;
        return f == c ? 0 : f > c ^ r ? 1 : -1
    }, v.div = function(e) {
        var r = this,
            t = r.constructor,
            i = r.c,
            o = (e = new t(e)).c,
            s = r.s == e.s ? 1 : -1,
            f = t.DP;
        if (f !== ~~f || 0 > f || f > c) throw Error(p);
        if (!o[0]) throw Error(d);
        if (!i[0]) return new t(0 * s);
        var u, h, l, a, g, w = o.slice(),
            v = u = o.length,
            E = i.length,
            M = i.slice(0, u),
            P = M.length,
            b = e,
            D = b.c = [],
            R = 0,
            A = f + (b.e = r.e - e.e) + 1;
        for (b.s = s, s = 0 > A ? 0 : A, w.unshift(0); P++ < u;) M.push(0);
        do {
            for (l = 0; 10 > l; l++) {
                if (u != (P = M.length)) a = u > P ? 1 : -1;
                else
                    for (g = -1, a = 0; ++g < u;)
                        if (o[g] != M[g]) {
                            a = o[g] > M[g] ? 1 : -1;
                            break
                        } if (!(0 > a)) break;
                for (h = P == u ? o : w; P;) {
                    if (M[--P] < h[P]) {
                        for (g = P; g && !M[--g];) M[g] = 9;
                        --M[g], M[P] += 10
                    }
                    M[P] -= h[P]
                }
                for (; !M[0];) M.shift()
            }
            D[R++] = a ? l : ++l, M[0] && a ? M[P] = i[v] || 0 : M = [i[v]]
        } while ((v++ < E || M[0] !== m) && s--);
        return D[0] || 1 == R || (D.shift(), b.e--), R > A && n(b, f, t.RM, M[0] !== m), b
    }, v.eq = function(e) {
        return !this.cmp(e)
    }, v.gt = function(e) {
        return this.cmp(e) > 0
    }, v.gte = function(e) {
        return this.cmp(e) > -1
    }, v.lt = function(e) {
        return this.cmp(e) < 0
    }, v.lte = function(e) {
        return this.cmp(e) < 1
    }, v.minus = v.sub = function(e) {
        var r, t, n, i, o = this,
            s = o.constructor,
            f = o.s,
            c = (e = new s(e)).s;
        if (f != c) return e.s = -c, o.plus(e);
        var u = o.c.slice(),
            h = o.e,
            l = e.c,
            a = e.e;
        if (!u[0] || !l[0]) return l[0] ? (e.s = -c, e) : new s(u[0] ? o : 0);
        if (f = h - a) {
            for ((i = 0 > f) ? (f = -f, n = u) : (a = h, n = l), n.reverse(), c = f; c--;) n.push(0);
            n.reverse()
        } else
            for (t = ((i = u.length < l.length) ? u : l).length, f = c = 0; t > c; c++)
                if (u[c] != l[c]) {
                    i = u[c] < l[c];
                    break
                } if (i && (n = u, u = l, l = n, e.s = -e.s), (c = (t = l.length) - (r = u.length)) > 0)
            for (; c--;) u[r++] = 0;
        for (c = r; t > f;) {
            if (u[--t] < l[t]) {
                for (r = t; r && !u[--r];) u[r] = 9;
                --u[r], u[t] += 10
            }
            u[t] -= l[t]
        }
        for (; 0 === u[--c];) u.pop();
        for (; 0 === u[0];) u.shift(), --a;
        return u[0] || (e.s = 1, u = [a = 0]), e.c = u, e.e = a, e
    }, v.mod = function(e) {
        var r, t = this,
            n = t.constructor,
            i = t.s,
            o = (e = new n(e)).s;
        if (!e.c[0]) throw Error(d);
        return t.s = e.s = 1, r = 1 == e.cmp(t), t.s = i, e.s = o, r ? new n(t) : (i = n.DP, o = n.RM, n.DP = n.RM = 0, t = t.div(e), n.DP = i, n.RM = o, this.minus(t.times(e)))
    }, v.plus = v.add = function(e) {
        var r, t = this,
            n = t.constructor,
            i = t.s,
            o = (e = new n(e)).s;
        if (i != o) return e.s = -o, t.minus(e);
        var s = t.e,
            f = t.c,
            c = e.e,
            u = e.c;
        if (!f[0] || !u[0]) return u[0] ? e : new n(f[0] ? t : 0 * i);
        if (f = f.slice(), i = s - c) {
            for (i > 0 ? (c = s, r = u) : (i = -i, r = f), r.reverse(); i--;) r.push(0);
            r.reverse()
        }
        for (f.length - u.length < 0 && (r = u, u = f, f = r), i = u.length, o = 0; i; f[i] %= 10) o = (f[--i] = f[i] + u[i] + o) / 10 | 0;
        for (o && (f.unshift(o), ++c), i = f.length; 0 === f[--i];) f.pop();
        return e.c = f, e.e = c, e
    }, v.pow = function(e) {
        var r = this,
            t = new r.constructor(1),
            n = t,
            i = 0 > e;
        if (e !== ~~e || -u > e || e > u) throw Error(g + "exponent");
        for (i && (e = -e); 1 & e && (n = n.times(r)), e >>= 1, e;) r = r.times(r);
        return i ? t.div(n) : n
    }, v.round = function(e, r) {
        var t = this.constructor;
        if (e === m) e = 0;
        else if (e !== ~~e || 0 > e || e > c) throw Error(p);
        return n(new t(this), e, r === m ? t.RM : r)
    }, v.sqrt = function() {
        var e, r, t, i = this,
            o = i.constructor,
            s = i.s,
            f = i.e,
            c = new o(.5);
        if (!i.c[0]) return new o(i);
        if (0 > s) throw Error(a + "No square root");
        s = Math.sqrt(i.toString()), 0 === s || s === 1 / 0 ? (r = i.c.join(""), r.length + f & 1 || (r += "0"), e = new o(Math.sqrt(r).toString()), e.e = ((f + 1) / 2 | 0) - (0 > f || 1 & f)) : e = new o(s.toString()), f = e.e + (o.DP += 4);
        do t = e, e = c.times(t.plus(i.div(t))); while (t.c.slice(0, f).join("") !== e.c.slice(0, f).join(""));
        return n(e, o.DP -= 4, o.RM)
    }, v.times = v.mul = function(e) {
        var r, t = this,
            n = t.constructor,
            i = t.c,
            o = (e = new n(e)).c,
            s = i.length,
            f = o.length,
            c = t.e,
            u = e.e;
        if (e.s = t.s == e.s ? 1 : -1, !i[0] || !o[0]) return new n(0 * e.s);
        for (e.e = c + u, f > s && (r = i, i = o, o = r, u = s, s = f, f = u), r = new Array(u = s + f); u--;) r[u] = 0;
        for (c = f; c--;) {
            for (f = 0, u = s + c; u > c;) f = r[u] + o[c] * i[u - c - 1] + f, r[u--] = f % 10, f = f / 10 | 0;
            r[u] = (r[u] + f) % 10
        }
        for (f ? ++e.e : r.shift(), c = r.length; !r[--c];) r.pop();
        return e.c = r, e
    }, v.toExponential = function(e) {
        return i(this, 1, e, e)
    }, v.toFixed = function(e) {
        return i(this, 2, e, this.e + e)
    }, v.toPrecision = function(e) {
        return i(this, 3, e, e - 1)
    }, v.toString = function() {
        return i(this)
    }, v.valueOf = v.toJSON = function() {
        return i(this, 4)
    }, o = r(), o["default"] = o.Big = o, "function" == typeof define && define.amd ? define(function() {
        return o
    }) : "undefined" != typeof module && module.exports ? module.exports = o : e.Big = o
}(this);

window.gu_products = {
    selectedUnits: [],
    packGuBundle: function(selectedUnits) {
        var packedString = "";
        try {
            for (var i = 0; i < selectedUnits.length; i++) {
                var unitStr = "";
                if (selectedUnits[i].type == "bundle") {
                    unitStr = "bundle:" + selectedUnits[i].id + ";quantity:" + selectedUnits[i].quantity;
                } else {
                    unitStr = selectedUnits[i].id + ":" + selectedUnits[i].quantity;
                }

                if (i != selectedUnits.length - 1) {
                    unitStr += ";"
                }
                packedString += unitStr;
            }
            return packedString;
        } catch (e) {
            console.log(e);
            return "";
        }
    },
    packSelectedUnits: function(selectedUnits) {
        var packedString = "";
        try {
            for (var i = 0; i < selectedUnits.length; i++) {
                var unitStr = selectedUnits[i].name + ";;;" + selectedUnits[i].id + ";;;" + selectedUnits[i].quantity + ";;;" + selectedUnits[i].type + ";;;";
                for (var j = 0; j < selectedUnits[i].lineItems.length; j++) {
                    unitStr += selectedUnits[i].lineItems[j].productId + ";" + selectedUnits[i].lineItems[j].variantName + ";" +
                        selectedUnits[i].lineItems[j].variantId + ";" + selectedUnits[i].lineItems[j].quantity + ";" +
                        selectedUnits[i].lineItems[j].priceInfo.tp + ";" + selectedUnits[i].lineItems[j].priceInfo.rp + ";" +
                        selectedUnits[i].lineItems[j].priceInfo.wp + ";" + selectedUnits[i].lineItems[j].productTitle + ";" +
                        selectedUnits[i].lineItems[j].sku;
                    if (j != selectedUnits[i].lineItems.length - 1) {
                        unitStr += ";;"
                    }
                }
                if (i !== (selectedUnits.length - 1)) {
                    unitStr += ";;;;"
                }
                packedString += unitStr;
            }
            return packedString;
        } catch (e) {
            console.log(e);
            return "";
        }

    },
    unpackSelectedUnits: function(packedString) {
        var selectedUnits = [];
        var packedUnits = packedString.split(";;;;");
        for (var i = 0; i < packedUnits.length; i++) {
            var unit = {};
            var packedUnit = packedUnits[i].split(";;;");
            unit.name = packedUnit[0];
            unit.id = packedUnit[1];
            unit.quantity = packedUnit[2];
            unit.type = packedUnit[3];
            unit.lineItems = [];
            var packedLineItems = packedUnit[4].split(";;");
            for (var j = 0; j < packedLineItems.length; j++) {
                var lineItem = {};
                var packedLI = packedLineItems[j].split(";");
                lineItem.productId = packedLI[0];
                lineItem.variantName = packedLI[1];
                lineItem.variantId = packedLI[2];
                lineItem.quantity = packedLI[3];
                lineItem.priceInfo = {
                    tp: packedLI[4],
                    rp: packedLI[5],
                    wp: packedLI[6]
                };
                lineItem.productTitle = packedLI[7];
                lineItem.sku = packedLI[8];
                unit.lineItems.push(lineItem);
            }
            selectedUnits.push(unit);
        }
        return selectedUnits;
    },
    addSelectedUnit: function(unit) {
        try {
            if (this.isValidUnit(unit)) {
                for (var i = 0; i < unit.lineItems.length; i++) {
                    var variantId = unit.lineItems[i].variantId;
                    var product = this.getParentAndVariant(variantId);
                    var priceInfo = this.getPriceInfoFromProduct(product, variantId);
                    unit.lineItems[i].priceInfo = priceInfo;
                    unit.lineItems[i].productTitle = product.title;
                    for (var j = 0; j < product.variants.length; j++) {
                        if (product.variants[j].id == unit.lineItems[i].variantId) {
                            unit.lineItems[i].sku = product.variants[j].sku;
                            unit.lineItems[i].variantTitle = product.variants[j].title;
                        }
                    }
                }
                var match = -1;
                for (var i = 0; i < this.selectedUnits.length; i++) {
                    if (this.selectedUnits[i].name == unit.name) {
                        match = i;
                        break;
                    }
                }
                if (match != -1) {
                    this.selectedUnits[match] = unit;
                } else {
                    this.selectedUnits.push(unit);
                }
                return 1;
            } else {
                return 0;
            }
        } catch (e) {
            console.log(e);
            return 0;
        }
    },

    //------------------------------------
    // FUNCTION:  Calculate Total Price Info From Products
    // IN: ProductClassNames (array of the class names for products)
    // OUT: Price Info Object { tp, wp, rp, up, dp }
    //      tp - Total Price
    //      wp - Wholesale Price
    //      rp - Retail Price
    //      up - Unit Price
    //      dp - Discount Price
    //------------------------------------
    calculatePriceInfoFromProducts: function(productClassNames, countryCode) {
        var products = [];
        var totalPrice = Big(0);
        var totalRetailPrice = Big(0);
        var totalWholesalePrice = Big(0);
        var productCount = 0;

        for (var i = 0; i < productClassNames.length; i++) {
            try {
                var productId = eval(productClassNames[i]);
            } catch (e) {
                console.log("Shopify Product Info: ERROR: product id: " + productClassNames[i] + " isn't defined in site vars");
                continue;
            }
            var priceInfo = this.getPriceInfoForProduct(productId, countryCode); // Convert class name into product ID with eval()
            totalPrice = totalPrice.add(priceInfo.tp || 0);
            totalRetailPrice = totalRetailPrice.add(priceInfo.rp || 0);
            totalWholesalePrice = totalWholesalePrice.add(priceInfo.wp || 0);
            productCount++;
        }

        // Make sure there is at least one product in the bundle. If not, return null
        if (productCount == 0) {
            return null;
        }

        var unitPrice = totalPrice.div(Big(productCount));
        var discountAmount = totalRetailPrice.sub(totalPrice);

        return {
            tp: totalPrice,
            rp: totalRetailPrice,
            wp: totalWholesalePrice,
            up: unitPrice,
            dp: discountAmount,
        };
    },

    calculatePriceInfoFromFragment: function(fragment, countryCode) {
        var fragmentParts = fragment.split(',');
        var variantids = [];
        for (var i = 0; i < fragmentParts.length; i++) {
            var frag = fragmentParts[i].split(':');
            var variantid = frag[0];
            var qty = frag[1];
            for (var j = 0; j < parseInt(qty, 10) || 0; j++) {
                variantids.push(variantid);
            }
        }
        return this.calculatePriceInfoFromVariants(variantids, countryCode);
    },

    //------------------------------------
    // FUNCTION:  Calculate Total Price Info For Variants
    // IN: VariantIds (array of variant IDS)
    // IN: CurrencyCode
    // OUT: Price Info Object { tp, wp, rp }
    //------------------------------------
    calculatePriceInfoFromVariants: function(variantids, countryCode) {
        var variants = [];
        var totalPrice = Big(0);
        var totalRetailPrice = Big(0);
        var totalWholesalePrice = Big(0);

        for (var i = 0; i < variantids.length; i++) {
            var priceInfo = this.getPriceInfoForVariant(variantids[i], countryCode);
            totalPrice = totalPrice.add(priceInfo.tp || 0);
            totalRetailPrice = totalRetailPrice.add(priceInfo.rp || 0);
            totalWholesalePrice = totalWholesalePrice.add(priceInfo.wp || 0);
        }
        return {
            tp: totalPrice,
            rp: totalRetailPrice,
            wp: totalWholesalePrice,
        };
    },

    //------------------------------------
    // FUNCTION:  Get Price Info For list of Variants
    // IN: VariantIds (array of variantids)
    // IN: CurrencyCode
    // OUT: Price Info Object { tp, wp, rp } - values are Big() objects
    //------------------------------------
    getPriceInfoForVariants: function(variantids, countryCode) {
        var priceInfos = [];
        for (var i = 0; i < variantids.length; i++) {
            var priceInfo = this.getPriceInfoForVariant(variantids[i], countryCode);
            priceInfos.push(priceInfo);
        }
        return priceInfos;
    },

    //------------------------------------
    // FUNCTION:  Get Price Info For Variant
    // IN: VariantId
    // IN: CurrencyCode
    // OUT: Price Info Object { tp, wp, rp } - values are Big() objects
    //------------------------------------
    getPriceInfoForVariant: function(variantid, countryCode) {
        var match = this.getParentAndVariant(variantid);
        return this.getPriceInfoFromProduct(match, countryCode);
    },

    //------------------------------------
    // FUNCTION:  Get Price Info For Product
    // IN: ProductID
    // OUT: Price Info Object { tp, wp, rp } - values are Big() objects
    //------------------------------------
    getPriceInfoForProduct: function(productid, countryCode) {
        var match = this.getProduct(productid);
        return this.getPriceInfoFromProduct(match, countryCode);
    },

    //------------------------------------
    // FUNCTION:  Get Price Info From Product
    // IN: Product (Shopify object)
    // OUT: Price Info Object { tp, wp, rp } - values are Big() objects
    //------------------------------------
    getPriceInfoFromProduct: function(product, variantId) {
        var tagPostfix = "";
        if (typeof variantId !== "undefined" && variantId !== null && variantId !== "") {
            tagPostfix = '_' + variantId;
        }
        var rptag = "rp" + tagPostfix;
        var wptag = "wp" + tagPostfix;
        var priceInfo = {};

        // check if the variant has a corresponding variant price
        // if not, use default pricing
        if (this.searchTags(product.tags, wptag) === null){
            priceInfo['wp'] = Big(this.searchTags(product.tags, 'wp'));
        } else {
            priceInfo['wp'] = Big(this.searchTags(product.tags, wptag));
        }

        if (this.searchTags(product.tags, rptag) === null){
            priceInfo['rp'] = Big(this.searchTags(product.tags, 'rp'));
        } else {
            priceInfo['rp'] = Big(this.searchTags(product.tags, rptag));
        }

        priceInfo['tp'] = Big(product.variants[0].price);

        return priceInfo;
    },


    //-----------------------------------
    // FUNCTION:  Get Product
    // IN: ProductID
    // OUT: Product object (shopify)
    //------------------------------------
    getProduct: function(productid) {
        if (gu_remote_data.productData == null) {
            return null;
        } else {
            for (var i = 0; i < gu_remote_data.productData.length; i++) {
                if (gu_remote_data.productData[i].id == productid) {
                    var matchedParent = gu_remote_data.productData[i];
                    return matchedParent;
                }
            }
            console.log("Shopify Product Info: ERROR: Product ID: " + productid + "not found in Shopify store");
            gulog.fireAndLogEvent("CODE RED: Shopify Product Error. " + productid + " not found in Shopify store " + gu_shopify_domain, "error");
            gulog.error("CODE RED: Shopify Product Error. " + productid + " not found in Shopify store " + gu_shopify_domain, null, {severity: "critical"});
            return null;
        }
    },

    //---------------------------------------------
    // FUNCTION:  Get Variant By Variant Title
    // IN: ProductID
    // IN: Variant Title
    // OUT: Product Variant object (shopify)
    //---------------------------------------------
    getVariantByTitle: function(productid, title) {
        if (gu_remote_data.productData == null) {
            return null;
        } else {
            var product = this.getProduct(productid);
            for (var j = 0; j < product.variants.length; j++) {
                if (product.variants[j].title == title) {
                    return product.variants[j];
                }
            }
            return null;
        }
    },

    getColorVariant: function(productid, color) {
        if (gu_remote_data.productData == null) {
            return null;
        } else {
            var product = this.getProduct(productid);
            for (var j = 0; j < product.variants.length; j++) {
                if (product.variants[j].option1 == color || product.variants[j].option2 == color || product.variants[j].option3 == color) {
                    return product.variants[j];
                }
            }
            return null;
        }
    },

    getFirstVariant: function(productid) {
        if (gu_remote_data.productData == null) {
            return null;
        } else {
            var product = this.getProduct(productid);
            for (var j = 0; j < product.variants.length; j++) {
                if (product.variants[j].position == 1) {
                    return product.variants[j];
                }
            }
            return null;
        }
    },

    getParentAndVariant: function(variantid) {
        if (gu_remote_data.productData == null) {
            return null;
        } else {
            for (var i = 0; i < gu_remote_data.productData.length; i++) {
                var matchedParent = gu_remote_data.productData[i];
                for (var j = 0; j < gu_remote_data.productData[i].variants.length; j++) {
                    if (gu_remote_data.productData[i].variants[j].id === variantid) {
                        var matchedVariant = gu_remote_data.productData[i].variants[j];
                        //matchedParent.variants = [ matchedVariant ];
                        return matchedParent;
                    }
                }
            }
            return null;
        }
    },

    getVariant: function(variantid) {
        if (gu_remote_data.productData == null) {
            return null;
        } else {
            for (var i = 0; i < gu_remote_data.productData.length; i++) {
                for (var j = 0; j < gu_remote_data.productData[i].variants.length; j++) {
                    if (gu_remote_data.productData[i].variants[j].id == variantid) {
                        return gu_remote_data.productData[i].variants[j];
                    }
                }
            }
            return null;
        }
    },

    isValidUnit: function(unit) {
        if (typeof unit.name == "undefined" || unit.name == null || unit.name.length === 0) {
            return false;
        }
        if (typeof unit.id == "undefined" || unit.id == null || unit.id.length === 0) {
            return false;
        }
        if (typeof unit.lineItems == "undefined" || unit.lineItems == null || unit.lineItems.length === 0) {
            return false;
        }
        for (var i = 0; i < unit.lineItems.length; i++) {
            if (!this.isValidLineItem(unit.lineItems[i])) {
                return false;
            }
        }
        return true;
    },

    isValidLineItem: function(lineItem) {
        if (typeof lineItem.productId == "undefined" || lineItem.productId == null || lineItem.productId <= 0) {
            return false;
        }
        if (typeof lineItem.variantName == "undefined" || lineItem.variantName == null || lineItem.variantName.length == 0) {
            return false;
        }
        if (typeof lineItem.quantity == "undefined" || lineItem.quantity == null || lineItem.quantity <= 0) {
            return false;
        }
        return true;
    },

    // Search tags are in the format key: value
    // ex.  ["wp: 5.99", "rp: 6.99"]
    // searchParam format finds first tag with matching key
    // ex searchTags( ["wp: 5.99", "rp: 6.99"], "wp")
    // returns "5.99"
    // Error returns:
    //   null - Tag not found
    //   "" - No value
    searchTags: function(tags, searchParam) {
        for (var i = 0; i < tags.length; i++) {
            try {
                var tag = tags[i].toLowerCase().replace(/\s/g, ''); // Remove whitespace
                if (tag.startsWith(searchParam.toLowerCase() + ":")) {
                    var valuePos = tag.indexOf(":") + 1;
                    if (tag.length == valuePos) { // No value after the tag? Return empty string
                        return "";
                    }
                    return tag.slice(valuePos);
                }
            } catch (e) {}
        }
        return null; // Tag not found? Return null"
    }
};

String.prototype.startsWith = function(str) {
    return this.indexOf(str) === 0;
};


function urlContainsPath(string) {
    var urlSuffix = window.location.pathname.split('/')[1];
    return string === urlSuffix.substring(0, urlSuffix.indexOf('-'));
}
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
if (typeof (gu_qs_to_str) != "function") {
    window.gu_qs_to_str = function(){return ""}
}


function gu_fire_event(eventName, action, identifier, propertyName, propertyValue) {
    var msg = "gu_fire_event fired prior to function def";
    gulog.error("gu_fire_event fired prior to opentag load.", null, { eventName: eventName })
}
// Direct To Checkout V2.3
var hasOffer = urlContainsPath('offer');
var isMobile = urlContainsPath('mobile');

function directToCheckout() {
    for (var i = 0; i <= 50; i++) {
        var section = document.getElementById("s" + i);
        if (section !== null) {
            section.style.display = "none";
        }
    }
}
// var gu_atf = gu_atf_default;




function showAboveTheFold() {
    for (var i = 3; i <= 50; i++) {
        if (i !== 7) {
            var section = document.getElementById("s" + i);
            if (section !== null) {
                section.style.display = "none";
            }
        }

        var ppAtf = document.getElementById('pp-atf');
        if (typeof(ppAtf) != 'undefined' && ppAtf != null) {
            // exists
            document.getElementById('pp-atf').classList.remove('hidden');
            document.getElementById('pp-atf').style.display = 'block';
        }
        var lpAtf = document.getElementById('lp-atf');
        if (typeof(lpAtf) != 'undefined' && lpAtf != null) {
            // exists
            document.getElementById('lp-atf').classList.add('hidden');
        }

        //find and hide the faq's in the footer on mobile
        var footerfaqs = document.getElementById('faq-footer');
        if (typeof(footerfaqs) != 'undefined' && footerfaqs != null) {
            // exists
            document.getElementById('faq-footer').classList.remove('hidden');
        }
    }
}


if (gu_qs['guatf'] == '0' || gu_qs['guatf'] == '1') {
    gu_atf = gu_qs['guatf'];
}

if(hasOffer || isMobile) {
    if (gu_qs['gudtc'] === '1') {
        directToCheckout();
    } else if (gu_atf === 1) {
        showAboveTheFold();
    }
}


// guGallery v3.0

function PrepGuGallery() {
    // <!--guGallery v3.0 -->
    var galleries = document.getElementsByClassName('guGallery');

    //id gallery
    for (var i = 0; i < galleries.length; i++) {
        var gallery = galleries[i];
        var newID = "guGallery-" + (i + 1) + "-thumbnails";
        galleries[i].setAttribute('id', newID);
        var items = gallery.getElementsByTagName('li');

        //id images in galleries
        for (var j = 0; j < items.length; j++) {
            var newId = "guGallery-" + (i+1) + '-thumbnail-' + (j+1);
            var curImg = items[j].getElementsByTagName('img');
            if (typeof curImg != 'undefined')
            {
                curImg[0].setAttribute('id', newId);
            }
            else  {
                console.log("Error in image gallery " + (i + 1) + ". Empty image slot in position " + (j + 1) + ".");
            }
        }
    }
}

function RunGuGallery() {
    var galleries = document.getElementsByClassName('guGallery');
    for (var i = 0; i < galleries.length; i++) {
        BuildGalleries(galleries[i], (i + 1));
    }
}

function BuildGalleries(gallery, num) {
    jQuery(function () {

        var curGallery = "guGallery-" + num;

        //create wrapper
        jQuery(gallery).before('<div id="' + curGallery + '"></div>');

        jQuery("#" + curGallery).append('<div id="' + curGallery + '-wrapperContainer" class="wrapperContainer"></div>');

        //create image wrapper
        jQuery("#" + curGallery +" #" + curGallery + "-wrapperContainer").append('<div id="' + curGallery + '-imageWrapper" class="imageWrapper"><img class="vc_single_image-img " src=""></div>');
        //jQuery("#"+ curGallery +"-imageWrapper").hide();

        //create video wrappers
        //youtube
        jQuery("#" + curGallery +" #" + curGallery + "-wrapperContainer").append('<div id="'+ curGallery +'-youtubeVideoWrapper" class="galleryVideoWrapper videoWrapper"><iframe width="560" height="308" src="" frameborder="0" allowfullscreen></iframe></div>');
        jQuery("#"+ curGallery +"-youtubevideoWrapper").hide();

        //wistia
        jQuery("#" + curGallery +" #" + curGallery + "-wrapperContainer").append('<div id="'+ curGallery +'-wistiaVideoWrapper" class="galleryVideoWrapper videoWrapper"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div id="'+ curGallery +'-wistiaPlaceholder" class="" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img id="'+ curGallery +'-wistiaThumbnail" src="" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" onload="this.parentNode.style.opacity=1;" /></div></div></div></div></div>');


        //sprout player
        jQuery("#" + curGallery +" #" + curGallery + "-wrapperContainer").append('<div id="'+ curGallery +'-sproutVideoWrapper" class="galleryVideoWrapper videoWrapper"></div>');

        //hide these wrappers
        jQuery("#" + curGallery).find('.imageWrapper').hide();
        jQuery("#" + curGallery).find('.galleryVideoWrapper').hide();


        //add in scripts
        var vidIDscript = document.createElement( "script" );
        vidIDscript.type = "text/javascript";
        vidIDscript.id = curGallery +"-wistiaIdScript";
        jQuery('#' + curGallery +'-wistiaVideoWrapper').append(vidIDscript);


        var defaultScript = document.createElement( "script" );
        defaultScript.type = "text/javascript";
        defaultScript.src = "https://fast.wistia.com/assets/external/E-v1.js";
        jQuery('#' + curGallery +'-wistiaVideoWrapper').append(defaultScript);

        //add in Sprout scripts
        var sproutScript = document.createElement( "script" );
        sproutScript.type = "text/javascript";
        sproutScript.src = "https://c.sproutvideo.com/player_api.js";
        jQuery('#' + curGallery +'-sproutVideoWrapper').append(sproutScript);

        //add thumbnail gallery into wrapper
        jQuery("#" + curGallery).append(gallery);

        var videoType = '';

        //run through thumbnails
        jQuery("#" + curGallery + "-thumbnails li").each(function (index) {

            //run through all images to create giddyBox divs if it's not a vid thumb
            // NOT JUST YOUTUBE, ALL VIDS!
            if (jQuery(this).find('img').attr('alt').indexOf("youtube") <= 0 || jQuery(this).find('img').attr('alt').indexOf("wistia") <= 0 || jQuery(this).find('img').attr('alt').indexOf("sproutvideo") <= 0) {

                //trim the src to use as giddyBox id
                var parts = jQuery(this).find('img').attr('src').split("/");
                var result = parts.pop();
                result = result.replace(/[|&;$%@"<>()+,]/g, "");
                var trimmedSrc = result.slice(0, result.length - 12);

                //get full src of image to place inside of giddybox
                var ogSrc = jQuery(this).find('img').attr('src');
                var lrgSrc = ogSrc.slice(0, ogSrc.length) + ogSrc.slice(ogSrc.length, ogSrc.length);

                //generate giddybox div
                jQuery("#" + curGallery + "-thumbnails").after('<div class="hidden"><div id="' + trimmedSrc + '"><img style="max-width:75vw; max-height: 90vh;" src="' + lrgSrc + '" /></img></div></div>');

            }

            //find first to display large
            if (index === 0) {
                //add border to show it is selected
                jQuery(this).find('img').addClass('selectedThumb');

                //check for autoplay in qs
                canAutoplay = 0;
                if (gu_qs['autoplay'] == '0' || gu_qs['autoplay'] == '1') {
                    canAutoplay = gu_qs['autoplay'];
                }
                else if (gu_qs['guautoplay'] == '0' || gu_qs['guautoplay'] == '1') {
                    canAutoplay = gu_qs['guautoplay'];
                }

                //check first thumb for embedded video links
                if (jQuery(this).find('img').attr('alt').indexOf("youtube") >= 0) {

                    videoType = 'youtube';

                    //first thumb is vid, load video
                    var url = jQuery(this).find('img').attr('alt') + "?";

                    //check if default video is set to auto play
                    if (canAutoplay == '1') {
                        url = url + "autoplay=1&";
                    }

                    LoadVideo(url, videoType);
                    jQuery("#"+ curGallery +"-youtubeVideoWrapper").fadeIn("fast");

                }
                else if (jQuery(this).find('img').attr('alt').indexOf("wistia") >= 0) {
                    videoType = 'wistia';

                    //first thumb is vid, load video
                    var url = jQuery(this).find('img').attr('alt');

                    LoadVideo(url, videoType, canAutoplay);
                    jQuery("#"+ curGallery +"-wistiaVideoWrapper").fadeIn("fast");

                }
                else if (jQuery(this).find('img').attr('alt').indexOf("sproutvideo") >= 0) {
                    videoType = 'sprout';

                    //first thumb is vid, load video
                    var url = jQuery(this).find('img').attr('alt');

                    LoadVideo(url, videoType, canAutoplay);
                    jQuery("#"+ curGallery +"-sproutVideoWrapper").fadeIn("fast");

                }

                //there is no video player attached, load the image
                else {
                    var src = jQuery(this).find('img').attr('src');

                    LoadImage(src);
                    jQuery("#" + curGallery + "-imageWrapper").fadeIn("fast");
                }
            }

            //check all for vids to add play button
            if (jQuery(this).find('img').attr('alt').indexOf("youtube") >= 0 || jQuery(this).find('img').attr('alt').indexOf("wistia") >= 0 || jQuery(this).find('img').attr('alt').indexOf("sproutvideo") >= 0) {
                //thumb has youtube alt
                //add play button
                //make sure there's no 'noarrow' cmd
                if (jQuery(this).find('img').attr('alt').indexOf("noarrow") == -1)
                {
                    jQuery(this).prepend('<div style="opacity:0;" class="playButton"><img src="/wp-content/uploads/Play-Button.png" /></img></div>');
                }

                //jQuery("#"+currentSection+ "  .playButton").clone().prependTo(jQuery(this)).show().removeClass('hidden');
            }
            //make all playbuttons visible now that they are in their proper location
            setTimeout(function () {
                jQuery('.playButton').animate({opacity: 1},400);
            }, 500);

        });

        //thumbnail click function
        jQuery("#" + curGallery + "-thumbnails img").on('click', function () {

            //remove selected class from all thumbs
            jQuery("#" + curGallery + "-thumbnails li img").each(function (index) {
                jQuery(this).removeClass('selectedThumb');
            });

            //add border to selected thumb
            jQuery(this).addClass('selectedThumb');

            //capture id for event tracking
            var thumbID = jQuery(this).attr('id');

            //check for youtube alt tag video src
            if (jQuery(this).attr('alt').indexOf('youtube') >= 0) {

                videoType = 'youtube';

                //there's an alt tag. Use for video embed
                var url = jQuery(this).attr('alt');

                //add autoplay since it's being clicked on
                url = url + '?autoplay=1&amp;';

                LoadVideo(url, videoType);

                //throw event of item clicked
                gu_fire_event('GalleryChanged', 'ViewedVideo', thumbID);

            }
            //check for wistia alt tag video src
            else if (jQuery(this).attr('alt').indexOf('wistia') >= 0) {
                videoType = 'wistia';

                //there's an alt tag. Use for video embed
                var url = jQuery(this).attr('alt');

                //add autoplay since it's being clicked on
                LoadVideo(url, videoType, 1);

                //throw event of item clicked
                gu_fire_event('GalleryChanged', 'ViewedVideo', thumbID);
            }
            //check for sprout alt tag video src
            else if (jQuery(this).attr('alt').indexOf('sproutvideo') >= 0) {
                videoType = 'sprout';

                //there's an alt tag. Use for video embed
                var url = jQuery(this).attr('alt');

                //add autoplay since it's being clicked on
                LoadVideo(url, videoType, 1);

                //throw event of item clicked
                gu_fire_event('GalleryChanged', 'ViewedVideo', thumbID);
            }
            else {

                //no valid video alt tag, replace main image with src of thumbnail
                var src = jQuery(this).attr('src');

                LoadImage(src);

                //throw event of item clicked
                gu_fire_event('GalleryChanged', 'ViewedImage', thumbID);

            }
        });

        /*//main img click function*/
        /*jQuery("#"+ curGallery +"-imageWrapper img").on('click',function() {*/

        /*    //clicked main image, launch proper giddybox*/
        /*    var srcParts = jQuery(this).attr('src').split("/");*/
        /*    var endPart = srcParts.pop();*/
        /*    //strip extension off*/
        /*    endPart = endPart.slice(0, endPart.length-4);*/
        /*    //strip out illegal chars*/
        /*    endPart = endPart.replace(/[|&;$%@"<>()+,]/g, "");*/

        /*    giddybox('#'+endPart+'');*/

        /*});*/

        function LoadVideo(vidURL, videoType, auto)
        {

            if (videoType === 'youtube') {
                //get youtube vid id
                var parts = vidURL.split('v=');
                var id = parts[parts.length - 1];

                var newId = "https://www.youtube.com/embed/" + id + "enablejsapi=1&amp;rel=0&amp;controls=1&amp;showinfo=0&amp";

                //set vid id
                jQuery("#"+ curGallery +"-youtubeVideoWrapper iframe").attr('src', newId);

                //if sprout id playing, pause it
                if(jQuery("#"+ curGallery +"-sproutVideoWrapper").is(':visible')) {
                    //remove the preroll, we won't need it again
                    jQuery("#" + curGallery + "-sproutVideoWrapper .sproutvideo-player.preroll").remove();

                    //pause the sprout vid and hide it
                    //get iframe src
                    var src = jQuery("#" + curGallery + "-sproutVideoWrapper .sproutvideo-player.main").attr('src');
                    var id = getSproutVidId(src);

                    var video = new SV.Player({videoId: id});
                    video.pause();
                }

                //make sure image and wistia vid is hidden and yt is shown
                jQuery("#"+ curGallery +"-imageWrapper, #"+ curGallery +" .galleryVideoWrapper").fadeOut("fast").promise().done(function() {
                    // Animation complete, fade in vid
                    jQuery("#"+ curGallery +"-youtubeVideoWrapper").fadeIn("fast");
                });

            }
            else if (videoType === 'wistia'){
                //get wistia vid id
                var parts = vidURL.split('/medias/');
                //take the last entry that will have the id in it and strip out and possible 'noarrow' text HEY HERE
                parts = parts[parts.length - 1].split(' ');
                var id = parts[0];

                //var newId = "https://fast.wistia.com/embed/medias/"+id+".jsonp";

                //set vid id
                //jQuery("#"+ curGallery +"-wistiaIdScript").attr('src', newId);
                jQuery("#"+ curGallery +"-wistiaPlaceholder").removeClass();
                jQuery("#"+ curGallery +"-wistiaPlaceholder").addClass('wistia_embed wistia_async_'+id+' videoFoam=true wistia_embed_initialized');
                jQuery("#"+ curGallery +"-wistiaThumbnail").attr('src', 'https://fast.wistia.com/embed/medias/'+id+'/swatch');

                //make sure image and YT vid is hidden and wistia is shown
                jQuery("#"+ curGallery +"-imageWrapper, #"+ curGallery +" .galleryVideoWrapper").fadeOut("fast").promise().done(function() {

                    // Animation complete, fade in vid
                    jQuery("#"+ curGallery +"-wistiaVideoWrapper").fadeIn("fast");
                });

                //if YT is playing, pause it
                if(jQuery("#"+ curGallery +"-youtubeVideoWrapper").is(':visible'))
                {
                    //stop video
                    jQuery("#"+ curGallery +"-youtubeVideoWrapper iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                    //jQuery("#"+curSection+ "  .galleryVideoWrapper iframe").attr('src', '');
                }

                //if sprout id playing, pause it
                if(jQuery("#"+ curGallery +"-sproutVideoWrapper").is(':visible')) {
                    //remove the preroll, we won't need it again
                    jQuery("#" + curGallery + "-sproutVideoWrapper .sproutvideo-player.preroll").remove();

                    //pause the sprout vid and hide it
                    //get iframe src
                    var src = jQuery("#" + curGallery + "-sproutVideoWrapper .sproutvideo-player.main").attr('src');
                    var id = getSproutVidId(src);

                    var video = new SV.Player({videoId: id});
                    video.pause();
                }

                if (typeof window[curGallery+"wistiaPlayer"] == 'undefined')
                {
                    window._wq = window._wq || [];
                    _wq.push({ id: id, onReady: function(video) {
                            window[curGallery+"wistiaPlayer"] = video;

                            if (auto == 1) {
                                video.play();
                            }
                        }});
                }
                else {

                    window[curGallery+"wistiaPlayer"].replaceWith(id);

                    if (auto == 1) {
                        window[curGallery+"wistiaPlayer"].play();
                    }
                }

            }
            else if (videoType === 'sprout') {

                var canAutoplay = false;
                //convert auto to bool
                if (auto == 1) {
                    canAutoplay = true;
                }


                //if YT is playing, pause it
                if(jQuery("#"+ curGallery +"-youtubeVideoWrapper").is(':visible'))
                {
                    //stop video
                    jQuery("#"+ curGallery +"-youtubeVideoWrapper iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                    //jQuery("#"+curSection+ "  .galleryVideoWrapper iframe").attr('src', '');
                }

                //separate the urls by preroll and main
                var theUrls = vidURL.split(',');

                var baseVidUrl = "https://videos.sproutvideo.com/embed/$$$";

                //no preroll by default
                var hasPreroll = false;

                //check for an additional preroll vid
                if (theUrls.length > 1)
                {
                    var prerollUrl = theUrls[0];
                    var mainUrl = theUrls[1];
                }
                else
                {
                    var mainUrl = theUrls[0];
                }

                if (typeof prerollUrl != "undefined")
                {
                    //only add in the preroll if we don't want to auto play
                    if (typeof auto == "undefined" || auto === 0)
                    {
                        var preID = prerollUrl.split('/embed/')[1];

                        prerollUrl = baseVidUrl.replace('$$$', preID);

                        var separator = (prerollUrl.indexOf('?') >= 0) ? '&' : '?';
                        var defaultPrerollParams = 'transparent=true&background=true&autoPlay=true&loop=true';

                        prerollUrl = prerollUrl + separator + defaultPrerollParams;

                        var preVid = '<iframe class="sproutvideo-player preroll" src="'+ prerollUrl +'" width="560" height="308" frameborder="0" allowfullscreen="" style="position: absolute; left: 50%;"  id="sproutPreroll"></iframe>';

                        jQuery("#"+ curGallery +"-sproutVideoWrapper").append(preVid);

                        // if there's preroll we need to make the main vid transparent
                        hasPreroll = true;
                    }
                }

                // sep out vid id
                var identifier = mainUrl.split('/embed/')[1];

                //swap id in
                mainUrl = baseVidUrl.replace('$$$', identifier);

                if (hasPreroll)
                {
                    var separator = (mainUrl.indexOf('?') >= 0) ? '&' : '?';
                    var defaultMainParams = 'transparent=true';
                    mainUrl = mainUrl + separator + defaultMainParams;
                }

                var vidId = identifier.split('/')[0];

                //determine if the desired video is the one already in the player
                var src = jQuery("#"+ curGallery +"-sproutVideoWrapper .sproutvideo-player.main").attr('src');
                if (typeof src != "undefined") {
                    var curID = getSproutVidId(src);
                }

                if (typeof curID != "undefined" && vidId === curID)
                {
                    //video we're trying to play is the one that's already in the player, just hit play on it and that's it
                    var video = new SV.Player({videoId: vidId});
                    video.play();

                }
                else
                {
                    //there is either no video in there yet, or it's a new one

                    var mainVid = '<iframe class="sproutvideo-player main" src="' + mainUrl + '" width="560" height="308" frameborder="0" allowfullscreen="" style="position: absolute; left: 50%;" id="sproutMainVid"></iframe>';

                    //pop vid players in
                    jQuery("#"+ curGallery +"-sproutVideoWrapper").append(mainVid);

                    //use api to play video if autoplay enabled
                    if (canAutoplay)
                    {
                        var video = new SV.Player({videoId: vidId});

                        video.bind('ready', function(e) {
                            video.play();
                        });
                    }

                }


                //make sure image and wistia vid is hidden and sprout is shown
                jQuery("#"+ curGallery +"-imageWrapper, #"+ curGallery +" .galleryVideoWrapper").fadeOut("fast").promise().done(function() {
                    // Animation complete, fade in vid
                    jQuery("#"+ curGallery +"-sproutVideoWrapper").fadeIn("fast");
                });
            }
        }

        function LoadImage(imgSrc)
        {
            //a little bit of magic to strip the generated thumbnail dimensions off the images for larger viewing
            var newSrc = imgSrc;



            jQuery("#" + curGallery + "-imageWrapper img").attr('src', newSrc);

            //if the trimmed image src is bogus and doesn't work just use the original src as a backup
            jQuery("#" + curGallery + "-imageWrapper img").error(function(){
                jQuery("#" + curGallery + "-imageWrapper img").attr('src', imgSrc);
            });

            //if YT is playing, pause it
            if(jQuery("#"+ curGallery +"-youtubeVideoWrapper").is(':visible'))
            {
                //stop video
                jQuery("#"+ curGallery +"-youtubeVideoWrapper iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                //jQuery("#"+curSection+ "  .galleryVideoWrapper iframe").attr('src', '');
            }

            //make sure video is hidden is visible and image is shown
            if (jQuery("#"+ curGallery +"-youtubeVideoWrapper").is(':visible')) {
                //make sure video is hidden and image is shown
                jQuery("#"+ curGallery +"-youtubeVideoWrapper").fadeOut( "fast", function() {

                    // Animation complete, fade in image
                    jQuery("#"+ curGallery +"-imageWrapper").fadeIn("fast");
                });
            }
            else if (jQuery("#"+ curGallery +"-wistiaVideoWrapper").is(':visible')) {
                //make sure video is hidden and image is shown
                jQuery("#"+ curGallery +"-wistiaVideoWrapper").fadeOut( "fast", function() {

                    // Animation complete, fade in image
                    jQuery("#" + curGallery + "-imageWrapper").fadeIn("fast");
                });
            }
            else if (jQuery("#"+ curGallery +"-sproutVideoWrapper").is(':visible')) {

                //empty wrapper
                //jQuery("#"+ curGallery +"-sproutVideoWrapper").empty();

                //remove the preroll, we won't need it again
                jQuery("#"+ curGallery +"-sproutVideoWrapper .sproutvideo-player.preroll").remove();

                //pause the sprout vid and hide it
                //get iframe src
                var src = jQuery("#"+ curGallery +"-sproutVideoWrapper .sproutvideo-player.main").attr('src');
                var id = getSproutVidId(src);

                var video = new SV.Player({videoId: id});
                video.pause();



                //make sure video is hidden and image is shown
                jQuery("#"+ curGallery +"-sproutVideoWrapper").fadeOut( "fast", function() {

                    // Animation complete, fade in image
                    jQuery("#" + curGallery + "-imageWrapper").fadeIn("fast");
                });
            }


        }

    });

    function getSproutVidId (src)
    {
        var id = src.split('https://videos.sproutvideo.com/embed/')[1].split('/')[0];
        return id;
    }
}
// <!-- Content Bundles 5.2 -->
// <!-- International currency display polyfill. Only actually installed if browser doesn't support Intl -->
//
// Content Substitution (Key/Value pairs object)
//
// --- Keys ---
// A Key selects one or more HTML nodes in the page. There are two types of Keys:
//   Default - the Key contains a string that exactly matches the contents of one or more HTML Nodes in the DOM.
//             The Key can contain HTML tags, and can contain the characters &, <, > (in other words, do not use HTML Entities).
//             If the Key contains HTML tags with attributes, the attribute values must be enclosed in single quotes (ie. <span style='font-size: 16px').
//             Note: The only character not permitted in a Default key is the double quote character ("). However, â€œ and â€ are permitted, which is what must be used in page content.
//
//   Id - if the key starts with ! (ie !myiframe), then it is interpreted as an HTML id, and matches the node with that id. HTML ids must be unique within a page.
//
// --- Values ---
// A Value specifies how a node's content or attributes are changed. There are two categories of values: Simple and Complex.
//
// -- Simple Value - Assignment. The Simple Assignment Value replaces the contents of the node matched by the key. The Value is and HTML string that can contain any character except double quote ("), just like
//    Default Key.
//    Example:
//   "key" : "Setting the node's contents to this string. <span class='firstClass secondClass'>Here's some examples</span> of special characters that are fine to use: (AppleÂ®) & (Microsoftâ„¢), !@#$%^*+_-.<>?",
//
// -- Complex Values. Complex values provide access to HTML attributes, allow navigating further below the HTML node found by the key, and provide more sophisticated means of content substitution.
//    The syntax for Complex Values is to enclose the value between two tilde characters, ie: "~complex value statement~". Multiple complex value statements can be specified, which are executed
//    sequentially, ie: "~complex value statement1~, ~complex value statement2~,~complex value statement3~".
//
//    HTML node naviation is specified via an HTML tag path, of the for: tag1-tag2-tag3, where each tag is an HTML tag. For example: p-span-iframe. You can also specify a count in from of a tag. For example, to
//    access an iframe inside of the third list element: ul-3li-iframe. The tags do not have to be direct descendents. In the previous example, there could have been additional elements betwee the third list
//    element and the iframe. The HTML node navigation is optional. If present, it is always immediately after the first ~.
//
//    Complex values also have a variety of Directives, which specify the action to be taken. Here are all the Directives:
//
//      - Assignment Directive. This Directive is specified by an equal sign (=) after the optional navigation path. The remainer of the Directive is an HTML string, just like the Simple Assignment Value, for example:
//         "key" : "~=<span style='font-weight:900'>Here's some examples</span> of special characters: !@$~",
//
//         Here's that same example with an HTML tag path:
//         "key" : "~p-ul-4li=<span class='firstClass secondClass'>Here's some examples</span> of special characters: !@$~",
//
//      - Replace Directive. This Directive allows you to replace just a portion of the node's content with another HTML string. This Directive is identified by 3 vertical bars (|) that contain the search text and
//        the replacement text. For example:
//         "key" : "~p-ul-4li|replace this text|with that text|~",
//         "key" : "~|replace this text|with that text|~",
//
//        The replacement text can be empty, which deletes the search text (essentially, replacing it with nothing).
//
//      - Attribute Directive. Enables you to create, edit and delete HTML attributes. The Attribute Directive is identified by an at sign (@), followed by the name of the attribute.
//        There are two variations Attribute Directive which come after the attribute name - assignment (=) and replace 3 vertical bars(|). These work exactly the same as the previous assignment and replace operations.
//        Here's some examples of the Attribute Assignment Directive:
//         "key" : "~iframe@src=https://domain.io/youtubevideo.mp4~",
//         "key" : "~@style=padding: 15px 20px !important; width: calc(100% - 60px); font-size: 22px;~",
//
//        And the Attribute Replace Directive:
//         "key" : "~iframe@src|replace this text|with that text|",
//         "key" : "~@style|padding-left:20px;|padding-left:10px|~",
//
//

// Content objects, in priority order
var guActiveContentArray = [];

// Price elements to fix up when remote data loads
var guPriceFixUpElements = [];

//An incrementing counter used to give each fixup element a unique id
var guFixUpIdCounter = 0;

// Convert a string, a number or a Big decimal objectg into localized currency
function guDisplayCurrency(value) {
    // Is value a string or a number? Convert it to a Big decimal. Otherwise it already is a big decimal.
    if (typeof value == "string" || typeof value == "number") {
        value = Big(value);
    }

    if (gu_currency != "usd") {
        value = value.mul(Big("1.03")).mul(Big(gu_remote_data.currencyData[gu_currency.toUpperCase()]));
    }

    // Make sure Intl is defined by putting it in a try-catch block. We are using a polyfill for browsers that don't natively support Intl
    // but that polyfill doesn't work for every browser. No Intl? Display it as an English USD value.
    try {
        var returnValue;
        if (value.mod(1).cmp(Big(0)) == 0) {
            returnValue = new Intl.NumberFormat(gu_languageAndCountry, { style: 'currency', currency: gu_currency, minimumFractionDigits: '0' }).format(value.toString());
        } else {
            returnValue = new Intl.NumberFormat(gu_languageAndCountry, { style: 'currency', currency: gu_currency }).format(value.toString());
        }

        if (gu_dollar_currency || gu_latam_country ) {
            var three_letter_currency = gu_currency.toUpperCase();
            var two_letter_currency = three_letter_currency.substring(0, 1);
            if ((returnValue.indexOf(three_letter_currency) == -1) && (returnValue.indexOf(two_letter_currency) == -1)) {
                returnValue += " " + three_letter_currency;
            }
        }

        return returnValue;
    } catch (e) {
        return "$" + value.toString();
    }
}

function guGetVCRowId(element) {
    if (element == document.body) {
        return "";
    }
    if (element.tagName == "DIV" && element.className.indexOf("vc_row") != -1 && element.className.indexOf("vc_inner") == -1) {
        return element.id;
    } else {
        return guGetVCRowId(element.parentElement);
    }
}

String.prototype.guReplaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function guReplaceHtmlEntities(s) {
    var amp = "&a" + "mp;";
    var gt = "&g" + "t;";
    var lt = "&l" + "t;";
    var nbsp = "&n" + "bsp;";
    return s
        .guReplaceAll(amp, "&")
        .guReplaceAll(gt, ">")
        .guReplaceAll(lt, "<")
        .guReplaceAll(nbsp, " ")
        .guReplaceAll("\"", "'")
        .guReplaceAll("\\\"", "'");
}

function guProcessEncodeEscapeChars(value) {
    value = value.replace(/\|\|/g, "\\\\1");
    value = value.replace(/\~\~/g, "\\\\2");
    value = value.replace(/\=\=/g, "\\\\3");
    return value;
}

function guDecodeEscapeChars(value) {
    value = value.replace(/\\\\1/g,"|");
    value = value.replace(/\\\\2/g,"~");
    value = value.replace(/\\\\3/g,"=");
    return value;
}

function guParsePath(key, element, elementPath) {
    if (elementPath.length > 0) {
        var elementList = elementPath.split("-");
        for (j=0; j < elementList.length; j++) {
            var count = parseInt(elementList[j]);
            var elements = element.getElementsByTagName(elementList[j].replace(/^\d+?/, ''));
            if (elements.length == 0) {
                console.log("guSubstituteContent() ERROR. Key: " + key + " Element[" + j+1 + "] (" + elementList[j] + ") in value navigation: " + elementPath + " not found");
                break;
            }
            if (isNaN(count)) {
                element = elements[0];
            } else {
                if (count > elements.length) {
                    console.log("guSubstituteContent() ERROR. Key: " + key + " Number of Element[" + j+1 + "] in value navigation: " + elementPath + " is: " + elements.length + ", which is less than required number of elements: " + count);
                    break;
                }
                element = elements[count-1];
            }
        }
    }
    return element;
}

function guFindBundle(element) {
    var originalElement = element;
    while (element !== document) {
        if ( (element.className.indexOf("offer") != -1) || (element.className.indexOf("presell") != -1) || (element.className.indexOf("presell-popup") != -1) || (element.className.indexOf("upsell") != -1) || (element.className.indexOf("storage") != -1) || (element.className.indexOf("inlineUpsell") != -1) || (element.className.indexOf("accessoriesText") != -1) ) {
            var productList = [];
            var classes = element.className.split(' ');
            for (var i=0; i < classes.length; i++) {
                if (classes[i].startsWith("guproduct")) {
                    productList.push(classes[i]);
                }
            }
            return productList;
        }
        element = element.parentNode;
    }

    // We got here because there was no parent with an offer class. Error!
    console.log("guFindBundle() ERROR. Parent DOM elements did not contain a class named offer. element: " + element);
    return [];
}

function guProcessCurrency(element, value, bundle) {
    // Check to see if there's a price token in the value string. Bail out quickly of not.
    var priceTokenIndex = value.indexOf("$$");
    if (priceTokenIndex == -1) {
        element.innerHTML = value;
        return;
    }

    // Remote data not loaded? Defer until it is. Since guOffer rearranges the mobile offer section, gotta generate an id for the element, then find it during fixup by that id
    if (!gu_remote_data.loaded) {
        var bundle = guFindBundle(element); // Get the products in the bundle that this element is in
        element.id = "gufixupid" + guFixUpIdCounter++;
        guPriceFixUpElements.push( {fixupElementId: element.id, fixupValue: value, bundle: bundle} );
        element.innerHTML = guSubstituteString("Fetching Price...");
        return;
    }

    // Find the bundle/presell/upsell that this element is in, then get the products in that bundle and all the prices for this bundle
    var prices = null;
    if (bundle === undefined) {
        prices = window.gu_products.calculatePriceInfoFromProducts(guFindBundle(element));
    } else {
        prices = window.gu_products.calculatePriceInfoFromProducts(bundle);
    }

    // If no products in the bundle, it is probably from a hidden bundle. So bail out.
    if (prices === null) {
        return;
    }

    // Replace all price tokens with internationalized actual prices
    var startSearch = 0;
    while (priceTokenIndex != -1) {

        // Extract the price token; but make sure we won't run over the length of the string
        if (value.length < (priceTokenIndex + 4)) {
            console.log("guProcessCurrency ERROR. String ended before $$ variable fully defined. element: " + element);
            return;
        }
        var textToReplace = value.slice(priceTokenIndex, priceTokenIndex + 4);
        var priceToken = value.slice(priceTokenIndex + 2, priceTokenIndex + 4).toLowerCase();

        try {
            value = value.replace(textToReplace, guDisplayCurrency(prices[priceToken].toString()));
        } catch (e) {
            // Raven.captureException(e, {
            //     value : value,
            //     textToReplace : textToReplace,
            //     priceToken : priceToken,
            //     prices : prices
            // });
        }

        // Find the next price token (or exit the loop)
        startSearch += 4;
        priceTokenIndex = value.indexOf("$$", startSearch);
    }

    element.innerHTML = value;
}

function guFixUpPrices() {
    for (var i = 0; i < guPriceFixUpElements.length; i++) {
        guProcessCurrency(document.getElementById(guPriceFixUpElements[i].fixupElementId), guPriceFixUpElements[i].fixupValue, guPriceFixUpElements[i].bundle);
    }
}

function guContentDoIt(element, key, value) {
    value = guProcessEncodeEscapeChars(value);
    if (value.startsWith("~")) { // Is this a Complex Value?
        var directives = value.split("~"); // Break out multiple directives and handle them in order
        for (var i=1; i < directives.length-1; i = i+2) {
            var directive = directives[i];
            var equalIndex = directive.indexOf("=");
            var atIndex = directive.indexOf("@");
            if (atIndex != -1) { // Attribute Directive (this test must be before Replace Directive)
                var elementPath = directive.slice(0, atIndex);
                var parsedElement = guParsePath(key, element, elementPath);
                directive = directive.slice(atIndex + 1);
                atIndex = directive.indexOf("=");
                if (atIndex != -1) {
                    var attrName = guDecodeEscapeChars(directive.slice(0, atIndex));
                    var attrValue = guDecodeEscapeChars(directive.slice(atIndex + 1));
                    parsedElement.setAttribute(attrName, attrValue);
                } else if (directive.indexOf("|") != -1) {
                    var attr = directive.split("|");
                    var attrName = guDecodeEscapeChars(attr[0]);
                    var replacePattern = guDecodeEscapeChars(attr[1]);
                    var replaceValue = (typeof attr[2] == "undefined") ? "" : guDecodeEscapeChars(attr[2]);
                    var originalValue = parsedElement.getAttribute(attrName);
                    if (originalValue !== null) {
                        var newValue = originalValue.replace(replacePattern, replaceValue);
                        parsedElement.setAttribute(attrName, newValue);
                    }
                } else {
                    console.log("guSubstituteContent() ERROR. Key: " + key + " Attribute Directive doesn't contain '=' or '|': " + directives[i]);
                }
            } else if (directive.indexOf("|") != -1) { // Replace Directive
                var replace = directive.split("|");
                var parsedElement = guParsePath(key, element, replace[0]);
                var replacePattern = guDecodeEscapeChars(guReplaceHtmlEntities(replace[1]));
                var replaceValue = (typeof replace[2] == "undefined") ? "" : guDecodeEscapeChars(replace[2]);
                guProcessCurrency(parsedElement, parsedElement.innerHTML.replace(replacePattern, replaceValue));
            } else if (equalIndex != -1) { // Assign Directive
                var elementPath = directive.slice(0, equalIndex);
                var parsedElement = guParsePath(key, element, elementPath);
                guProcessCurrency(parsedElement, guDecodeEscapeChars(directive.slice(equalIndex + 1)));
            } else {
                console.log("guSubstituteContent()  ERROR. Key: " + key + " Complex Directive doesn't contain '=' or '|': " + directives[i]);
            }
        }
    } else { // Simple Assign
        guProcessCurrency(element, guDecodeEscapeChars(value));

    }
}

// Replace content, starting at domNode. If convertCurrency is true, then also convert text that
// that starts with a $ into local currency
function guSubstituteContent (domNode, convertCurrency) {

    if (typeof convertCurrency == "undefined") convertCurrency = false;

    var skipTags = {
        NOSCRIPT : "",
        SCRIPT : "",
        HEAD : "",
        LINK : "",
        BODY : "",
        HTML : ""
    };

    if (gu_qs["gudebugcontent"] == 1) {
        for (var j = 0; j < guActiveContentArray.length; j++) {
            guDumpContent(guActiveContentArray[j]);
        }
    }

    // Run through all the elements in the document
    var elements = domNode.getElementsByTagName("*");
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var key, value;
        var found = false;

        if (typeof skipTags[element.tagName] != "undefined") { continue; }

        // Special case - translate the placeholder attribute on an input element
        if (element.tagName == "INPUT") {
            var placeholder = element.getAttribute("placeholder");
            if (placeholder !== null) {
                element.setAttribute("placeholder", guSubstituteString(placeholder));
            }
            continue;
        }

        // Is convertCurrency true?
        if (convertCurrency) {
            var e = element.textContent;
            if (e[0] == '$') {
                if (e.length < 2) {
                    console.log("guSubstituteContent() ConvertCurrency ERROR. element contains $ only; no dollar value. parentElement: " + element.parentElement.textContent);
                    element.style.backgroundColor = "yellow";
                } else if (e == "$--") {
                    // Do nothing; value hasn't been calculated yet
                } else {
                    try {
                        element.textContent = guDisplayCurrency(e.slice(1).replace(new RegExp(',', 'g'), ''));
                    }
                    catch(error) {
                        console.error("ContentBundles guSubstituteContent: Exception from guDisplayCurrency. e.slice(1): " + e.slice(1) + " e: " + e + " error: " + error);
                    }
                }
                continue;
            }
        }

        // Does the element have an id? See if there's a matching content Key so.
        var id = element.id;
        if (id != "") {
            key = "!" + id;
            for (var j = 0; j < guActiveContentArray.length; j++) {
                value = guActiveContentArray[j][key];
                if (typeof value != "undefined") {
                    guContentDoIt(element, key, value);
                    found = true;
                    break;
                }
            }
            if (found) { continue; }
        }

        // Is there a content Key match based on the value of the element's innerHTML?
        var innerHTML = element.innerHTML;
        if (innerHTML == "") { continue; }
        key = guReplaceHtmlEntities(innerHTML);
        for (var j = 0; j < guActiveContentArray.length; j++) {
            value = guActiveContentArray[j][key];
            if (typeof value != "undefined") {
                guContentDoIt(element, key, value);
                found = true;
                break;
            }
        }
        if (found) { continue; }
    }
}

// Do language translation for a single HTML string. Returns translated string.
function guSubstituteString(html) {
    var key = guReplaceHtmlEntities(html);
    for (var i = 0; i < guActiveContentArray.length; i++) {
        var value = guActiveContentArray[i][key];
        if (typeof value != "undefined") {
            return value;
        }
    }
    return html;
}

function guDumpContent(contentArray) {
    for(var key in contentArray){
        var content;
        var value = contentArray[key];
        if (key.startsWith("!")) {
            if (key.length == 0) {
                console.log("ContentDebug ERROR: A key was found that is an empty string");
            } else if (key.length == 1) {
                console.log("ContentDebug ERROR: A key was found that is just an exclamation point (!)");
            } else {
                content = document.getElementById(key.slice(1));
                if (content === null) {
                    console.log("ContentDebug ERROR: id key: " + key + " not found in the document");
                } else {
                    console.log("Key: " + key + " Matches: " + content.outerHTML);
                }
            }
        }
    }
    console.log(" ");
}

function guInitializeContentBundles() {
    var languageFound = false;

    var gu_cb = gu_qs["gucb"];
    if (typeof gu_cb != "undefined" && typeof gu_content_bundles != "undefined") {
        var gu_content_bundle = gu_content_bundles[gu_cb];
        if (typeof gu_content_bundle != "undefined") {
            if (typeof gu_content_bundle[gu_translationLanguage] != "undefined") {
                guActiveContentArray.push(gu_content_bundle[gu_translationLanguage]);
                languageFound = true;
            } else if (typeof gu_content_bundle[gu_language] != "undefined") {
                guActiveContentArray.push(gu_content_bundle[gu_language]);
                languageFound = true;
            } else if (typeof gu_content_bundle["default"] != "undefined") {
                guActiveContentArray.push(gu_content_bundle["default"]);
            }
        }
    }

    if (typeof gu_default_content != "undefined") {
        if (typeof gu_default_content[gu_translationLanguage] != "undefined") {
            guActiveContentArray.push(gu_default_content[gu_translationLanguage]);
            languageFound = true;
        } else if (typeof gu_default_content[gu_language] != "undefined") {
            guActiveContentArray.push(gu_default_content[gu_language]);
            languageFound = true;
        }
        if (typeof gu_default_content["default"] != "undefined") {
            guActiveContentArray.push(gu_default_content["default"]);
        }
    }

    if (typeof gu_intl_content != "undefined") {
        if (typeof gu_intl_content[gu_translationLanguage] != "undefined") {
            guActiveContentArray.push(gu_intl_content[gu_translationLanguage]);
            languageFound = true;
        } else if (typeof gu_intl_content[gu_language] != "undefined") {
            guActiveContentArray.push(gu_intl_content[gu_language]);
            languageFound = true;
        }
    }

    if (typeof gu_common_content != "undefined") {
        if (languageFound) {
            if (typeof gu_common_content[gu_translationLanguage] != "undefined") {
                guActiveContentArray.push(gu_common_content[gu_translationLanguage]);
            } else if (typeof gu_common_content[gu_language] != "undefined") {
                guActiveContentArray.push(gu_common_content[gu_language]);
            }
        }
        else if (typeof gu_common_content["default"] != "undefined") {
            guActiveContentArray.push(gu_common_content["default"]);
        }
    }

    //temp running of gallery after translations have occurred
    document.addEventListener('DOMContentLoaded', function () {
        if (gu_qs['guatf'] == '0' || gu_qs['guatf'] == '1') {
            gu_atf = gu_qs['guatf'];
        }

        if (hasOffer || isMobile) {
            if (gu_qs['gudtc'] === '1') {
                directToCheckout();
            } else if (gu_atf === 1) {
                showAboveTheFold();
            }
        }
        PrepGuGallery();
        guSubstituteContent(document);
        RunGuGallery();
        document.body.style.opacity = 1;
        // guDisplayComplete();
    });
}

// Register our price fixup callback for when all remote data is loaded
guRemoteDataRegisterCallback(guFixUpPrices, true);

guInitializeContentBundles();

//  OpenTag def
function gu_fire_event(eventName, action, identifier, propertyName, propertyValue) {
    var msg = "gu_fire_event fired prior to function def";
    console.log(eventName + " " + msg);

}



// RequestIdPersistance Callback BL V1.1
var dataFlush = false;

function RequestIdPersistanceCallbackBL(response) {
    try {
        if (!dataFlush) {
            var tmp_gu_qs = gu_deparam(document.location.search);
            if (response.converted) {
                dataFlush = true;
                var hldFp = localData.fp;
                var hldUser_id = localData.user_id;
                var oKeys = Object.keys(localData);
                for (var i = 0; i < oKeys.length; i++) {
                    localData[oKeys[i]] = null
                }
                localData.flushed = true;
                localData.fp = hldFp;
                localData.user_id = hldUser_id;
                localData.search = document.location.search;
                if (response.user_id && response.user_id.length >= 36) {
                    localData.user_id = response.user_id
                }
                if (tmp_gu_qs["req_id"] && localData["req_id"] !== tmp_gu_qs["req_id"]) {
                    localData["req_id"] = null;
                    localData["sub_id"] = null;
                    localData["aff_id"] = null
                }
                gu_assign(localData, tmp_gu_qs);
                saveDataLocal(localData);
                if (tmp_gu_qs.guu) {
                    localData.user_id = tmp_gu_qs.guu
                } else {
                    tmp_gu_qs.guu = localData.user_id
                }
                gu_qs = tmp_gu_qs
            } else {
                if (response["req_id"]) {
                    if (localData["req_id"] && response["req_id"] != localData["req_id"]) {} else {
                        for (var i = 0; i < requiredData.length; i++) {
                            var key = requiredData[i];
                            if (!localData[key] || localData[key] == "undefined" && response[key]) {
                                localData[key] = response[key]
                            }
                        }
                    }
                }
                if (response.user_id && response.user_id.length >= 36 && response["guu_ttl"] && response["guu_ttl"] > Date.now()) {
                    localData.user_id = response.user_id;
                    gu_qs["guu"] = localData["user_id"]
                }
                if (response.search) {
                    var tmp = gu_deparam(response.search);
                    gu_assign(tmp, gu_qs);
                    gu_assign(gu_qs, tmp)
                }
                gu_assign(localData, gu_qs);
                copyDataTo_gu_qs(gu_qs, localData);
                localData.search = gu_qs_to_str(gu_qs);
                saveDataLocal(localData)
            }
            if (typeof gu_linkfix !== "undefined" && gu_linkfix) {
                gu_linkfix()
            }
            if (dataFlush) {
                giddySubmit(dataFlush)
            }
        } else {
            dataFlush = !dataFlush
        }
    } catch (e) {}
}


// Video Autoplay V1.1
var //gu_autoplay = gu_autoplay_default,
    gu_mainvideo;

gu_videoEl = document.getElementById('mainvideo');
gu_mainvideo = gu_videoEl;
if (gu_mainvideo != null) {
    gu_mainvideo = gu_mainvideo.getElementsByTagName('iframe');
    if (gu_mainvideo.length > 0) {
        gu_mainvideo = gu_mainvideo[0];
        if (gu_mainvideo !== null && gu_mainvideo.src !== undefined && gu_videoEl.offsetHeight != 0) {
            if (gu_qs['autoplay'] == '0' || gu_qs['autoplay'] == '1') {
                gu_autoplay = gu_qs['autoplay'];
            }
            if (gu_qs['guautoplay'] == '0' || gu_qs['guautoplay'] == '1') {
                gu_autoplay = gu_qs['guautoplay'];
            }
            var gu_url = gu_mainvideo.src.split('?');
            var gu_src_qs = gu_deparam(gu_url[1]);
            gu_src_qs['autoplay'] = gu_autoplay;
            gu_mainvideo.src = gu_url[0] + gu_qs_to_str(gu_src_qs);


            // Remove autoplay param
            gu_qs['guautoplay'] = '0';
        }
    }
}
