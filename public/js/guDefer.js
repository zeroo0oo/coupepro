function addGALinkerToGuQS() {
    if (typeof (window.ga) === ' undefined' || !window.ga || typeof(window.ga.getAll) !== 'function' ) {
        setTimeout(addGALinkerToGuQS, 150);
    } else {
        try {
            if (typeof (window.gu_qs) === 'undefined' || !window.gu_qs) {
                window.gu_qs = {};
            }
            var tracker = ga.getAll()[0];
            var linker = new window.gaplugins.Linker(tracker);
            var linkerParam = linker.decorate("a").replace('a?', '');
            var queryParams = linkerParam.split('&');
            for (var i = 0; i < queryParams.length; i++) {
                var param = queryParams[i].split('=');
                gu_qs[param[0]] = param[1];
            }
        } catch (e) {
            console.log(e);
        }
    }
}
addGALinkerToGuQS();

/*
 *
 * Giddybox v1
 * Â© 2017 The GiddyUp Group, LLC ALL RIGHTS RESERVED
 *
 * How to use:
 * giddybox('id','loading','borderless');                                                                                                                        â€”â€” Standard mode
 * giddybox('youtube-id','autoplay','controls');                                                                                                                 â€”â€” YouTube mode
 *
 */

// Prepare the canvas - hide referenced elements
jQuery(function() {

    // Hide .hidden
    jQuery('.hidden').hide();

    // Check anchor tags for giddyboxes
    jQuery('a').each(function() {
        if (jQuery(this).attr('href') != null) { // If contains href
            var href = jQuery(this).attr('href');
            if (~href.indexOf('giddybox-')) { // If href contains giddybox
                href = href.split('giddybox-')[1];
                if (~href.indexOf('?')) {
                    href = href.split('?')[0];
                }
                jQuery(this).removeAttr('href');
                jQuery(this).attr('onclick', 'giddybox("' + href + '");');
            }
        }
    });

    // If onclick contains giddybox
    /*jQuery('[onclick]').each(function() {
        var id = jQuery(this).attr('onclick');
        if (~id.indexOf('#')) { id = id.replace('#',''); }                                                                                                      // If id/el contains #
        if (~id.indexOf("giddybox")) { id = id.split("'")[1]; jQuery('#'+id).hide(); }
    });*/
});

// Kickstart the Giddybox
function giddybox(el, loading, border) {
    jQuery(function() {

        // Build the Giddybox (if it doesn't exist)
        if (!jQuery('#giddybox').length) {
            jQuery('body').append('<div id="giddybox"><div id="giddybox-wrapper" class="animated"><div id="giddybox-innerWrapper"></div></div></div>'); // Create a Giddybox
            jQuery('#giddybox-wrapper').wrap('<div class="center-giddybox"><div class="center-anchor"></div></div>'); // Center the content
            jQuery('#giddybox-wrapper').append('<a class="giddybox-closeBtn">X</a>'); // Create the close button
        } else {
            jQuery('#giddybox').removeClass('fullwidth fullheight youtube');
        } // Clean up

        var // Define all variables
            giddybox = jQuery('#giddybox'),
            wrapper = jQuery('#giddybox-wrapper'),
            innerWrapper = jQuery('#giddybox-innerWrapper');

        // Show the Giddybox
        giddybox.fadeIn();
        setTimeout(function() { // Added time needed to finish processes
            if (giddybox.find('iframe').length) { // â€” If iframe is detected â€”
                if (loading == 'loading' || loading == 'true' || loading == 1) { // If it has loading option
                    innerWrapper.css('overflow', 'hidden'); // Remove scrollbars
                    // Loading animation
                    giddybox.prepend('<div class="loading"><div class="center"><div class="center-anchor"><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></div></div></div>');
                    jQuery('#giddybox .loading').fadeIn();
                    // After loaded
                    giddybox.find('iframe').on('load', function() { // Wait for iframe to load
                        if (giddybox.find('iframe').height() > 1000) {
                            wrapper.height('1000px');
                        } // If iframe is too tall
                        wrapper.addClass('fadeInUp').css('opacity', '1'); // Fade in
                        setTimeout(function() {
                            jQuery('#giddybox .loading').remove();
                        }, 1000); // Remove loading animation
                    });
                } else if (giddybox.find('iframe').attr('src').indexOf('youtube') !== -1) { // â€” If iframe contains youtube
                    var wWidth = jQuery(window).width() - 40;
                    wrapper.addClass('fadeInUp').css('opacity', '1'); // Fade in
                    innerWrapper.css('overflow', 'hidden'); // Remove scrollbars
                    giddybox.addClass('youtube');

                    if (giddybox.find('iframe').width() > wWidth) {
                        giddybox.find('iframe').attr('width', wWidth);
                    } // â€” If iframe is larger than window â€”
                } else { // â€” Or else â€”
                    wrapper.addClass('fadeInUp').css('opacity', '1'); // Fade in
                    innerWrapper.css('overflow', 'hidden'); // Remove scrollbars
                }
                if (border == 'borderless' || border == 'false') {
                    wrapper.addClass('borderless');
                }
            } else {
                wrapper.addClass('fadeInUp').css('opacity', '1');
            }
        }, 300);

        // Close the Giddybox
        function closeGiddybox() {
            giddybox.fadeOut();
            if (innerWrapper.find('iframe').length) {
                jQuery('body').css('overflow', 'auto');
            } // Re-enable scroll on iframe
            setTimeout(function() {
                // Clean up
                var content = innerWrapper.html();

                if (!jQuery('#giddybox-storage').length && !~el.indexOf('youtube')) {
                    jQuery('#giddybox').after('<div id="giddybox-storage">' + content + '</div>');
                } else if (!~el.indexOf('youtube')) {
                    jQuery('#giddybox-storage').append(content);
                }

                innerWrapper.empty();

                // Reset
                giddybox.removeClass('fullsize youtube');
                wrapper.removeClass('fadeInUp').css('opacity', '0');

                wrapper.removeAttr('style'); // Reset attributes
                innerWrapper.removeAttr('style');
            }, 500);
        }

        // Close the Giddybox btn
        jQuery('.giddybox-closeBtn').on('click', function() {
            closeGiddybox();
        });

        // Close if click outside of wrapper
        // jQuery(document).on('mouseup touchstart',function(e) { if (!wrapper.is(e.target) && wrapper.has(e.target).length === 0 && wrapper.is(':visible') ) { closeGiddybox(); } });

        // Add inline content to Giddybox
        jQuery.fn.outerHTML = function() {
            return jQuery('<div />').append(this.eq(0).clone()).html();
        }; // OuterHTML Fn

        if (~el.indexOf('#')) { // If el contains #
            var content = jQuery(el).outerHTML();
            jQuery(el).remove();
        } else { // If it doesn't contain # add it in
            var content = jQuery('#' + el).outerHTML();
            jQuery('#' + el).remove();
        }

        innerWrapper.empty().append(content); // Add content

        // YouTube player
        if (~el.indexOf('youtube')) {
            var autoplay = 0,
                controls = 0,
                id = el.toString().split(/-(.+)/)[1]; // Get YouTube Video ID

            // Set GiddyBox in YouTube mode
            giddybox.addClass('youtube');

            // Set params
            if (loading == 'true' || loading == 'autoplay') {
                autoplay = 1;
            }
            if (border == 'true' || loading == 'controls') {
                controls = 1;
            }

            // Add Video
            innerWrapper.empty().append('<iframe id="' + id + '" enablejsapi="1" width="800" height="450" src="https://www.youtube.com/embed/' + id + '?rel=0&controls=' + controls + '&showinfo=0&autoplay=' + autoplay + '" frameborder="0" allowfullscreen></iframe>');

            // Hook up analytics event generation to the video
            if (gu_youtubeAPILoaded) {
                gu_initvideo(id);
            } else {
                gu_uninitializedYoutubeVideos.push(id);
            }
        }

        // Resize if wider than window
        setTimeout(function() {
            var ogWidth = parseInt(wrapper.width());

            jQuery(window).on('resize', function() {
                var wWidth = parseInt(jQuery(window).width()) - 60,
                    gWidth = parseInt(wrapper.width());

                if (wWidth <= gWidth && wWidth <= ogWidth) {
                    giddybox.addClass('fullwidth');
                } else if (wWidth >= ogWidth) {
                    giddybox.removeClass('fullwidth');
                }
            }).resize();
        }, 500);

        // Resize if taller than window
        setTimeout(function() {
            var ogHeight = parseInt(wrapper.height());

            jQuery(window).on('resize', function() {
                var wHeight = parseInt(jQuery(window).height()) - 60,
                    gHeight = parseInt(wrapper.height());

                if (wHeight <= gHeight && wHeight <= ogHeight) {
                    giddybox.addClass('fullheight');
                } else if (wHeight >= ogHeight) {
                    giddybox.removeClass('fullheight');
                }
            }).resize();
        }, 500);

        // Prevent scroll
        innerWrapper.on('DOMMouseScroll mousewheel scroll touchmove', function(ev) {
            var scrollHeight = jQuery(window).height() + jQuery(window).scrollTop();
            var $this = jQuery(this),
                scrollTop = this.scrollTop,
                scrollHeight = this.scrollHeight,
                height = $this.height(),
                delta = (ev.type == 'DOMMouseScroll' ? ev.originalEvent.detail * -40 : ev.originalEvent.wheelDelta),
                up = delta > 0,
                prevent = function() {
                    ev.stopPropagation();
                    ev.preventDefault();
                    ev.returnValue = false;
                    return false;
                }

            // When reached bottom of element
            if (!up && -delta > scrollHeight - height - scrollTop) {
                $this.scrollTop(scrollHeight);
                return prevent();

                // When reached top of element
            } else if (up && delta > scrollTop) {
                $this.scrollTop(0);
                return prevent();
            }
        });

        // Prevent scroll on iframe
        if (innerWrapper.find('iframe').length) {
            jQuery('body').css({
                'width': '100%',
                'height': '100%',
                'overflow': 'hidden'
            });
        }

    }); // end jQuery block
} // end giddybox fn
// International Content Hider v1.1

// <!-- Hide/show all necessary info when viewing intl pages -->
jQuery(function() {
    if (gu_languageAndCountry != "en-us") {

        //hit the three shipping elements in offer-3
        for (var i = 1; i < 4; i++) {
            jQuery('#offer-3-bundle-' + i + '-shipping').addClass('hidden');
        }

        //hit the four shipping elements in offer-4
        for (var i = 1; i < 5; i++) {
            jQuery('#offer-4-bundle-' + i + '-shipping').addClass('hidden');
        }

        //Hide domestic guarantee badge and show intl badge on desktop
        if (!mobileDevice) {
            jQuery('#domestic-badges').addClass('hidden');
            jQuery('#intl-badges').removeClass('hidden');
            jQuery('#intl-guarantee').removeClass('hidden');
        }

        //Swap presell-popup 50% text to translatable version
        jQuery('#presell-popup-discount-domestic').hide();
        jQuery('#presell-popup-discount-intl').show();
        //multiple presell-popups will have classes not ids
        jQuery('.presell-popup-discount-domestic').hide();
        jQuery('.presell-popup-discount-intl').show();

    }

    //check to show impressum in applicable countries
    if (gu_country == "de" || gu_country == "ch" || gu_country == "at") {
        jQuery('#footer-impressum-cta').show();
    }
});
// RequestId Persistance V1.0

!function(e){if("object"==typeof exports)module.exports=e();else if("function"==typeof define&&define.amd)define(e);else{var t;"undefined"!=typeof window?t=window:"undefined"!=typeof global?t=global:"undefined"!=typeof self&&(t=self),t.objectHash=e()}}(function(){return function e(t,n,r){function o(a, u) { if (!n[a]) { if (!t[a]) { var s = "function" == typeof require && require; if (!u && s) return s(a, !0); if (i) return i(a, !0); throw new Error("Cannot find module '" + a + "'") } var f = n[a] = { exports: {} }; t[a][0].call(f.exports, function (e) { var n = t[a][1][e]; return o(n ? n : e) }, f, f.exports, e, t, n, r) } return n[a].exports }for(var i="function"==typeof require&&require,a=0;a<r.length;a++)o(r[a]);return o}({1:[function(e,t,n){(function (r, o, i, a, u, s, f, c, l) { "use strict"; function d(e, t) { return t = h(e, t), g(e, t) } function h(e, t) { if (t = t || {}, t.algorithm = t.algorithm || "sha1", t.encoding = t.encoding || "hex", t.excludeValues = !!t.excludeValues, t.algorithm = t.algorithm.toLowerCase(), t.encoding = t.encoding.toLowerCase(), t.ignoreUnknown = t.ignoreUnknown === !0, t.respectType = t.respectType !== !1, t.respectFunctionNames = t.respectFunctionNames !== !1, t.respectFunctionProperties = t.respectFunctionProperties !== !1, t.unorderedArrays = t.unorderedArrays === !0, t.unorderedSets = t.unorderedSets !== !1, t.replacer = t.replacer || void 0, "undefined" == typeof e) throw new Error("Object argument required."); for (var n = 0; n < m.length; ++n)m[n].toLowerCase() === t.algorithm.toLowerCase() && (t.algorithm = m[n]); if (m.indexOf(t.algorithm) === -1) throw new Error('Algorithm "' + t.algorithm + '"  not supported. supported values: ' + m.join(", ")); if (v.indexOf(t.encoding) === -1 && "passthrough" !== t.algorithm) throw new Error('Encoding "' + t.encoding + '"  not supported. supported values: ' + v.join(", ")); return t } function p(e) { if ("function" != typeof e) return !1; var t = /^function\s+\w*\s*\(\s*\)\s*{\s+\[native code\]\s+}$/i; return null != t.exec(Function.prototype.toString.call(e)) } function g(e, t) { var n; n = "passthrough" !== t.algorithm ? b.createHash(t.algorithm) : new w, "undefined" == typeof n.write && (n.write = n.update, n.end = n.update); var r = y(t, n); if (r.dispatch(e), n.update || n.end(""), n.digest) return n.digest("buffer" === t.encoding ? void 0 : t.encoding); var o = n.read(); return "buffer" === t.encoding ? o : o.toString(t.encoding) } function y(e, t, n) { n = n || []; var r = function (e) { return t.update ? t.update(e, "utf8") : t.write(e, "utf8") }; return { dispatch: function (t) { e.replacer && (t = e.replacer(t)); var n = typeof t; return null === t && (n = "null"), this["_" + n](t) }, _object: function (t) { var o = /\[object (.*)\]/i, a = Object.prototype.toString.call(t), u = o.exec(a); u = u ? u[1] : "unknown:[" + a + "]", u = u.toLowerCase(); var s = null; if ((s = n.indexOf(t)) >= 0) return this.dispatch("[CIRCULAR:" + s + "]"); if (n.push(t), "undefined" != typeof i && i.isBuffer && i.isBuffer(t)) return r("buffer:"), r(t); if ("object" === u || "function" === u) { var f = Object.keys(t).sort(); e.respectType === !1 || p(t) || f.splice(0, 0, "prototype", "__proto__", "constructor"), r("object:" + f.length + ":"); var c = this; return f.forEach(function (n) { c.dispatch(n), r(":"), e.excludeValues || c.dispatch(t[n]), r(",") }) } if (!this["_" + u]) { if (e.ignoreUnknown) return r("[" + u + "]"); throw new Error('Unknown object type "' + u + '"') } this["_" + u](t) }, _array: function (t, o) { o = "undefined" != typeof o ? o : e.unorderedArrays !== !1; var i = this; if (r("array:" + t.length + ":"), !o || t.length <= 1) return t.forEach(function (e) { return i.dispatch(e) }); var a = [], u = t.map(function (t) { var r = new w, o = n.slice(), i = y(e, r, o); return i.dispatch(t), a = a.concat(o.slice(n.length)), r.read().toString() }); return n = n.concat(a), u.sort(), this._array(u, !1) }, _date: function (e) { return r("date:" + e.toJSON()) }, _symbol: function (e) { return r("symbol:" + e.toString()) }, _error: function (e) { return r("error:" + e.toString()) }, _boolean: function (e) { return r("bool:" + e.toString()) }, _string: function (e) { r("string:" + e.length + ":"), r(e) }, _function: function (t) { r("fn:"), p(t) ? this.dispatch("[native]") : this.dispatch(t.toString()), e.respectFunctionNames !== !1 && this.dispatch("function-name:" + String(t.name)), e.respectFunctionProperties && this._object(t) }, _number: function (e) { return r("number:" + e.toString()) }, _xml: function (e) { return r("xml:" + e.toString()) }, _null: function () { return r("Null") }, _undefined: function () { return r("Undefined") }, _regexp: function (e) { return r("regex:" + e.toString()) }, _uint8array: function (e) { return r("uint8array:"), this.dispatch(Array.prototype.slice.call(e)) }, _uint8clampedarray: function (e) { return r("uint8clampedarray:"), this.dispatch(Array.prototype.slice.call(e)) }, _int8array: function (e) { return r("uint8array:"), this.dispatch(Array.prototype.slice.call(e)) }, _uint16array: function (e) { return r("uint16array:"), this.dispatch(Array.prototype.slice.call(e)) }, _int16array: function (e) { return r("uint16array:"), this.dispatch(Array.prototype.slice.call(e)) }, _uint32array: function (e) { return r("uint32array:"), this.dispatch(Array.prototype.slice.call(e)) }, _int32array: function (e) { return r("uint32array:"), this.dispatch(Array.prototype.slice.call(e)) }, _float32array: function (e) { return r("float32array:"), this.dispatch(Array.prototype.slice.call(e)) }, _float64array: function (e) { return r("float64array:"), this.dispatch(Array.prototype.slice.call(e)) }, _arraybuffer: function (e) { return r("arraybuffer:"), this.dispatch(new Uint8Array(e)) }, _url: function (e) { return r("url:" + e.toString(), "utf8") }, _map: function (t) { r("map:"); var n = Array.from(t); return this._array(n, e.unorderedSets !== !1) }, _set: function (t) { r("set:"); var n = Array.from(t); return this._array(n, e.unorderedSets !== !1) }, _blob: function () { if (e.ignoreUnknown) return r("[blob]"); throw Error('Hashing Blob objects is currently not supported\n(see https://github.com/puleos/object-hash/issues/26)\nUse "options.replacer" or "options.ignoreUnknown"\n') }, _domwindow: function () { return r("domwindow") }, _process: function () { return r("process") }, _timer: function () { return r("timer") }, _pipe: function () { return r("pipe") }, _tcp: function () { return r("tcp") }, _udp: function () { return r("udp") }, _tty: function () { return r("tty") }, _statwatcher: function () { return r("statwatcher") }, _securecontext: function () { return r("securecontext") }, _connection: function () { return r("connection") }, _zlib: function () { return r("zlib") }, _context: function () { return r("context") }, _nodescript: function () { return r("nodescript") }, _httpparser: function () { return r("httpparser") }, _dataview: function () { return r("dataview") }, _signal: function () { return r("signal") }, _fsevent: function () { return r("fsevent") }, _tlswrap: function () { return r("tlswrap") } } } function w() { return { buf: "", write: function (e) { this.buf += e }, end: function (e) { this.buf += e }, read: function () { return this.buf } } } var b = e("crypto"); n = t.exports = d, n.sha1 = function (e) { return d(e) }, n.keys = function (e) { return d(e, { excludeValues: !0, algorithm: "sha1", encoding: "hex" }) }, n.MD5 = function (e) { return d(e, { algorithm: "md5", encoding: "hex" }) }, n.keysMD5 = function (e) { return d(e, { algorithm: "md5", encoding: "hex", excludeValues: !0 }) }; var m = b.getHashes ? b.getHashes().slice() : ["sha1", "md5"]; m.push("passthrough"); var v = ["buffer", "hex", "binary", "base64"]; n.writeToStream = function (e, t, n) { return "undefined" == typeof n && (n = t, t = {}), t = h(e, t), y(t, n).dispatch(e) } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/fake_8c3adc78.js", "/")},{buffer:3,crypto:5,lYpoI2:10}],2:[function(e,t,n){(function (e, t, r, o, i, a, u, s, f) { var c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; !function (e) { "use strict"; function t(e) { var t = e.charCodeAt(0); return t === i || t === l ? 62 : t === a || t === d ? 63 : t < u ? -1 : t < u + 10 ? t - u + 26 + 26 : t < f + 26 ? t - f : t < s + 26 ? t - s + 26 : void 0 } function n(e) { function n(e) { f[l++] = e } var r, i, a, u, s, f; if (e.length % 4 > 0) throw new Error("Invalid string. Length must be a multiple of 4"); var c = e.length; s = "=" === e.charAt(c - 2) ? 2 : "=" === e.charAt(c - 1) ? 1 : 0, f = new o(3 * e.length / 4 - s), a = s > 0 ? e.length - 4 : e.length; var l = 0; for (r = 0, i = 0; r < a; r += 4, i += 3)u = t(e.charAt(r)) << 18 | t(e.charAt(r + 1)) << 12 | t(e.charAt(r + 2)) << 6 | t(e.charAt(r + 3)), n((16711680 & u) >> 16), n((65280 & u) >> 8), n(255 & u); return 2 === s ? (u = t(e.charAt(r)) << 2 | t(e.charAt(r + 1)) >> 4, n(255 & u)) : 1 === s && (u = t(e.charAt(r)) << 10 | t(e.charAt(r + 1)) << 4 | t(e.charAt(r + 2)) >> 2, n(u >> 8 & 255), n(255 & u)), f } function r(e) { function t(e) { return c.charAt(e) } function n(e) { return t(e >> 18 & 63) + t(e >> 12 & 63) + t(e >> 6 & 63) + t(63 & e) } var r, o, i, a = e.length % 3, u = ""; for (r = 0, i = e.length - a; r < i; r += 3)o = (e[r] << 16) + (e[r + 1] << 8) + e[r + 2], u += n(o); switch (a) { case 1: o = e[e.length - 1], u += t(o >> 2), u += t(o << 4 & 63), u += "=="; break; case 2: o = (e[e.length - 2] << 8) + e[e.length - 1], u += t(o >> 10), u += t(o >> 4 & 63), u += t(o << 2 & 63), u += "=" }return u } var o = "undefined" != typeof Uint8Array ? Uint8Array : Array, i = "+".charCodeAt(0), a = "/".charCodeAt(0), u = "0".charCodeAt(0), s = "a".charCodeAt(0), f = "A".charCodeAt(0), l = "-".charCodeAt(0), d = "_".charCodeAt(0); e.toByteArray = n, e.fromByteArray = r }("undefined" == typeof n ? this.base64js = {} : n) }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/base64-js/lib/b64.js", "/node_modules/gulp-browserify/node_modules/base64-js/lib")},{buffer:3,lYpoI2:10}],3:[function(e,t,n){(function (t, r, o, i, a, u, s, f, c) { function o(e, t, n) { if (!(this instanceof o)) return new o(e, t, n); var r = typeof e; if ("base64" === t && "string" === r) for (e = M(e); e.length % 4 !== 0;)e += "="; var i; if ("number" === r) i = q(e); else if ("string" === r) i = o.byteLength(e, t); else { if ("object" !== r) throw new Error("First argument needs to be a number, array or string."); i = q(e.length) } var a; o._useTypedArrays ? a = o._augment(new Uint8Array(i)) : (a = this, a.length = i, a._isBuffer = !0); var u; if (o._useTypedArrays && "number" == typeof e.byteLength) a._set(e); else if (F(e)) for (u = 0; u < i; u++)o.isBuffer(e) ? a[u] = e.readUInt8(u) : a[u] = e[u]; else if ("string" === r) a.write(e, 0, t); else if ("number" === r && !o._useTypedArrays && !n) for (u = 0; u < i; u++)a[u] = 0; return a } function l(e, t, n, r) { n = Number(n) || 0; var i = e.length - n; r ? (r = Number(r), r > i && (r = i)) : r = i; var a = t.length; K(a % 2 === 0, "Invalid hex string"), r > a / 2 && (r = a / 2); for (var u = 0; u < r; u++){ var s = parseInt(t.substr(2 * u, 2), 16); K(!isNaN(s), "Invalid hex string"), e[n + u] = s } return o._charsWritten = 2 * u, u } function d(e, t, n, r) { var i = o._charsWritten = J(H(t), e, n, r); return i } function h(e, t, n, r) { var i = o._charsWritten = J(O(t), e, n, r); return i } function p(e, t, n, r) { return h(e, t, n, r) } function g(e, t, n, r) { var i = o._charsWritten = J(V(t), e, n, r); return i } function y(e, t, n, r) { var i = o._charsWritten = J(P(t), e, n, r); return i } function w(e, t, n) { return 0 === t && n === e.length ? Q.fromByteArray(e) : Q.fromByteArray(e.slice(t, n)) } function b(e, t, n) { var r = "", o = ""; n = Math.min(e.length, n); for (var i = t; i < n; i++)e[i] <= 127 ? (r += W(o) + String.fromCharCode(e[i]), o = "") : o += "%" + e[i].toString(16); return r + W(o) } function m(e, t, n) { var r = ""; n = Math.min(e.length, n); for (var o = t; o < n; o++)r += String.fromCharCode(e[o]); return r } function v(e, t, n) { return m(e, t, n) } function _(e, t, n) { var r = e.length; (!t || t < 0) && (t = 0), (!n || n < 0 || n > r) && (n = r); for (var o = "", i = t; i < n; i++)o += Y(e[i]); return o } function I(e, t, n) { for (var r = e.slice(t, n), o = "", i = 0; i < r.length; i += 2)o += String.fromCharCode(r[i] + 256 * r[i + 1]); return o } function E(e, t, n, r) { r || (K("boolean" == typeof n, "missing or invalid endian"), K(void 0 !== t && null !== t, "missing offset"), K(t + 1 < e.length, "Trying to read beyond buffer length")); var o = e.length; if (!(t >= o)) { var i; return n ? (i = e[t], t + 1 < o && (i |= e[t + 1] << 8)) : (i = e[t] << 8, t + 1 < o && (i |= e[t + 1])), i } } function A(e, t, n, r) { r || (K("boolean" == typeof n, "missing or invalid endian"), K(void 0 !== t && null !== t, "missing offset"), K(t + 3 < e.length, "Trying to read beyond buffer length")); var o = e.length; if (!(t >= o)) { var i; return n ? (t + 2 < o && (i = e[t + 2] << 16), t + 1 < o && (i |= e[t + 1] << 8), i |= e[t], t + 3 < o && (i += e[t + 3] << 24 >>> 0)) : (t + 1 < o && (i = e[t + 1] << 16), t + 2 < o && (i |= e[t + 2] << 8), t + 3 < o && (i |= e[t + 3]), i += e[t] << 24 >>> 0), i } } function B(e, t, n, r) { r || (K("boolean" == typeof n, "missing or invalid endian"), K(void 0 !== t && null !== t, "missing offset"), K(t + 1 < e.length, "Trying to read beyond buffer length")); var o = e.length; if (!(t >= o)) { var i = E(e, t, n, !0), a = 32768 & i; return a ? (65535 - i + 1) * -1 : i } } function x(e, t, n, r) { r || (K("boolean" == typeof n, "missing or invalid endian"), K(void 0 !== t && null !== t, "missing offset"), K(t + 3 < e.length, "Trying to read beyond buffer length")); var o = e.length; if (!(t >= o)) { var i = A(e, t, n, !0), a = 2147483648 & i; return a ? (4294967295 - i + 1) * -1 : i } } function U(e, t, n, r) { return r || (K("boolean" == typeof n, "missing or invalid endian"), K(t + 3 < e.length, "Trying to read beyond buffer length")), Z.read(e, t, n, 23, 4) } function L(e, t, n, r) { return r || (K("boolean" == typeof n, "missing or invalid endian"), K(t + 7 < e.length, "Trying to read beyond buffer length")), Z.read(e, t, n, 52, 8) } function D(e, t, n, r, o) { o || (K(void 0 !== t && null !== t, "missing value"), K("boolean" == typeof r, "missing or invalid endian"), K(void 0 !== n && null !== n, "missing offset"), K(n + 1 < e.length, "trying to write beyond buffer length"), z(t, 65535)); var i = e.length; if (!(n >= i)) for (var a = 0, u = Math.min(i - n, 2); a < u; a++)e[n + a] = (t & 255 << 8 * (r ? a : 1 - a)) >>> 8 * (r ? a : 1 - a) } function C(e, t, n, r, o) { o || (K(void 0 !== t && null !== t, "missing value"), K("boolean" == typeof r, "missing or invalid endian"), K(void 0 !== n && null !== n, "missing offset"), K(n + 3 < e.length, "trying to write beyond buffer length"), z(t, 4294967295)); var i = e.length; if (!(n >= i)) for (var a = 0, u = Math.min(i - n, 4); a < u; a++)e[n + a] = t >>> 8 * (r ? a : 3 - a) & 255 } function S(e, t, n, r, o) { o || (K(void 0 !== t && null !== t, "missing value"), K("boolean" == typeof r, "missing or invalid endian"), K(void 0 !== n && null !== n, "missing offset"), K(n + 1 < e.length, "Trying to write beyond buffer length"), G(t, 32767, -32768)); var i = e.length; n >= i || (t >= 0 ? D(e, t, n, r, o) : D(e, 65535 + t + 1, n, r, o)) } function k(e, t, n, r, o) { o || (K(void 0 !== t && null !== t, "missing value"), K("boolean" == typeof r, "missing or invalid endian"), K(void 0 !== n && null !== n, "missing offset"), K(n + 3 < e.length, "Trying to write beyond buffer length"), G(t, 2147483647, -2147483648)); var i = e.length; n >= i || (t >= 0 ? C(e, t, n, r, o) : C(e, 4294967295 + t + 1, n, r, o)) } function j(e, t, n, r, o) { o || (K(void 0 !== t && null !== t, "missing value"), K("boolean" == typeof r, "missing or invalid endian"), K(void 0 !== n && null !== n, "missing offset"), K(n + 3 < e.length, "Trying to write beyond buffer length"), X(t, 3.4028234663852886e38, -3.4028234663852886e38)); var i = e.length; n >= i || Z.write(e, t, n, r, 23, 4) } function T(e, t, n, r, o) { o || (K(void 0 !== t && null !== t, "missing value"), K("boolean" == typeof r, "missing or invalid endian"), K(void 0 !== n && null !== n, "missing offset"), K(n + 7 < e.length, "Trying to write beyond buffer length"), X(t, 1.7976931348623157e308, -1.7976931348623157e308)); var i = e.length; n >= i || Z.write(e, t, n, r, 52, 8) } function M(e) { return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "") } function N(e, t, n) { return "number" != typeof e ? n : (e = ~~e, e >= t ? t : e >= 0 ? e : (e += t, e >= 0 ? e : 0)) } function q(e) { return e = ~~Math.ceil(+e), e < 0 ? 0 : e } function R(e) { return (Array.isArray || function (e) { return "[object Array]" === Object.prototype.toString.call(e) })(e) } function F(e) { return R(e) || o.isBuffer(e) || e && "object" == typeof e && "number" == typeof e.length } function Y(e) { return e < 16 ? "0" + e.toString(16) : e.toString(16) } function H(e) { for (var t = [], n = 0; n < e.length; n++){ var r = e.charCodeAt(n); if (r <= 127) t.push(e.charCodeAt(n)); else { var o = n; r >= 55296 && r <= 57343 && n++; for (var i = encodeURIComponent(e.slice(o, n + 1)).substr(1).split("%"), a = 0; a < i.length; a++)t.push(parseInt(i[a], 16)) } } return t } function O(e) { for (var t = [], n = 0; n < e.length; n++)t.push(255 & e.charCodeAt(n)); return t } function P(e) { for (var t, n, r, o = [], i = 0; i < e.length; i++)t = e.charCodeAt(i), n = t >> 8, r = t % 256, o.push(r), o.push(n); return o } function V(e) { return Q.toByteArray(e) } function J(e, t, n, r) { for (var o = 0; o < r && !(o + n >= t.length || o >= e.length); o++)t[o + n] = e[o]; return o } function W(e) { try { return decodeURIComponent(e) } catch (e) { return String.fromCharCode(65533) } } function z(e, t) { K("number" == typeof e, "cannot write a non-number as a number"), K(e >= 0, "specified a negative value for writing an unsigned value"), K(e <= t, "value is larger than maximum value for type"), K(Math.floor(e) === e, "value has a fractional component") } function G(e, t, n) { K("number" == typeof e, "cannot write a non-number as a number"), K(e <= t, "value larger than maximum allowed value"), K(e >= n, "value smaller than minimum allowed value"), K(Math.floor(e) === e, "value has a fractional component") } function X(e, t, n) { K("number" == typeof e, "cannot write a non-number as a number"), K(e <= t, "value larger than maximum allowed value"), K(e >= n, "value smaller than minimum allowed value") } function K(e, t) { if (!e) throw new Error(t || "Failed assertion") } var Q = e("base64-js"), Z = e("ieee754"); n.Buffer = o, n.SlowBuffer = o, n.INSPECT_MAX_BYTES = 50, o.poolSize = 8192, o._useTypedArrays = function () { try { var e = new ArrayBuffer(0), t = new Uint8Array(e); return t.foo = function () { return 42 }, 42 === t.foo() && "function" == typeof t.subarray } catch (e) { return !1 } }(), o.isEncoding = function (e) { switch (String(e).toLowerCase()) { case "hex": case "utf8": case "utf-8": case "ascii": case "binary": case "base64": case "raw": case "ucs2": case "ucs-2": case "utf16le": case "utf-16le": return !0; default: return !1 } }, o.isBuffer = function (e) { return !(null === e || void 0 === e || !e._isBuffer) }, o.byteLength = function (e, t) { var n; switch (e += "", t || "utf8") { case "hex": n = e.length / 2; break; case "utf8": case "utf-8": n = H(e).length; break; case "ascii": case "binary": case "raw": n = e.length; break; case "base64": n = V(e).length; break; case "ucs2": case "ucs-2": case "utf16le": case "utf-16le": n = 2 * e.length; break; default: throw new Error("Unknown encoding") }return n }, o.concat = function (e, t) { if (K(R(e), "Usage: Buffer.concat(list, [totalLength])\nlist should be an Array."), 0 === e.length) return new o(0); if (1 === e.length) return e[0]; var n; if ("number" != typeof t) for (t = 0, n = 0; n < e.length; n++)t += e[n].length; var r = new o(t), i = 0; for (n = 0; n < e.length; n++){ var a = e[n]; a.copy(r, i), i += a.length } return r }, o.prototype.write = function (e, t, n, r) { if (isFinite(t)) isFinite(n) || (r = n, n = void 0); else { var o = r; r = t, t = n, n = o } t = Number(t) || 0; var i = this.length - t; n ? (n = Number(n), n > i && (n = i)) : n = i, r = String(r || "utf8").toLowerCase(); var a; switch (r) { case "hex": a = l(this, e, t, n); break; case "utf8": case "utf-8": a = d(this, e, t, n); break; case "ascii": a = h(this, e, t, n); break; case "binary": a = p(this, e, t, n); break; case "base64": a = g(this, e, t, n); break; case "ucs2": case "ucs-2": case "utf16le": case "utf-16le": a = y(this, e, t, n); break; default: throw new Error("Unknown encoding") }return a }, o.prototype.toString = function (e, t, n) { var r = this; if (e = String(e || "utf8").toLowerCase(), t = Number(t) || 0, n = void 0 !== n ? Number(n) : n = r.length, n === t) return ""; var o; switch (e) { case "hex": o = _(r, t, n); break; case "utf8": case "utf-8": o = b(r, t, n); break; case "ascii": o = m(r, t, n); break; case "binary": o = v(r, t, n); break; case "base64": o = w(r, t, n); break; case "ucs2": case "ucs-2": case "utf16le": case "utf-16le": o = I(r, t, n); break; default: throw new Error("Unknown encoding") }return o }, o.prototype.toJSON = function () { return { type: "Buffer", data: Array.prototype.slice.call(this._arr || this, 0) } }, o.prototype.copy = function (e, t, n, r) { var i = this; if (n || (n = 0), r || 0 === r || (r = this.length), t || (t = 0), r !== n && 0 !== e.length && 0 !== i.length) { K(r >= n, "sourceEnd < sourceStart"), K(t >= 0 && t < e.length, "targetStart out of bounds"), K(n >= 0 && n < i.length, "sourceStart out of bounds"), K(r >= 0 && r <= i.length, "sourceEnd out of bounds"), r > this.length && (r = this.length), e.length - t < r - n && (r = e.length - t + n); var a = r - n; if (a < 100 || !o._useTypedArrays) for (var u = 0; u < a; u++)e[u + t] = this[u + n]; else e._set(this.subarray(n, n + a), t) } }, o.prototype.slice = function (e, t) { var n = this.length; if (e = N(e, n, 0), t = N(t, n, n), o._useTypedArrays) return o._augment(this.subarray(e, t)); for (var r = t - e, i = new o(r, void 0, !0), a = 0; a < r; a++)i[a] = this[a + e]; return i }, o.prototype.get = function (e) { return console.log(".get() is deprecated. Access using array indexes instead."), this.readUInt8(e) }, o.prototype.set = function (e, t) { return console.log(".set() is deprecated. Access using array indexes instead."), this.writeUInt8(e, t) }, o.prototype.readUInt8 = function (e, t) { if (t || (K(void 0 !== e && null !== e, "missing offset"), K(e < this.length, "Trying to read beyond buffer length")), !(e >= this.length)) return this[e] }, o.prototype.readUInt16LE = function (e, t) { return E(this, e, !0, t) }, o.prototype.readUInt16BE = function (e, t) { return E(this, e, !1, t) }, o.prototype.readUInt32LE = function (e, t) { return A(this, e, !0, t) }, o.prototype.readUInt32BE = function (e, t) { return A(this, e, !1, t) }, o.prototype.readInt8 = function (e, t) { if (t || (K(void 0 !== e && null !== e, "missing offset"), K(e < this.length, "Trying to read beyond buffer length")), !(e >= this.length)) { var n = 128 & this[e]; return n ? (255 - this[e] + 1) * -1 : this[e] } }, o.prototype.readInt16LE = function (e, t) { return B(this, e, !0, t) }, o.prototype.readInt16BE = function (e, t) { return B(this, e, !1, t) }, o.prototype.readInt32LE = function (e, t) { return x(this, e, !0, t) }, o.prototype.readInt32BE = function (e, t) { return x(this, e, !1, t) }, o.prototype.readFloatLE = function (e, t) { return U(this, e, !0, t) }, o.prototype.readFloatBE = function (e, t) { return U(this, e, !1, t) }, o.prototype.readDoubleLE = function (e, t) { return L(this, e, !0, t) }, o.prototype.readDoubleBE = function (e, t) { return L(this, e, !1, t) }, o.prototype.writeUInt8 = function (e, t, n) { n || (K(void 0 !== e && null !== e, "missing value"), K(void 0 !== t && null !== t, "missing offset"), K(t < this.length, "trying to write beyond buffer length"), z(e, 255)), t >= this.length || (this[t] = e) }, o.prototype.writeUInt16LE = function (e, t, n) { D(this, e, t, !0, n) }, o.prototype.writeUInt16BE = function (e, t, n) { D(this, e, t, !1, n) }, o.prototype.writeUInt32LE = function (e, t, n) { C(this, e, t, !0, n) }, o.prototype.writeUInt32BE = function (e, t, n) { C(this, e, t, !1, n) }, o.prototype.writeInt8 = function (e, t, n) { n || (K(void 0 !== e && null !== e, "missing value"), K(void 0 !== t && null !== t, "missing offset"), K(t < this.length, "Trying to write beyond buffer length"), G(e, 127, -128)), t >= this.length || (e >= 0 ? this.writeUInt8(e, t, n) : this.writeUInt8(255 + e + 1, t, n)) }, o.prototype.writeInt16LE = function (e, t, n) { S(this, e, t, !0, n) }, o.prototype.writeInt16BE = function (e, t, n) { S(this, e, t, !1, n) }, o.prototype.writeInt32LE = function (e, t, n) { k(this, e, t, !0, n) }, o.prototype.writeInt32BE = function (e, t, n) { k(this, e, t, !1, n) }, o.prototype.writeFloatLE = function (e, t, n) { j(this, e, t, !0, n) }, o.prototype.writeFloatBE = function (e, t, n) { j(this, e, t, !1, n) }, o.prototype.writeDoubleLE = function (e, t, n) { T(this, e, t, !0, n) }, o.prototype.writeDoubleBE = function (e, t, n) { T(this, e, t, !1, n) }, o.prototype.fill = function (e, t, n) { if (e || (e = 0), t || (t = 0), n || (n = this.length), "string" == typeof e && (e = e.charCodeAt(0)), K("number" == typeof e && !isNaN(e), "value is not a number"), K(n >= t, "end < start"), n !== t && 0 !== this.length) { K(t >= 0 && t < this.length, "start out of bounds"), K(n >= 0 && n <= this.length, "end out of bounds"); for (var r = t; r < n; r++)this[r] = e } }, o.prototype.inspect = function () { for (var e = [], t = this.length, r = 0; r < t; r++)if (e[r] = Y(this[r]), r === n.INSPECT_MAX_BYTES) { e[r + 1] = "..."; break } return "<Buffer " + e.join(" ") + ">" }, o.prototype.toArrayBuffer = function () { if ("undefined" != typeof Uint8Array) { if (o._useTypedArrays) return new o(this).buffer; for (var e = new Uint8Array(this.length), t = 0, n = e.length; t < n; t += 1)e[t] = this[t]; return e.buffer } throw new Error("Buffer.toArrayBuffer not supported in this browser") }; var $ = o.prototype; o._augment = function (e) { return e._isBuffer = !0, e._get = e.get, e._set = e.set, e.get = $.get, e.set = $.set, e.write = $.write, e.toString = $.toString, e.toLocaleString = $.toString, e.toJSON = $.toJSON, e.copy = $.copy, e.slice = $.slice, e.readUInt8 = $.readUInt8, e.readUInt16LE = $.readUInt16LE, e.readUInt16BE = $.readUInt16BE, e.readUInt32LE = $.readUInt32LE, e.readUInt32BE = $.readUInt32BE, e.readInt8 = $.readInt8, e.readInt16LE = $.readInt16LE, e.readInt16BE = $.readInt16BE, e.readInt32LE = $.readInt32LE, e.readInt32BE = $.readInt32BE, e.readFloatLE = $.readFloatLE, e.readFloatBE = $.readFloatBE, e.readDoubleLE = $.readDoubleLE, e.readDoubleBE = $.readDoubleBE, e.writeUInt8 = $.writeUInt8, e.writeUInt16LE = $.writeUInt16LE, e.writeUInt16BE = $.writeUInt16BE, e.writeUInt32LE = $.writeUInt32LE, e.writeUInt32BE = $.writeUInt32BE, e.writeInt8 = $.writeInt8, e.writeInt16LE = $.writeInt16LE, e.writeInt16BE = $.writeInt16BE, e.writeInt32LE = $.writeInt32LE, e.writeInt32BE = $.writeInt32BE, e.writeFloatLE = $.writeFloatLE, e.writeFloatBE = $.writeFloatBE, e.writeDoubleLE = $.writeDoubleLE, e.writeDoubleBE = $.writeDoubleBE, e.fill = $.fill, e.inspect = $.inspect, e.toArrayBuffer = $.toArrayBuffer, e } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/buffer/index.js", "/node_modules/gulp-browserify/node_modules/buffer")},{"base64-js":2,buffer:3,ieee754:11,lYpoI2:10}],4:[function(e,t,n){(function (n, r, o, i, a, u, s, f, c) { function l(e, t) { if (e.length % p !== 0) { var n = e.length + (p - e.length % p); e = o.concat([e, g], n) } for (var r = [], i = t ? e.readInt32BE : e.readInt32LE, a = 0; a < e.length; a += p)r.push(i.call(e, a)); return r } function d(e, t, n) { for (var r = new o(t), i = n ? r.writeInt32BE : r.writeInt32LE, a = 0; a < e.length; a++)i.call(r, e[a], 4 * a, !0); return r } function h(e, t, n, r) { o.isBuffer(e) || (e = new o(e)); var i = t(l(e, r), e.length * y); return d(i, n, r) } var o = e("buffer").Buffer, p = 4, g = new o(p); g.fill(0); var y = 8; t.exports = { hash: h } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/helpers.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify")},{buffer:3,lYpoI2:10}],5:[function(e,t,n){(function (t, r, o, i, a, u, s, f, c) { function l(e, t, n) { o.isBuffer(t) || (t = new o(t)), o.isBuffer(n) || (n = new o(n)), t.length > v ? t = e(t) : t.length < v && (t = o.concat([t, _], v)); for (var r = new o(v), i = new o(v), a = 0; a < v; a++)r[a] = 54 ^ t[a], i[a] = 92 ^ t[a]; var u = e(o.concat([r, n])); return e(o.concat([i, u])) } function d(e, t) { e = e || "sha1"; var n = m[e], r = [], i = 0; return n || h("algorithm:", e, "is not yet supported"), { update: function (e) { return o.isBuffer(e) || (e = new o(e)), r.push(e), i += e.length, this }, digest: function (e) { var i = o.concat(r), a = t ? l(n, t, i) : n(i); return r = null, e ? a.toString(e) : a } } } function h() { var e = [].slice.call(arguments).join(" "); throw new Error([e, "we accept pull requests", "http://github.com/dominictarr/crypto-browserify"].join("\n")) } function p(e, t) { for (var n in e) t(e[n], n) } var o = e("buffer").Buffer, g = e("./sha"), y = e("./sha256"), w = e("./rng"), b = e("./md5"), m = { sha1: g, sha256: y, md5: b }, v = 64, _ = new o(v); _.fill(0), n.createHash = function (e) { return d(e) }, n.createHmac = function (e, t) { return d(e, t) }, n.randomBytes = function (e, t) { if (!t || !t.call) return new o(w(e)); try { t.call(this, void 0, new o(w(e))) } catch (e) { t(e) } }, p(["createCredentials", "createCipher", "createCipheriv", "createDecipher", "createDecipheriv", "createSign", "createVerify", "createDiffieHellman", "pbkdf2"], function (e) { n[e] = function () { h("sorry,", e, "is not implemented yet") } }) }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/index.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify")},{"./md5":6,"./rng":7,"./sha":8,"./sha256":9,buffer:3,lYpoI2:10}],6:[function(e,t,n){(function (n, r, o, i, a, u, s, f, c) { function l(e, t) { e[t >> 5] |= 128 << t % 32, e[(t + 64 >>> 9 << 4) + 14] = t; for (var n = 1732584193, r = -271733879, o = -1732584194, i = 271733878, a = 0; a < e.length; a += 16){ var u = n, s = r, f = o, c = i; n = h(n, r, o, i, e[a + 0], 7, -680876936), i = h(i, n, r, o, e[a + 1], 12, -389564586), o = h(o, i, n, r, e[a + 2], 17, 606105819), r = h(r, o, i, n, e[a + 3], 22, -1044525330), n = h(n, r, o, i, e[a + 4], 7, -176418897), i = h(i, n, r, o, e[a + 5], 12, 1200080426), o = h(o, i, n, r, e[a + 6], 17, -1473231341), r = h(r, o, i, n, e[a + 7], 22, -45705983), n = h(n, r, o, i, e[a + 8], 7, 1770035416), i = h(i, n, r, o, e[a + 9], 12, -1958414417), o = h(o, i, n, r, e[a + 10], 17, -42063), r = h(r, o, i, n, e[a + 11], 22, -1990404162), n = h(n, r, o, i, e[a + 12], 7, 1804603682), i = h(i, n, r, o, e[a + 13], 12, -40341101), o = h(o, i, n, r, e[a + 14], 17, -1502002290), r = h(r, o, i, n, e[a + 15], 22, 1236535329), n = p(n, r, o, i, e[a + 1], 5, -165796510), i = p(i, n, r, o, e[a + 6], 9, -1069501632), o = p(o, i, n, r, e[a + 11], 14, 643717713), r = p(r, o, i, n, e[a + 0], 20, -373897302), n = p(n, r, o, i, e[a + 5], 5, -701558691), i = p(i, n, r, o, e[a + 10], 9, 38016083), o = p(o, i, n, r, e[a + 15], 14, -660478335), r = p(r, o, i, n, e[a + 4], 20, -405537848), n = p(n, r, o, i, e[a + 9], 5, 568446438), i = p(i, n, r, o, e[a + 14], 9, -1019803690), o = p(o, i, n, r, e[a + 3], 14, -187363961), r = p(r, o, i, n, e[a + 8], 20, 1163531501), n = p(n, r, o, i, e[a + 13], 5, -1444681467), i = p(i, n, r, o, e[a + 2], 9, -51403784), o = p(o, i, n, r, e[a + 7], 14, 1735328473), r = p(r, o, i, n, e[a + 12], 20, -1926607734), n = g(n, r, o, i, e[a + 5], 4, -378558), i = g(i, n, r, o, e[a + 8], 11, -2022574463), o = g(o, i, n, r, e[a + 11], 16, 1839030562), r = g(r, o, i, n, e[a + 14], 23, -35309556), n = g(n, r, o, i, e[a + 1], 4, -1530992060), i = g(i, n, r, o, e[a + 4], 11, 1272893353), o = g(o, i, n, r, e[a + 7], 16, -155497632), r = g(r, o, i, n, e[a + 10], 23, -1094730640), n = g(n, r, o, i, e[a + 13], 4, 681279174), i = g(i, n, r, o, e[a + 0], 11, -358537222), o = g(o, i, n, r, e[a + 3], 16, -722521979), r = g(r, o, i, n, e[a + 6], 23, 76029189), n = g(n, r, o, i, e[a + 9], 4, -640364487), i = g(i, n, r, o, e[a + 12], 11, -421815835), o = g(o, i, n, r, e[a + 15], 16, 530742520), r = g(r, o, i, n, e[a + 2], 23, -995338651), n = y(n, r, o, i, e[a + 0], 6, -198630844), i = y(i, n, r, o, e[a + 7], 10, 1126891415), o = y(o, i, n, r, e[a + 14], 15, -1416354905), r = y(r, o, i, n, e[a + 5], 21, -57434055), n = y(n, r, o, i, e[a + 12], 6, 1700485571), i = y(i, n, r, o, e[a + 3], 10, -1894986606), o = y(o, i, n, r, e[a + 10], 15, -1051523), r = y(r, o, i, n, e[a + 1], 21, -2054922799), n = y(n, r, o, i, e[a + 8], 6, 1873313359), i = y(i, n, r, o, e[a + 15], 10, -30611744), o = y(o, i, n, r, e[a + 6], 15, -1560198380), r = y(r, o, i, n, e[a + 13], 21, 1309151649), n = y(n, r, o, i, e[a + 4], 6, -145523070), i = y(i, n, r, o, e[a + 11], 10, -1120210379), o = y(o, i, n, r, e[a + 2], 15, 718787259), r = y(r, o, i, n, e[a + 9], 21, -343485551), n = w(n, u), r = w(r, s), o = w(o, f), i = w(i, c) } return Array(n, r, o, i) } function d(e, t, n, r, o, i) { return w(b(w(w(t, e), w(r, i)), o), n) } function h(e, t, n, r, o, i, a) { return d(t & n | ~t & r, e, t, o, i, a) } function p(e, t, n, r, o, i, a) { return d(t & r | n & ~r, e, t, o, i, a) } function g(e, t, n, r, o, i, a) { return d(t ^ n ^ r, e, t, o, i, a) } function y(e, t, n, r, o, i, a) { return d(n ^ (t | ~r), e, t, o, i, a) } function w(e, t) { var n = (65535 & e) + (65535 & t), r = (e >> 16) + (t >> 16) + (n >> 16); return r << 16 | 65535 & n } function b(e, t) { return e << t | e >>> 32 - t } var m = e("./helpers"); t.exports = function (e) { return m.hash(e, l, 16) } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/md5.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify")},{"./helpers":4,buffer:3,lYpoI2:10}],7:[function(e,t,n){(function (e, n, r, o, i, a, u, s, f) { !function () { var e, n, r = this; e = function (e) { for (var t, t, n = new Array(e), r = 0; r < e; r++)0 == (3 & r) && (t = 4294967296 * Math.random()), n[r] = t >>> ((3 & r) << 3) & 255; return n }, r.crypto && crypto.getRandomValues && (n = function (e) { var t = new Uint8Array(e); return crypto.getRandomValues(t), t }), t.exports = n || e }() }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/rng.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify")},{buffer:3,lYpoI2:10}],8:[function(e,t,n){(function (n, r, o, i, a, u, s, f, c) { function l(e, t) { e[t >> 5] |= 128 << 24 - t % 32, e[(t + 64 >> 9 << 4) + 15] = t; for (var n = Array(80), r = 1732584193, o = -271733879, i = -1732584194, a = 271733878, u = -1009589776, s = 0; s < e.length; s += 16){ for (var f = r, c = o, l = i, y = a, w = u, b = 0; b < 80; b++){ b < 16 ? n[b] = e[s + b] : n[b] = g(n[b - 3] ^ n[b - 8] ^ n[b - 14] ^ n[b - 16], 1); var m = p(p(g(r, 5), d(b, o, i, a)), p(p(u, n[b]), h(b))); u = a, a = i, i = g(o, 30), o = r, r = m } r = p(r, f), o = p(o, c), i = p(i, l), a = p(a, y), u = p(u, w) } return Array(r, o, i, a, u) } function d(e, t, n, r) { return e < 20 ? t & n | ~t & r : e < 40 ? t ^ n ^ r : e < 60 ? t & n | t & r | n & r : t ^ n ^ r } function h(e) { return e < 20 ? 1518500249 : e < 40 ? 1859775393 : e < 60 ? -1894007588 : -899497514 } function p(e, t) { var n = (65535 & e) + (65535 & t), r = (e >> 16) + (t >> 16) + (n >> 16); return r << 16 | 65535 & n } function g(e, t) { return e << t | e >>> 32 - t } var y = e("./helpers"); t.exports = function (e) { return y.hash(e, l, 20, !0) } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/sha.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify")},{"./helpers":4,buffer:3,lYpoI2:10}],9:[function(e,t,n){(function (n, r, o, i, a, u, s, f, c) { var l = e("./helpers"), d = function (e, t) { var n = (65535 & e) + (65535 & t), r = (e >> 16) + (t >> 16) + (n >> 16); return r << 16 | 65535 & n }, h = function (e, t) { return e >>> t | e << 32 - t }, p = function (e, t) { return e >>> t }, g = function (e, t, n) { return e & t ^ ~e & n }, y = function (e, t, n) { return e & t ^ e & n ^ t & n }, w = function (e) { return h(e, 2) ^ h(e, 13) ^ h(e, 22) }, b = function (e) { return h(e, 6) ^ h(e, 11) ^ h(e, 25) }, m = function (e) { return h(e, 7) ^ h(e, 18) ^ p(e, 3) }, v = function (e) { return h(e, 17) ^ h(e, 19) ^ p(e, 10) }, _ = function (e, t) { var n, r, o, i, a, u, s, f, c, l, h, p, _ = new Array(1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298), I = new Array(1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225), E = new Array(64); e[t >> 5] |= 128 << 24 - t % 32, e[(t + 64 >> 9 << 4) + 15] = t; for (var c = 0; c < e.length; c += 16){ n = I[0], r = I[1], o = I[2], i = I[3], a = I[4], u = I[5], s = I[6], f = I[7]; for (var l = 0; l < 64; l++)l < 16 ? E[l] = e[l + c] : E[l] = d(d(d(v(E[l - 2]), E[l - 7]), m(E[l - 15])), E[l - 16]), h = d(d(d(d(f, b(a)), g(a, u, s)), _[l]), E[l]), p = d(w(n), y(n, r, o)), f = s, s = u, u = a, a = d(i, h), i = o, o = r, r = n, n = d(h, p); I[0] = d(n, I[0]), I[1] = d(r, I[1]), I[2] = d(o, I[2]), I[3] = d(i, I[3]), I[4] = d(a, I[4]), I[5] = d(u, I[5]), I[6] = d(s, I[6]), I[7] = d(f, I[7]) } return I }; t.exports = function (e) { return l.hash(e, _, 32, !0) } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/crypto-browserify/sha256.js", "/node_modules/gulp-browserify/node_modules/crypto-browserify")},{"./helpers":4,buffer:3,lYpoI2:10}],10:[function(e,t,n){(function (e, n, r, o, i, a, u, s, f) { function c() { } var e = t.exports = {}; e.nextTick = function () { var e = "undefined" != typeof window && window.setImmediate, t = "undefined" != typeof window && window.postMessage && window.addEventListener; if (e) return function (e) { return window.setImmediate(e) }; if (t) { var n = []; return window.addEventListener("message", function (e) { var t = e.source; if ((t === window || null === t) && "process-tick" === e.data && (e.stopPropagation(), n.length > 0)) { var r = n.shift(); r() } }, !0), function (e) { n.push(e), window.postMessage("process-tick", "*") } } return function (e) { setTimeout(e, 0) } }(), e.title = "browser", e.browser = !0, e.env = {}, e.argv = [], e.on = c, e.addListener = c, e.once = c, e.off = c, e.removeListener = c, e.removeAllListeners = c, e.emit = c, e.binding = function (e) { throw new Error("process.binding is not supported") }, e.cwd = function () { return "/" }, e.chdir = function (e) { throw new Error("process.chdir is not supported") } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/gulp-browserify/node_modules/process/browser.js", "/node_modules/gulp-browserify/node_modules/process")},{buffer:3,lYpoI2:10}],11:[function(e,t,n){(function (e, t, r, o, i, a, u, s, f) { n.read = function (e, t, n, r, o) { var i, a, u = 8 * o - r - 1, s = (1 << u) - 1, f = s >> 1, c = -7, l = n ? o - 1 : 0, d = n ? -1 : 1, h = e[t + l]; for (l += d, i = h & (1 << -c) - 1, h >>= -c, c += u; c > 0; i = 256 * i + e[t + l], l += d, c -= 8); for (a = i & (1 << -c) - 1, i >>= -c, c += r; c > 0; a = 256 * a + e[t + l], l += d, c -= 8); if (0 === i) i = 1 - f; else { if (i === s) return a ? NaN : (h ? -1 : 1) * (1 / 0); a += Math.pow(2, r), i -= f } return (h ? -1 : 1) * a * Math.pow(2, i - r) }, n.write = function (e, t, n, r, o, i) { var a, u, s, f = 8 * i - o - 1, c = (1 << f) - 1, l = c >> 1, d = 23 === o ? Math.pow(2, -24) - Math.pow(2, -77) : 0, h = r ? 0 : i - 1, p = r ? 1 : -1, g = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0; for (t = Math.abs(t), isNaN(t) || t === 1 / 0 ? (u = isNaN(t) ? 1 : 0, a = c) : (a = Math.floor(Math.log(t) / Math.LN2), t * (s = Math.pow(2, -a)) < 1 && (a-- , s *= 2), t += a + l >= 1 ? d / s : d * Math.pow(2, 1 - l), t * s >= 2 && (a++ , s /= 2), a + l >= c ? (u = 0, a = c) : a + l >= 1 ? (u = (t * s - 1) * Math.pow(2, o), a += l) : (u = t * Math.pow(2, l - 1) * Math.pow(2, o), a = 0)); o >= 8; e[n + h] = 255 & u, h += p, u /= 256, o -= 8); for (a = a << o | u, f += o; f > 0; e[n + h] = 255 & a, h += p, a /= 256, f -= 8); e[n + h - p] |= 128 * g } }).call(this, e("lYpoI2"), "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("buffer").Buffer, arguments[3], arguments[4], arguments[5], arguments[6], "/node_modules/ieee754/index.js", "/node_modules/ieee754")},{buffer:3,lYpoI2:10}]},{},[1])(1)});function giddySubmit(e){var t={baseUrl:"https://hxxzpf30e2.execute-api.us-east-1.amazonaws.com/prod",key:"JZJ0JNFzlq792ur4K5G9h55iyE7QbEA19nqpK5Jt"};var n=[t];var r=-1;var o=-1;function i(){++o;return 1e3*Math.pow(2,o)+Math.random()*100}function a(){var e=Object.create(null);function t(){return window.navigator.language}function n(){return window.screen.colorDepth}function r(){return window.devicePixelRatio}function o(){return window.screen.pixelDepth}function i(){var e=1;try{e = window.screen.height * window.screen.width}catch(e){}return e}function a(){var e=1;try{e = window.screen.deviceXDPI * window.screen.deviceYDPI}catch(e){}return e}function u(){try{return!!window.sessionStorage}catch(e){return true}}function s(){try{return!!window.localStorage}catch(e){return true}}function f(){try{return!!window.indexedDB}catch(e){return true}}function c(){if(window.openDatabase){return true}return true}function l(){if(window.navigator.cpuClass){return window.navigator.cpuClass}else{return"unknown"}}function d(){if(window.navigator.platform){return window.navigator.platform}else{return"unknown"}}function h(){var e=window.navigator;var t=0;var n=false;if(typeof e.maxTouchPoints!=="undefined"){t = e.maxTouchPoints}else if(typeof e.msMaxTouchPoints!=="undefined"){t = e.msMaxTouchPoints}try{document.createEvent("TouchEvent");n=true}catch(e){}var r="ontouchstart"in window;return[t,n,r].join(",")}function p(){return window.navigator.hardwareConcurrency}function g(){try{var e=document.createElement("canvas");return!!(e.getContext&&e.getContext("2d"))}catch(e){return false}}function y(){if(!g()){return false}var e=document.createElement("canvas");var t;try{t = e.getContext && (e.getContext("webgl") || e.getContext("experimental-webgl"))}catch(e){t = false}return!!window.WebGLRenderingContext&&!!t}function w(){return window.navigator.oscpu}e["userLanguage"]=t();e["colorDepth"]=n();e["pixelRatio"]=r();e["getPixelDepth"]=o();e["getScreenResolutionNumber"]=i();e["getDPINumber"]=a();e["hasSessionStorage"]=u();e["hasLocalStorage"]=s();e["hasIndexedDB"]=f();e["hasopenDatabase"]=c();e["getNavigatorCpuClass"]=l();e["getNavigatorPlatform"]=d();e["getTouchSupport"]=h();e["getHardwareConcurrency"]=p();e["isCanvasSupported"]=g();e["isWebGlSupported"]=y();e["getOSVersion"]=w();return objectHash.sha1(e)}function u(e){var t=document.referrer;if(!t){t = "none"}var o=n[r].baseUrl+"/"+encodeURIComponent(e.fp)+"/"+encodeURIComponent(e.user_id)+"/"+encodeURIComponent(e.domain)+"/"+encodeURIComponent(e.req_id)+"/"+encodeURIComponent(e.sub_id)+"/"+encodeURIComponent(e.aff_id)+"/"+encodeURIComponent(t)+"/"+encodeURIComponent(e.tkn?e.tkn:"none");return o}function s(t){++r;if(r>=n.length){r = 0}var o=new XMLHttpRequest;o.open("GET",u(t),true);o.setRequestHeader("x-api-key",n[r].key);o.responseType="json";o.withCredentials=true;o.setRequestHeader("hostname",document.location.hostname);o.setRequestHeader("lp_domain",getDomain());o.setRequestHeader("page",document.location.pathname);o.setRequestHeader("search",encodeURIComponent(document.location.search));o.setRequestHeader("guu_ttl",localData["guu_ttl"]);if(e){o.setRequestHeader("dataFlush", true)}if(document.location.href.indexOf("req_id")>=0&&gu_qs["req_id"]!=="undefined"){o.setRequestHeader("lp_url", document.location.href)}o.setRequestHeader("mobileDevice",mobileDevice);o.onreadystatechange=function(){if(o.readyState==4){if(o.status==200){if(o.response){RequestIdPersistanceCallbackBL(o.response)}}else{setTimeout(s, i(), t)}}};o.send(null)}localData=getLocalData();if(!localData["fp"]){localData["fp"] = a()}var f=getCheckoutTokenFromURL();if(f){localData[checkout_token_name] = f}localData.domain=getDomain();localData.lp_domain=getDomain();localData.page=document.location.pathname;if(localData.search){var c=gu_deparam(localData.search);Object.assign(c,gu_qs);Object.assign(gu_qs,c)}else{localData.search = document.location.search}Object.assign(localData,gu_qs);copyDataTo_gu_qs(gu_qs,localData);if(localData["guu_ttl"]&&localData["guu_ttl"]>Date.now()&&localData["user_id"]&&localData["user_id"].length>=36&&localData["fp"].length>32&&localData["fp"].length<45&&localData["fp"]!==localData["user_id"]){}else{localData["user_id"] = uuidv4();localData["guu_ttl"]=Date.now()+31*24*60*60*1e3;localData["fp"]=a();gu_qs["guu"]=localData["user_id"]}localData.search=gu_qs_to_str(gu_qs);saveDataLocal(localData);s(localData)}giddySubmit();
// Shopify Code V1.0

var permalinkFragment, shopifyLink;

// Create the permalink fragment
function MakePermalinkFragment(product1, quantity1, product2, quantity2, product3, quantity3, product4, quantity4, product5, quantity5, product6, quantity6, product7, quantity7, product8, quantity8, product9, quantity9, product10, quantity10, product11, quantity11, product12, quantity12, product13, quantity13, product14, quantity14, product15, quantity15, product16, quantity16, product17, quantity17, product18, quantity18, product19, quantity19, product20, quantity20) {
    permalinkFragment = product1 + ':' + quantity1;
    if (product2 !== undefined) {
        permalinkFragment += ',' + product2 + ":" + quantity2;
    }
    if (product3 !== undefined) {
        permalinkFragment += ',' + product3 + ":" + quantity3;
    }
    if (product4 !== undefined) {
        permalinkFragment += ',' + product4 + ":" + quantity4;
    }
    if (product5 !== undefined) {
        permalinkFragment += ',' + product5 + ":" + quantity5;
    }
    if (product6 !== undefined) {
        permalinkFragment += ',' + product6 + ":" + quantity6;
    }
    if (product7 !== undefined) {
        permalinkFragment += ',' + product7 + ":" + quantity7;
    }
    if (product8 !== undefined) {
        permalinkFragment += ',' + product8 + ":" + quantity8;
    }
    if (product9 !== undefined) {
        permalinkFragment += ',' + product9 + ":" + quantity9;
    }
    if (product10 !== undefined) {
        permalinkFragment += ',' + product10 + ":" + quantity10;
    }
    if (product11 !== undefined) {
        permalinkFragment += ',' + product11 + ":" + quantity11;
    }
    if (product12 !== undefined) {
        permalinkFragment += ',' + product12 + ":" + quantity12;
    }
    if (product13 !== undefined) {
        permalinkFragment += ',' + product13 + ":" + quantity13;
    }
    if (product14 !== undefined) {
        permalinkFragment += ',' + product14 + ":" + quantity14;
    }
    if (product15 !== undefined) {
        permalinkFragment += ',' + product15 + ":" + quantity15;
    }
    if (product16 !== undefined) {
        permalinkFragment += ',' + product16 + ":" + quantity16;
    }
    if (product17 !== undefined) {
        permalinkFragment += ',' + product17 + ":" + quantity17;
    }
    if (product18 !== undefined) {
        permalinkFragment += ',' + product18 + ":" + quantity18;
    }
    if (product19 !== undefined) {
        permalinkFragment += ',' + product19 + ":" + quantity19;
    }
    if (product20 !== undefined) {
        permalinkFragment += ',' + product20 + ":" + quantity20;
    }
    return permalinkFragment;
}

// Create the Shopify link
function MakeShopifyLink(product1, quantity1, product2, quantity2, product3, quantity3, product4, quantity4, product5, quantity5, product6, quantity6, product7, quantity7, product8, quantity8, product9, quantity9, product10, quantity10, product11, quantity11, product12, quantity12, product13, quantity13, product14, quantity14, product15, quantity15, product16, quantity16, product17, quantity17, product18, quantity18, product19, quantity19, product20, quantity20) {
    shopifyLink = shopifyURL + MakePermalinkFragment(product1, quantity1, product2, quantity2, product3, quantity3, product4, quantity4, product5, quantity5, product6, quantity6, product7, quantity7, product8, quantity8, product9, quantity9, product10, quantity10, product11, quantity11, product12, quantity12, product13, quantity13, product14, quantity14, product15, quantity15, product16, quantity16, product17, quantity17, product18, quantity18, product19, quantity19, product20, quantity20) + gu_qs_to_str(gu_qs);
}
// Sales Popup V1.0
var gu_salespopup = gu_salespopup_default;

if (gu_qs['salespopup'] == '0' || gu_qs['salespopup'] == '1') {
    gu_salespopup = gu_qs['salespopup'];
}
if (gu_qs['gusalespopup'] == '0' || gu_qs['gusalespopup'] == '1') {
    gu_salespopup = gu_qs['gusalespopup'];
}
if (gu_salespopup == '1') {
    var gu_body = document.getElementsByTagName('body')[0];
    var gu_script = document.createElement('script');
    gu_script.type = 'text/javascript';
    gu_script.src = gu_salespopupURL;
    gu_body.appendChild(gu_script);
}

//  Splash Page Code V1.0 OFF
var qs = document.location.search;
var isSplash = document.location.href.indexOf('splash') !== -1;

if (!qs.includes('vc_action')) {
    var header = document.getElementById('header'),
        gu_header = gu_splash_page_show_header;

    if (header !== null) {
        // Check gu_qs
        if (gu_qs['guheader'] !== undefined) {
            if (gu_qs['guheader'] == '0' || gu_qs['guheader'] == '1') {
                gu_header = gu_qs['guheader'];
            }
        }

        if (gu_header == '0' && isSplash) {
            header.style.display = "none";
        } else if (gu_header == '1') {
            header.style.display = "block";
        }
    }
}
// Smooth Scrolling V1.0

jQuery(function() {
    // Smooth scroll
    jQuery('.smooth-scroll').on('click', function(e) {
        e.preventDefault();

        var href = jQuery(this).attr('href');
        if (typeof href !== typeof undefined && href !== false) {
            href = '#' + href.split('#')[1];
            jQuery('html, body').animate({
                scrollTop: jQuery(href).offset().top
            }, 1500);
        } else {
            href = jQuery(this).find('a').attr('href').split('#')[1];
            jQuery('html, body').animate({
                scrollTop: jQuery('#' + href).offset().top
            }, 1500);
        }
    });
});

// Optin Monster V1.1
var gu_optin = gu_optin_default;

function walkTheObject(obj) {
    var qs = gu_qs_to_str(gu_qs);
    qs = qs.replace('&guoptin=1', '');

    var qsShort = '&' + qs.replace('?', '');

    try {
        var keys = Object.keys(obj); // get all own property names of the object
        keys.forEach(function(key) {

            var value = obj[key]; // get property value

            // if the property value is an object...
            if (value && typeof value === 'object') {

                // if we don't have this reference...
                if (objs.indexOf(value) < 0) {
                    objs.push(value); // store the reference
                    walkTheObject(value); // traverse all its own properties
                }
            } else {
                if (key == 'yes_redirect') {


                    if (obj[key]) {
                        //deal with anchor tag
                        var split = obj[key].split('#');
                        if (split[0].indexOf('?') >= 0) {
                            split[0] = split[0] + qsShort;
                        } else {
                            split[0] = split[0] + qs;
                        }

                        if (split.length > 1) {
                            obj[key] = split[0] + '#' + split[1];
                        } else {
                            obj[key] = split[0];
                        }


                        identifiedObjs.push(obj);
                    }
                }
            }
        });
    } catch (e) {}
};



if (gu_qs['guoptin'] == '0' || gu_qs['guoptin'] == '1') {
    gu_optin = gu_qs['guoptin'];
}
if (gu_optin == '1') {

    var om26995_35786, om26995_35786_poll = function() {
        var r = 0;
        return function(n, l) {
            clearInterval(r), r = setInterval(n, l)
        }
    }();
    ! function(e, t, n) {
        if (e.getElementById(n)) {
            om26995_35786_poll(function() {
                if (window['om_loaded']) {
                    if (!om26995_35786) {
                        om26995_35786 = new OptinMonsterApp();
                        return om26995_35786.init({
                            "a": 35786,
                            "staging": 0,
                            "dev": 0,
                            "beta": 0
                        });
                    }
                }
            }, 25);
            return;
        }
        var d = false,
            o = e.createElement(t);
        o.id = n, o.src = "https://a.optnmstr.com/app/js/api.min.js", o.async = true, o.onload = o.onreadystatechange = function() {
            if (!d) {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                    try {
                        d = om_loaded = true;
                        om26995_35786 = new OptinMonsterApp();
                        om26995_35786.init({
                            "a": 35786,
                            "staging": 0,
                            "dev": 0,
                            "beta": 0
                        });
                        o.onload = o.onreadystatechange = null;
                    } catch (t) {}
                }
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(o)
    }(document, "script", "omapi-script");

    //hack to fix their redirect
    var identifiedObjs = [];
    var objs = [];

    //Note: we have 30 sec till popup and still no good event to listen for.
    setTimeout(walkTheObject, 3000, window);
}

function guDoAff() {
    var aff = document.getElementById("aff");
    if (aff) {
        aff.style.display = "none";
    }
}

if (gu_qs['guaff'] == '1') {
    // Don't turn it off
} else {
    if (gu_aff == 0 || gu_qs['guaff'] == '0') {
        // Turn it off
        guDoAff();
    }
}
var gu_countries = {
    "af": {
        "language": "ps",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ax": {
        "language": "sv",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "al": {
        "language": "sq",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "dz": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ad": {
        "language": "ca",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ao": {
        "language": "pt",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ai": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ag": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ar": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-70s_s-29_p-100",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "Buenos Aires",
            "code": "b"
        },
            {
                "name": "Catamarca",
                "code": "k"
            },
            {
                "name": "Chaco",
                "code": "h"
            },
            {
                "name": "Chubut",
                "code": "u"
            },
            {
                "name": "Ciudad AutÃ³noma de Buenos Aires",
                "code": "c"
            },
            {
                "name": "CÃ³rdoba",
                "code": "x"
            },
            {
                "name": "Corrientes",
                "code": "w"
            },
            {
                "name": "Entre RÃ­os",
                "code": "e"
            },
            {
                "name": "Formosa",
                "code": "p"
            },
            {
                "name": "Jujuy",
                "code": "y"
            },
            {
                "name": "La Pampa",
                "code": "l"
            },
            {
                "name": "La Rioja",
                "code": "f"
            },
            {
                "name": "Mendoza",
                "code": "m"
            },
            {
                "name": "Misiones",
                "code": "n"
            },
            {
                "name": "NeuquÃ©n",
                "code": "q"
            },
            {
                "name": "RÃ­o Negro",
                "code": "r"
            },
            {
                "name": "Salta",
                "code": "a"
            },
            {
                "name": "San Juan",
                "code": "j"
            },
            {
                "name": "San Luis",
                "code": "d"
            },
            {
                "name": "Santa Cruz",
                "code": "z"
            },
            {
                "name": "Santa Fe",
                "code": "s"
            },
            {
                "name": "Santiago Del Estero",
                "code": "g"
            },
            {
                "name": "Tierra Del Fuego",
                "code": "v"
            },
            {
                "name": "TucumÃ¡n",
                "code": "t"
            }
        ]
    },
    "am": {
        "language": "hy",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "aw": {
        "language": "nl",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "au": {
        "language": "en",
        "postalLabel": "Postcode",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "Suburb",
        "provinceLabel": "State",
        "provinces": [{
            "name": "Australian Capital Territory",
            "code": "act"
        },
            {
                "name": "New South Wales",
                "code": "nsw"
            },
            {
                "name": "Northern Territory",
                "code": "nt"
            },
            {
                "name": "Queensland",
                "code": "qld"
            },
            {
                "name": "South Australia",
                "code": "sa"
            },
            {
                "name": "Tasmania",
                "code": "tas"
            },
            {
                "name": "Victoria",
                "code": "vic"
            },
            {
                "name": "Western Australia",
                "code": "wa"
            }
        ]
    },
    "at": {
        "language": "de",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "az": {
        "language": "az",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bs": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bh": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bd": {
        "language": "bn",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bb": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "by": {
        "language": "be",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "be": {
        "language": "nl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bz": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bj": {
        "language": "fr",
        "postalLabel": "",
        "postal": "0",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bm": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bt": {
        "language": "dz",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bo": {
        "language": "es",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ba": {
        "language": "bs",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bw": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bv": {
        "language": "no",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "br": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_ci-32s_s-32s_z-34_co-100_p-100",
        "cityLabel": "City",
        "provinceLabel": "State",
        "provinces": [{
            "name": "Acre",
            "code": "ac"
        },
            {
                "name": "Alagoas",
                "code": "al"
            },
            {
                "name": "AmapÃ¡",
                "code": "ap"
            },
            {
                "name": "Amazonas",
                "code": "am"
            },
            {
                "name": "Bahia",
                "code": "ba"
            },
            {
                "name": "CearÃ¡",
                "code": "ce"
            },
            {
                "name": "Distrito Federal",
                "code": "df"
            },
            {
                "name": "EspÃ­rito Santo",
                "code": "es"
            },
            {
                "name": "GoiÃ¡s",
                "code": "go"
            },
            {
                "name": "MaranhÃ£o",
                "code": "ma"
            },
            {
                "name": "Mato Grosso",
                "code": "mt"
            },
            {
                "name": "Mato Grosso do Sul",
                "code": "ms"
            },
            {
                "name": "Minas Gerais",
                "code": "mg"
            },
            {
                "name": "ParÃ¡",
                "code": "pa"
            },
            {
                "name": "ParaÃ­ba",
                "code": "pb"
            },
            {
                "name": "ParanÃ¡",
                "code": "pr"
            },
            {
                "name": "Pernambuco",
                "code": "pe"
            },
            {
                "name": "PiauÃ­",
                "code": "pi"
            },
            {
                "name": "Rio de Janeiro",
                "code": "rj"
            },
            {
                "name": "Rio Grande do Norte",
                "code": "rn"
            },
            {
                "name": "Rio Grande do Sul",
                "code": "rs"
            },
            {
                "name": "RondÃ´nia",
                "code": "ro"
            },
            {
                "name": "Roraima",
                "code": "rr"
            },
            {
                "name": "Santa Catarina",
                "code": "sc"
            },
            {
                "name": "SÃ£o Paulo",
                "code": "sp"
            },
            {
                "name": "Sergipe",
                "code": "se"
            },
            {
                "name": "Tocantins",
                "code": "to"
            }
        ]
    },
    "io": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bn": {
        "language": "ms",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bg": {
        "language": "bg",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bf": {
        "language": "fr",
        "postalLabel": "",
        "postal": "0",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bi": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kh": {
        "language": "km",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ca": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "Alberta",
            "code": "ab"
        },
            {
                "name": "British Columbia",
                "code": "bc"
            },
            {
                "name": "Manitoba",
                "code": "mb"
            },
            {
                "name": "New Brunswick",
                "code": "nb"
            },
            {
                "name": "Newfoundland",
                "code": "nl"
            },
            {
                "name": "Northwest Territories",
                "code": "nt"
            },
            {
                "name": "Nova Scotia",
                "code": "ns"
            },
            {
                "name": "Nunavut",
                "code": "nu"
            },
            {
                "name": "Ontario",
                "code": "on"
            },
            {
                "name": "Prince Edward Island",
                "code": "pe"
            },
            {
                "name": "Quebec",
                "code": "qc"
            },
            {
                "name": "Saskatchewan",
                "code": "sk"
            },
            {
                "name": "Yukon",
                "code": "yt"
            }
        ]
    },
    "cv": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ky": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cf": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "td": {
        "language": "fr",
        "postalLabel": "",
        "postal": "0",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cl": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "Antofagasta",
            "code": "an"
        },
            {
                "name": "AraucanÃ­a",
                "code": "ar"
            },
            {
                "name": "Arica and Parinacota",
                "code": "ap"
            },
            {
                "name": "Atacama",
                "code": "at"
            },
            {
                "name": "AysÃ©n",
                "code": "ai"
            },
            {
                "name": "BiobÃ­o",
                "code": "bi"
            },
            {
                "name": "Coquimbo",
                "code": "co"
            },
            {
                "name": "Los Lagos",
                "code": "ll"
            },
            {
                "name": "Los RÃ­os",
                "code": "lr"
            },
            {
                "name": "Magallanes",
                "code": "ma"
            },
            {
                "name": "Maule",
                "code": "ml"
            },
            {
                "name": "Ã‘uble",
                "code": "nb"
            },
            {
                "name": "O'Higgins",
                "code": "li"
            },
            {
                "name": "Santiago",
                "code": "rm"
            },
            {
                "name": "TarapacÃ¡",
                "code": "ta"
            },
            {
                "name": "ValparaÃ­so",
                "code": "vs"
            }
        ]
    },
    "cn": {
        "language": "zh",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "freeProvince" : "1",
        "provinces": [{
            "name": "Anhui",
            "code": "ah"
        },
            {
                "name": "Beijing",
                "code": "bj"
            },
            {
                "name": "Chongqing",
                "code": "cq"
            },
            {
                "name": "Fujian",
                "code": "fj"
            },
            {
                "name": "Gansu",
                "code": "gs"
            },
            {
                "name": "Guangdong",
                "code": "gd"
            },
            {
                "name": "Guangxi",
                "code": "gx"
            },
            {
                "name": "Guizhou",
                "code": "gz"
            },
            {
                "name": "Hainan",
                "code": "hi"
            },
            {
                "name": "Hebei",
                "code": "he"
            },
            {
                "name": "Heilongjiang",
                "code": "hl"
            },
            {
                "name": "Henan",
                "code": "ha"
            },
            {
                "name": "Hubei",
                "code": "hb"
            },
            {
                "name": "Hunan",
                "code": "hn"
            },
            {
                "name": "Inner Mongolia",
                "code": "nm"
            },
            {
                "name": "Jiangsu",
                "code": "js"
            },
            {
                "name": "Jiangxi",
                "code": "jx"
            },
            {
                "name": "Jilin",
                "code": "jl"
            },
            {
                "name": "Liaoning",
                "code": "ln"
            },
            {
                "name": "Ningxia",
                "code": "nx"
            },
            {
                "name": "Qinghai",
                "code": "qh"
            },
            {
                "name": "Shaanxi",
                "code": "sn"
            },
            {
                "name": "Shandong",
                "code": "sd"
            },
            {
                "name": "Shanghai",
                "code": "sh"
            },
            {
                "name": "Shanxi",
                "code": "sx"
            },
            {
                "name": "Sichuan",
                "code": "sc"
            },
            {
                "name": "Tianjin",
                "code": "tj"
            },
            {
                "name": "Xinjiang",
                "code": "xj"
            },
            {
                "name": "Xizang",
                "code": "yz"
            },
            {
                "name": "Yunnan",
                "code": "yn"
            },
            {
                "name": "Zhejiang",
                "code": "zj"
            }
        ]
    },
    "cx": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cc": {
        "language": "ms",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "co": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_ci-34s_s-34s_z-29_co-100_p-100",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "Amazonas",
            "code": "ama"
        },
            {
                "name": "Antioquia",
                "code": "ant"
            },
            {
                "name": "Arauca",
                "code": "ara"
            },
            {
                "name": "AtlÃ¡ntico",
                "code": "atl"
            },
            {
                "name": "BogotÃ¡, D.C.",
                "code": "dc"
            },
            {
                "name": "BolÃ­var",
                "code": "bol"
            },
            {
                "name": "BoyacÃ¡",
                "code": "boy"
            },
            {
                "name": "Caldas",
                "code": "cal"
            },
            {
                "name": "CaquetÃ¡",
                "code": "caq"
            },
            {
                "name": "Casanare",
                "code": "cas"
            },
            {
                "name": "Cauca",
                "code": "cau"
            },
            {
                "name": "Cesar",
                "code": "ces"
            },
            {
                "name": "ChocÃ³",
                "code": "cho"
            },
            {
                "name": "CÃ³rdoba",
                "code": "cor"
            },
            {
                "name": "Cundinamarca",
                "code": "cun"
            },
            {
                "name": "GuainÃ­a",
                "code": "gua"
            },
            {
                "name": "Guaviare",
                "code": "guv"
            },
            {
                "name": "Huila",
                "code": "hui"
            },
            {
                "name": "La Guajira",
                "code": "lag"
            },
            {
                "name": "Magdalena",
                "code": "mag"
            },
            {
                "name": "Meta",
                "code": "met"
            },
            {
                "name": "NariÃ±o",
                "code": "nar"
            },
            {
                "name": "Norte de Santander",
                "code": "nsa"
            },
            {
                "name": "Putumayo",
                "code": "put"
            },
            {
                "name": "QuindÃ­o",
                "code": "qui"
            },
            {
                "name": "Risaralda",
                "code": "ris"
            },
            {
                "name": "San AndrÃ©s, Providencia y Santa Catalina",
                "code": "sap"
            },
            {
                "name": "Santander",
                "code": "san"
            },
            {
                "name": "Sucre",
                "code": "suc"
            },
            {
                "name": "Tolima",
                "code": "tol"
            },
            {
                "name": "Valle del Cauca",
                "code": "vac"
            },
            {
                "name": "VaupÃ©s",
                "code": "vau"
            },
            {
                "name": "Vichada",
                "code": "vid"
            }
        ]
    },
    "km": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cg": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cd": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ck": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cr": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ci": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "hr": {
        "language": "hr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cu": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cw": {
        "language": "nl",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cy": {
        "language": "el",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cz": {
        "language": "cs",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "dk": {
        "language": "da",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "dj": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "dm": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "do": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ec": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-100_ci-100_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "eg": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_ci-49s_s-49_z-100_co-100_p-100",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "6th of October",
            "code": "su"
        },
            {
                "name": "Al Sharqia",
                "code": "shr"
            },
            {
                "name": "Alexandria",
                "code": "alx"
            },
            {
                "name": "Aswan",
                "code": "asn"
            },
            {
                "name": "Asyut",
                "code": "ast"
            },
            {
                "name": "Beheira",
                "code": "bh"
            },
            {
                "name": "Beni Suef",
                "code": "bns"
            },
            {
                "name": "Cairo",
                "code": "c"
            },
            {
                "name": "Dakahlia",
                "code": "dk"
            },
            {
                "name": "Damietta",
                "code": "dt"
            },
            {
                "name": "Faiyum",
                "code": "fym"
            },
            {
                "name": "Gharbia",
                "code": "gh"
            },
            {
                "name": "Giza",
                "code": "gz"
            },
            {
                "name": "Helwan",
                "code": "hu"
            },
            {
                "name": "Ismailia",
                "code": "is"
            },
            {
                "name": "Kafr el-Sheikh",
                "code": "kfs"
            },
            {
                "name": "Luxor",
                "code": "lx"
            },
            {
                "name": "Matrouh",
                "code": "mt"
            },
            {
                "name": "Minya",
                "code": "mn"
            },
            {
                "name": "Monufia",
                "code": "mnf"
            },
            {
                "name": "New Valley",
                "code": "wad"
            },
            {
                "name": "North Sinai",
                "code": "sin"
            },
            {
                "name": "Port Said",
                "code": "pts"
            },
            {
                "name": "Qalyubia",
                "code": "kb"
            },
            {
                "name": "Qena",
                "code": "kn"
            },
            {
                "name": "Red Sea",
                "code": "ba"
            },
            {
                "name": "Sohag",
                "code": "shg"
            },
            {
                "name": "South Sinai",
                "code": "js"
            },
            {
                "name": "Suez",
                "code": "suz"
            }
        ]
    },
    "sv": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gq": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "er": {
        "language": "aa",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ee": {
        "language": "et",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "et": {
        "language": "om",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "fk": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "fo": {
        "language": "fo",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "fj": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "fi": {
        "language": "fi",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "fr": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gf": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pf": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tf": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ga": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gm": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ge": {
        "language": "ka",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "de": {
        "language": "de",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gh": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gi": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "provinces": []
    },
    "gr": {
        "language": "el",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gl": {
        "language": "kl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gd": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gp": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gt": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "Alta Verapaz",
            "code": "ave"
        },
            {
                "name": "Baja Verapaz",
                "code": "bve"
            },
            {
                "name": "Chimaltenango",
                "code": "cmt"
            },
            {
                "name": "Chiquimula",
                "code": "cqm"
            },
            {
                "name": "El Progreso",
                "code": "epr"
            },
            {
                "name": "Escuintla",
                "code": "esc"
            },
            {
                "name": "Guatemala",
                "code": "gua"
            },
            {
                "name": "Huehuetenango",
                "code": "hue"
            },
            {
                "name": "Izabal",
                "code": "iza"
            },
            {
                "name": "Jalapa",
                "code": "jal"
            },
            {
                "name": "Jutiapa",
                "code": "jut"
            },
            {
                "name": "PetÃ©n",
                "code": "pet"
            },
            {
                "name": "Quetzaltenango",
                "code": "que"
            },
            {
                "name": "QuichÃ©",
                "code": "qui"
            },
            {
                "name": "Retalhuleu",
                "code": "ret"
            },
            {
                "name": "SacatepÃ©quez",
                "code": "sac"
            },
            {
                "name": "San Marcos",
                "code": "sma"
            },
            {
                "name": "Santa Rosa",
                "code": "sro"
            },
            {
                "name": "SololÃ¡",
                "code": "sol"
            },
            {
                "name": "SuchitepÃ©quez",
                "code": "suc"
            },
            {
                "name": "TotonicapÃ¡n",
                "code": "tot"
            },
            {
                "name": "Zacapa",
                "code": "zac"
            }
        ]
    },
    "gg": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gn": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gw": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "gy": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ht": {
        "language": "ht",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "hm": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "va": {
        "language": "la",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "hn": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "hk": {
        "language": "zh",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_ci-100_co-100_p-100_s-0_z-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "hu": {
        "language": "hu",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_ci-100_a1-49s_a2-50_z-100_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "is": {
        "language": "is",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_z-29s_ci-70_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "in": {
        "language": "hi",
        "postalLabel": "Pincode",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "State",
        "provinces": [{
            "name": "Andaman and Nicobar",
            "code": "an"
        },
            {
                "name": "Andhra Pradesh",
                "code": "ap"
            },
            {
                "name": "Arunachal Pradesh",
                "code": "ar"
            },
            {
                "name": "Assam",
                "code": "as"
            },
            {
                "name": "Bihar",
                "code": "br"
            },
            {
                "name": "Chandigarh",
                "code": "ch"
            },
            {
                "name": "Chattisgarh",
                "code": "cg"
            },
            {
                "name": "Dadra and Nagar Haveli",
                "code": "dn"
            },
            {
                "name": "Daman and Diu",
                "code": "dd"
            },
            {
                "name": "Delhi",
                "code": "dl"
            },
            {
                "name": "Goa",
                "code": "ga"
            },
            {
                "name": "Gujarat",
                "code": "gj"
            },
            {
                "name": "Haryana",
                "code": "hr"
            },
            {
                "name": "Himachal Pradesh",
                "code": "hp"
            },
            {
                "name": "Jammu and Kashmir",
                "code": "jk"
            },
            {
                "name": "Jharkhand",
                "code": "jh"
            },
            {
                "name": "Karnataka",
                "code": "ka",
                "tax_name": "sgst"
            },
            {
                "name": "Kerala",
                "code": "kl"
            },
            {
                "name": "Lakshadweep",
                "code": "ld"
            },
            {
                "name": "Madhya Pradesh",
                "code": "mp"
            },
            {
                "name": "Maharashtra",
                "code": "mh"
            },
            {
                "name": "Manipur",
                "code": "mn"
            },
            {
                "name": "Meghalaya",
                "code": "ml"
            },
            {
                "name": "Mizoram",
                "code": "mz"
            },
            {
                "name": "Nagaland",
                "code": "nl"
            },
            {
                "name": "Orissa",
                "code": "or"
            },
            {
                "name": "Puducherry",
                "code": "py"
            },
            {
                "name": "Punjab",
                "code": "pb"
            },
            {
                "name": "Rajasthan",
                "code": "rj"
            },
            {
                "name": "Sikkim",
                "code": "sk"
            },
            {
                "name": "Tamil Nadu",
                "code": "tn"
            },
            {
                "name": "Telangana",
                "code": "ts"
            },
            {
                "name": "Tripura",
                "code": "tr"
            },
            {
                "name": "Uttar Pradesh",
                "code": "up"
            },
            {
                "name": "Uttarakhand",
                "code": "uk"
            },
            {
                "name": "West Bengal",
                "code": "wb"
            }
        ]
    },
    "id": {
        "language": "id",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "Aceh",
            "code": "ac"
        },
            {
                "name": "Bali",
                "code": "ba"
            },
            {
                "name": "Bangka Belitung",
                "code": "bb"
            },
            {
                "name": "Banten",
                "code": "bt"
            },
            {
                "name": "Bengkulu",
                "code": "be"
            },
            {
                "name": "Gorontalo",
                "code": "go"
            },
            {
                "name": "Jakarta",
                "code": "jk"
            },
            {
                "name": "Jambi",
                "code": "ja"
            },
            {
                "name": "Jawa Barat",
                "code": "jb"
            },
            {
                "name": "Jawa Tengah",
                "code": "jt"
            },
            {
                "name": "Jawa Timur",
                "code": "ji"
            },
            {
                "name": "Kalimantan Barat",
                "code": "kb"
            },
            {
                "name": "Kalimantan Selatan",
                "code": "ks"
            },
            {
                "name": "Kalimantan Tengah",
                "code": "kt"
            },
            {
                "name": "Kalimantan Timur",
                "code": "ki"
            },
            {
                "name": "Kalimantan Utara",
                "code": "ku"
            },
            {
                "name": "Kepulauan Riau",
                "code": "kr"
            },
            {
                "name": "Lampung",
                "code": "la"
            },
            {
                "name": "Maluku",
                "code": "ma"
            },
            {
                "name": "Maluku Utara",
                "code": "mu"
            },
            {
                "name": "Nusa Tenggara Barat",
                "code": "nb"
            },
            {
                "name": "Nusa Tenggara Timur",
                "code": "nt"
            },
            {
                "name": "Papua",
                "code": "pa"
            },
            {
                "name": "Papua Barat",
                "code": "pb"
            },
            {
                "name": "Riau",
                "code": "ri"
            },
            {
                "name": "Sulawesi Barat",
                "code": "sr"
            },
            {
                "name": "Sulawesi Selatan",
                "code": "sn"
            },
            {
                "name": "Sulawesi Tengah",
                "code": "st"
            },
            {
                "name": "Sulawesi Tenggara",
                "code": "sg"
            },
            {
                "name": "Sulawesi Utara",
                "code": "sa"
            },
            {
                "name": "Sumatra Barat",
                "code": "sb"
            },
            {
                "name": "Sumatra Selatan",
                "code": "ss"
            },
            {
                "name": "Sumatra Utara",
                "code": "su"
            },
            {
                "name": "Yogyakarta",
                "code": "yo"
            }
        ]
    },
    "ir": {
        "language": "fa",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "iq": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ie": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "County",
        "provinces": [{
            "name": "Carlow",
            "code": "cw"
        },
            {
                "name": "Cavan",
                "code": "cn"
            },
            {
                "name": "Clare",
                "code": "ce"
            },
            {
                "name": "Cork",
                "code": "co"
            },
            {
                "name": "Donegal",
                "code": "dl"
            },
            {
                "name": "Dublin",
                "code": "d"
            },
            {
                "name": "Galway",
                "code": "g"
            },
            {
                "name": "Kerry",
                "code": "ky"
            },
            {
                "name": "Kildare",
                "code": "ke"
            },
            {
                "name": "Kilkenny",
                "code": "kk"
            },
            {
                "name": "Laois",
                "code": "ls"
            },
            {
                "name": "Leitrim",
                "code": "lm"
            },
            {
                "name": "Limerick",
                "code": "lk"
            },
            {
                "name": "Longford",
                "code": "ld"
            },
            {
                "name": "Louth",
                "code": "lh"
            },
            {
                "name": "Mayo",
                "code": "mo"
            },
            {
                "name": "Meath",
                "code": "mh"
            },
            {
                "name": "Monaghan",
                "code": "mn"
            },
            {
                "name": "Offaly",
                "code": "oy"
            },
            {
                "name": "Roscommon",
                "code": "rn"
            },
            {
                "name": "Sligo",
                "code": "so"
            },
            {
                "name": "Tipperary",
                "code": "ta"
            },
            {
                "name": "Waterford",
                "code": "wd"
            },
            {
                "name": "Westmeath",
                "code": "wh"
            },
            {
                "name": "Wexford",
                "code": "wx"
            },
            {
                "name": "Wicklow",
                "code": "ww"
            }
        ]
    },
    "im": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "il": {
        "language": "he",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "it": {
        "language": "it",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-49s_a2-50_ci-34s_s-34s_z-29_co-100_p-100",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "Agrigento",
            "code": "ag"
        },
            {
                "name": "Alessandria",
                "code": "al"
            },
            {
                "name": "Ancona",
                "code": "an"
            },
            {
                "name": "Aosta",
                "code": "ao"
            },
            {
                "name": "Arezzo",
                "code": "ar"
            },
            {
                "name": "Ascoli Piceno",
                "code": "ap"
            },
            {
                "name": "Asti",
                "code": "at"
            },
            {
                "name": "Avellino",
                "code": "av"
            },
            {
                "name": "Bari",
                "code": "ba"
            },
            {
                "name": "Barletta-Andria-Trani",
                "code": "bt"
            },
            {
                "name": "Belluno",
                "code": "bl"
            },
            {
                "name": "Benevento",
                "code": "bn"
            },
            {
                "name": "Bergamo",
                "code": "bg"
            },
            {
                "name": "Biella",
                "code": "bi"
            },
            {
                "name": "Bologna",
                "code": "bo"
            },
            {
                "name": "Bolzano",
                "code": "bz"
            },
            {
                "name": "Brescia",
                "code": "bs"
            },
            {
                "name": "Brindisi",
                "code": "br"
            },
            {
                "name": "Cagliari",
                "code": "ca"
            },
            {
                "name": "Caltanissetta",
                "code": "cl"
            },
            {
                "name": "Campobasso",
                "code": "cb"
            },
            {
                "name": "Carbonia-Iglesias",
                "code": "ci"
            },
            {
                "name": "Caserta",
                "code": "ce"
            },
            {
                "name": "Catania",
                "code": "ct"
            },
            {
                "name": "Catanzaro",
                "code": "cz"
            },
            {
                "name": "Chieti",
                "code": "ch"
            },
            {
                "name": "Como",
                "code": "co"
            },
            {
                "name": "Cosenza",
                "code": "cs"
            },
            {
                "name": "Cremona",
                "code": "cr"
            },
            {
                "name": "Crotone",
                "code": "kr"
            },
            {
                "name": "Cuneo",
                "code": "cn"
            },
            {
                "name": "Enna",
                "code": "en"
            },
            {
                "name": "Fermo",
                "code": "fm"
            },
            {
                "name": "Ferrara",
                "code": "fe"
            },
            {
                "name": "Firenze",
                "code": "fi"
            },
            {
                "name": "Foggia",
                "code": "fg"
            },
            {
                "name": "ForlÃ¬-Cesena",
                "code": "fc"
            },
            {
                "name": "Frosinone",
                "code": "fr"
            },
            {
                "name": "Genova",
                "code": "ge"
            },
            {
                "name": "Gorizia",
                "code": "go"
            },
            {
                "name": "Grosseto",
                "code": "gr"
            },
            {
                "name": "Imperia",
                "code": "im"
            },
            {
                "name": "Isernia",
                "code": "is"
            },
            {
                "name": "L'Aquila",
                "code": "aq"
            },
            {
                "name": "La Spezia",
                "code": "sp"
            },
            {
                "name": "Latina",
                "code": "lt"
            },
            {
                "name": "Lecce",
                "code": "le"
            },
            {
                "name": "Lecco",
                "code": "lc"
            },
            {
                "name": "Livorno",
                "code": "li"
            },
            {
                "name": "Lodi",
                "code": "lo"
            },
            {
                "name": "Lucca",
                "code": "lu"
            },
            {
                "name": "Macerata",
                "code": "mc"
            },
            {
                "name": "Mantova",
                "code": "mn"
            },
            {
                "name": "Massa-Carrara",
                "code": "ms"
            },
            {
                "name": "Matera",
                "code": "mt"
            },
            {
                "name": "Medio Campidano",
                "code": "vs"
            },
            {
                "name": "Messina",
                "code": "me"
            },
            {
                "name": "Milano",
                "code": "mi"
            },
            {
                "name": "Modena",
                "code": "mo"
            },
            {
                "name": "Monza e Brianza",
                "code": "mb"
            },
            {
                "name": "Napoli",
                "code": "na"
            },
            {
                "name": "Novara",
                "code": "no"
            },
            {
                "name": "Nuoro",
                "code": "nu"
            },
            {
                "name": "Ogliastra",
                "code": "og"
            },
            {
                "name": "Olbia-Tempio",
                "code": "ot"
            },
            {
                "name": "Oristano",
                "code": "or"
            },
            {
                "name": "Padova",
                "code": "pd"
            },
            {
                "name": "Palermo",
                "code": "pa"
            },
            {
                "name": "Parma",
                "code": "pr"
            },
            {
                "name": "Pavia",
                "code": "pv"
            },
            {
                "name": "Perugia",
                "code": "pg"
            },
            {
                "name": "Pesaro e Urbino",
                "code": "pu"
            },
            {
                "name": "Pescara",
                "code": "pe"
            },
            {
                "name": "Piacenza",
                "code": "pc"
            },
            {
                "name": "Pisa",
                "code": "pi"
            },
            {
                "name": "Pistoia",
                "code": "pt"
            },
            {
                "name": "Pordenone",
                "code": "pn"
            },
            {
                "name": "Potenza",
                "code": "pz"
            },
            {
                "name": "Prato",
                "code": "po"
            },
            {
                "name": "Ragusa",
                "code": "rg"
            },
            {
                "name": "Ravenna",
                "code": "ra"
            },
            {
                "name": "Reggio Calabria",
                "code": "rc"
            },
            {
                "name": "Reggio Emilia",
                "code": "re"
            },
            {
                "name": "Rieti",
                "code": "ri"
            },
            {
                "name": "Rimini",
                "code": "rn"
            },
            {
                "name": "Roma",
                "code": "rm"
            },
            {
                "name": "Rovigo",
                "code": "ro"
            },
            {
                "name": "Salerno",
                "code": "sa"
            },
            {
                "name": "Sassari",
                "code": "ss"
            },
            {
                "name": "Savona",
                "code": "sv"
            },
            {
                "name": "Siena",
                "code": "si"
            },
            {
                "name": "Siracusa",
                "code": "sr"
            },
            {
                "name": "Sondrio",
                "code": "so"
            },
            {
                "name": "Taranto",
                "code": "ta"
            },
            {
                "name": "Teramo",
                "code": "te"
            },
            {
                "name": "Terni",
                "code": "tr"
            },
            {
                "name": "Torino",
                "code": "to"
            },
            {
                "name": "Trapani",
                "code": "tp"
            },
            {
                "name": "Trento",
                "code": "tn"
            },
            {
                "name": "Treviso",
                "code": "tv"
            },
            {
                "name": "Trieste",
                "code": "ts"
            },
            {
                "name": "Udine",
                "code": "ud"
            },
            {
                "name": "Varese",
                "code": "va"
            },
            {
                "name": "Venezia",
                "code": "ve"
            },
            {
                "name": "Verbano-Cusio-Ossola",
                "code": "vb"
            },
            {
                "name": "Vercelli",
                "code": "vc"
            },
            {
                "name": "Verona",
                "code": "vr"
            },
            {
                "name": "Vibo Valentia",
                "code": "vv"
            },
            {
                "name": "Vicenza",
                "code": "vi"
            },
            {
                "name": "Viterbo",
                "code": "vt"
            }
        ]
    },
    "jm": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "jp": {
        "language": "ja",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Prefecture",
        "freeProvince" : "1",
        "provinces": [{
            "name": "Aichi",
            "code": "jp-23"
        },
            {
                "name": "Akita",
                "code": "jp-05"
            },
            {
                "name": "Aomori",
                "code": "jp-02"
            },
            {
                "name": "Chiba",
                "code": "jp-12"
            },
            {
                "name": "Ehime",
                "code": "jp-38"
            },
            {
                "name": "Fukui",
                "code": "jp-18"
            },
            {
                "name": "Fukuoka",
                "code": "jp-40"
            },
            {
                "name": "Fukushima",
                "code": "jp-07"
            },
            {
                "name": "Gifu",
                "code": "jp-21"
            },
            {
                "name": "Gunma",
                "code": "jp-10"
            },
            {
                "name": "Hiroshima",
                "code": "jp-34"
            },
            {
                "name": "HokkaidÅ",
                "code": "jp-01"
            },
            {
                "name": "HyÅgo",
                "code": "jp-28"
            },
            {
                "name": "Ibaraki",
                "code": "Jp-08"
            },
            {
                "name": "Ishikawa",
                "code": "jp-17"
            },
            {
                "name": "Iwate",
                "code": "jp-03"
            },
            {
                "name": "Kagawa",
                "code": "jp-37"
            },
            {
                "name": "Kagoshima",
                "code": "jp-46"
            },
            {
                "name": "Kanagawa",
                "code": "jp-14"
            },
            {
                "name": "KÅchi",
                "code": "jp-39"
            },
            {
                "name": "Kumamoto",
                "code": "jp-43"
            },
            {
                "name": "KyÅto",
                "code": "jp-26"
            },
            {
                "name": "Mie",
                "code": "jp-24"
            },
            {
                "name": "Miyagi",
                "code": "jp-04"
            },
            {
                "name": "Miyazaki",
                "code": "jp-45"
            },
            {
                "name": "Nagano",
                "code": "jp-20"
            },
            {
                "name": "Nagasaki",
                "code": "jp-42"
            },
            {
                "name": "Nara",
                "code": "jp-29"
            },
            {
                "name": "Niigata",
                "code": "jp-15"
            },
            {
                "name": "ÅŒita",
                "code": "jp-44"
            },
            {
                "name": "Okayama",
                "code": "jp-33"
            },
            {
                "name": "Okinawa",
                "code": "jp-47"
            },
            {
                "name": "ÅŒsaka",
                "code": "jp-27"
            },
            {
                "name": "Saga",
                "code": "jp-41"
            },
            {
                "name": "Saitama",
                "code": "jp-11"
            },
            {
                "name": "Shiga",
                "code": "jp-25"
            },
            {
                "name": "Shimane",
                "code": "jp-32"
            },
            {
                "name": "Shizuoka",
                "code": "jp-22"
            },
            {
                "name": "Tochigi",
                "code": "jp-09"
            },
            {
                "name": "Tokushima",
                "code": "jp-36"
            },
            {
                "name": "TÅkyÅ",
                "code": "Jp-13"
            },
            {
                "name": "Tottori",
                "code": "jp-31"
            },
            {
                "name": "Toyama",
                "code": "jp-16"
            },
            {
                "name": "Wakayama",
                "code": "jp-30"
            },
            {
                "name": "Yamagata",
                "code": "jp-06"
            },
            {
                "name": "Yamaguchi",
                "code": "jp-35"
            },
            {
                "name": "Yamanashi",
                "code": "jp-19"
            },

        ]
    },
    "je": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "jo": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kz": {
        "language": "kk",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ke": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ki": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kp": {
        "language": "ko",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "xk": {
        "language": "sq",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kw": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kg": {
        "language": "ky",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "la": {
        "language": "lo",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "lv": {
        "language": "lv",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "lb": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ls": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "lr": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ly": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "li": {
        "language": "de",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "lt": {
        "language": "lt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "lu": {
        "language": "lb",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mo": {
        "language": "zh",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mk": {
        "language": "mk",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mg": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mw": {
        "language": "ny",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "my": {
        "language": "ms",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "State/territory",
        "provinces": [{
            "name": "Johor",
            "code": "jhr"
        },
            {
                "name": "Kedah",
                "code": "kdh"
            },
            {
                "name": "Kelantan",
                "code": "ktn"
            },
            {
                "name": "Kuala Lumpur",
                "code": "kul"
            },
            {
                "name": "Labuan",
                "code": "lbn"
            },
            {
                "name": "Melaka",
                "code": "mlk"
            },
            {
                "name": "Negeri Sembilan",
                "code": "nsn"
            },
            {
                "name": "Pahang",
                "code": "phg"
            },
            {
                "name": "Perak",
                "code": "prk"
            },
            {
                "name": "Perlis",
                "code": "pls"
            },
            {
                "name": "Pulau Pinang",
                "code": "png"
            },
            {
                "name": "Putrajaya",
                "code": "pjy"
            },
            {
                "name": "Sabah",
                "code": "sbh"
            },
            {
                "name": "Sarawak",
                "code": "swk"
            },
            {
                "name": "Selangor",
                "code": "sgr"
            },
            {
                "name": "Terengganu",
                "code": "trg"
            }
        ]
    },
    "mv": {
        "language": "dv",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ml": {
        "language": "fr",
        "postalLabel": "",
        "postal": "0",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mt": {
        "language": "mt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mq": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mr": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mu": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "yt": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mx": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "State",
        "provinces": [{
            "name": "Aguascalientes",
            "code": "ags"
        },
            {
                "name": "Baja California",
                "code": "bc"
            },
            {
                "name": "Baja California Sur",
                "code": "bcs"
            },
            {
                "name": "Campeche",
                "code": "camp"
            },
            {
                "name": "Chiapas",
                "code": "chis"
            },
            {
                "name": "Chihuahua",
                "code": "chih"
            },
            {
                "name": "Ciudad de MÃ©xico",
                "code": "df"
            },
            {
                "name": "Coahuila",
                "code": "coah"
            },
            {
                "name": "Colima",
                "code": "col"
            },
            {
                "name": "Durango",
                "code": "dgo"
            },
            {
                "name": "Guanajuato",
                "code": "gto"
            },
            {
                "name": "Guerrero",
                "code": "gro"
            },
            {
                "name": "Hidalgo",
                "code": "hgo"
            },
            {
                "name": "Jalisco",
                "code": "jal"
            },
            {
                "name": "MÃ©xico",
                "code": "mex"
            },
            {
                "name": "MichoacÃ¡n",
                "code": "mich"
            },
            {
                "name": "Morelos",
                "code": "mor"
            },
            {
                "name": "Nayarit",
                "code": "nay"
            },
            {
                "name": "Nuevo LeÃ³n",
                "code": "nl"
            },
            {
                "name": "Oaxaca",
                "code": "oax"
            },
            {
                "name": "Puebla",
                "code": "pue"
            },
            {
                "name": "QuerÃ©taro",
                "code": "qro"
            },
            {
                "name": "Quintana Roo",
                "code": "q roo"
            },
            {
                "name": "San Luis PotosÃ­",
                "code": "slp"
            },
            {
                "name": "Sinaloa",
                "code": "sin"
            },
            {
                "name": "Sonora",
                "code": "son"
            },
            {
                "name": "Tabasco",
                "code": "tab"
            },
            {
                "name": "Tamaulipas",
                "code": "tamps"
            },
            {
                "name": "Tlaxcala",
                "code": "tlax"
            },
            {
                "name": "Veracruz",
                "code": "ver"
            },
            {
                "name": "YucatÃ¡n",
                "code": "yuc"
            },
            {
                "name": "Zacatecas",
                "code": "zac"
            }
        ]
    },
    "md": {
        "language": "ro",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mc": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mn": {
        "language": "mn",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "me": {
        "language": "sr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ms": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ma": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mz": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mm": {
        "language": "my",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "na": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "nr": {
        "language": "na",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "np": {
        "language": "ne",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "nl": {
        "language": "nl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "e-100_f-49s_l-50_a1-69s_a2-30_z-69s_ci-30_co-100_p-100_s-0",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "an": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "nc": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "nz": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "Auckland",
            "code": "auk"
        },
            {
                "name": "Bay of Plenty",
                "code": "bop"
            },
            {
                "name": "Canterbury",
                "code": "can"
            },
            {
                "name": "Gisborne",
                "code": "gis"
            },
            {
                "name": "Hawke's Bay",
                "code": "hkb"
            },
            {
                "name": "Manawatu-Wanganui",
                "code": "mwt"
            },
            {
                "name": "Marlborough",
                "code": "mbh"
            },
            {
                "name": "Nelson",
                "code": "nsn"
            },
            {
                "name": "Northland",
                "code": "ntl"
            },
            {
                "name": "Otago",
                "code": "ota"
            },
            {
                "name": "Southland",
                "code": "stl"
            },
            {
                "name": "Taranaki",
                "code": "tki"
            },
            {
                "name": "Tasman",
                "code": "tas"
            },
            {
                "name": "Waikato",
                "code": "wko"
            },
            {
                "name": "Wellington",
                "code": "wgn"
            },
            {
                "name": "West Coast",
                "code": "wtc"
            }
        ]
    },
    "ni": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ne": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ng": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "State",
        "provinces": [{
            "name": "Abia",
            "code": "ab"
        },
            {
                "name": "Abuja Federal Capital Territory",
                "code": "fc"
            },
            {
                "name": "Adamawa",
                "code": "ad"
            },
            {
                "name": "Akwa Ibom",
                "code": "ak"
            },
            {
                "name": "Anambra",
                "code": "an"
            },
            {
                "name": "Bauchi",
                "code": "ba"
            },
            {
                "name": "Bayelsa",
                "code": "by"
            },
            {
                "name": "Benue",
                "code": "be"
            },
            {
                "name": "Borno",
                "code": "bo"
            },
            {
                "name": "Cross River",
                "code": "cr"
            },
            {
                "name": "Delta",
                "code": "de"
            },
            {
                "name": "Ebonyi",
                "code": "eb"
            },
            {
                "name": "Edo",
                "code": "ed"
            },
            {
                "name": "Ekiti",
                "code": "ek"
            },
            {
                "name": "Enugu",
                "code": "en"
            },
            {
                "name": "Gombe",
                "code": "go"
            },
            {
                "name": "Imo",
                "code": "im"
            },
            {
                "name": "Jigawa",
                "code": "ji"
            },
            {
                "name": "Kaduna",
                "code": "kd"
            },
            {
                "name": "Kano",
                "code": "kn"
            },
            {
                "name": "Katsina",
                "code": "kt"
            },
            {
                "name": "Kebbi",
                "code": "ke"
            },
            {
                "name": "Kogi",
                "code": "ko"
            },
            {
                "name": "Kwara",
                "code": "kw"
            },
            {
                "name": "Lagos",
                "code": "la"
            },
            {
                "name": "Nasarawa",
                "code": "na"
            },
            {
                "name": "Niger",
                "code": "ni"
            },
            {
                "name": "Ogun",
                "code": "og"
            },
            {
                "name": "Ondo",
                "code": "on"
            },
            {
                "name": "Osun",
                "code": "os"
            },
            {
                "name": "Oyo",
                "code": "oy"
            },
            {
                "name": "Plateau",
                "code": "pl"
            },
            {
                "name": "Rivers",
                "code": "ri"
            },
            {
                "name": "Sokoto",
                "code": "so"
            },
            {
                "name": "Taraba",
                "code": "ta"
            },
            {
                "name": "Yobe",
                "code": "yo"
            },
            {
                "name": "Zamfara",
                "code": "za"
            }
        ]
    },
    "nu": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "nf": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "no": {
        "language": "no",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "om": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pk": {
        "language": "ur",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ps": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pa": {
        "language": "es",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "Bocas del Toro",
            "code": "pa-1",
            "tax_name": "itbms"
        },
            {
                "name": "ChiriquÃ­",
                "code": "pa-4",
                "tax_name": "itbms"
            },
            {
                "name": "CoclÃ©",
                "code": "pa-2",
                "tax_name": "itbms"
            },
            {
                "name": "ColÃ³n",
                "code": "pa-3",
                "tax_name": "itbms"
            },
            {
                "name": "DariÃ©n",
                "code": "pa-5",
                "tax_name": "itbms"
            },
            {
                "name": "EmberÃ¡",
                "code": "pa-em",
                "tax_name": "itbms"
            },
            {
                "name": "Herrera",
                "code": "pa-6",
                "tax_name": "itbms"
            },
            {
                "name": "Kuna Yala",
                "code": "pa-ky",
                "tax_name": "itbms"
            },
            {
                "name": "Los Santos",
                "code": "pa-7",
                "tax_name": "itbms"
            },
            {
                "name": "NgÃ¶be-BuglÃ©",
                "code": "pa-nb",
                "tax_name": "itbms"
            },
            {
                "name": "PanamÃ¡",
                "code": "pa-8",
                "tax_name": "itbms"
            },
            {
                "name": "PanamÃ¡ Oeste",
                "code": "pa-10",
                "tax_name": "itbms"
            },
            {
                "name": "Veraguas",
                "code": "pa-9",
                "tax_name": "itbms"
            }
        ]
    },
    "pg": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "py": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pe": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ph": {
        "language": "tl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pn": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pl": {
        "language": "pl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pt": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "AÃ§ores",
            "code": "pt-20"
        },
            {
                "name": "Aveiro",
                "code": "pt-01"
            },
            {
                "name": "Beja",
                "code": "pt-02"
            },
            {
                "name": "Braga",
                "code": "pt-03"
            },
            {
                "name": "BraganÃ§a",
                "code": "pt-04"
            },
            {
                "name": "Castelo Branco",
                "code": "pt-05"
            },
            {
                "name": "Coimbra",
                "code": "pt-06"
            },
            {
                "name": "Ã‰vora",
                "code": "pt-07"
            },
            {
                "name": "Faro",
                "code": "pt-08"
            },
            {
                "name": "Guarda",
                "code": "pt-09"
            },
            {
                "name": "Leiria",
                "code": "pt-10"
            },
            {
                "name": "Lisboa",
                "code": "pt-11"
            },
            {
                "name": "Madeira",
                "code": "pt-30"
            },
            {
                "name": "Portalegre",
                "code": "pt-12"
            },
            {
                "name": "Porto",
                "code": "pt-13"
            },
            {
                "name": "SantarÃ©m",
                "code": "pt-14"
            },
            {
                "name": "SetÃºbal",
                "code": "pt-15"
            },
            {
                "name": "Viana do Castelo",
                "code": "pt-16"
            },
            {
                "name": "Vila Real",
                "code": "pt-17"
            },
            {
                "name": "Viseu",
                "code": "pt-18"
            }
        ]
    },
    "qa": {
        "language": "ar",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "cm": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "re": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ro": {
        "language": "ro",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "County",
        "provinces": [{
            "name": "Alba",
            "code": "ab"
        },
            {
                "name": "Arad",
                "code": "ar"
            },
            {
                "name": "ArgeÈ™",
                "code": "ag"
            },
            {
                "name": "BacÄƒu",
                "code": "bc"
            },
            {
                "name": "Bihor",
                "code": "bh"
            },
            {
                "name": "BistriÈ›a-NÄƒsÄƒud",
                "code": "bn"
            },
            {
                "name": "BotoÈ™ani",
                "code": "bt"
            },
            {
                "name": "BrÄƒila",
                "code": "br"
            },
            {
                "name": "BraÈ™ov",
                "code": "bv"
            },
            {
                "name": "BucureÈ™ti",
                "code": "b"
            },
            {
                "name": "BuzÄƒu",
                "code": "bz"
            },
            {
                "name": "CÄƒlÄƒraÈ™i",
                "code": "cl"
            },
            {
                "name": "CaraÈ™-Severin",
                "code": "cs"
            },
            {
                "name": "Cluj",
                "code": "cj"
            },
            {
                "name": "ConstanÈ›a",
                "code": "ct"
            },
            {
                "name": "Covasna",
                "code": "cv"
            },
            {
                "name": "DÃ¢mboviÈ›a",
                "code": "db"
            },
            {
                "name": "Dolj",
                "code": "dj"
            },
            {
                "name": "GalaÈ›i",
                "code": "gl"
            },
            {
                "name": "Giurgiu",
                "code": "gr"
            },
            {
                "name": "Gorj",
                "code": "gj"
            },
            {
                "name": "Harghita",
                "code": "hr"
            },
            {
                "name": "Hunedoara",
                "code": "hd"
            },
            {
                "name": "IalomiÈ›a",
                "code": "il"
            },
            {
                "name": "IaÈ™i",
                "code": "is"
            },
            {
                "name": "Ilfov",
                "code": "if"
            },
            {
                "name": "MaramureÈ™",
                "code": "mm"
            },
            {
                "name": "MehedinÈ›i",
                "code": "mh"
            },
            {
                "name": "MureÈ™",
                "code": "ms"
            },
            {
                "name": "NeamÈ›",
                "code": "nt"
            },
            {
                "name": "Olt",
                "code": "ot"
            },
            {
                "name": "Prahova",
                "code": "ph"
            },
            {
                "name": "SÄƒlaj",
                "code": "sj"
            },
            {
                "name": "Satu Mare",
                "code": "sm"
            },
            {
                "name": "Sibiu",
                "code": "sb"
            },
            {
                "name": "Suceava",
                "code": "sv"
            },
            {
                "name": "Teleorman",
                "code": "tr"
            },
            {
                "name": "TimiÈ™",
                "code": "tm"
            },
            {
                "name": "Tulcea",
                "code": "tl"
            },
            {
                "name": "VÃ¢lcea",
                "code": "vl"
            },
            {
                "name": "Vaslui",
                "code": "vs"
            },
            {
                "name": "Vrancea",
                "code": "vn"
            }
        ]
    },
    "ru": {
        "language": "ru",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "Altai Krai",
            "code": "alt"
        },
            {
                "name": "Altai Republic",
                "code": "al"
            },
            {
                "name": "Amur Oblast",
                "code": "amu"
            },
            {
                "name": "Arkhangelsk Oblast",
                "code": "ark"
            },
            {
                "name": "Astrakhan Oblast",
                "code": "ast"
            },
            {
                "name": "Belgorod Oblast",
                "code": "bel"
            },
            {
                "name": "Bryansk Oblast",
                "code": "bry"
            },
            {
                "name": "Chechen Republic",
                "code": "ce"
            },
            {
                "name": "Chelyabinsk Oblast",
                "code": "che"
            },
            {
                "name": "Chukotka Autonomous Okrug",
                "code": "chu"
            },
            {
                "name": "Chuvash Republic",
                "code": "cu"
            },
            {
                "name": "Irkutsk Oblast",
                "code": "irk"
            },
            {
                "name": "Ivanovo Oblast",
                "code": "iva"
            },
            {
                "name": "Jewish Autonomous Oblast",
                "code": "yev"
            },
            {
                "name": "Kabardino-Balkarian Republic",
                "code": "kb"
            },
            {
                "name": "Kaliningrad Oblast",
                "code": "kgd"
            },
            {
                "name": "Kaluga Oblast",
                "code": "klu"
            },
            {
                "name": "Kamchatka Krai",
                "code": "kam"
            },
            {
                "name": "Karachayâ€“Cherkess Republic",
                "code": "kc"
            },
            {
                "name": "Kemerovo Oblast",
                "code": "kem"
            },
            {
                "name": "Khabarovsk Krai",
                "code": "kha"
            },
            {
                "name": "Khanty-Mansi Autonomous Okrug",
                "code": "khm"
            },
            {
                "name": "Kirov Oblast",
                "code": "kir"
            },
            {
                "name": "Komi Republic",
                "code": "ko"
            },
            {
                "name": "Kostroma Oblast",
                "code": "kos"
            },
            {
                "name": "Krasnodar Krai",
                "code": "kda"
            },
            {
                "name": "Krasnoyarsk Krai",
                "code": "kya"
            },
            {
                "name": "Kurgan Oblast",
                "code": "kgn"
            },
            {
                "name": "Kursk Oblast",
                "code": "krs"
            },
            {
                "name": "Leningrad Oblast",
                "code": "len"
            },
            {
                "name": "Lipetsk Oblast",
                "code": "lip"
            },
            {
                "name": "Magadan Oblast",
                "code": "mag"
            },
            {
                "name": "Mari El Republic",
                "code": "me"
            },
            {
                "name": "Moscow",
                "code": "mow"
            },
            {
                "name": "Moscow Oblast",
                "code": "mos"
            },
            {
                "name": "Murmansk Oblast",
                "code": "mur"
            },
            {
                "name": "Nizhny Novgorod Oblast",
                "code": "niz"
            },
            {
                "name": "Novgorod Oblast",
                "code": "ngr"
            },
            {
                "name": "Novosibirsk Oblast",
                "code": "nvs"
            },
            {
                "name": "Omsk Oblast",
                "code": "oms"
            },
            {
                "name": "Orenburg Oblast",
                "code": "ore"
            },
            {
                "name": "Oryol Oblast",
                "code": "orl"
            },
            {
                "name": "Penza Oblast",
                "code": "pnz"
            },
            {
                "name": "Perm Krai",
                "code": "per"
            },
            {
                "name": "Primorsky Krai",
                "code": "pri"
            },
            {
                "name": "Pskov Oblast",
                "code": "psk"
            },
            {
                "name": "Republic of Adygeya",
                "code": "ad"
            },
            {
                "name": "Republic of Bashkortostan",
                "code": "ba"
            },
            {
                "name": "Republic of Buryatia",
                "code": "bu"
            },
            {
                "name": "Republic of Dagestan",
                "code": "da"
            },
            {
                "name": "Republic of Ingushetia",
                "code": "in"
            },
            {
                "name": "Republic of Kalmykia",
                "code": "kl"
            },
            {
                "name": "Republic of Karelia",
                "code": "kr"
            },
            {
                "name": "Republic of Khakassia",
                "code": "kk"
            },
            {
                "name": "Republic of Mordovia",
                "code": "mo"
            },
            {
                "name": "Republic of North Ossetiaâ€“Alania",
                "code": "se"
            },
            {
                "name": "Republic of Tatarstan",
                "code": "ta"
            },
            {
                "name": "Rostov Oblast",
                "code": "ros"
            },
            {
                "name": "Ryazan Oblast",
                "code": "rya"
            },
            {
                "name": "Saint Petersburg",
                "code": "spe"
            },
            {
                "name": "Sakha Republic (Yakutia)",
                "code": "Sa"
            },
            {
                "name": "Sakhalin Oblast",
                "code": "sak"
            },
            {
                "name": "Samara Oblast",
                "code": "sam"
            },
            {
                "name": "Saratov Oblast",
                "code": "sar"
            },
            {
                "name": "Smolensk Oblast",
                "code": "smo"
            },
            {
                "name": "Stavropol Krai",
                "code": "sta"
            },
            {
                "name": "Sverdlovsk Oblast",
                "code": "sve"
            },
            {
                "name": "Tambov Oblast",
                "code": "tam"
            },
            {
                "name": "Tomsk Oblast",
                "code": "tom"
            },
            {
                "name": "Tula Oblast",
                "code": "tul"
            },
            {
                "name": "Tver Oblast",
                "code": "tve"
            },
            {
                "name": "Tyumen Oblast",
                "code": "tyu"
            },
            {
                "name": "Tyva Republic",
                "code": "ty"
            },
            {
                "name": "Udmurtia",
                "code": "ud"
            },
            {
                "name": "Ulyanovsk Oblast",
                "code": "uly"
            },
            {
                "name": "Vladimir Oblast",
                "code": "vla"
            },
            {
                "name": "Volgograd Oblast",
                "code": "vgg"
            },
            {
                "name": "Vologda Oblast",
                "code": "vlg"
            },
            {
                "name": "Voronezh Oblast",
                "code": "vor"
            },
            {
                "name": "Yamalo-Nenets Autonomous Okrug",
                "code": "yan"
            },
            {
                "name": "Yaroslavl Oblast",
                "code": "yar"
            },
            {
                "name": "Zabaykalsky Krai",
                "code": "zab"
            }
        ]
    },
    "rw": {
        "language": "rw",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "bl": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sh": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kn": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "lc": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "mf": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "pm": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ws": {
        "language": "sm",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sm": {
        "language": "it",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "st": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sa": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sn": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "rs": {
        "language": "sr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sc": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sl": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sg": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sx": {
        "language": "nl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sk": {
        "language": "sk",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "si": {
        "language": "sl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sb": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "so": {
        "language": "so",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "za": {
        "language": "zu",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "Eastern Cape",
            "code": "ec"
        },
            {
                "name": "Free State",
                "code": "fs"
            },
            {
                "name": "Gauteng",
                "code": "gt"
            },
            {
                "name": "KwaZulu-Natal",
                "code": "nl"
            },
            {
                "name": "Limpopo",
                "code": "lp"
            },
            {
                "name": "Mpumalanga",
                "code": "mp"
            },
            {
                "name": "North West",
                "code": "nw"
            },
            {
                "name": "Northern Cape",
                "code": "nc"
            },
            {
                "name": "Western Cape",
                "code": "wc"
            }
        ]
    },
    "gs": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "kr": {
        "language": "ko",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "e-100_l-50s_f-49_z-100_co-100_s-49s_ci-50_a1-69s_a2-30_p-100",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "freeProvince" : "1",
        "provinces": [{
            "name": "Busan",
            "code": "kr-26"
        },
            {
                "name": "Chungbuk",
                "code": "kr-43"
            },
            {
                "name": "Chungnam",
                "code": "kr-44"
            },
            {
                "name": "Daegu",
                "code": "kr-27"
            },
            {
                "name": "Daejeon",
                "code": "kr-30"
            },
            {
                "name": "Gangwon",
                "code": "kr-42"
            },
            {
                "name": "Gwangju",
                "code": "kr-29"
            },
            {
                "name": "Gyeongbuk",
                "code": "kr-47"
            },
            {
                "name": "Gyeonggi",
                "code": "kr-41"
            },
            {
                "name": "Gyeongnam",
                "code": "kr-48"
            },
            {
                "name": "Incheon",
                "code": "kr-28"
            },
            {
                "name": "Jeju",
                "code": "kr-49"
            },
            {
                "name": "Jeonbuk",
                "code": "kr-45"
            },
            {
                "name": "Jeonnam",
                "code": "kr-46"
            },
            {
                "name": "Sejong",
                "code": "kr-50"
            },
            {
                "name": "Seoul",
                "code": "kr-11"
            },
            {
                "name": "Ulsan",
                "code": "kr-31"
            }
        ]
    },
    "ss": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "es": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Province",
        "provinces": [{
            "name": "A CoruÃ±a",
            "code": "c"
        },
            {
                "name": "Ãlava",
                "code": "vi"
            },
            {
                "name": "Albacete",
                "code": "ab"
            },
            {
                "name": "Alicante",
                "code": "a"
            },
            {
                "name": "AlmerÃ­a",
                "code": "al"
            },
            {
                "name": "Asturias",
                "code": "o"
            },
            {
                "name": "Ãvila",
                "code": "av"
            },
            {
                "name": "Badajoz",
                "code": "ba"
            },
            {
                "name": "Balears",
                "code": "pm"
            },
            {
                "name": "Barcelona",
                "code": "b"
            },
            {
                "name": "Burgos",
                "code": "bu"
            },
            {
                "name": "CÃ¡ceres",
                "code": "cc"
            },
            {
                "name": "CÃ¡diz",
                "code": "ca"
            },
            {
                "name": "Cantabria",
                "code": "s"
            },
            {
                "name": "CastellÃ³n",
                "code": "cs"
            },
            {
                "name": "Ceuta",
                "code": "ce"
            },
            {
                "name": "Ciudad Real",
                "code": "cr"
            },
            {
                "name": "CÃ³rdoba",
                "code": "co"
            },
            {
                "name": "Cuenca",
                "code": "cu"
            },
            {
                "name": "Girona",
                "code": "gi"
            },
            {
                "name": "Granada",
                "code": "gr"
            },
            {
                "name": "Guadalajara",
                "code": "gu"
            },
            {
                "name": "GuipÃºzcoa",
                "code": "ss"
            },
            {
                "name": "Huelva",
                "code": "h"
            },
            {
                "name": "Huesca",
                "code": "hu"
            },
            {
                "name": "JaÃ©n",
                "code": "j"
            },
            {
                "name": "La Rioja",
                "code": "lo"
            },
            {
                "name": "Las Palmas",
                "code": "gc"
            },
            {
                "name": "LeÃ³n",
                "code": "le"
            },
            {
                "name": "Lleida",
                "code": "l"
            },
            {
                "name": "Lugo",
                "code": "lu"
            },
            {
                "name": "Madrid",
                "code": "m"
            },
            {
                "name": "MÃ¡laga",
                "code": "ma"
            },
            {
                "name": "Melilla",
                "code": "ml"
            },
            {
                "name": "Murcia",
                "code": "mu"
            },
            {
                "name": "Navarra",
                "code": "na"
            },
            {
                "name": "Ourense",
                "code": "or"
            },
            {
                "name": "Palencia",
                "code": "p"
            },
            {
                "name": "Pontevedra",
                "code": "po"
            },
            {
                "name": "Salamanca",
                "code": "sa"
            },
            {
                "name": "Santa Cruz de Tenerife",
                "code": "tf"
            },
            {
                "name": "Segovia",
                "code": "sg"
            },
            {
                "name": "Sevilla",
                "code": "se"
            },
            {
                "name": "Soria",
                "code": "so"
            },
            {
                "name": "Tarragona",
                "code": "t"
            },
            {
                "name": "Teruel",
                "code": "te"
            },
            {
                "name": "Toledo",
                "code": "to"
            },
            {
                "name": "Valencia",
                "code": "v"
            },
            {
                "name": "Valladolid",
                "code": "va"
            },
            {
                "name": "Vizcaya",
                "code": "bi"
            },
            {
                "name": "Zamora",
                "code": "za"
            },
            {
                "name": "Zaragoza",
                "code": "z"
            }
        ]
    },
    "lk": {
        "language": "si",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "vc": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sd": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sr": {
        "language": "nl",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sj": {
        "language": "no",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sz": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "se": {
        "language": "sv",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ch": {
        "language": "de",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "sy": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tw": {
        "language": "zh",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tj": {
        "language": "tg",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tz": {
        "language": "sw",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "th": {
        "language": "th",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tl": {
        "language": "pt",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tg": {
        "language": "fr",
        "postalLabel": "",
        "postal": "0",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tk": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "to": {
        "language": "to",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tt": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tn": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "3",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tr": {
        "language": "tr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tm": {
        "language": "tk",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tc": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "tv": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ug": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ua": {
        "language": "uk",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ae": {
        "language": "ar",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "Region",
        "provinces": [{
            "name": "Abu Dhabi",
            "code": "Az"
        },
            {
                "name": "Ajman",
                "code": "aj"
            },
            {
                "name": "Dubai",
                "code": "du"
            },
            {
                "name": "Fujairah",
                "code": "fu"
            },
            {
                "name": "Ras al-Khaimah",
                "code": "rk"
            },
            {
                "name": "Sharjah",
                "code": "sh"
            },
            {
                "name": "Umm al-Quwain",
                "code": "uq"
            }
        ]
    },
    "gb": {
        "language": "en",
        "postalLabel": "Postcode",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "us": {
        "language": "en",
        "postalLabel": "Zip code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "State",
        "provinces": [{
            "name": "Alabama",
            "code": "al"
        },
            {
                "name": "Alaska",
                "code": "ak"
            },
            {
                "name": "American Samoa",
                "code": "as"
            },
            {
                "name": "Arizona",
                "code": "az"
            },
            {
                "name": "Arkansas",
                "code": "ar"
            },
            {
                "name": "Armed Forces Americas",
                "code": "aa"
            },
            {
                "name": "Armed Forces Europe",
                "code": "ae"
            },
            {
                "name": "Armed Forces Pacific",
                "code": "ap"
            },
            {
                "name": "California",
                "code": "ca"
            },
            {
                "name": "Colorado",
                "code": "co"
            },
            {
                "name": "Connecticut",
                "code": "ct"
            },
            {
                "name": "Delaware",
                "code": "de"
            },
            {
                "name": "District of Columbia",
                "code": "dc"
            },
            {
                "name": "Federated States of Micronesia",
                "code": "fm"
            },
            {
                "name": "Florida",
                "code": "fl"
            },
            {
                "name": "Georgia",
                "code": "ga"
            },
            {
                "name": "Guam",
                "code": "gu"
            },
            {
                "name": "Hawaii",
                "code": "hi"
            },
            {
                "name": "Idaho",
                "code": "id"
            },
            {
                "name": "Illinois",
                "code": "il"
            },
            {
                "name": "Indiana",
                "code": "in"
            },
            {
                "name": "Iowa",
                "code": "ia"
            },
            {
                "name": "Kansas",
                "code": "ks"
            },
            {
                "name": "Kentucky",
                "code": "ky"
            },
            {
                "name": "Louisiana",
                "code": "la"
            },
            {
                "name": "Maine",
                "code": "me"
            },
            {
                "name": "Marshall Islands",
                "code": "mh"
            },
            {
                "name": "Maryland",
                "code": "md"
            },
            {
                "name": "Massachusetts",
                "code": "ma"
            },
            {
                "name": "Michigan",
                "code": "mi"
            },
            {
                "name": "Minnesota",
                "code": "mn"
            },
            {
                "name": "Mississippi",
                "code": "ms"
            },
            {
                "name": "Missouri",
                "code": "mo"
            },
            {
                "name": "Montana",
                "code": "mt"
            },
            {
                "name": "Nebraska",
                "code": "ne"
            },
            {
                "name": "Nevada",
                "code": "nv"
            },
            {
                "name": "New Hampshire",
                "code": "nh"
            },
            {
                "name": "New Jersey",
                "code": "nj"
            },
            {
                "name": "New Mexico",
                "code": "nm"
            },
            {
                "name": "New York",
                "code": "ny"
            },
            {
                "name": "North Carolina",
                "code": "nc"
            },
            {
                "name": "North Dakota",
                "code": "nd"
            },
            {
                "name": "Northern Mariana Islands",
                "code": "mp"
            },
            {
                "name": "Ohio",
                "code": "oh"
            },
            {
                "name": "Oklahoma",
                "code": "ok"
            },
            {
                "name": "Oregon",
                "code": "or"
            },
            {
                "name": "Palau",
                "code": "pw"
            },
            {
                "name": "Pennsylvania",
                "code": "pa"
            },
            {
                "name": "Puerto Rico",
                "code": "pr"
            },
            {
                "name": "Rhode Island",
                "code": "ri"
            },
            {
                "name": "South Carolina",
                "code": "sc"
            },
            {
                "name": "South Dakota",
                "code": "sd"
            },
            {
                "name": "Tennessee",
                "code": "tn"
            },
            {
                "name": "Texas",
                "code": "tx"
            },
            {
                "name": "Utah",
                "code": "ut"
            },
            {
                "name": "Vermont",
                "code": "vt"
            },
            {
                "name": "Virgin Islands",
                "code": "vi"
            },
            {
                "name": "Virginia",
                "code": "va"
            },
            {
                "name": "Washington",
                "code": "wa"
            },
            {
                "name": "West Virginia",
                "code": "wv"
            },
            {
                "name": "Wisconsin",
                "code": "wi"
            },
            {
                "name": "Wyoming",
                "code": "wy"
            }
        ]
    },
    "um": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "uy": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "uz": {
        "language": "uz",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "vu": {
        "language": "bi",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ve": {
        "language": "es",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "vn": {
        "language": "vi",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "vg": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "wf": {
        "language": "fr",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "0",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "eh": {
        "language": "ar",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "ye": {
        "language": "ar",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "zm": {
        "language": "en",
        "postalLabel": "Postal code",
        "postal": "1",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    },
    "zw": {
        "language": "en",
        "postalLabel": "",
        "postal": "0",
        "digits": "2",
        "formBuilder": "",
        "cityLabel": "City",
        "provinceLabel": "",
        "provinces": []
    }
};
// guOffers v2.999
// --- Notes ---
// --- If upgrading from 2.5 make sure all of the offers work ok
//
// --- Add to header under the appearance theme editor ---
// - add 'data-name="<?php wp_title(''); ?>"' on the <body> tag
//
//

// exclude from running in vc editor (wp bakery)
var qs = document.location.search;
if (!qs.includes('vc_editable')) {

    // universal vars
    var titaniumOverride = [];
    var carthook = false;

    // fns
    jQuery.fn.outerHTML = function () {
        return jQuery('<div />').append(this.eq(0).clone()).html();
    };

    function fireButtonEvent(target) {
        var ID;
        if (!target.id) {
            //element  has no id, pass parent id
            ID = target.parentElement.id;
        } else {
            ID = target.id;
        }
        gulog.fireAndLogEvent("Goto Offer Section", "Click", null, "Button ID", ID);
    }

    function prepareCanvas() {
        if (mobileDevice) {
            jQuery('#footer, #badges').slideUp();

            if (!jQuery('#offer .offer.selected .quantity').length) {
                jQuery('#offer #buy-now').slideUp();
            }
        } else {
            jQuery('#faq, #footer, #badges').slideUp();
        }
    }

    // general fns
    function prepPage() {
        jQuery(function () {
            // remove google translate link
            if (jQuery('#google-translate').length) {
                setTimeout(function () {
                    jQuery('#google-translate f.goog-logo-link').removeAttr('href');
                }, 1000);
            }

            // extra fns
            prepBobbingArrow();
            prepAnchorsForGiddyboxes();
        });
    }

    function prepBobbingArrow() {
        jQuery(function () {
            // append bobbing arrow
            jQuery('#s1').append('<a class="bobbing_arrow animated bounce infinite" onclick="bobbingArrowClick();"><img src="/images/dropdown-arrow.svg" /></a>');
            var bobbingArrow = jQuery('#s1 .bobbing_arrow');
            setTimeout(function () {
                bobbingArrow.fadeIn('slow');
            }, 1000);

            // hide the bobbing arrow on click
            jQuery(document).scroll(function () {
                if (jQuery(document).scrollTop() > 200) {
                    bobbingArrow.fadeOut();
                }
            });
        });
    }

    function bobbingArrowClick() {
        jQuery(function () {
            jQuery('html, body').animate({scrollTop: jQuery("#s2").offset().top}, 1000);
            gu_fire_event('BobbingArrow', 'Tap', 's1');
        });
    }

    function prepAnchorsForGiddyboxes() {
        jQuery(function () {
            jQuery('a').each(function () {
                // if anchor contains href
                if (jQuery(this).attr('href') != null) {
                    var href = jQuery(this).attr('href');

                    // if href contains giddybox
                    if (~href.indexOf('giddybox-')) {
                        href = href.split('giddybox-')[1];
                        if (~href.indexOf('?')) {
                            href = href.split('?')[0];
                        }
                        jQuery(this).removeAttr('href');
                        jQuery(this).attr('onclick', 'giddybox("' + href + '");');
                    }
                }
            });
        });
    }

    prepPage(); // add page fns

    function prepOffers() {
        jQuery(function () {
            var offers = '#offer .offer';

            // run for each offer
            jQuery(offers).each(function (index) {
                var target = '#' + jQuery(this).attr('id');

                var carthookUrl = jQuery(this).find('button').attr('onclick');
                var qsParams = gu_qs_to_str(gu_qs);

                //check if carthook url already has parameters and encode
                if (carthookUrl) {
                    if (carthookUrl.match('ref=')) {
                        var baseUrl = carthookUrl.split('ref=')[0];

                        var encodedUrl = encodeURIComponent(window.location);

                        carthookUrl = baseUrl + 'ref=' + encodedUrl;
                    }
                    else {
                        //just add the params on without encoding
                        carthookUrl += qsParams;
                    }
                }



                // set offer onclick
                if (jQuery('#offer').length && !jQuery('#offer').hasClass('special')) {


                    //check if button has an external link in the onclick, if so, set it up for the whole bundle
                    if (carthookUrl && carthookUrl.indexOf('http') >= 0)
                    {
                        carthook = true;
                        jQuery(this).attr('onclick', "window.location.href = '" + carthookUrl + "';");
                        jQuery(this).find('button').attr("onclick", "window.location.href = '" + carthookUrl + "';");
                    }
                    else {
                        jQuery(this).attr('onclick', 'offerClick(this.id);');
                    }

                }

                if (jQuery('#offer').length && jQuery('#offer').hasClass('special2')) {

                    //check if button has an external link in the onclick, if so, set it up for the whole bundle
                    if (carthookUrl && carthookUrl.indexOf('http') >= 0)
                    {
                        carthook = true;
                        jQuery(this).attr('onclick', "window.location.href = '" + carthookUrl + "';");
                        jQuery(this).find('button').attr("onclick", "window.location.href = '" + carthookUrl + "';");
                    }
                    else {
                        jQuery(this).attr('onclick', 'special1order(this.id);');
                    }

                }

                // mobile only
                if (mobileDevice) {

                    if (jQuery(this).find('.carthookUrl').length) {
                        carthookUrl = jQuery.trim(jQuery(this).find('.carthookUrl').text());

                        //check if carthook url already has parameters and encode
                        if (carthookUrl) {
                            if (carthookUrl.match('ref=')) {
                                var baseUrl = carthookUrl.split('ref=')[0];

                                var encodedUrl = encodeURIComponent(window.location);

                                carthookUrl = baseUrl + 'ref=' + encodedUrl;
                            }
                            else {
                                //just add the params on without encoding
                                carthookUrl += qsParams;
                            }
                        }


                        carthook = true;
                        jQuery(this).attr('onclick', "window.location.href = '" + carthookUrl + "';");
                        delete jQuery(this .carthookUrl);
                    }

                    //this is the selected bundle and we're doing the carthook thing, apply the onclick to the order now button to get full url + gu_qs
                    if(carthook && jQuery(this).hasClass('selected')){
                        jQuery('#buy-now button').attr('onClick', jQuery(this).attr('onclick'));
                    }


                    // set offer index
                    var offerIndex = index + 1;

                    // reconstruct product image
                    var img = jQuery('.image img', this);
                    var imgHTML = img.outerHTML();
                    img.parent().remove();
                    jQuery(this).prepend('<div class="left img"><div class="center"><div class="center-anchor">' + imgHTML + '</div></div></div>');

                    // get badge
                    if (jQuery(target).find('.ribbon').length) {
                        var badge = jQuery('.ribbon', target);

                        // add badge
                        if (badge.length) {
                            var badgeHTML = badge.outerHTML();
                            badge.remove();
                            jQuery(target).append(badgeHTML);
                            jQuery(target).find('.ribbon').show();
                        }

                        // fix badge text size
                        fixBadgeTextSize(target);
                    }


                    // reconstruct content
                    jQuery('.text', this).wrapAll('<div class="content"><div class="center"><div class="center-anchor"><div class="wrapper"></div></div></div></div>');
                    var content = jQuery('.content', this).outerHTML();
                    jQuery('.wpb_column', this).remove();

                    if (!jQuery('.content', this).length) {
                        jQuery('.left', this).after(content);
                    }

                    // add checkbox
                    jQuery('.left', this).before('<div class="left"><div class="center"><div class="center-anchor"><div class="checkbox" id="bundle-' + (index + 1) + '-checkbox"><input name="check" type="checkbox"></div></div></div></div>');

                    // check if pre-selected
                    if (jQuery(this).hasClass('selected')) {
                        jQuery('.checkbox input', this).prop('checked', true);
                    }

                    // add quantity
                    if (jQuery('.quantity', this).length) {
                        // fix onclick
                        var onClick = jQuery(this).attr('onclick');
                        if (!jQuery('#offer').hasClass('special')) {
                            // if (!jQuery('.presell-popup').length || mobileDevice) { // xtrapc mobile fix
                            jQuery(this).attr({
                                'onclick': 'selectOffer(this.id,true)',
                                'data-onclick': onClick
                            });
                            // } else {
                            //     jQuery(this).attr('data-onclick', onClick);
                            // }
                        }

                        // add html
                        if (jQuery('#offer').hasClass('special')) {
                            var qtyHTML = '<div class="qty-wrapper"><form action="" method="post"><label for="name">' + guSubstituteString('Qty') + '</label><a id="' + this.id + '-minus" class="minus btn" onclick="offerQtySub(this.id);"></a><input id="partridge" name="partridge" type="text" value="0"><a id="' + this.id + '-plus" class="plus btn" onclick="special1order(this.id);"></a></form><p style="text-align: center;"></p></div>';
                        } else {
                            var qtyHTML = '<div class="qty-wrapper"><form action="" method="post"><label for="name">' + guSubstituteString('Qty') + '</label><a id="' + this.id + '-minus" class="minus btn" onclick="offerQtySub(this.id);"></a><input id="partridge" name="partridge" type="text" value="1"><a id="' + this.id + '-plus" class="plus btn" onclick="offerQtyAdd(this.id);"></a></form><p style="text-align: center;"></p></div>';
                        }
                        jQuery('.quantity', this).prepend(qtyHTML);

                        // move qty outside of content
                        var qtyHTML = jQuery('.quantity', this).outerHTML();
                        jQuery('.quantity', this).remove();
                        jQuery('.content', this).after(qtyHTML);

                        //add padding left to line it up witht the content
                        // get items width
                        var checkboxWidth = jQuery('input[type=checkbox]', this).closest('.left').width();
                        var imgWidth = jQuery('.img', this).width();

                        var thePadding = checkboxWidth + imgWidth;

                        if (!jQuery('.quantity', this).is(':visible')) {
                            jQuery('.quantity', this).css('padding-left', thePadding, 'important');
                        }
                    }
                }

                // desktop only
                else {
                    // add ribbon
                    if (jQuery('.ribbon', this).length && !jQuery('.ribbon', this).hasClass('hidden')) {
                        addRibbon(target);
                    }

                    // add quantity
                    if (jQuery('.quantity', this).length) {
                        // fix onclick
                        var onClick = jQuery(this).attr('onclick');
                        if (!jQuery('#offer').hasClass('special')) {
                            onClick = onClick.replace('this.id', 'this.id,true');
                            jQuery('.cta', this).attr('onclick', onClick);
                        }

                        if (!jQuery('.presell-popup').length || mobileDevice) {
                            if (jQuery('#offer').hasClass('special')) {
                                jQuery(this).removeAttr('onclick');
                            } else {
                                jQuery(this).attr('onclick', 'selectOffer(this.id,true)');
                            }
                        } else {
                            jQuery(this).removeAttr('onclick');
                        }

                        // add html
                        if (jQuery('#offer').hasClass('special')) {
                            var qtyHTML = '<div class="qty-wrapper"><h5>' + guSubstituteString("Quantity:") + '</h5><a id="' + this.id + '-minus" class="minus btn" onclick="offerQtySub(this.id);"></a><input name="quantity" type="text" value="0"><a id="' + this.id + '-plus" class="plus btn" onclick="special1order(this.id);"></a></div>';
                        } else {
                            var qtyHTML = '<div class="qty-wrapper"><h5>' + guSubstituteString("Quantity:") + '</h5><a id="' + this.id + '-minus" class="minus btn" onclick="offerQtySub(this.id);"></a><input name="quantity" type="text" value="1"><a id="' + this.id + '-plus" class="plus btn" onclick="offerQtyAdd(this.id);"></a></div>';
                        }

                        jQuery('.quantity', this).prepend(qtyHTML);
                        if (!jQuery('#offer').hasClass('special')) {
                            jQuery('.quantity', this).show();
                        }

                        //if there is no CTA in the bundle section, assume that we're using an independent qty selector
                        //and hide the minus and number when the qty is zero (and by default)
                        // if (!jQuery('.quantity',this).siblings('.cta').length) {
                        //     offerQtyZeroOut(jQuery('.quantity',this));
                        // }


                    }
                }

                // if extras exist, change Order Now to next
                if (jQuery('#offer #colors').length || jQuery('#offer #presell').length || jQuery('#offer #accessories').length || jQuery('#size-select').length) {

                    // Update buy now btn
                    var text = jQuery('#offer #buy-now-next button').text(),
                        bg_color = jQuery('#offer #buy-now-next button').css('background-color'),
                        txt_color = jQuery('#offer #buy-now-next button').css('color');

                    jQuery('#offer #buy-now').addClass('next');
                    jQuery('#offer #buy-now.next button').text(text);
                    jQuery('#offer #buy-now.next button').css({
                        'background-color': '' + bg_color + '',
                        'color': '' + txt_color + ''
                    });
                }
            });

            // wrap mobile offers
            if (mobileDevice) {
                jQuery('#offer .offer').wrapAll('<div class="offersWrapper"></div>');
            }
        });
    }

    function fixBadgeTextSize(target) {
        jQuery(function () {
            var ribbonWidth = jQuery('.ribbon', target).width();
            var textWidth = jQuery('.ribbon p', target).width();

            // test widths
            if (ribbonWidth - 60 < textWidth) {
                // get font size
                var fontSize = parseInt(jQuery('.ribbon p', target).css('font-size').replace('px', '')) - 2;
                var fontSize = fontSize + 'px';

                // set font size
                jQuery('.ribbon p', target).css('font-size', fontSize);

                // test again
                fixBadgeTextSize(target);
            }
        });
    }

    function setContentWidth() {
        // mobile only
        if (mobileDevice) {
            jQuery(function () {
                var offers = '#offer .offer';

                // run for each offer
                jQuery(offers).each(function () {
                    // get offer width
                    var offerPaddingLeft = parseInt(jQuery(this).css('padding-left').replace('px', ''));
                    var offerPaddingRight = parseInt(jQuery(this).css('padding-right').replace('px', ''));
                    var offerWidth = jQuery(this).width() - (offerPaddingLeft + offerPaddingRight);

                    // get items width
                    var checkboxWidth = jQuery('input[type=checkbox]', this).width();
                    var imgWidth = jQuery('.img', this).width();

                    // get content width
                    var contentWidth = offerWidth - (checkboxWidth + imgWidth + 20); // +20 for extra slack

                    // if offer has a badge
                    if (jQuery(this).find('.badge').length) {
                        contentWidth = contentWidth - 25;
                    }

                    // set content width
                    jQuery('.content', this).width(contentWidth);
                });
            });
        }
    }

    function setOfferHeights() {
        // mobile only
        if (mobileDevice) {
            jQuery(function () {
                var offers = '#offer .offer';

                // run for each offer
                jQuery(offers).each(function () {
                    // set height
                    var imgHeight = jQuery('.img', this).height() + 5;
                    var conHeight = jQuery('.content .wrapper', this).height() + 5;
                    var height = (imgHeight > conHeight) ? imgHeight : conHeight;

                    // set img wrapper height
                    if (imgHeight < conHeight) {
                        jQuery('.img', this).closest('.left').height(height);
                    }

                    // set content height
                    jQuery('.content', this).height(height);

                    // set checkbox height
                    jQuery('.checkbox', this).closest('.left').height(height);
                });
            });
        }
    }

    function hideCheckout() {
        var checkout_el = jQuery('#checkout');
        var presell = findPresell();
        if (checkout_el.is(':visible')) {
            if (jQuery('#colors').length && jQuery('#colors').is(':visible') ||
                jQuery('#accessories').length && jQuery('#accessories').is(':visible') ||
                presell && presell.is(':visible')) {
                if (mobileDevice) {
                    checkout_el.slideUp();
                } else {
                    checkout_el.slideUp(1500);
                }
            }
        }
    }

    function hideExtras() {
        if (jQuery('#colors').length && jQuery('#colors').is(':visible')) {
            jQuery('#colors').slideUp();
        }
        if (jQuery('#accessories').length && jQuery('#accessories').is(':visible')) {
            jQuery('#accessories').slideUp();
        }
        if (jQuery('#presell').length && jQuery('#presell').is(':visible')) {
            jQuery('#presell').slideUp();
        }
    }

    function addRibbon(target) {
        jQuery(function () {
            // check if ribbon was already added (in case of a re-test)
            if (!jQuery('.corner-ribbon', target).length) {
                // get the ribbon
                var ribbon = jQuery('.ribbon p', target).outerHTML();
                var ribbonBgColor = jQuery('.ribbon', target).css('background-color');

                // add the ribbon
                jQuery('.ribbon', target).remove();
                jQuery(target).prepend('<div class="corner-ribbon top-right sticky" style="background-color: ' + ribbonBgColor + ';">' + ribbon + '</div>');
            }

            // fix ribbon text size
            var ribbonWidth = jQuery('.corner-ribbon', target).width();
            var textWidth = jQuery('.corner-ribbon p', target).width();

            // test widths
            if (ribbonWidth - 60 < textWidth) {
                // get font size
                var fontSize = parseInt(jQuery('.corner-ribbon p', target).css('font-size').replace('px', '')) - 2;
                var fontSize = fontSize + 'px';

                // set font size
                jQuery('.corner-ribbon p').css('font-size', fontSize);

                // test again
                addRibbon(target);
            }
        });
    }

    // prep offers for checkout
    prepOffers();

    function offerClick(id, qty) {
        jQuery(function () {
            // hide chat
            if (jQuery('#chat-widget-container').length) {
                jQuery('#chat-widget-container').hide();
            }

            //make sure that we have retrieved all necessary data before proceeding, if not, force user to reload page and try again
            if (!gu_remote_data.loaded) {
                //create new Giddybox to prompt reload of page
                var reloader_el = '\
                                <style type="text/css">\
                                    .reload_button button {color: white !important; background-color: #f47300 !important; font-weight: bold; font-size: 24px !important; text-transform: uppercase; transition: all 200ms ease 0s !important; margin: 0 auto !important; width: 50% !important; box-shadow: 0 2px 0px 0px #2a4a58 !important;}\
                                    .reload_button button:hover { color:white !important; background-color:#B40019 !important;font-weight: bold; text-transform: uppercase; transition:0.7s; transform:translate(0, -3px); }\
                                </style>\
                                <div id="reloader">\
                                    <p style="text-align: center; padding: 20px;" id="reloader-text">There was a problem retrieving pricing. Please click here to reload the page.</p>\
                                    <div class="vc_btn3-container  cta vc_btn3-center reload_button">\
                                        <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-color-grey" style="" onclick="location.reload();">Reload</button>\
                                    </div>\
                                </div>\
                            ';

                var reloadNode = createEl(reloader_el);

                document.getElementById('about').parentNode.insertBefore(reloadNode, document.getElementById('about').nextSibling);

                guSubstituteContent(document.getElementById('reloader'));

                setTimeout(function () {
                    giddybox("reloader");
                    // hide close btn
                    jQuery('#giddybox .giddybox-closeBtn').css('display', 'none');
                }, 500);


            } else {
                // set target for bundles w/ quantity
                if (qty == true) {
                    // set target
                    var target = jQuery('#' + jQuery('#' + id).closest('.offer').attr('id'));

                    // update onclicks for each offer w/ quantity
                    jQuery('#offer .offer').each(function () {
                        if (jQuery('.quantity', this).length) {
                            if (!jQuery('.presell-popup').length) {
                                // update btn onclick
                                jQuery('.cta', this).attr('onclick', '');

                                // update bundle onclick
                                if (!jQuery('#offer').hasClass('special')) {
                                    jQuery(this).attr('onclick', 'offerClick(this.id);');
                                }
                            }
                        }
                    });

                    // add prevActivated class
                    setTimeout(function () {
                        jQuery('#offer').addClass('prevActivated');
                    }, 1000);
                }

                // set target (no quantity)
                else {
                    var target = jQuery('#' + id);
                }

                // reset qty if qty exist in offer
                if (mobileDevice && jQuery('#offer .offer.selected .quantity').length) {
                    jQuery('#offer .offer:not(.selected) .quantity input').val('1');
                }

                // optm Hash
                window.location.hash = "selected";

                // check products api
                if (gu_remote_data.productData.length < 1) {
                    console.log('no product data');
                }

                // extra fns
                disableSalesPopup();
                selectOffer(target);
                //submitBundle();

                if (jQuery('#accessories').is(':visible')) {
                    dupeAccessories();
                }

                // hide the checkout
                hideCheckout();

                // fire cake pixel

                // fire event
                gulog.fireAndLogEvent("Select Bundle", "Click", null, "Bundle Name", id);

            }
        });
    }

    function offerQtyAdd(selector) {
        jQuery(function () {
            var inp = jQuery('#' + selector).siblings('input');

            // update canvas
            hideCheckout();

            // add 1
            if (inp.val() >= 1) {
                var val = inp.val();
                val++;
                inp.val(val);
            } else if (inp.val() == 0) {
                //questionable (could be for special only)
                //must be an independent qty currently at zero. Fade in other elements and add one
                var val = inp.val();
                val++;
                inp.val(val);
                jQuery('#' + selector).siblings('.minus').add(inp).fadeIn();
            }

            if (jQuery('#offer').hasClass('special')) {
                updateStoragePrices();
            }

            if (jQuery('#price-tab').length) {
                getPricebar();
            }
        });
    }

    function offerQtySub(selector) {
        jQuery(function () {
            var inp = jQuery('#' + selector).siblings('input');

            // update canvas
            hideCheckout();

            // special1
            if (jQuery('#offer').hasClass('special') && inp.val() == 1) {
                jQuery('#' + selector).closest('.offer').removeClass('selected');
                inp.val(0);

                jQuery('#' + selector).closest('.offer').find('.quantity .plus').attr('onclick', 'special1order(this.id);');
            }

            // subtract 1
            if (inp.val() >= 2) {
                var val = inp.val();
                val--;
                inp.val(val);
            }

            if (jQuery('#offer').hasClass('special')) {
                updateStoragePrices();

                // deactivate checkbox for special2
                if (jQuery('#offer').hasClass('special2') && jQuery('#' + selector).closest('.quantity').find('input').val() == 0) {
                    jQuery('#' + selector).closest('.offer').find('.checkbox').removeAttr('onclick');
                    jQuery('#' + selector).closest('.offer').find('.checkbox input').prop('checked', false);
                    setTimeout(function () {
                        jQuery('#' + selector).closest('.offer').attr('onclick', 'special1order(this.id);');
                    }, 200);
                }
            }

            if (jQuery('#price-tab').length && jQuery('#offer #price-tab .price').attr('data-price') > 0) {
                getPricebar();
            }

        });
    }


    function special1() {
        // prep
        jQuery('#storage').slideDown();
        getStorage();
        getPricebar();

        // scroll
        jQuery('html, body').animate({scrollTop: jQuery('#storage').offset().top}, 500);

        // cake pixel
        gulog.fireAndLogEvent("Select Bundle");
    }

    function special1Setup() {
        // setup storage
        jQuery('#storage .storage').each(function (index) {
            // add onclick
            jQuery(this).attr('onclick', 'storageClick(' + index + ');');

            // create checkmark
            jQuery(this).prepend('<div class="checkmark"><div class="center"><div class="center-anchor"><input name="check" type="checkbox" /></div></div>');

            // check first checkmark
            if (index == 0) {
                jQuery('.checkmark input', this).prop('checked', true);
            }

            // set checkmark size
            var height = jQuery('img.alignleft', this).height();
            jQuery('.checkmark', this).height(height);

            // get size
            var storageSize;
            jQuery(this).each(function () {
                storageSize = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('stor_') === 0;
                }).join();
            });
            storageSize = storageSize.split('_')[1];

            // get product
            function getPrices(index) {
                var storageIndex = index;

                jQuery('#offer .offer:visible').each(function (index) {
                    var productClass;
                    jQuery('#offer .offer:visible').eq(index).each(function () {
                        productClass = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var discProductClass;
                    jQuery('#offer .offer:visible').eq(index).each(function () {
                        discProductClass = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('qd-guproduct_') === 0;
                        }).join();
                    });
                    discProductClass = discProductClass.split('qd-')[1];

                    var product = gu_products.getProduct(eval(productClass));
                    if (product == undefined || product == null) {
                        console.log('finding products...');
                        setTimeout(function () {
                            getPrices(storageIndex);
                        }, 500)
                    } else {
                        // get prices
                        var discProduct = gu_products.getProduct(eval(discProductClass));
                        var variant = product.variants[storageIndex];
                        var discVariantPrice = discProduct.variants[storageIndex].price;

                        // set unit price prices
                        var unitPrice = variant.price;

                        jQuery('#storage .storage').eq(storageIndex).find('.unitprice').text(guDisplayCurrency(unitPrice) + ' / each');

                        // jQuery('#storage .storage').eq(index).attr('data-unit', unitPrice);
                        // jQuery('#storage .storage').eq(index).attr('data-perunit', unitPrice);
                        // jQuery('#storage .storage').eq(index).attr('data-discprice', discVariantPrice);

                        // add prices
                        if (!jQuery('#storage .storage').eq(storageIndex).find('.prices').length) {
                            jQuery('#storage .storage').eq(storageIndex).append('<div class="prices"></div>');
                        }

                        // add bundle
                        jQuery('#storage .storage').eq(storageIndex).find('.prices').append('<div class="bundle bundle-' + (index + 1) + '"></div>');

                        // set unit prices
                        var bundle = jQuery('#storage .storage').eq(storageIndex).find('.prices .bundle-' + (index + 1) + '');
                        bundle.attr('data-unit', unitPrice);
                        bundle.attr('data-discprice', discVariantPrice);
                    }
                });
            }

            getPrices(index);
        });

        // update cta onclick
        jQuery('#offer .offer').each(function () {
            jQuery('.cta', this).attr('onclick', 'special1order(this.id);');
        });

        // fix storage
        jQuery('#storage .storage').wrapAll('<div class="storage-wrap"></div>');
    }

    if (jQuery('#offer').hasClass('special')) {
        special1Setup();
    }

    function special1order(id) {
        // hide chat
        if (jQuery('#chat-widget-container').length) {
            jQuery('#chat-widget-container').hide();
        }

        var offerId = jQuery('#' + jQuery('#' + id).closest('.offer').attr('id'));

        // offerId mobile fix
        if (jQuery(offerId).hasClass('checkbox')) {
            offerId = jQuery('#' + jQuery(offerId).closest('.offer').attr('id'));
        }

        if (mobileDevice) {
            if (jQuery(offerId).hasClass('selected') && jQuery('#offer').hasClass('special2')) {
                // if already selected
                setTimeout(function () {
                    jQuery(offerId).removeClass('selected');
                    jQuery('.quantity input', offerId).val(0);
                    jQuery('.checkbox input', offerId).prop('checked', false);

                    // update onclicks
                    jQuery('.checkbox', offerId).removeAttr('onclick');
                    jQuery(offerId).attr('onclick', 'special1order(this.id);');

                    // update canvas
                    // hideCheckout();
                    updateStoragePrices();
                    getPricebar();
                }, 200);
            } else if (jQuery('#offer').hasClass('special2')) {
                // select
                jQuery(offerId).addClass('selected');
                jQuery('.checkbox input', offerId).prop('checked', true);

                // update onclicks
                jQuery(offerId).removeAttr('onclick');
                jQuery('.checkbox', offerId).attr('onclick', 'special1order(this.id);');

                // set qty
                jQuery('.quantity input', offerId).val(1);
            } else {
                // select
                jQuery(offerId).addClass('selected');

                // set qty
                jQuery('.quantity input', offerId).val(1);

                if (jQuery('#chat-widget-container').length) {
                    jQuery('#chat-widget-container').hide();
                }
            }
        } else {
            // select
            jQuery(offerId).addClass('selected');

            // set qty
            jQuery('.quantity input', offerId).val(1);

        }

        // set structure
        // jQuery('.cta',offerId).slideUp();
        // jQuery('.quantity',offerId).slideDown();
        jQuery('.quantity .plus', offerId).attr('onclick', 'offerQtyAdd(this.id);');

        // scroll to bottom
        if (!jQuery('#storage').is(':visible')) {
            setTimeout(function () {
                jQuery('html, body').animate({scrollTop: jQuery(document).height()}, 'slow');
            }, 500);
        }

        getPricebar();
        prepareCanvas();
        updateStoragePrices();
    }

    function storageClick(selected) {
        var selectedEl = jQuery('#storage .storage').eq(selected);
        jQuery('#storage .storage').each(function (index) {
            if (index == selected) {
                if (selectedEl.hasClass('selected')) {
                    selectedEl.removeClass('selected');
                    selectedEl.find('.checkmark input').prop('checked', false);
                } else {
                    selectedEl.addClass('selected');
                    selectedEl.find('.checkmark input').prop('checked', true);
                }
            } else {
                jQuery(this).removeClass('selected');
                jQuery(this).find('.checkmark input').prop('checked', false);
            }
        });

        // select 32gb if none selected
        if (!jQuery('#storage .storage.selected').length) {
            jQuery('#storage .storage').eq(0).addClass('selected');
        }

        // update total
        // updateStoragePrices();
        getPricebar();
    }

    function updateStoragePrices() {
        // get qty
        var totalQty = 0;
        jQuery('#offer .offer.selected:visible').each(function () {
            var qty = jQuery('.quantity input', this).val();
            totalQty = Big(totalQty).add(qty);
        });

        // get price diff
        jQuery('#storage .storage').each(function (index) {
            var unitEl = jQuery('.unitprice', this);
            var storageIndex = index;

            if (storageIndex > 0) {
                // get first bundle highest cost
                var bundles = [];

                jQuery('#storage .storage').first().find('.prices .bundle').each(function () {
                    var price = jQuery(this).attr('data-unit');
                    if (totalQty > 1) {
                        var discPrice = jQuery(this).attr('data-discprice');
                        var discPrice = Big(discPrice).mul((totalQty - 1));
                        var totalPrice = Big(price).add(discPrice);
                        price = Big(totalPrice).div(totalQty);
                    }
                    bundles.push(price);
                });

                var highest = 0;
                jQuery.each(bundles, function (index, value) {
                    if (parseInt(value) > parseInt(highest)) highest = value;
                });

                var firstBundleHighest = highest;

                // get current bundle highest cost
                bundles = [];

                jQuery('.prices .bundle', this).each(function () {
                    var price = jQuery(this).attr('data-unit');
                    if (totalQty > 1) {
                        var discPrice = jQuery(this).attr('data-discprice');
                        var discPrice = Big(discPrice).mul((totalQty - 1));
                        var totalPrice = Big(price).add(discPrice);
                        price = Big(totalPrice).div(totalQty);
                    }
                    bundles.push(price);
                });

                highest = 0;
                jQuery.each(bundles, function (index, value) {
                    if (parseInt(value) > parseInt(highest)) highest = value;
                });

                var currentBundleHighest = highest;

                // get price difference
                var priceDiff = guDisplayCurrency(Big(currentBundleHighest).sub(firstBundleHighest));

                // set text
                unitEl.text('+' + priceDiff + ' ' + guSubstituteString('per PhotoStick'));
            }
        });
    }

    // dechoker size select
    function dechoker_init() {
        jQuery(function () {

            var $el = jQuery('#size-select');

            // check if chocker exists
            if ($el.length) {

                // vars
                var chokerClass     = '.choker';
                var activeClass     = 'active';

                var $choker         = $el.find(chokerClass);
                var $option         = $choker.find('ul li');

                // hide depending on offer
                var productClasses;
                jQuery('#offer .offer.selected').each(function () {
                    productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('guproduct_') === 0;
                    }).join();
                });
                var offerIndex = productClasses.split(',').length;
                console.log('offer index length',offerIndex);

                if (offerIndex == 1) {
                    if ($choker.eq(0).length) {
                        $choker.eq(0).show();
                    }
                    if ($choker.eq(1).length) {
                        $choker.eq(1).hide();
                    }
                    if ($choker.eq(2).length) {
                        $choker.eq(2).hide();
                    }
                    if ($choker.eq(3).length) {
                        $choker.eq(3).hide();
                    }
                    if ($choker.eq(4).length) {
                        $choker.eq(4).hide();
                    }
                } else if (offerIndex == 2) {
                    if ($choker.eq(0).length) {
                        $choker.eq(0).show();
                    }
                    if ($choker.eq(1).length) {
                        $choker.eq(1).show();
                    }
                    if ($choker.eq(2).length) {
                        $choker.eq(2).hide();
                    }
                    if ($choker.eq(3).length) {
                        $choker.eq(3).hide();
                    }
                    if ($choker.eq(4).length) {
                        $choker.eq(4).hide();
                    }
                } else if (offerIndex == 3) {
                    if ($choker.eq(0).length) {
                        $choker.eq(0).show();
                    }
                    if ($choker.eq(1).length) {
                        $choker.eq(1).show();
                    }
                    if ($choker.eq(2).length) {
                        $choker.eq(2).show();
                    }
                    if ($choker.eq(3).length) {
                        $choker.eq(3).hide();
                    }
                    if ($choker.eq(4).length) {
                        $choker.eq(4).hide();
                    }
                } else if (offerIndex == 4) {
                    if ($choker.eq(0).length) {
                        $choker.eq(0).show();
                    }
                    if ($choker.eq(1).length) {
                        $choker.eq(1).show();
                    }
                    if ($choker.eq(2).length) {
                        $choker.eq(2).show();
                    }
                    if ($choker.eq(3).length) {
                        $choker.eq(3).show();
                    }
                    if ($choker.eq(4).length) {
                        $choker.eq(4).hide();
                    }
                } else if (offerIndex == 5) {
                    if ($choker.eq(0).length) {
                        $choker.eq(0).show();
                    }
                    if ($choker.eq(1).length) {
                        $choker.eq(1).show();
                    }
                    if ($choker.eq(2).length) {
                        $choker.eq(2).show();
                    }
                    if ($choker.eq(3).length) {
                        $choker.eq(3).show();
                    }
                    if ($choker.eq(4).length) {
                        $choker.eq(4).show();
                    }
                }

                // set defaults
                $choker.each(function() {
                    if (jQuery(this).find('.active').length === 0 ) {

                        // clean up
                        jQuery('ul li',this).removeClass(activeClass);
                        jQuery('ul li input',this).prop('checked', false);

                        // set first as active
                        jQuery('ul li',this).first().addClass(activeClass);
                        jQuery('ul li',this).first().find('input').prop('checked', true);
                    } else {
                        jQuery('.active input',this).prop('checked', true);
                    }
                });

                // on click
                $option.on('click', function () {

                    // reset checkout
                    if (jQuery('#checkout').is(':visible')) {
                        prepCheckout();
                    }

                    // clear class & checkbox
                    jQuery(this).closest(chokerClass).find('ul li').removeClass(activeClass);
                    jQuery(this).closest(chokerClass).find('ul li input').prop('checked', false);

                    // set to active
                    jQuery(this).addClass(activeClass);
                    jQuery(this).find('input').prop('checked', true);
                });
            }

        });
    }

    function getDechoker() {
        dechoker_init();

        jQuery(function () {

            // show the dechoker
            var $el = jQuery('#size-select');
            $el.slideDown();

            if (!jQuery('#checkout').is(':visible')) {
                console.log('scroll to dechoker!!!');
                setTimeout(function () {
                    jQuery('html, body').animate({scrollTop: $el.offset().top}, 500);
                }, 500);
            }

            // get pricebar
            getPricebar();
        });

    }

    function buyNowClick() {
        jQuery(function () {
            var target = jQuery('#offer .offer.selected:visible').attr('id');

            if (jQuery('#offer .offer.selected .quantity').length) {
                offerClick(target, true);
            } else {
                offerClick(target);
            }
        });
    }

    function selectOffer(target, qty) {
        jQuery(function () {
            // has qty -> only select offer
            if (qty == true) {
                target = jQuery('#' + target);

                // reset counter for other offers
                if (mobileDevice) {
                    // reset qty
                    if (!jQuery('#offer').hasClass('special')) {
                        jQuery('#offer .offer:not(.selected) .quantity input').val('1');
                    }

                    // open the pricebar
                    if (jQuery('#price-tab').length) {
                        getPricebar();
                    }
                }
            } else {
                prepareCanvas();
            }

            // select offer
            if (!target.hasClass('selected')) {
                // remove prev selection as long as there is a CTA button
                // if (target.find('.cta').length) {
                // questionable if statement (may need "special only")
                var selected = jQuery('#offer .offer.selected:visible');
                if (selected.length) {
                    selected.removeClass('selected');
                    selected.find('.checkbox input').prop('checked', false);
                }

                // mobile only (checkbox)
                if (mobileDevice) {
                    // remove prev selected checkbox
                    if (selected.length) {
                        selected.removeClass('selected');
                    }

                    // check target checkbox
                    target.find('.checkbox input').prop('checked', true);
                }
                // }

                // set target as selected as long as there's some quantity or no selector at all
                // another questionable if statement for special only maybe
                if (target.find('.quantity input').val() != 0) {
                    target.addClass('selected');
                }
            }

            // check for extras
            // questionable if statement
            if (qty != true) {
                getExtras(target);
            }
            else
            {
                //if there's a qty' and we've already selected something,
                if (jQuery('#offer').hasClass('prevActivated')) {
                    //make sure we're displaying the correct and current presells and upsells for selected bundle
                    presell = findPresell(true);
                }
            }
        });
    }

    var canUpdateUnits = false;

    function buildUnits(categorizedProducts, canUpdate) {

        var selectedTypes = Object.keys(categorizedProducts);

        for (var i = 0; i < selectedTypes.length; i++) {
            //if we have products for this type, proceed
            if (categorizedProducts[selectedTypes[i]].products) {
                //fix trailing comma
                if (categorizedProducts[selectedTypes[i]].products.slice(-1) === ',') {
                    categorizedProducts[selectedTypes[i]].products = categorizedProducts[selectedTypes[i]].products.slice(0, -1);
                }


                var unit = {
                    name: categorizedProducts[selectedTypes[i]].name,
                    id: categorizedProducts[selectedTypes[i]].id,
                    quantity: 1,
                    type: selectedTypes[i],
                    lineItems: []
                };

                var prods = categorizedProducts[selectedTypes[i]].products.split(',');

                for (var j = 0; j < prods.length; j++) {
                    var id = prods[j].split(':')[0];
                    var qty = prods[j].split(':')[1];

                    var product = gu_products.getVariant(id);

                    var item = {
                        productId: product.product_id,
                        variantName: product.title,
                        variantId: product.id,
                        quantity: parseInt(qty)
                    };

                    unit.lineItems.push(item);
                }

                gu_products.addSelectedUnit(unit);
            }
        }




        //get selected bundle
        // gu_products.addSelectedUnit(buildBundleUnits());
        //
        // //get presell units if active
        // var presell = jQuery('#presell');
        // if (presell.length && presell.hasClass('active')) {
        //     gu_products.addSelectedUnit(buildPresellUnits(presell));
        // }
        //
        // // add presell popup units if active
        // var presellPopup = jQuery('.presell-popup.active');
        // if (presellPopup.length) {
        //     gu_products.addSelectedUnit(buildPresellPopupUnits(presellPopup));
        // }
        //
        // // add upsell units if active
        // var upsell = jQuery('.upsell.active');
        // if (upsell.length) {
        //     gu_products.addSelectedUnit(buildPresellPopupUnits(upsell));
        // }
        //
        //     // add inline upsell units if active
        //     var inlineUpsell = jQuery('.inlineUpsell.active');
        //     if (inlineUpsell.length) {
        //         gu_products.addSelectedUnit(buildInlineUpsellUnits(inlineUpsell));
        //     }
        //
        //     if(canUpdate)
        //     {
        //         canUpdateUnits = true;
        //     }
        //
        // if (canUpdateUnits) {
        //
        //     updateSelectedUnits();
        // }
    }

    function buildUpsellUnits(upsell) {
        //get associated products
        var upsellClass;
        upsell.each(function () {
            upsellClass = jQuery.grep(this.className.split(" "), function (v, i) {
                return v.indexOf('guproduct_') === 0;
            }).join();
        });
        var upsellProduct = upsellClass.split(',');

        var name = 'Upsell';
        var id = upsell.attr('id');
        var unitQty = 1;
        var unit = buildUnit(name, id, unitQty, 'upsell', upsellProduct);
        return unit;
    }

    function buildInlineUpsellUnits(inlineUpsell)
    {
        //get associated products
        var inlineUpsellClass; inlineUpsell.each(function() { inlineUpsellClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('guproduct_') === 0; }).join(); });
        var inlineUpsellProduct = inlineUpsellClass.split(',');

        var name = 'Inline Upsell';
        var id = inlineUpsell.attr('id');
        var unitQty = 1;
        var unit = buildUnit(name, id, unitQty, 'inlineUpsell', inlineUpsellProduct);
        return unit;
    }

    function buildPresellPopupUnits(presellPopup) {
        //get associated products
        var presellPopupClasses;
        presellPopup.each(function () {
            presellPopupClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                return v.indexOf('guproduct_') === 0;
            }).join();
        });
        var presellPopupProducts = presellPopupClasses.split(',');

        var name = 'Presell Popup';
        var id = presellPopup.attr('id');
        var unitQty = 1;
        var unit = buildUnit(name, id, unitQty, 'presell popup', presellPopupProducts);
        return unit;
    }

    function buildPresellUnits(presell) {
        //get associated products
        var presellClasses;
        presell.each(function () {
            presellClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                return v.indexOf('guproduct_') === 0;
            }).join();
        });
        var presellProducts = presellClasses.split(',');


        var name = 'Presell';
        var id = presell.attr('id');

        if (jQuery('.quantity', presell).length) {
            var unitQty = jQuery('.quantity input', presell).val();
        } else {
            var unitQty = 1;
        }

        var unit = buildUnit(name, id, unitQty, 'presell', presellProducts);
        return unit;
    }

    function buildBundleUnits() {
        var name = jQuery('body').attr('data-name');
        var target = jQuery('#offer .offer.selected:visible').attr('id');
        var productClasses;
        jQuery('#' + target).each(function () {
            productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                return v.indexOf('guproduct_') === 0;
            }).join();
        });
        var products = productClasses.split(',');
        var colors = jQuery('#colors');
        var sizes = jQuery('#sizes');
        var storage = jQuery('#storage');
        var unit = {
            name: name,
            id: target,
            quantity: 1,
            type: 'bundle',
            lineItems: []
        };

        var bundleQty = jQuery('#offer .offer.selected .quantity input').val();

        // separate titanium products
        if (titaniumOverride.length) {
            // set values
            jQuery(titaniumOverride).each(function (index, value) {
                var originalVal = value;
                titaniumOverride[index] = products[value];
                products[originalVal] = '';
            });

            // remove clears
            products = products.filter(Boolean);
        }

        // merge similar products
        var counts = {};
        jQuery.each(products, function (key, value) {
            if (!counts.hasOwnProperty(value)) {
                counts[value] = 1;
            } else {
                counts[value]++;
            }
        });

        jQuery.each(counts, function (key, value) {
            if (key == '' || key == null) {
                delete counts[key];
            }
        });

        console.log('*************************');
        console.log('counts', counts);

        // create units
        if (colors.length) {
            var color = jQuery('#colors .color.active ').attr('data-color');

            // variety
            if (color == 'variety') {
                var varietyClasses;
                jQuery('#' + target).each(function () {
                    varietyClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('vp-') === 0;
                    }).join();
                });
                var varieties = varietyClasses.split(',');

                for (i = 0; i < varieties.length; i++) {
                    var product = gu_products.getProduct(eval(products[i]));
                    var varColor = varieties[i].replace('vp-', '');
                    if (~varColor.indexOf('-')) {
                        varColor = varColor.replace(/\-/g, ' ');
                    }
                    var variant = jQuery.grep(product.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        return (variantName == varColor);
                    });

                    // add item
                    var item = {
                        productId: (gu_products.getProduct(eval(products[i]))).id,
                        variantName: variant[0].title,
                        variantId: variant[0].id,
                        quantity: 1
                    };
                    unit.lineItems.push(item);
                }

                // add premium colors
                var premiumColorClass;
                jQuery('#colors').each(function () {
                    premiumColorClass = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('guproduct_') === 0;
                    }).join();
                });
                if (premiumColorClass) {
                    var premiumColorObj = gu_products.getProduct(eval(premiumColorClass));
                    var premiumVariant = jQuery.grep(premiumColorObj.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        return (variantName === 'variety');
                    });

                    // add item
                    var premiumItem = {
                        productId: premiumVariant[0].product_id,
                        variantName: premiumVariant[0].title,
                        variantId: premiumVariant[0].id,
                        quantity: 1
                    };
                    unit.lineItems.push(premiumItem);
                }
            } else {
                // standard
                // add products
                jQuery.each(counts, function (key, value) {
                    var product = gu_products.getProduct(eval(key));
                    var variant = jQuery.grep(product.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        if (~color.indexOf('-')) {
                            color = color.replace(/\-/g, ' ');
                        }
                        return (variantName === color);
                    });

                    // add item
                    var item = {
                        productId: eval(key),
                        variantName: variant[0].title,
                        variantId: variant[0].id,
                        quantity: value
                    };
                    unit.lineItems.push(item);

                    // add premium colors
                    if (jQuery('#colors').length && jQuery('#colors .color.active .price').length) {
                        var premiumColorClass;
                        jQuery('#colors').each(function () {
                            premiumColorClass = jQuery.grep(this.className.split(" "), function (v, i) {
                                return v.indexOf('guproduct_') === 0;
                            }).join();
                        });
                        var premiumColorObj = gu_products.getProduct(eval(premiumColorClass));
                        var premiumVariant = jQuery.grep(premiumColorObj.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            if (~color.indexOf('-')) {
                                color = color.replace(/\-/g, ' ');
                            }
                            return (variantName === color);
                        });

                        if (Big(premiumVariant[0].price) > 1) { // big
                            // add item
                            var premiumItem = {
                                productId: premiumVariant[0].product_id,
                                variantName: premiumVariant[0].title,
                                variantId: premiumVariant[0].id,
                                quantity: value
                            };
                            unit.lineItems.push(premiumItem);
                        }
                    }
                });

                // add discounted products
                var discQtyProd;
                jQuery('#offer .offer.selected').each(function () {
                    discQtyProd = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('qd-') === 0;
                    }).join();
                });
                if (bundleQty > 1 && discQtyProd != '') {
                    jQuery.each(counts, function (key, value) {
                        var product = gu_products.getProduct(eval(key));
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            if (~color.indexOf('-')) {
                                color = color.replace(/\-/g, ' ');
                            }
                            return (variantName === color);
                        });

                        if (variant[0].price > 1) {
                            var product = gu_products.getProduct(eval(discQtyProd.replace('qd-', '')));
                            var variantName = variant[0].title;
                            var bundleQty = jQuery('#offer .offer.selected .quantity input').val();
                            var variant = jQuery.grep(product.variants, function (item) {
                                var vn = item.title;
                                return (vn === variantName);
                            });

                            // add item
                            var item = {
                                productId: variant[0].product_id,
                                variantName: variant[0].title,
                                variantId: variant[0].id,
                                quantity: (value * (bundleQty - 1))
                            };
                            unit.lineItems.push(item);
                        }
                    });
                }

                // add titanium products
                if (titaniumOverride.length) {
                    // merge similar products
                    var counts = {};
                    jQuery.each(titaniumOverride, function (key, value) {
                        if (!counts.hasOwnProperty(value)) {
                            counts[value] = 1;
                        } else {
                            counts[value]++;
                        }
                    });

                    jQuery.each(counts, function (key, value) {
                        if (key == '' || key == null) {
                            delete counts[key];
                        }
                    });

                    // report
                    jQuery.each(counts, function (key, value) {
                        // find item
                        //validate that the eval'ed key is valid before fetching its product info
                        try {
                            var prodVal = eval(key);

                            if (typeof prodVal != 'undefined')
                            {
                                var product = gu_products.getProduct(prodVal);
                                var variant = jQuery.grep(product.variants, function (item) {
                                    var variantName = item.title.toLowerCase();
                                    return (variantName === 'titanium');
                                });

                                // reporting
                                var item = {
                                    productId: variant[0].product_id,
                                    variantName: variant[0].title,
                                    variantId: variant[0].id,
                                    quantity: value
                                };
                                unit.lineItems.push(item);
                            }
                        }
                        catch (e) {
                            //do nothing. The product we attempted to report doesn't exist. Just move on.
                        }


                    });
                }
            }
            return unit;
        } else if (sizes.length) {
            var size = jQuery('#sizes .size.active ').attr('data-size');

            // add products
            jQuery.each(counts, function (key, value) {
                var product = gu_products.getProduct(eval(key));
                var variant = jQuery.grep(product.variants, function (item) {
                    var variantName = item.title.toLowerCase();
                    return (variantName === size);
                });

                // add item
                var item = {
                    productId: eval(key),
                    variantName: variant[0].title,
                    variantId: variant[0].id,
                    quantity: value
                };
                unit.lineItems.push(item);

                // add premium sizes
                if (jQuery('#sizes').length && jQuery('#sizes .sizes.active .price').length) {
                    var premiumColorClass;
                    jQuery('#sizes').each(function () {
                        premiumColorClass = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var premiumColorObj = gu_products.getProduct(eval(premiumColorClass));
                    var premiumVariant = jQuery.grep(premiumColorObj.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        if (~color.indexOf('-')) {
                            color = color.replace(/\-/g, ' ');
                        }
                        return (variantName === color);
                    });

                    if (Big(premiumVariant[0].price) > 1) { // big
                        // add item
                        var premiumItem = {
                            productId: premiumVariant[0].product_id,
                            variantName: premiumVariant[0].title,
                            variantId: premiumVariant[0].id,
                            quantity: value
                        };
                        unit.lineItems.push(premiumItem);
                    }
                }
            });

            // add discounted products
            var discQtyProd;
            jQuery('#offer .offer.selected').each(function () {
                discQtyProd = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('qd-') === 0;
                }).join();
            });
            if (bundleQty > 1 && discQtyProd != '') {
                jQuery.each(counts, function (key, value) {
                    var product = gu_products.getProduct(eval(key));
                    var variant = jQuery.grep(product.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        if (~color.indexOf('-')) {
                            color = color.replace(/\-/g, ' ');
                        }
                        return (variantName === color);
                    });

                    if (variant[0].price > 1) {
                        var product = gu_products.getProduct(eval(discQtyProd.replace('qd-', '')));
                        var variantName = variant[0].title;
                        var bundleQty = jQuery('#offer .offer.selected .quantity input').val();
                        var variant = jQuery.grep(product.variants, function (item) {
                            var vn = item.title;
                            return (vn === variantName);
                        });

                        // add item
                        var item = {
                            productId: variant[0].product_id,
                            variantName: variant[0].title,
                            variantId: variant[0].id,
                            quantity: (value * (bundleQty - 1))
                        };
                        unit.lineItems.push(item);
                    }
                });
            }
            return unit;

        } else if (storage.length) {
            var stor = jQuery('#storage .storage-wrap .storage.selected').index();

            // add products
            jQuery.each(counts, function(key,value){
                var product = gu_products.getProduct(eval(key));
                var variant = product.variants[stor];

                // add item
                var item = {
                    productId: eval(key),
                    variantName: variant.title,
                    variantId: variant.id,
                    quantity: value
                };
                unit.lineItems.push(item);

                // add premium sizes
                if (jQuery('#storage').length && jQuery('#storage .sizes.active .price').length) {
                    var premiumColorClass;
                    jQuery('#storage').each(function () {
                        premiumColorClass = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var premiumColorObj = gu_products.getProduct(eval(premiumColorClass));
                    var premiumVariant = jQuery.grep(premiumColorObj.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        if (~color.indexOf('-')) {
                            color = color.replace(/\-/g, ' ');
                        }
                        return (variantName === color);
                    });

                    if (Big(premiumVariant[0].price) > 1) { // big
                        // add item
                        var premiumItem = {
                            productId: premiumVariant[0].product_id,
                            variantName: premiumVariant[0].title,
                            variantId: premiumVariant[0].id,
                            quantity: value
                        };
                        unit.lineItems.push(premiumItem);
                    }
                }
            });

            // add discounted products
            var discQtyProd;
            jQuery('#offer .offer.selected').each(function () {
                discQtyProd = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('qd-') === 0;
                }).join();
            });
            if (bundleQty > 1 && discQtyProd != '') {
                jQuery.each(counts, function (key, value) {
                    var product = gu_products.getProduct(eval(key));
                    var variant = product.variants[stor];

                    if (variant.price > 1) {
                        var product = gu_products.getProduct(eval(discQtyProd.replace('qd-', '')));
                        var variantName = variant.title;
                        var bundleQty = jQuery('#offer .offer.selected .quantity input').val();
                        var variant = jQuery.grep(product.variants, function (item) {
                            var vn = item.title;
                            return (vn === variantName);
                        });

                        // add item
                        var item = {
                            productId: variant[0].product_id,
                            variantName: variant[0].title,
                            variantId: variant[0].id,
                            quantity: (value * (bundleQty - 1))
                        };
                        unit.lineItems.push(item);
                    }
                });
            }

            // gu_products.addSelectedUnit(unit);
            // updateSelectedUnits();
            // return unit;
        }

        //standard
        else {
            // check for color options
            var colorClasses;
            jQuery('#offer .offer.selected').each(function () {
                colorClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('c-') === 0;
                }).join();
            });
            var colorOpt = colorClasses.split(',');

            if (colorOpt[0] == '') {
                colorOpt = false;
            }

            // add products
            if (colorOpt) {
                jQuery.each(products, function (index, value) {
                    var product = gu_products.getProduct(eval(value));
                    var color = colorOpt[index].replace('c-', '');
                    var variant = jQuery.grep(product.variants, function (item) {
                        var variantName = item.title.toLowerCase();
                        return (variantName === color);
                    });

                    // report
                    var item = {
                        productId: variant[0].product_id,
                        variantName: product.title + ' - ' + variant[0].title,
                        variantId: variant[0].id,
                        quantity: 1
                    };
                    unit.lineItems.push(item);
                });

                // standard checkout - no color opt
            } else {
                jQuery.each(counts, function (key, value) {
                    // check if single bundle
                    var bundleSize = jQuery('#offer .offer').length;
                    if (bundleSize == 1) {
                        value = parseInt(jQuery('#offer .offer.selected .quantity input').val());
                    }

                    if (isNaN(value)) {
                        value = 1;
                    }

                    // report
                    var item = {
                        productId: (gu_products.getProduct(eval(key))).id,
                        variantName: (gu_products.getProduct(eval(key))).title,
                        variantId: (gu_products.getFirstVariant(eval(key))).id,
                        quantity: value
                    };
                    unit.lineItems.push(item);
                });
            }

            // add discounted products
            var discQtyProd;
            jQuery('#offer .offer.selected').each(function () {
                discQtyProd = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('qd-') === 0;
                }).join();
            });
            if (bundleQty > 1 && discQtyProd != '') {
                jQuery.each(counts, function (key, value) {
                    var product = gu_products.getProduct(eval(key));

                    if (product.variants[0].price > 1) {
                        var product = gu_products.getProduct(eval(discQtyProd.replace('qd-', '')));
                        var variantName = product.title;
                        var bundleQty = jQuery('#offer .offer.selected .quantity input').val();

                        // add item
                        var item = {
                            productId: product.id,
                            variantName: product.title,
                            variantId: product.variants[0].id,
                            quantity: (value * (bundleQty - 1))
                        };
                        unit.lineItems.push(item);
                    }
                });
            }

            return unit;
        }
    }

    function getExtras(target) {
        jQuery(function () {
            if (target.hasClass('selected')) {
                var colors = jQuery('#colors');

                var accessories = jQuery('#accessories');
                var sizes = jQuery('#sizes');
                var storage = jQuery('#storage');
                var sizeSelector = jQuery('#size-select');

                // reset presell popups
                if (jQuery('.presell-popup').length) {
                    jQuery('.presell-popup').each(function () {
                        jQuery(this).removeClass('active inactive');
                    });
                }

                // reset inline presells
                if (jQuery('.presell:not(.offer)').length) {
                    jQuery('.presell:not(.offer)').each(function () {
                        //jQuery(this).removeClass('active inactive');
                        presellReset();
                        jQuery(this).slideUp();
                    });
                }

                var presell = findPresell();

                // add prices to bundles
                jQuery('#offer .offer:visible').each(function () {
                    var productClasses;
                    jQuery(this).each(function () {
                        productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var products = productClasses.split(',');
                    var prices = [];
                    var total = 0;

                    // get price for each bundle class (uses first variant)
                    jQuery.each(products, function (index, value) {
                        var product = gu_products.getProduct(eval(value));
                        var price = product.variants[0].price;
                        prices.push(Number(Big(price)));
                    });

                    for (var i = 0; i < prices.length; i++) {
                        total = Big(total).add(prices[i]);
                    }

                    jQuery(this).attr('data-price', total);
                });

                // check for extras
                if (colors.length || accessories.length || presell || sizes.length || storage.length) {
                    if (colors.length) {
                        getColors();
                    } else if (accessories.length) {
                        getAccessories();
                    } else if (sizes.length) {
                        getSizes();
                    } else if (storage.length) {
                        getStorage();
                    } else if (sizeSelector.length > 0) {
                        console.log('test');
                        getDechoker();
                    } else if (presell) {
                        getPresell(presell);
                    }
                }

                // no extras; direct to checkout
                else {
                    prepCheckout();
                }
            }

            // retry
            else {
                setTimeout(function () {
                    getExtras(target);
                }, 500);
            }
        });
    }

    function getActiveColor() {
        var color = jQuery('#offer #colors .color.active').attr('data-color');
        if (color == 'variety') {
            color = jQuery('#offer #colors .color:first-child').attr('data-color');
        }
        if (color == 'variety') {
            color = jQuery('#offer #colors .color:nth-child(2)').attr('data-color');
        }
        return color;
    }

    function getColors() {
        jQuery(function () {
            getPricebar();
            var colors = jQuery('#offer #colors');
            var varietyOption = jQuery('#color-list .variety');

            //get inital img src
            var imgSrc = jQuery('#color-image img').attr('src');

            // wrap elements
            if (!jQuery('.item', colors).length) {
                jQuery('.image, .list', colors).wrapAll('<div class="item"></div>');
            }

            // create the form
            if (!jQuery('.list form', colors).length) {
                // add the form
                jQuery('.list', colors).append('<form></form>');
                var id = 0;
                jQuery('.list ul li', colors).each(function (index) {
                    // get html
                    var colorText = jQuery(this).text();
                    var color = jQuery(this).data('color');
                    var bgColor = jQuery(this).find('span')[0].style.color;
                    id = id + 1;

                    // add color
                    jQuery('.list form', colors).append('<span data-color="' + color + '" id="color-' + id + '" class="color ' + color + '" onclick="setColor(this.id);"><p class="title">' + colorText + '</p><span class="cover" style="background-color: ' + bgColor + '"></span><input type="radio" value="' + color + '" /></span>');

                    // set default to color of image
                    if (imgSrc.indexOf(color) >= 0) {
                        jQuery('#color-' + id).addClass('active');
                    }

                });

                // clean up
                jQuery('.list .wpb_wrapper', colors).remove();

                varietyOption = jQuery('#color-list .variety');

                // initial variety check
                // doesn't have variety option
                if (!jQuery('#offer .offer.selected:visible').hasClass('variety')) {

                    jQuery(varietyOption, colors).hide();
                    //jQuery('#color-list form').append(varietyOption);

                    if (jQuery(varietyOption).hasClass('active')) {
                        jQuery(varietyOption).removeClass('active');
                    }
                } else {
                    // set color to first available option
                    var firstAvailId = jQuery('.color:first-child').attr('id');
                    //setColor(firstAvailId);
                }

            } else {
                // variety check
                if (jQuery('#offer .offer.selected:visible').hasClass('variety') && jQuery(varietyOption, colors).length) {
                    //jQuery('#color-list form').prepend(varietyOption);
                    jQuery(varietyOption, colors).show();

                } else {
                    jQuery(varietyOption, colors).hide();

                    // check if previous bundle had variety selected and revert to defaut if so
                    //jQuery('#color-list form').append(varietyOption);

                    if (varietyOption.hasClass('active')) {
                        // set color to first available option
                        var firstAvailId = jQuery('.color:first-child').attr('id');

                        if (firstAvailId == varietyOption.attr('id')) {
                            //first option is variety and that's not supported, choose next option
                            firstAvailId = jQuery('.color:nth-child(2)').attr('id');
                        }
                    }
                }
            }

            setColor(firstAvailId);

            // add premium color prices if any
            var premiumColors;
            colors.each(function () {
                premiumColors = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('guproduct_') === 0;
                }).join();
            });
            if (premiumColors.length) {
                premiumColors = gu_products.getProduct(eval(premiumColors));

                // check each variant for price
                for (var i = 0; i < premiumColors.variants.length; i++) {
                    if (premiumColors.variants[i].price > 1) {
                        var colorEl = '#offer #colors .color';
                        var price = premiumColors.variants[i].price;

                        // find matching color
                        for (var c = 0; c < jQuery(colorEl).length; c++) {
                            var color = jQuery(colorEl).eq(c);
                            var colorName = color.attr('data-color');

                            // check if variety has any premium colors
                            if (colorName == 'variety') {
                                var varietyColorClasses;
                                jQuery('#offer .offer.selected').each(function () {
                                    varietyColorClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                                        return v.indexOf('vp-') === 0;
                                    }).join();
                                });
                                if (varietyColorClasses.length) {
                                    var varietyColors = varietyColorClasses.split(',');

                                    // check each variety color
                                    for (var v = 0; v < varietyColors.length; v++) {
                                        var varietyColor = varietyColors[v].replace('vp-', '');
                                        if (varietyColor == premiumColors.variants[i].title.toLowerCase()) {
                                            // set price if none exist
                                            if (!jQuery('#offer #colors .variety .price').length) {
                                                jQuery('#offer #colors .variety .title').before('<p class="price" data-price="' + price + '">' + guDisplayCurrency(price) + '</p>');
                                            }
                                        }
                                    }
                                }
                            }

                            // set price for all other colors
                            else if (colorName == premiumColors.variants[i].title.toLowerCase()) {
                                if (!color.find('.price').length) {
                                    color.find('.title').before('<p class="price" data-price="' + price + '">' + guDisplayCurrency(price) + '</p>');
                                }
                            }
                        }
                    }
                }
            }

            //set image URL


            // set color sizes
            setColorSizes();

            jQuery('#colors').slideDown();

            setTimeout(function () {
                var height = jQuery('#colors').height();
                var wHeight = jQuery(window).height();
                var ptHeight = jQuery('#offer #price-tab').height();
                var tHeight = height + ptHeight;

                if (mobileDevice && tHeight < wHeight) {
                    var space = wHeight - tHeight;
                    jQuery('#colors').append('<div class="colors-space" style="display: none; height: ' + space + 'px;"></div>');
                    jQuery('#colors .colors-space').slideDown();
                }

                // scroll to colors
                if (!jQuery('#offer').hasClass('prevActivated')) {
                    jQuery('html, body').animate({scrollTop: jQuery('#colors').offset().top}, 1000);
                }
            }, 500);

            // fire event
            gu_fire_event("View Color Selection");
        });
    }

    function setColor(id) {
        var colors = jQuery('#offer #colors');
        var currentColor = jQuery('#' + id, colors);
        var currentColorName = jQuery('#' + id, colors).data('color');
        var prevColorName = currentColor.closest('.item').find('.color.active').data('color');

        var img = currentColor.closest('.item').find('.image img');
        var imgSrc = img.attr('src');

        // set image qty
        var productClasses;
        jQuery('#offer .offer.selected:visible').each(function () {
            productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                return v.indexOf('guproduct_') === 0;
            }).join();
        });
        var products = productClasses.split(',');
        var item = jQuery('#offer #colors .item');
        var img = jQuery('#offer #colors .image img');

        if (!jQuery('#colors').is(':visible')) {
            // first click
            var src = img.attr('src').replace('1-', products.length + '-');
            // img.attr('src',src);
            // img.attr('srcset',src);
            imgSrc = src;

        } else {
            // all other clicks
            var prevQty = item.attr('data-qty');
            var src = img.attr('src').replace(prevQty + '-', products.length + '-');

            imgSrc = src;

            // fire event
            gu_fire_event("Change Color Selection", "Click", null, "Selected Color", currentColorName);
        }

        item.attr('data-qty', products.length);

        // set image
        img.attr('src', imgSrc.replace(prevColorName, currentColorName));
        img.attr('srcset', imgSrc.replace(prevColorName, currentColorName));
        colors.attr('data-prev', currentColorName);

        // set active
        currentColor.closest('.item').find('.color.active').removeClass('active');
        currentColor.addClass('active');

        // hide checkout
        var checkout_el = jQuery('#checkout');
        if (checkout_el.length && checkout_el.is(':visible')) {
            checkout_el.slideUp('1500');
        }

        // reset presell popups
        if (jQuery('.presell-popup').length) {
            jQuery('.presell-popup').each(function () {
                jQuery(this).removeClass('active inactive');
            });
        }

        //set colors on accessory imgs
        if (jQuery('#accessories').length) {
            setAccessoryColor();
        }

        getPricebar();
    }

    function setColorSizes() {
        jQuery(function () {
            var colors = jQuery('#offer #colors');
            if (jQuery('.list form', colors).length) {
                var formWidth = jQuery('.list form', colors).width();
                var numOfColors = jQuery('.item', colors).eq(0).find('form .color').length;
                var colorMarginRight = parseInt(jQuery('.item form .color', colors).css('margin-right').replace('px', ''));
                var colorWidth = ((100 / numOfColors) - colorMarginRight) + '%';

                // set sizes
                jQuery('.item form .color', colors).each(function () {
                    // set width
                    jQuery(this).width(colorWidth);

                    // set height
                    var colorHeight = jQuery(this).width();
                    jQuery('.cover, input', this).height(colorHeight);
                });
            }
        });
    }

    function getAccessories() {
        jQuery(function () {
            getPricebar();
            var accessories = jQuery('#accessories');

            // set up
            if (!jQuery('.gallery', accessories).hasClass('item')) {
                // add item class
                jQuery('.gallery', accessories).addClass('item');

                // build the accessories
                var productClasses;
                accessories.each(function () {
                    productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('guproduct_') === 0;
                    }).join();
                });
                var products = productClasses.split(',');

                // add the product image
                jQuery('#offer #accessories .gallery ul').before('<div class="image"><h2 class="title">title</h2><img src="" /></div>');

                // set up each accessory
                jQuery('#offer #accessories .gallery ul li').each(function (index) {
                    var title = gu_products.getProduct(eval(products[index])).title;
                    var price = gu_products.getFirstVariant(eval(products[index])).price;
                    var displayPrice = guDisplayCurrency(price);

                    // If titanium, tag for later
                    if (~title.toLowerCase().indexOf('titanium')) {
                        jQuery(this).addClass('titanium');
                    }

                    // add the content
                    jQuery(this).prepend('<p class="title">' + guSubstituteString(title) + '</p>');
                    jQuery(this).append('<p class="price" data-price="' + price + '"><strong>' + displayPrice + '</strong></p>');

                    //make sure image is wanted
                    if (accessories.hasClass('noImg')) {
                        //instead add in text instead of image
                        jQuery('#accessoriesText #accessoryPrice').text(gu_products.getPriceInfoForProduct(eval(products[index])).rp.toString());
                        //hide individual price

                        jQuery('.price', this).addClass('hidden');

                    }

                    jQuery(this).attr({
                        'id': 'accessory-row-c' + index,
                        'onclick': 'accessoryClick(this.id);'
                    });

                    // wrap the image
                    jQuery('img', this).wrapAll('<div class="accessory-image"></div>');
                });
            }

            dupeAccessories();

            // slide down
            if (!accessories.is(':visible')) {
                accessories.slideDown('fast');
                setTimeout(function () {
                    jQuery('html, body').animate({scrollTop: jQuery('#accessories').offset().top}, 1000);
                }, 500);
            }
        });
    }

    function dupeAccessories() {
        jQuery(function () {
            var accessories = jQuery('#accessories');

            // clean up
            jQuery('.item', accessories).slice(1).remove();
            jQuery('.item ul li', accessories).each(function () {
                var id = jQuery(this).attr('id');
                id = id.replace('r1', 'row');
                jQuery(this).attr('id', id);
                jQuery(this).removeClass('active');
            });

            // dupe
            var accessories_HTML = jQuery('.item', accessories).outerHTML();
            var products;
            jQuery('#offer .offer.selected').each(function () {
                products = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('guproduct_') === 0;
                }).join();
            });
            products = products.split(',');
            //filter again for only products that support accessory selection
            var validProducts = [];
            jQuery.each(products, function (index, value) {
                if (jQuery.inArray("no_accessory", gu_products.getProduct(eval(value)).tags) == -1)
                {
                    validProducts.push(this);
                }

            });

            var numOfProducts = validProducts.length;

            if (numOfProducts > 1) {
                for (var i = 1; i < numOfProducts; i++) {
                    jQuery('.item', accessories).last().after(accessories_HTML);
                }
            }

            setAccessoryColor();

            // set ids
            jQuery('.item', accessories).each(function (index) {

                //set title and img
                if (jQuery('#colors').length) {
                    jQuery('.image .title', this).text(jQuery('#offer #colors .item .image h2').text() + ' #' + (index + 1));
                } else {
                    jQuery('.image .title', this).text(jQuery('#accessories .prodTitle').text() + ' #' + (index + 1));
                }


                jQuery('ul li', this).each(function () {
                    var id = jQuery(this).attr('id');
                    id = id.replace('row', 'r' + (index + 1));
                    jQuery(this).attr('id', id);
                });
            });
        });
    }

    function setAccessoryColor(itemIndex) {
        jQuery(function () {
            var accessories = jQuery('#accessories');
            jQuery('.item', accessories).each(function (index) {

                if (jQuery('.titanium', this).hasClass('active')) {
                    //console.log('found an active one');
                    var src = '/wp-content/uploads/1-titanium.png';
                    jQuery('.image img', this).attr('src', src);
                } else {
                    if (jQuery('#colors').length) {
                        //console.log('no active titanium');
                        var color = jQuery('#colors .color.active').attr('data-color');
                        var src = '/wp-content/uploads/1-' + color + '.png';
                        jQuery('.image img', this).attr('src', src);
                    } else if (accessories.hasClass('noImg')) {
                        if (!jQuery('.image .accessoriesText', this).length) {
                            //grab accessoryText
                            var accessoryText = jQuery('#accessoriesText');
                            //instead add in text instead of image
                            var dupeAccessText = accessoryText.clone().insertBefore(jQuery('.image img', this));
                            dupeAccessText.removeAttr("id");
                            dupeAccessText.removeClass("hidden");
                            dupeAccessText.show();
                        }

                    } else {
                        var src = jQuery('#offer .offer:first-child img').attr('src');
                        jQuery('.image img', this).attr('src', src);

                        if (mobileDevice) {
                            jQuery('.image img', this).css('max-width', '150px')
                        } else {
                            jQuery('.image img', this).css('max-width', '100%')
                        }
                    }
                }

            });
        });
    }

    function accessoryClick(id) {
        jQuery(function () {
            var accessory = jQuery('#' + id);
            accessory.toggleClass('active');

            // hide checkout
            var checkout_el = jQuery('#checkout');
            if (checkout_el.length && checkout_el.is(':visible')) {
                checkout_el.slideUp('1500');
            }

            // reset presell popups
            if (jQuery('.presell-popup').length) {
                jQuery('.presell-popup').each(function () {
                    jQuery(this).removeClass('active inactive');
                });
            }

            getPricebar();

            // add price to pricebar
            var pricebar = jQuery('#offer #price-tab');
            var presell = findPresell();
            var bundleTotal = jQuery('#offer .offer.selected:visible').attr('data-price');

            // add up the prices
            var pricebarTotal = bundleTotal;
            jQuery('.gallery ul li.active', accessories).each(function () {
                var price = jQuery('.price', this).attr('data-price');
                pricebarTotal = Big(pricebarTotal).add(Big(price));
            });

            // check for presell
            if (presell) {
                var presellTotal = presell.data('price');
                var presellQty = jQuery('.quantity input', presell).val();

                // add presell to pricebar total
                pricebarTotal = Big(pricebarTotal).add(Big(presellTotal).mul(presellQty));
            }

            // set the total
            var pricebarTotalDisplay = guDisplayCurrency(pricebarTotal);
            jQuery('.price', pricebar).text(pricebarTotalDisplay);
            jQuery('.price', pricebar).attr('data-price', pricebarTotal);

            //check for image replacement
            if (accessory.hasClass('titanium')) {
                setAccessoryColor(id.slice(-1));
            }
        });
    }

    function checkAccessories(total) {
        var accessories = jQuery('#accessories');
        if (accessories.length && accessories.is(':visible')) {
            jQuery('.gallery ul li.active', accessories).each(function () {
                var price = jQuery('.price', this).attr('data-price');
                total = Big(total).add(Big(price));
            });
        }
        return total
    }

    //SIZES
    function getActiveSize() {
        var size = jQuery('#offer #sizes .size.active').attr('data-size');
        return size;
    }

    function getSizes() {
        jQuery(function () {
            getPricebar();
            var sizes = jQuery('#offer #sizes');
            var imgSrc = jQuery('#size-image img').attr('src');

            // wrap elements
            if (!jQuery('.item', sizes).length) {
                jQuery('.image, .list', sizes).wrapAll('<div class="item"></div>');
            }

            // create the form
            if (!jQuery('.list form', sizes).length) {
                // add the form
                jQuery('.list', sizes).append('<form></form>');
                var id = 0;
                jQuery('.list ul li', sizes).each(function (index) {
                    // get html
                    var sizeText = jQuery('span:nth-child(1)', this).text();
                    var sizeSubtext = jQuery('span:nth-child(2)', this).text();
                    var size = jQuery(this).data('size');
                    var price = jQuery(this).data('price');
                    var bgColor = '#dadada;';
                    id = id + 1;
                    var sizeOverlay = sizeText.charAt(0).toUpperCase();

                    // add color
                    if (jQuery(this).hasClass('default')) {
                        jQuery('.list form', sizes).append('<span data-size="' + size + '" id="size-' + id + '" class="size default ' + size + '" onclick="selectSize(this.id);"><p class="title">' + sizeText + '</p><p class="title">' + sizeSubtext + '</p><span class="cover" style="background-color: ' + bgColor + '"><p class="sizeOverlay" id="sizeOverlay-' + id + '">' + sizeOverlay + '</p></span><input type="radio" value="' + size + '" /></span>');
                    } else {
                        if (price !== undefined) {
                            jQuery('.list form', sizes).append('<span data-price="' + price + '" data-size="' + size + '" id="size-' + id + '" class="size ' + size + '" onclick="selectSize(this.id);"><p class="title">' + sizeText + '</p><p class="title">' + sizeSubtext + '</p><span class="cover" style="background-color: ' + bgColor + '"><p class="sizeOverlay" id="sizeOverlay-' + id + '">' + sizeOverlay + '</p></span><input type="radio" value="' + size + '" /></span>');
                        } else {
                            jQuery('.list form', sizes).append('<span data-size="' + size + '" id="size-' + id + '" class="size ' + size + '" onclick="selectSize(this.id);"><p class="title">' + sizeText + '</p><p class="title">' + sizeSubtext + '</p><span class="cover" style="background-color: ' + bgColor + '"><p class="sizeOverlay" id="sizeOverlay-' + id + '">' + sizeOverlay + '</p></span><input type="radio" value="' + size + '" /></span>');
                        }
                    }

                    // set default to color of image
                    if (imgSrc.indexOf(size) >= 0) {
                        jQuery('#size-' + id).addClass('active');
                    }

                });

                // clean up
                jQuery('.list .wpb_wrapper', sizes).remove();

                // set initial size
                if (jQuery('.size.default').length) {
                    var defaultId = jQuery('.size.default').attr('id');
                } else {
                    var defaultId = jQuery('.size:first-child').attr('id');
                }
                selectSize(defaultId);
            }

            //select the currently selected size to populate it
            selectSize(jQuery('.size.active').attr('id'));

            // add premium size prices if any
            var premiumSizes;
            sizes.each(function () {
                premiumSizes = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('guproduct_') === 0;
                }).join();
            });
            if (premiumSizes.length) {
                premiumSizes = gu_products.getProduct(eval(premiumSizes));

                // check each variant for price
                for (var i = 0; i < premiumSizes.variants.length; i++) {
                    if (premiumSizes.variants[i].price > 1) {
                        var sizesEl = '#offer #sizes .sizes';
                        var price = premiumSizes.variants[i].price;

                        // find matching color
                        for (var c = 0; c < jQuery(sizesEl).length; c++) {
                            var size = jQuery(sizesEl).eq(c);
                            var sizeName = size.attr('data-size');

                            // set price for all other colors
                            if (sizeName == premiumSizes.variants[i].title.toLowerCase()) {
                                if (!size.find('.price').length) {
                                    size.find('.title').before('<p class="price" data-price="' + price + '">' + guDisplayCurrency(price) + '</p>');
                                }
                            }
                        }
                    }
                }
            }

            //set image URL


            // set color sizes
            setSizeSizes();

            jQuery('#sizes').slideDown();

            setTimeout(function () {
                var height = jQuery('#sizes').height();
                var wHeight = jQuery(window).height();
                var ptHeight = jQuery('#offer #price-tab').height();
                var tHeight = height + ptHeight;

                if (mobileDevice && tHeight < wHeight) {
                    var space = wHeight - tHeight;
                    jQuery('#sizes').append('<div class="sizes-space" style="display: none; height: ' + space + 'px;"></div>');
                    jQuery('#sizes .sizes-space').slideDown();
                }

                // scroll to sizes
                if (!jQuery('#offer').hasClass('prevActivated')) {
                    jQuery('html, body').animate({scrollTop: jQuery('#sizes').offset().top}, 1000);
                }
            }, 500);
        });
    }

    function selectSize(id) {
        var sizes = jQuery('#offer #sizes');
        var currentSize = jQuery('#' + id, sizes);
        var currentSizeName = jQuery('#' + id, sizes).data('size');
        var prevSizeName = currentSize.closest('.item').find('.size.active').data('size');

        var img = jQuery('#offer #sizes #size-image img');

        // get bundle image
        var selectedImageSrc = jQuery('#offer .offer.selected:visible img').first().attr('src');

        // set image
        img.attr('src', selectedImageSrc);
        img.attr('srcset', selectedImageSrc);
        sizes.attr('data-prev', currentSizeName);

        //get current src and try to replace the sizes
        var imgSrc = img.attr('src');

        img.attr('src',imgSrc.replace(prevSizeName,currentSizeName));
        img.attr('srcset',imgSrc.replace(prevSizeName,currentSizeName));

        // wip
        getPricebar(id);

        // var productClasses; jQuery('#offer .offer.selected:visible').each(function() { productClasses = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('guproduct_') === 0; }).join(); });
        // var products = productClasses.split(',');
        // var item = jQuery('#offer #sizes .item');
        // var img = jQuery('#offer #sizes .image img');

        // if (!jQuery('#sizes').is(':visible'))
        // {
        //     // first click
        //     var src = img.attr('src').replace('1-',products.length + '-');
        //     // img.attr('src',src);
        //     // img.attr('srcset',src);
        //     imgSrc = src;

        // }

        // else {
        //     // all other clicks
        //     var prevQty = item.attr('data-qty');
        //     var src = img.attr('src').replace(prevQty + '-',products.length + '-');

        //     imgSrc = src;
        // }

        // item.attr('data-qty',products.length);

        // // set image

        //sizes.attr('data-prev',currentSizeName);

        // set active
        currentSize.closest('.item').find('.size.active .sizeOverlay').removeClass('active');
        currentSize.closest('.item').find('.size.active').removeClass('active');
        currentSize.addClass('active');
        currentSize.find('.sizeOverlay').addClass('active');

        // hide checkout
        var checkout_el = jQuery('#checkout');
        if (checkout_el.length && checkout_el.is(':visible')) {
            checkout_el.slideUp('1500');
        }

        // reset presell popups
        if (jQuery('.presell-popup').length) {
            jQuery('.presell-popup').each(function () {
                jQuery(this).removeClass('active inactive');
            });
        }

        getPricebar();
    }

    function setSizeSizes() {
        jQuery(function () {
            var sizes = jQuery('#offer #sizes');
            if (jQuery('.list form', sizes).length) {
                var formWidth = jQuery('.list form', sizes).width();
                var numOfSizes = jQuery('.item', sizes).eq(0).find('form .size').length;
                var sizeMarginRight = parseInt(jQuery('.item form .size', sizes).css('margin-right').replace('px', ''));
                var sizeWidth = ((100 / numOfSizes) - sizeMarginRight) + '%';

                // set sizes
                jQuery('.item form .size', sizes).each(function () {
                    // set width
                    jQuery(this).width(sizeWidth);

                    // set height
                    var sizeHeight = jQuery(this).width();
                    jQuery('.cover, input', this).height(sizeHeight);
                });
            }
        });
    }

    //STORAGE
    function getActiveStorage() {
        var size = jQuery('#offer #storage .size.active').attr('data-size');
        return size;
    }

    function getStorage() {
        jQuery(function () {
            if (!jQuery('#offer').hasClass('special')) {
                getPricebar();
            }
            var sizes = jQuery('#offer #storage');
            //var varietyOption = jQuery('#color-list .variety');

            //get inital img src
            var imgSrc = jQuery('#size-image img').attr('src');

            // wrap elements
            if (!jQuery('.item', sizes).length) {
                jQuery('.image, .list', sizes).wrapAll('<div class="item"></div>');
            }

            // create the form
            if (!jQuery('.list form', sizes).length) {
                // add the form
                jQuery('.list', sizes).append('<form></form>');
                var id = 0;
                jQuery('.list ul li', sizes).each(function (index) {
                    // get html
                    var sizeText = jQuery('span:nth-child(1)', this).text();
                    var sizeSubtext = jQuery('span:nth-child(2)', this).text();
                    var size = jQuery(this).data('size');
                    var bgColor = '#dadada;';
                    var sizeOverlay = sizeText.charAt(0).toUpperCase();
                    id = id + 1;

                    // add color
                    jQuery('.list form', sizes).append('<span data-size="' + size + '" id="size-' + id + '" class="size ' + size + '" onclick="selectSize(this.id);"><p class="title">' + sizeText + '</p><p class="title">' + sizeSubtext + '</p><span class="cover" style="background-color: ' + bgColor + '"><p class="sizeOverlay" id="sizeOverlay-' + id + '">' + sizeOverlay + '</p></span><input type="radio" value="' + size + '" /></span>');

                    // set default to color of image
                    if (imgSrc.indexOf(size) >= 0) {
                        jQuery('#size-' + id).addClass('active');
                    }

                });

                // clean up
                jQuery('.list .wpb_wrapper', sizes).remove();

                //varietyOption = jQuery('#color-list .variety');

                // // initial variety check
                // // doesn't have variety option
                // if (!jQuery('#offer .offer.selected:visible').hasClass('variety')) {

                //     jQuery(varietyOption,colors).hide();
                //     //jQuery('#color-list form').append(varietyOption);

                //     if (jQuery(varietyOption).hasClass('active')) {
                //         jQuery(varietyOption).removeClass('active');
                //     }
                // }
                // else {
                // set color to first available option
                var firstAvailId = jQuery('.size:first-child').attr('id');
                selectStorage(firstAvailId);
                //}

            }
            // else
            // {
            //     // variety check
            //     if (jQuery('#offer .offer.selected:visible').hasClass('variety') && jQuery(varietyOption,colors).length) {
            //         //jQuery('#color-list form').prepend(varietyOption);
            //         jQuery(varietyOption,colors).show();

            //     } else {
            //         jQuery(varietyOption,colors).hide();

            //         // check if previous bundle had variety selected and revert to defaut if so
            //         //jQuery('#color-list form').append(varietyOption);

            //         if (varietyOption.hasClass('active'))
            //         {
            //             // set color to first available option
            //             var firstAvailId = jQuery('.color:first-child').attr('id');

            //             if(firstAvailId == varietyOption.attr('id'))
            //             {
            //                 //first option is variety and that's not supported, choose next option
            //                 firstAvailId = jQuery('.color:nth-child(2)').attr('id');
            //             }
            //         }
            //     }
            // }

            selectStorage();

            // add premium size prices if any
            var premiumSizes;
            sizes.each(function () {
                premiumSizes = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('guproduct_') === 0;
                }).join();
            });
            if (premiumSizes.length) {
                premiumSizes = gu_products.getProduct(eval(premiumSizes));

                // check each variant for price
                for (var i = 0; i < premiumSizes.variants.length; i++) {
                    if (premiumSizes.variants[i].price > 1) {
                        var sizesEl = '#offer #storage .sizes';
                        var price = premiumSizes.variants[i].price;

                        // find matching color
                        for (var c = 0; c < jQuery(sizesEl).length; c++) {
                            var size = jQuery(sizesEl).eq(c);
                            var sizeName = size.attr('data-size');

                            // set price for all other colors
                            if (sizeName == premiumSizes.variants[i].title.toLowerCase()) {
                                if (!size.find('.price').length) {
                                    size.find('.title').before('<p class="price" data-price="' + price + '">' + guDisplayCurrency(price) + '</p>');
                                }
                            }
                        }
                    }
                }
            }

            //set image URL


            // set color sizes
            setStorageStor();

            setTimeout(function () {
                var height = jQuery('#storage').height();
                var wHeight = jQuery(window).height();
                var ptHeight = jQuery('#offer #price-tab').height();
                var tHeight = height + ptHeight;

                if (mobileDevice && tHeight < wHeight) {
                    var space = wHeight - tHeight;
                    jQuery('#storage').append('<div class="sizes-space" style="display: none; height: ' + space + 'px;"></div>');
                    jQuery('#storage .sizes-space').slideDown();
                }

                // scroll to sizes
                if (!jQuery('#offer').hasClass('prevActivated')) {
                    if (!jQuery('#offer').hasClass(('special'))) {
                        jQuery('#storage').slideDown();
                        jQuery('html, body').animate({scrollTop: jQuery('#storage').offset().top}, 1000);
                    }
                }
            }, 500);
        });
    }

    if (mobileDevice && jQuery('#offer').hasClass('special')) {
        guRemoteDataRegisterCallback(getStorage);
    }

    function selectStorage(id) {
        var sizes = jQuery('#offer #storage');
        var currentSize = jQuery('#' + id, sizes);
        var currentSizeName = jQuery('#' + id, sizes).data('size');
        var prevSizeName = currentSize.closest('.item').find('.size.active').data('size');

        var img = jQuery('#offer #storage #size-image img');
        var imgSrc = img.attr('src');

        // get bundle image
        var selectedImageSrc = jQuery('#offer .offer.selected:visible img').first().attr('src');

        // set image
        img.attr('src', selectedImageSrc);
        img.attr('srcset', selectedImageSrc);
        sizes.attr('data-prev', currentSizeName);


        // var productClasses; jQuery('#offer .offer.selected:visible').each(function() { productClasses = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('guproduct_') === 0; }).join(); });
        // var products = productClasses.split(',');
        // var item = jQuery('#offer #storage .item');
        // var img = jQuery('#offer #storage .image img');

        // if (!jQuery('#storage').is(':visible'))
        // {
        //     // first click
        //     var src = img.attr('src').replace('1-',products.length + '-');
        //     // img.attr('src',src);
        //     // img.attr('srcset',src);
        //     imgSrc = src;

        // }

        // else {
        //     // all other clicks
        //     var prevQty = item.attr('data-qty');
        //     var src = img.attr('src').replace(prevQty + '-',products.length + '-');

        //     imgSrc = src;
        // }

        // item.attr('data-qty',products.length);

        // // set image
        // img.attr('src',imgSrc.replace(prevSizeName,currentSizeName));
        // img.attr('srcset',imgSrc.replace(prevSizeName,currentSizeName));
        // sizes.attr('data-prev',currentSizeName);

        // set active
        currentSize.closest('.item').find('.size.active .sizeOverlay').removeClass('active');
        currentSize.closest('.item').find('.size.active').removeClass('active');

        currentSize.addClass('active');
        currentSize.find('.sizeOverlay').addClass('active');

        // hide checkout
        var checkout_el = jQuery('#checkout');
        if (checkout_el.length && checkout_el.is(':visible')) {
            checkout_el.slideUp('1500');
        }

        // reset presell popups
        if (jQuery('.presell-popup').length) {
            jQuery('.presell-popup').each(function () {
                jQuery(this).removeClass('active inactive');
            });
        }

        getPricebar();
    }

    function setStorageStor() {
        jQuery(function () {
            var sizes = jQuery('#offer #storage');
            if (jQuery('.list form', sizes).length) {
                var formWidth = jQuery('.list form', sizes).width();
                var numOfSizes = jQuery('.item', sizes).eq(0).find('form .size').length;
                var sizeMarginRight = parseInt(jQuery('.item form .size', sizes).css('margin-right').replace('px', ''));
                var sizeWidth = ((100 / numOfSizes) - sizeMarginRight) + '%';

                // set sizes
                jQuery('.item form .size', sizes).each(function () {
                    // set width
                    jQuery(this).width(sizeWidth);

                    // set height
                    var sizeHeight = jQuery(this).width();
                    jQuery('.cover, input', this).height(sizeHeight);
                });
            }
        });
    }

    function findPresell (display)
    {
        //if we are to display the new presell, hide all others first
        if (display)
        {
            // reset inline presells
            if (jQuery('.presell:not(.offer)').length) {
                jQuery('.presell:not(.offer)').each(function () {
                    //jQuery(this).removeClass('active inactive');
                    presellReset();
                    jQuery(this).slideUp();
                });
            }
        }
        //check selected bundle for specific presell to show
        var presellClasses; jQuery('#offer .offer.selected:visible').each(function() { presellClasses = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('presell-') === 0 && v.indexOf('presell-popup') === -1; }).join(); });

        var validPresell = true;
        if (presellClasses != '' || typeof presellClasses != "undefined")
        {
            validPresell = false;
        }

        if (validPresell && typeof dtc == 'undefined') {
            var presellClass = presellClasses.split(',');
            var presell = jQuery('#'+presellClass);
        }
        //if no specific presell in the bundle, check for a default on the page
        else
        {
            var presell = jQuery('#presell:not(.inactive)');
            //make sure jQuery doesn't just return an empty object
            if (presell.length == 0)
            {
                presell = null;
            }
        }

        //return one if we found one
        if (presell )
        {
            if (display)
            {
                presell.slideDown();
            }
            return presell;
        }
        else {
            return null;
        }
    }

    function getPresell(presell) {
        jQuery(function () {
            getPricebar();

            //fix default headline
            jQuery('#presell').prepend(jQuery('#presell-header'));

            //fix current headline if different
            presell.prepend(presell.find('.presell-header'));

            // open the presell if it's not already
            presell.slideDown();

            setTimeout(function () {
                jQuery('html, body').animate({scrollTop: presell.offset().top}, 1000);
            }, 400);


            // fire event
            var presellID = presell.attr('id');
            gu_fire_event("View Inline Upsell", null, null, "Inline Upsell ID", presellID);

        });
    }

    // get presell data
    var presell = findPresell();
    var btn = jQuery('.cta:not(.active) button', presell);
    var btnTxt = btn.text();


    function presellClick() {
        jQuery(function () {

            var presell = findPresell();
            var qty = jQuery('.quantity', presell);
            var btn = jQuery('.cta:not(.active) button', presell);
            var btnTxt = btn.text();
            presell.addClass('active');


            //'Added' btn vars
            var actBtn = jQuery('.cta.active button', presell);
            var actBtnTxt = actBtn.text();
            var actBtnBg = actBtn.css('background-color');
            var actBtnColor = actBtn.css('color');

            // activate btn
            presell.addClass('active');
            btn.text(actBtnTxt);
            actBtn.text(btnTxt);
            btn.css({
                'background-color': '' + actBtnBg + '',
                'color': '' + actBtnColor + ''
            });

            // add qty html
            if (qty.length) {
                // add html
                qty.empty();
                qty.append('<h5>' + guSubstituteString("Quantity:") + '</h5><a class="plus btn" onclick="presellAdd();"></a><input name="quantity" type="text" value="1"><a class="minus btn" onclick="presellSub();"></a>');

                // hide button & show the quantity
                setTimeout(function () {


                    if (qty.hasClass('single')) {
                        setTimeout(function () {
                            btn.text(guSubstituteString("Remove?"));
                            btn.parent().removeClass('cta');
                            btn.parent().addClass("subtle");
                            btn.attr('onclick', 'presellDecline();');
                            jQuery('#offer #price-tab button').click();
                        }, 400);
                    }
                    else if (qty.hasClass('match')) {
                        //start the qty as the same qty as the bundle
                        var products; jQuery('#offer .offer.selected').each(function () { products = jQuery.grep(this.className.split(" "), function (v, i) { return v.indexOf('guproduct_') === 0; }).join(); });
                        var productList = [];
                        var productList = products.split(',');
                        var presellQty = productList.length;
                        //qty.find('input').val(presellQty);

                        //run preselladd() for additional prods
                        if (presellQty > 1)
                        {
                            for (var i = 0; i < presellQty - 1; i++) {
                                presellAdd();
                            }
                        }

                        jQuery('.cta:not(.active)', presell).fadeOut();
                        setTimeout(function () {
                            qty.fadeIn();
                        }, 400);
                    }
                    else {
                        jQuery('.cta:not(.active)', presell).fadeOut();
                        setTimeout(function () {
                            qty.fadeIn();
                        }, 400);
                    }
                    //if there's a default decline button, hide it
                    jQuery('#presell #presell-decline').slideUp();
                    //hide again for current if different
                    presell.find('.presell-decline').slideUp();

                }, 400);
            }

            // get price for each presell class (uses first variant)
            var presellClasses;
            presell.each(function () {
                presellClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                    return v.indexOf('guproduct_') === 0;
                }).join();
            });
            var presellProducts = presellClasses.split(',');
            var prices = [];
            var presellTotal = 0;

            // set pesell price
            jQuery.each(presellProducts, function (index, value) {
                var product = gu_products.getProduct(eval(value));
                var price = product.variants[0].price;
                prices.push(Number(Big(price)));
            });

            for (var i = 0; i < prices.length; i++) {
                presellTotal = Number(Big(presellTotal).add(prices[i]));
            }

            presell.attr('data-price', presellTotal);

            // get bundle total
            var bundleQty = 1;
            if (jQuery('#offer .offer.selected .quantity').length) {
                bundleQty = jQuery('#offer .offer.selected .quantity input').val();
            }
            var bundleTotal = Big(jQuery('#offer .offer.selected:visible').data('price')).mul(bundleQty);

            // set pricebar total // wip
            var pricebar = jQuery('#offer #price-tab');
            var pricebarTotal = Big(bundleTotal).add(presellTotal);

            // check for accessories
            pricebarTotal = checkAccessories(pricebarTotal);

            // check for sizes
            var sizes = jQuery('#offer #sizes');
            var currentSize = jQuery('.size.active', sizes).attr('id');
            if (currentSize !== undefined) {
                var price = jQuery('#' + currentSize).attr('data-price');

                // check if price exists
                if (price !== undefined) {

                    // add qty if exists
                    var sizeQtyClass; jQuery('#offer .offer.selected:visible').each(function() { sizeQtyClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('size_qty') === 0; }).join(); });
                    if (sizeQtyClass !== undefined){
                        var sizeQty = parseInt(sizeQtyClass.replace('size_qty_',''));
                        price = Big(price).mul(sizeQty);
                    }

                    // set price
                    pricebarTotal = Big(pricebarTotal).add(price);
                }
            }

            var pricebarTotalDisplay = guDisplayCurrency(pricebarTotal);
            jQuery('.price', pricebar).text(pricebarTotalDisplay);
            jQuery('.price', pricebar).attr('data-price', pricebarTotal);

            // hide checkout
            var checkout_el = jQuery('#checkout');
            if (checkout_el.length && checkout_el.is(':visible')) {
                checkout_el.slideUp('1500');
                getPricebar();
            }

            // reset presell popups
            if (jQuery('.presell-popup').length) {
                jQuery('.presell-popup').each(function () {
                    jQuery(this).removeClass('active inactive');
                });
            }

            // fire event
            gu_fire_event("Yes To Inline Upsell", "Click");
        });
    }

    function presellReset()
    {
        jQuery(function () {
            var presell = findPresell();

            //jQuery('#offer #price-tab button').click();

            jQuery('#presell #presell-decline').slideUp();

            if (presell)
            {
                jQuery('#presell #presell-decline').slideDown();
                presell.find('.presell-decline').slideDown();

                presell.removeClass('active');
                var removeBtn = presell.find('.subtle:not(#presell-decline):not(.presell-decline) button');

                removeBtn.text(btnTxt);
                removeBtn.attr('onclick', 'presellClick();');
                removeBtn.parent().addClass('cta');
                removeBtn.parent().removeClass('subtle');
            }



            // remove from selected units
            for (var i = 0; i < gu_products.selectedUnits.length; i++) {
                if (gu_products.selectedUnits[i].name.toLowerCase() == 'presell') {
                    gu_products.selectedUnits.splice(i, 1);
                    updateSelectedUnits();
                }
            }

            // fire event
            //gu_fire_event("No To Inline Upsell", "Click");
        })
    }

    function presellDecline() {
        jQuery(function () {
            var presell = findPresell();

            jQuery('#offer #price-tab button').click();

            jQuery('#presell #presell-decline').slideUp();
            presell.find('.presell-decline').slideUp();

            var removeBtn = presell.find('.subtle:not(#presell-decline):not(.presell-decline) button');

            removeBtn.text(btnTxt);
            removeBtn.attr('onclick', 'presellClick();');
            removeBtn.parent().addClass('cta');
            removeBtn.parent().removeClass('subtle');

            presell.removeClass('active');

            // remove from selected units
            for (var i = 0; i < gu_products.selectedUnits.length; i++) {
                if (gu_products.selectedUnits[i].name.toLowerCase() == 'presell') {
                    gu_products.selectedUnits.splice(i, 1);
                    updateSelectedUnits();
                }
            }

            // fire event
            gu_fire_event("No To Inline Upsell", "Click");
        })
    }

    function presellAdd() {
        jQuery(function () {
            var presell = findPresell();
            var qty = jQuery('.quantity', presell);

            var inp = jQuery('input', qty);
            presell.addClass('active');

            // add 1
            if (inp.val() >= 0) {
                var val = inp.val();
                val++;
                inp.val(val);
            }

            // add price to pricebar
            var pricebar = jQuery('#offer #price-tab');

            // get bundle total
            var bundleQty = 1;
            if (jQuery('#offer .offer.selected .quantity').length) {
                bundleQty = jQuery('#offer .offer.selected .quantity input').val();
            }
            var bundleTotal = Big(jQuery('#offer .offer.selected:visible').data('price')).mul(bundleQty);

            // presell total
            var presellTotal = presell.data('price');
            var presellQty = jQuery('input', qty).val();
            var pricebarTotal = Big(bundleTotal).add(Big(presellTotal).mul(presellQty));

            // check for accessories
            pricebarTotal = checkAccessories(pricebarTotal);

            // check for sizes
            var sizes = jQuery('#offer #sizes');
            var currentSize = jQuery('.size.active', sizes).attr('id');
            if (currentSize !== undefined) {
                var price = jQuery('#' + currentSize).attr('data-price');

                // check if price exists
                if (price !== undefined) {

                    // add qty if exists
                    var sizeQtyClass; jQuery('#offer .offer.selected:visible').each(function() { sizeQtyClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('size_qty') === 0; }).join(); });
                    if (sizeQtyClass !== undefined){
                        var sizeQty = parseInt(sizeQtyClass.replace('size_qty_',''));
                        price = Big(price).mul(sizeQty);
                    }

                    // set price
                    pricebarTotal = Big(pricebarTotal).add(price);
                }
            }

            var pricebarTotalDisplay = guDisplayCurrency(pricebarTotal);
            jQuery('.price', pricebar).text(pricebarTotalDisplay);
            jQuery('.price', pricebar).attr('data-price', pricebarTotal);

            // hide checkout
            var checkout_el = jQuery('#checkout');
            if (checkout_el.length && checkout_el.is(':visible')) {
                checkout_el.slideUp('1500');
                getPricebar();
            }

            // reset presell popups
            if (jQuery('.presell-popup').length) {
                jQuery('.presell-popup').each(function () {
                    jQuery(this).removeClass('active inactive');
                });
            }
        });
    }

    function presellSub() {
        jQuery(function () {
            var presell = findPresell();
            var qty = jQuery('.quantity', presell);
            var btn = jQuery('.cta:not(.active) button', presell);
            var btnTxt = btn.text();
            var btnBg = btn.css('background-color');
            var btnColor = btn.css('color');

            var actBtn = jQuery('.cta.active button', presell);
            var actBtnTxt = actBtn.text();

            var inp = jQuery('input', qty);

            // hide
            if (inp.val() == 1) {
                // deactivate
                presell.removeClass('active');
                btn.text(actBtnTxt);
                actBtn.text(btnTxt);
                btn.css({
                    'background-color': '' + btnBg + '',
                    'color': '' + btnColor + ''
                });

                // show button & hide the quantity
                setTimeout(function () {
                    qty.fadeOut();
                    setTimeout(function () {
                        jQuery('.cta:not(.active)', presell).fadeIn();
                    }, 400);
                }, 100);

                // get bundle total
                var bundleQty = 1;
                if (jQuery('#offer .offer.selected .quantity').length) {
                    bundleQty = jQuery('#offer .offer.selected .quantity input').val();
                }
                var bundleTotal = Big(jQuery('#offer .offer.selected:visible').data('price')).mul(bundleQty);

                // check for sizes
                var sizes = jQuery('#offer #sizes');
                var currentSize = jQuery('.size.active', sizes).attr('id');
                if (currentSize !== undefined) {
                    var price = jQuery('#' + currentSize).attr('data-price');

                    // check if price exists
                    if (price !== undefined) {

                        // add qty if exists
                        var sizeQtyClass; jQuery('#offer .offer.selected:visible').each(function() { sizeQtyClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('size_qty') === 0; }).join(); });
                        if (sizeQtyClass !== undefined){
                            var sizeQty = parseInt(sizeQtyClass.replace('size_qty_',''));
                            price = Big(price).mul(sizeQty);
                        }

                        // set price
                        bundleTotal = Big(bundleTotal).add(price);
                    }
                }

                // sub price from pricebar
                var pricebar = jQuery('#offer #price-tab');
                var pricebarTotal = bundleTotal;

                // check for accessories
                pricebarTotal = checkAccessories(pricebarTotal);

                var pricebarTotalDisplay = guDisplayCurrency(pricebarTotal);
                jQuery('.price', pricebar).text(pricebarTotalDisplay);
                jQuery('.price', pricebar).attr('data-price', pricebarTotal);

                // remove from selected units
                for (var i = 0; i < gu_products.selectedUnits.length; i++) {
                    if (gu_products.selectedUnits[i].name.toLowerCase() == 'presell') {
                        gu_products.selectedUnits.splice(i, 1);
                        updateSelectedUnits();
                    }
                }

                // fire event
                gu_fire_event("Remove Inline Upsell", "Click");
            }

            // sub 1
            else if (inp.val() >= 1) {
                var val = inp.val();
                val--;
                inp.val(val);

                // sub price from pricebar
                var pricebar = jQuery('#offer #price-tab');

                var bundleQty = 1;
                if (jQuery('#offer .offer.selected .quantity').length) {
                    bundleQty = jQuery('#offer .offer.selected .quantity input').val();
                }
                var bundleTotal = Big(jQuery('#offer .offer.selected:visible').data('price')).mul(bundleQty);

                // check for sizes
                var sizes = jQuery('#offer #sizes');
                var currentSize = jQuery('.size.active', sizes).attr('id');
                if (currentSize !== undefined) {
                    var price = jQuery('#' + currentSize).attr('data-price');

                    // check if price exists
                    if (price !== undefined) {

                        // add qty if exists
                        var sizeQtyClass; jQuery('#offer .offer.selected:visible').each(function() { sizeQtyClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('size_qty') === 0; }).join(); });
                        if (sizeQtyClass !== undefined){
                            var sizeQty = parseInt(sizeQtyClass.replace('size_qty_',''));
                            price = Big(price).mul(sizeQty);
                        }

                        // set price
                        bundleTotal = Big(bundleTotal).add(price);
                    }
                }

                var presellTotal = presell.data('price');
                var presellQty = jQuery('input', qty).val();
                var pricebarTotal = Big(bundleTotal).add(Big(presellTotal).mul(presellQty));
                var pricebarTotalDisplay = guDisplayCurrency(pricebarTotal);

                jQuery('.price', pricebar).text(pricebarTotalDisplay);
                jQuery('.price', pricebar).attr('data-price', pricebarTotal);
            }

            // hide checkout
            var checkout_el = jQuery('#checkout');
            if (checkout_el.length && checkout_el.is(':visible')) {
                checkout_el.slideUp('1500');
                getPricebar();
            }

            // reset presell popups
            if (jQuery('.presell-popup').length) {
                jQuery('.presell-popup').each(function () {
                    jQuery(this).removeClass('active inactive');
                });
            }
        });
    }

    function presellPopupClick(status) {
        jQuery(function () {
            // set status
            var popup = jQuery('#' + jQuery('.presell-popup:visible').attr('id'));
            if (status == true) {
                popup.addClass('active');

                // fire event
                var popupID = popup.attr('id');
                gu_fire_event("Yes To Pop Sale", "Click", null, "Popup ID", popupID);
            } else if (!popup.hasClass('active') && !popup.hasClass('inactive')) {
                popup.addClass('inactive');
            }

            // remove from selected units
            if (!status == true) {
                for (var i = 0; i < gu_products.selectedUnits.length; i++) {
                    if (gu_products.selectedUnits[i].name.toLowerCase() == 'presell popup') {
                        gu_products.selectedUnits.splice(i, 1);
                        updateSelectedUnits();
                    }
                }

                // fire event
                gu_fire_event("No To Pop Sale", "Click", null, "Popup ID", popupID);
            }

            // close giddybox & reset x btn
            jQuery('#giddybox .giddybox-closeBtn').attr('onclick', '');
            jQuery('#giddybox .giddybox-closeBtn').click();

            // checkout
            prepCheckout();
        });
    }



    function inlineUpsellClick() {
        jQuery(function() {
            //presell.addClass('active');

            //upsell
            var inlineUpsell = jQuery('#' + jQuery('.inlineUpsell:visible').attr('id'));
            var qty = jQuery('.quantity',inlineUpsell);
            var btn = jQuery('.cta:not(.active) a',inlineUpsell);

            // activate btn
            jQuery('.inlineUpsell:visible').addClass('active');


            setTimeout(function() {
                btn.text(guSubstituteString("Remove?"));
                btn.parent().removeClass('cta');
                btn.parent().addClass("subtle");
                btn.attr('onclick','inlineUpsellRemove();');
                //jQuery('#offer #price-tab button').click();
            }, 400);

            prepCheckout();
        });
    }

    function inlineUpsellRemove()
    {
        jQuery(function() {

            var inlineUpsell = jQuery('#' + jQuery('.inlineUpsell:visible').attr('id'));

            var removeBtn = jQuery('.cta:not(.active) a',inlineUpsell);
            var actBtn = jQuery('.e3.cta button', inlineUpsell);

            //btn vars
            var btnTxt = actBtn.text();
            var btnBg = actBtn.css('background-color');
            var btnColor = actBtn.css('color');

            removeBtn.text(btnTxt);
            removeBtn.attr('onclick','inlineUpsellClick();');
            removeBtn.parent().addClass('cta');
            removeBtn.parent().removeClass('subtle');

            inlineUpsell.removeClass('active');

            prepCheckout();
        });
    }

    function inlineUpsellChecked(checkBox){

        var inlineUpsell = jQuery('#' + jQuery('.inlineUpsell:visible').attr('id'));

        if (checkBox.checked) {
            inlineUpsell.addClass('active');
        }
        else{
            inlineUpsell.removeClass('active');
        }

        prepCheckout('reload');
    }

    function getPricebar() {
        jQuery(function() {
            var pricebar = '#offer #price-tab';
            var pricebarSpace = '.price-tab-space';
            var colors = jQuery('#offer #colors');
            var presell = findPresell();
            var qty = jQuery('.quantity', presell);
            var sizes = jQuery('#offer #sizes');
            var storage = jQuery('#offer #storage');
            var accessories = jQuery('#offer #accessories');
            var sizeSelector = jQuery('#size-select');

            // bundle price
            if (jQuery('#offer').hasClass('special')) {
                // special1
                var bundleQty = 0;

                // hide checkout
                if (jQuery('#checkout').is(':visible')) {
                    jQuery('#checkout').slideUp();
                }

                // prep offers
                jQuery('#offer .offer:visible').each(function () {
                    // get bundle qty
                    if (jQuery('.quantity', this).is(':visible')) {
                        var sQty = jQuery('.quantity input', this).val();
                        bundleQty = Big(bundleQty).add(sQty);
                    }

                    // set prices
                    var productClasses;
                    jQuery(this).each(function () {
                        productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var products = productClasses.split(',');
                    var prices = [];
                    var total = 0;

                    // get price for each bundle class (uses first variant)
                    function getBundlePrice(value) {
                        var product = gu_products.getProduct(eval(value));
                        if (product != null) {
                            var price = product.variants[0].price;
                            prices.push(Number(Big(price)));
                        } else {
                            console.log('finding bundle prices');
                            setTimeout(function () {
                                getBundlePrice(value);
                            }, 500)
                        }
                    }

                    jQuery.each(products, function (index, value) {
                        getBundlePrice(value);
                    });

                    for (var i = 0; i < prices.length; i++) {
                        total = Big(total).add(prices[i]);
                    }

                    jQuery(this).attr('data-price', total);
                });

                // if (bundleQty > 1) {
                //     console.log('more than 1');
                //     // var discPrice = jQuery('#storage .storage.selected').attr('data-discprice');
                //     // var bundleTotal = Big(discPrice).mul((bundleQty - 1));
                //     // bundleTotal = Big(bundleTotal).add(bundlePrice);
                // } else {
                //     var index = jQuery('#offer .offer.selected:visible').index();
                //     var bundlePrice = jQuery('#storage .storage.selected').find('.prices .bundle-'+(index + 1)+'').attr('data-unit');
                //     var bundleTotal = bundlePrice;
                // }

                // get total qty
                var totalQty = 0;
                jQuery('#offer .offer.selected:visible').each(function () {
                    var qty = jQuery('.quantity input', this).val();
                    totalQty = Big(totalQty).add(qty);
                });

                // get most expensive product
                var bundles = {
                    prices: [],
                    index: []
                };

                jQuery('#offer .offer.selected:visible').each(function () {
                    var bundleIndex = jQuery(this).index();
                    var unitPrice = jQuery('#storage .storage.selected').find('.prices .bundle-' + (bundleIndex + 1) + '').attr('data-unit');

                    bundles.prices.push(unitPrice);
                    bundles.index.push(bundleIndex);
                });

                var highest = 0;
                var highestIndex = 0;
                jQuery.each(bundles.prices, function (index, value) {
                    if (parseInt(value) > parseInt(highest)) {
                        highest = value;
                        highestIndex = bundles.index[index];
                    }
                });

                var highestUnitPrice = highest;
                var bundleTotal = highestUnitPrice;

                // add discounted products if > 1 qty
                if (totalQty > 1) {
                    var totalDiscPrice = 0;
                    jQuery('#offer .offer.selected:visible').each(function () {
                        var bundleIndex = jQuery(this).index();
                        var discPrice = jQuery('#storage .storage.selected').find('.prices .bundle-' + (bundleIndex + 1) + '').attr('data-discprice');

                        if (bundleIndex == highestIndex) {
                            var discQty = jQuery('.quantity input', this).val() - 1;
                        } else {
                            var discQty = jQuery('.quantity input', this).val();
                        }

                        // if there are discounted products
                        if (discQty > 0) {
                            // calc total discounted price
                            totalDiscPrice = Big(totalDiscPrice).add(Big(discPrice).mul(discQty));

                            // calc total price
                            bundleTotal = Big(highestUnitPrice).add(totalDiscPrice);
                        }
                    });
                }

                // hide pricebar
                if (bundleTotal == 0) {
                    jQuery('#storage, #checkout, #price-tab, .price-tab-space').slideUp();
                    jQuery('#faq, #footer, #badges').slideDown();

                    // reset storage
                    setTimeout(function () {
                        jQuery('#storage .storage').each(function (index) {
                            if (index == 0) {
                                jQuery(this).addClass('selected');
                                jQuery(this).find('.checkmark input').prop('checked', true);
                            } else {
                                jQuery(this).removeClass('selected');
                                jQuery(this).find('.checkmark input').prop('checked', false);
                            }
                        });
                    }, 500);
                }

            } else {
                // standard
                var bundleTotal = 0;
                var bundleQty = 1;
                var discPrice = null;

                // check for qty selector
                if (jQuery('#offer .offer.selected .quantity').length) {
                    bundleQty = jQuery('#offer .offer.selected .quantity input').val();

                    // check for discount
                    var discClass; jQuery('#offer .offer.selected:visible').each(function() { discClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('qd-') === 0; }).join(); });
                    if (discClass !== undefined){

                        // create array for each class
                        var discClasses = discClass.split(',');
                        for (var i = 0; i < discClasses.length; ++i) {

                            // get discounted product and set price
                            var product = gu_products.getProduct(eval(discClasses[i].replace('qd-', '')));
                            discPrice = product.variants[0].price;
                        }
                    }
                }

                // if qty discount exists
                if (discPrice !== null) {

                    // set first qty at regular price, the rest discounted
                    var price = jQuery('#offer .offer.selected:visible').data('price');
                    var discTotal = Big(discPrice).mul((bundleQty - 1));

                    // set bundle total
                    bundleTotal = Big(price).add(discTotal);

                } else {
                    bundleTotal = Big(jQuery('#offer .offer.selected:visible').data('price')).mul(bundleQty);
                }
            }

            // btn properties
            var nextBtn = jQuery('#offer #buy-now-next button');
            var nextBtnTxt = nextBtn.text();
            var nextBtnBg = nextBtn.css('background-color');
            var nextBtnColor = nextBtn.css('color');

            var buyBtn = jQuery('#offer #buy-now-checkout button');
            var buyBtnTxt = buyBtn.text();
            var buyBtnBg = buyBtn.css('background-color');
            var buyBtnColor = buyBtn.css('color');

            // set data
            if (colors.length && presell && sizes.length && accessories.length && sizeSelector.length) {
                var text = nextBtnTxt;
                var color = nextBtnColor;
                var bg = nextBtnBg;
                var state = 'next';
            } else if (jQuery('#offer').length && jQuery('#offer').hasClass(('special')) && !jQuery('#storage').is(':visible')) {
                var text = nextBtnTxt;
                var color = nextBtnColor;
                var bg = nextBtnBg;
                var state = 'next';
            } else if (colors.length || presell || sizes.length || accessories.length || storage.length || sizeSelector.length) {
                var text = buyBtnTxt;
                var color = buyBtnColor;
                var bg = buyBtnBg;
                var state = 'checkout';
            }

            // special
            if (jQuery('#storage').length) {
                if (jQuery('#storage').is(':visible')) {
                    var text = buyBtnTxt;
                    var color = buyBtnColor;
                    var bg = buyBtnBg;
                    var state = 'checkout';

                    jQuery(pricebar).removeClass('next').addClass(state);
                    jQuery('button', jQuery(pricebar)).text(text);
                    jQuery('button', jQuery(pricebar)).css({
                        'color': color,
                        'background-color': bg
                    });

                    jQuery('#offer #price-tab button').attr('onclick', "prepCheckout('trigger');");
                } else if (!jQuery('#storage').is(':visible')) {
                    var text = nextBtnTxt;
                    var color = nextBtnColor;
                    var bg = nextBtnBg;
                    var state = 'next';

                    jQuery(pricebar).removeClass('next').addClass(state);
                    jQuery('button', jQuery(pricebar)).text(text);
                    jQuery('button', jQuery(pricebar)).css({
                        'color': color,
                        'background-color': bg
                    });

                    jQuery('#offer #price-tab button').attr('onclick', 'special1();');
                }
            }

            // add premium colors
            if (colors.length && jQuery('.color.active .price', colors).length) {
                var premiumColorPrice = jQuery('.color.active .price', colors).attr('data-price');
                bundleTotal = Big(bundleTotal).add(Big(premiumColorPrice));
            }

            // build the price tab
            var bundleTotalDisplay = guDisplayCurrency(bundleTotal);

            if (!jQuery(pricebar).length) {
                // get offer price
                var totalTxt = guSubstituteString('Total');
                var onClick = "prepCheckout('trigger')";
                jQuery('#offer > .vc_column_container').append('<div id="price-tab" class="' + state + '"><h2 class="total">' + totalTxt + ': <span class="price" data-price="' + bundleTotal + '">' + bundleTotalDisplay + '</span></h2><button onclick="' + onClick + '" style="background-color: ' + bg + '; color: ' + color + ';">' + text + '</button></div>');
                jQuery('#offer #price-tab').slideDown();

                // special
                if (jQuery('#offer').length && jQuery('#offer').hasClass(('special')) && !jQuery('#storage').is(':visible')) {
                    jQuery('#offer #price-tab button').attr('onclick', 'special1();');
                }

                // add blank space
                setTimeout(function () {
                    var height = jQuery(pricebar).height();

                    jQuery('#offer').after('<div class="price-tab-space"></div>');
                    jQuery('.price-tab-space').height(height).slideDown('fast');
                }, 300);
            }

            // set total
            else {
                // show the pricebar
                if (jQuery(pricebar).length && !jQuery(pricebar).is(':visible')) {
                    jQuery(pricebar).slideDown();
                    jQuery(pricebarSpace).slideDown();
                }

                // set new data
                setTimeout(function () {
                    if (jQuery(pricebar).hasClass('next') && colors.is(':visible') && presell.is(':visible')) {
                        var text = buyBtnTxt;
                        var color = buyBtnColor;
                        var bg = buyBtnBg;
                        var state = 'checkout';

                        jQuery(pricebar).removeClass('next').addClass(state);
                        jQuery('button', jQuery(pricebar)).text(text);
                        jQuery('button', jQuery(pricebar)).css({
                            'color': color,
                            'background-color': bg
                        });
                    }
                }, 250);

                var priceBarTotal = bundleTotal;

                // add presell
                if (presell && presell.hasClass('active')) {
                    // set pricebar total
                    var presellTotal = presell.data('price');
                    var presellQty = jQuery('input', qty).val();
                    priceBarTotal = Big(bundleTotal).add(Big(presellTotal).mul(presellQty));
                }

                // add accessories
                if (accessories.length) {
                    jQuery('#accessories .item').each(function () {
                        var item = this;
                        jQuery(item).find('.isotope-item.active').each(function () {
                            var accessoryPrice = jQuery(this).find('.price').attr('data-price');
                            priceBarTotal = Big(priceBarTotal).add(Big(accessoryPrice));
                        });
                    });
                    // set pricebar total
                    // var presellTotal = presell.data('price');
                    // var presellQty = jQuery('input',qty).val();
                    // priceBarTotal = Big(priceBarTotal).add(Big(presellTotal).mul(presellQty));
                    // priceBarTotalDisplay = guDisplayCurrency(pricebarTotal);
                }

                var priceBarTotalDisplay = guDisplayCurrency(priceBarTotal);

                jQuery('.price', jQuery(pricebar)).text(priceBarTotalDisplay);
                jQuery('.price', jQuery(pricebar)).attr('data-price', priceBarTotal);

            }

            // add size price
            var sizes = jQuery('#offer #sizes');
            var currentSize = jQuery('.size.active', sizes).attr('id');

            if (currentSize !== undefined) {

                var currentSize = jQuery('#' + currentSize);

                var price = currentSize.attr('data-price');
                var firstSize = jQuery('#size-1',sizes);
                var firstSize_math = parseInt(firstSize.attr('data-math'));
                var priceTab_price = jQuery('#offer #price-tab .price');
                var total = priceTab_price.attr('data-price');
                var newTotal = total;

                if (price !== undefined) {

                    // subtract previous size price from total
                    // if (firstSize_math > 0) {
                    //     newTotal = Big(total).sub(firstSize_math);
                    // }

                    // set new math in first
                    // firstSize.attr('data-math',price);

                    // add qty if exists
                    var sizeQtyClass; jQuery('#offer .offer.selected:visible').each(function() { sizeQtyClass = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('size_qty') === 0; }).join(); });
                    if (sizeQtyClass !== undefined){
                        var sizeQty = parseInt(sizeQtyClass.replace('size_qty_',''));
                        price = Big(price).mul(sizeQty);
                    }

                    // add new price
                    newTotal = Big(newTotal).add(price);

                    // set new total
                    var pricebarTotalDisplay = guDisplayCurrency(newTotal);
                    priceTab_price.text(pricebarTotalDisplay);
                    priceTab_price.attr('data-price', newTotal);

                } else {

                    // // subtract previous size price from total
                    // if (newTotal !== undefined) {
                    //     if (firstSize_math > 0) {
                    //         newTotal = Big(total).sub(firstSize_math);
                    //     }
                    //
                    //     // set new total
                    //     var pricebarTotalDisplay = guDisplayCurrency(newTotal);
                    //     priceTab_price.text(pricebarTotalDisplay);
                    //     priceTab_price.attr('data-price', newTotal);
                    // }
                    //
                    // // set back to default
                    // firstSize.attr('data-math','0');
                }
            }
        });
    }

    function hidePricebar() {
        jQuery(function () {
            var pricebar = jQuery('#offer #price-tab');
            if (pricebar.length && pricebar.is(':visible')) {
                pricebar.slideUp();
                jQuery('.price-tab-space').slideUp(100);
            }
        });
    }

    // prep checkout
    // use dtc = true to go direct to checkout (skips extras)
    function prepCheckout(dtc) {
        jQuery(function() {

            // extras setup
            var colors = jQuery('#colors');
            var accessories = jQuery('#accessories');
            var pricebar = '#offer #price-tab';

            var presell = findPresell();

            var presellPopupClasses; jQuery('#offer .offer.selected:visible').each(function() { presellPopupClasses = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('presell-popup-') === 0; }).join(); });
            if (presellPopupClasses != '' && dtc != true) {
                var presellPopups = presellPopupClasses.split(',');
            }
            var inlineUpsellClasses; jQuery('#offer .offer.selected:visible').each(function() { inlineUpsellClasses = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('inlineUpsell-') === 0; }).join(); });
            if (inlineUpsellClasses != '') {
                var inlineUpsells = inlineUpsellClasses.split(',');
            }

            var upsellClasses; jQuery('#offer .offer.selected:visible').each(function() { upsellClasses = jQuery.grep(this.className.split(" "), function(v, i){ return v.indexOf('upsell-') === 0; }).join(); });
            if (upsellClasses != '') {
                var upsells = upsellClasses.split(',');
            }

            var sizeSelector = jQuery('#size-select');

            // remove color spacing
            if (mobileDevice && jQuery('.colors-space', colors).length) {
                jQuery('.colors-space', colors).slideUp();
                setTimeout(function () {
                    jQuery('.colors-space', colors).remove();
                }, 1000);
            }

            // clear prof. array
            gu_products.selectedUnits = [];

            // profitability reporting for special
            if (jQuery('#offer').length && jQuery('#offer').hasClass('special')) {
                // get qty
                var totalQty = 0;
                jQuery('#offer .offer.selected:visible').each(function () {
                    var qty = jQuery('.quantity input', this).val();
                    totalQty = Big(totalQty).add(qty);
                });

                // get storage
                var storageIndex = jQuery('#storage .storage-wrap .storage.selected').index();

                // get products
                var iphoneOffer = jQuery('#offer .offer.iphone');
                var androidOffer = jQuery('#offer .offer.android');
                var desktopOffer = jQuery('#offer .offer.desktop');
                var products = {
                    regPrice: [],
                    discPrice: [],
                    prices: [],
                    qty: [],
                    index: []
                };

                function getProducts(offer) {
                    // get reg prices
                    var productClasses;
                    offer.each(function () {
                        productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    productClasses = productClasses.split(',');

                    for (var i = 0; i < productClasses.length; ++i) {
                        products.regPrice.push(productClasses[i]);

                        // get prices
                        var price = gu_products.getProduct(eval(productClasses[i]));
                        products.prices.push(Big(price.variants[storageIndex].price)); // big

                        // get qty
                        var qty = offer.find('.quantity input').val();
                        products.qty.push(qty);

                        // get index
                        var index = offer.index();
                        products.index.push(index);
                    }

                    // get disc prices
                    var discProductClasses;
                    offer.each(function () {
                        discProductClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('qd-guproduct_') === 0;
                        }).join();
                    });
                    discProductClasses = discProductClasses.split(',');

                    for (var i = 0; i < productClasses.length; ++i) {
                        products.discPrice.push(discProductClasses[i]);
                    }
                }

                // get iphone
                if (iphoneOffer.hasClass('selected')) {
                    getProducts(iphoneOffer);
                }

                // get android
                if (androidOffer.hasClass('selected')) {
                    getProducts(androidOffer);
                }

                // get desktop
                if (desktopOffer.hasClass('selected')) {
                    getProducts(desktopOffer);
                }

                // get variants
                var variants = {
                    regPrice: [],
                    discPrice: []
                };

                // reg variants
                for (var i = 0; i < products.regPrice.length; ++i) {
                    var productInfo = gu_products.getProduct(eval(products.regPrice[i]));
                    var variantId = productInfo.variants[storageIndex].id;
                    variants.regPrice.push(variantId);
                }

                // disc variants
                for (var i = 0; i < products.discPrice.length; ++i) {
                    var productInfo = gu_products.getProduct(eval(products.discPrice[i].split('qd-')[1]));
                    var variantId = productInfo.variants[storageIndex].id;
                    variants.discPrice.push(variantId);
                }

                // get most expensive item
                var highest = 0;
                var highestIndex = 0;
                jQuery.each(products.prices, function (index, value) {
                    if (value > highest) {
                        highest = value;
                        highestIndex = index;
                    }
                });
                var realIndex = products.index[highestIndex];

                // report most expensive item first
                var variant = gu_products.getVariant(variants.regPrice[highestIndex]);
                var product = gu_products.getProduct(variant.product_id);

                var unit = {
                    name: product.title + ' ' + variant.title,
                    id: jQuery('#offer .offer:visible').eq(realIndex).attr('id'),
                    quantity: 1,
                    type: 'bundle',
                    lineItems: [
                        {
                            productId: product.id,
                            variantName: product.title + ' ' + variant.title,
                            variantId: variant.id,
                            quantity: 1
                        }
                    ]
                };

                gu_products.addSelectedUnit(unit);

                if (totalQty > 1) {
                    for (var i = 0; i < variants.discPrice.length; ++i) {
                        // get qty
                        var qty = products.qty[i];

                        // exclude most expensive item
                        if (i == highestIndex && variants.regPrice.length > 1) {
                            qty = qty - 1;
                        }

                        // add variant to frag
                        if (qty > 0) {
                            var variant = gu_products.getVariant(variants.discPrice[i]);
                            var product = gu_products.getProduct(variant.product_id);

                            console.log('length', jQuery('#offer .offer.selected').length);
                            if (qty > 1 && i == highestIndex && jQuery('#offer .offer.selected').length < 2) {
                                qty = (qty - 1);
                            }

                            var unit = {
                                name: product.title + ' ' + variant.title,
                                id: jQuery('#offer .offer:visible').eq(products.index[i]).attr('id'),
                                quantity: 1,
                                type: 'bundle',
                                lineItems: [
                                    {
                                        productId: product.id,
                                        variantName: product.title + ' ' + variant.title,
                                        variantId: variant.id,
                                        quantity: qty
                                    }
                                ]
                            };

                            gu_products.addSelectedUnit(unit);
                        }
                    }
                }
            }

            // end

            //dechoker check
            var pass = false;
            if (dtc == undefined || dtc == "trigger") {
                pass = true;
            }

            if (accessories.length && !accessories.is(':visible') && pass) {
                getAccessories();
            }

            // check for next on pricebar
            else if (presell && !presell.is(':visible') && pass) {
                getPresell(presell);
            }

            // check for dechoker
            else if (sizeSelector.length > 0 && pass && dtc !== "trigger") {
                if (!sizeSelector.is(':visible')) {
                    console.log('test1');
                    getDechoker();
                } else if (sizeSelector.is(':visible') && jQuery('#checkout').is(':visible')) {
                    console.log('test2');
                    jQuery('#checkout').slideUp();
                    getDechoker();
                } else if (sizeSelector.is(':visible') && !jQuery('#checkout').is(':visible')) {
                    console.log('test3');
                    getDechoker();
                }
            }

            // check for presell popups
            else if (presellPopups != undefined && pass) {
                prepPresellPopups(presellPopups);
            }

            // check for inline upsells
            // else if (inlineUpsells != undefined && dtc == undefined)
            // {
            //     prepInlineUpsells(inlineUpsells);
            // }
            //
            // // check for upsells
            // else if (upsells != undefined && dtc == undefined)
            // {
            //     prepUpsells(upsells);
            // }

            // checkout
            else {

                // check for inline upsells
                if (inlineUpsells != undefined && dtc != 'reload') {
                    prepInlineUpsells(inlineUpsells);
                }

                // check for upsells
                if (upsells != undefined && dtc != 'reload') {
                    prepUpsells(upsells);
                }

                // check for presell popups
                if (presellPopupClasses == '') {
                    console.log('presell popup class is blank');
                    console.log(presellPopupClasses);
                }
                if (presellPopupClasses != undefined && presellPopupClasses != '' && dtc != 'reload' && !jQuery('#' + presellPopupClasses).hasClass('inactive') && !jQuery('#' + presellPopupClasses).hasClass('active')) {
                    prepPresellPopups(presellPopupClasses);
                }

                createFragment();
            }
        });
    }

    function prepPresellPopups(popupClasses) {
        jQuery(function () {

            var popup = '';

            // check if array or string
            if (typeof popupClasses === 'string') {
                popup = jQuery('#' + popupClasses);
            } else {
                popup = jQuery('#' + popupClasses[0]);
            }

            if (!popup.hasClass('active') && !popup.hasClass('inactive')) {

                // open giddybox but check if array or string
                if (typeof popupClasses === 'string') {
                    giddybox(popupClasses);
                } else {
                    giddybox(popupClasses[0]);
                }

                setTimeout(function () {
                    // update img
                    if (popup.hasClass('colors')) {
                        if (popup.attr('data-prevImg') == undefined) {
                            var prevColor = jQuery('#offer #colors .color:first-child ').attr('data-color');

                            //if the first option is variety, use the next option
                            if (prevColor === 'variety') {
                                prevColor = jQuery('#offer #colors .color:nth-child(2) ').attr('data-color');
                            }

                        } else {
                            var prevColor = popup.attr('data-prevImg');
                        }

                        var activeColor = getActiveColor();
                        var img = jQuery('.image img', popup);
                        var imgSrc = img.attr('src').replace(prevColor, activeColor);

                        // set img
                        jQuery('#giddybox .presell-popup .image img').attr({
                            'src': imgSrc,
                            'srcset': imgSrc
                        });

                        // set prev
                        jQuery('#' + popup[0].id).attr('data-prevImg', activeColor);
                    }

                    // update close btn
                    jQuery('#giddybox .giddybox-closeBtn').attr('onclick','presellPopupClick();')
                }, 200);
            }

            else {
                prepCheckout(true);
            }
        });
    }

    function prepInlineUpsells(inlineUpsellClasses) {
        jQuery(function()
        {
            var show = false;
            if (jQuery('#main-form .inlineUpsell').length != 0) {
                //reset any currently visible inline upsells
                jQuery('#main-form .inlineUpsell').removeClass('active').removeClass('current').slideUp();
                jQuery('#main-form .inlineUpsell').find("input").prop("checked", false)

                //if the current upsell has 'shown' class it has been shown, so we should auto show the next current one
                if (jQuery('#main-form .inlineUpsell').hasClass('shown')) {
                    jQuery('#main-form .inlineUpsell').removeClass('shown');
                    show = true;
                }

            }

            var upsell = jQuery('#' + inlineUpsellClasses[0]);
            upsell.addClass('current');
            jQuery('#main-form .submit').before(upsell);
            if (show)
            {
                upsell.slideDown().addClass('shown');
            }

            //prepCheckout(true);
        });
    }

    function prepUpsells(upsellClasses) {
        jQuery(function()
        {
            var show = false;
            if (jQuery('#main-form .upsell').length != 0)
            {
                //reset any currently visible inline upsells
                jQuery('#main-form .upsell').removeClass('current').removeClass('active').slideUp();
                jQuery('#main-form .upsell').find("input").prop("checked", false)

                //if the current upsell is already visible, suto show the next current one
                if (jQuery('#main-form .upsell').is(':visible'))
                {
                    show = true;
                }

            }

            var upsell = jQuery('#' + upsellClasses[0]);
            upsell.addClass('current');
            jQuery('#main-form').append(upsell);
            if (show)
            {
                upsell.slideDown();
            }

            //prepCheckout(true);
        });
    }


    function buildUnit(name, id, qty, type, products) {
        qty = parseInt(qty);
        var unit = {
            name: name,
            id: id,
            quantity: qty,
            type: type,
            lineItems: []
        }
        var qty = (jQuery('#' + id).find('.quantity').length) ? jQuery('#' + id).find('.quantity input').val() : 1;

        jQuery.each(products, function (index, value) {
            var product = gu_products.getProduct(eval(value));

            if (jQuery('#' + id).hasClass('colors')) {
                var color = getActiveColor();
                var variant = jQuery.grep(product.variants, function (item) {
                    var variantName = item.title.toLowerCase();
                    if (~color.indexOf('-')) {
                        color = color.replace(/\-/g, ' ');
                    }
                    return (variantName === color);
                });

                // set presell unit qty to 1
                if (name.toLowerCase() === 'presell') {
                    qty = 1;
                }

                var item = {
                    productId: product.id,
                    variantName: variant[index].title,
                    variantId: variant[index].id,
                    quantity: qty
                };
            } else if (jQuery('#' + id).hasClass('sizes')) {
                var size = getActiveSize();
                var variant = jQuery.grep(product.variants, function (item) {
                    var variantName = item.title.toLowerCase();
                    if (~size.indexOf('-')) {
                        size = size.replace(/\-/g, ' ');
                    }
                    return (variantName === size);
                });

                // set presell unit qty to 1
                if (name.toLowerCase() == 'presell') {
                    qty = 1;
                }

                var item = {
                    productId: product.id,
                    variantName: variant[index].title,
                    variantId: variant[index].id,
                    quantity: qty
                };
            } else {
                // set presell unit qty to 1
                if (name.toLowerCase() == 'presell') {
                    qty = 1;
                }

                var variant = gu_products.getFirstVariant(eval(value));
                var item = {
                    productId: product.id,
                    variantName: variant.title,
                    variantId: variant.id,
                    quantity: qty
                };
            }

            unit.lineItems.push(item);
        });

        return (unit);

    }


    function createFragment() {
        jQuery(function () {
            var categorizedProducts = {
                'bundle' : {},
                'presell' : {},
                'presell popup' : {},
                'inlineUpsell' : {},
                'upsell' : {},
                'accessories' : {}
            };
            var fragment = '';

            // special1 fragment creation
            if (jQuery('#offer').length && jQuery('#offer').hasClass('special')) {
                // get qty
                var totalQty = 0;
                jQuery('#offer .offer.selected:visible').each(function () {
                    var qty = jQuery('.quantity input', this).val();
                    totalQty = Big(totalQty).add(qty);
                });

                // get storage
                var storageIndex = jQuery('#storage .storage-wrap .storage.selected').index();

                // get products
                var iphoneOffer = jQuery('#offer .offer.iphone');
                var androidOffer = jQuery('#offer .offer.android');
                var desktopOffer = jQuery('#offer .offer.desktop');
                var products = {
                    regPrice: [],
                    discPrice: [],
                    prices: [],
                    qty: []
                };

                function getProducts(offer) {
                    // get reg prices
                    var productClasses;
                    offer.each(function () {
                        productClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    productClasses = productClasses.split(',');

                    for (var i = 0; i < productClasses.length; ++i) {
                        products.regPrice.push(productClasses[i]);

                        // get prices
                        var price = gu_products.getProduct(eval(productClasses[i]));
                        products.prices.push(Big(price.variants[storageIndex].price)); // big

                        // get qty
                        var qty = offer.find('.quantity input').val();
                        products.qty.push(qty);
                    }

                    // get disc prices
                    var discProductClasses;
                    offer.each(function () {
                        discProductClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('qd-guproduct_') === 0;
                        }).join();
                    });
                    discProductClasses = discProductClasses.split(',');

                    for (var i = 0; i < productClasses.length; ++i) {
                        products.discPrice.push(discProductClasses[i]);
                    }
                }

                // get iphone
                if (iphoneOffer.hasClass('selected')) {
                    getProducts(iphoneOffer);
                }

                // get android
                if (androidOffer.hasClass('selected')) {
                    getProducts(androidOffer);
                }

                // get desktop
                if (desktopOffer.hasClass('selected')) {
                    getProducts(desktopOffer);
                }

                // get variants
                var variants = {
                    regPrice: [],
                    discPrice: []
                };

                // reg variants
                for (var i = 0; i < products.regPrice.length; ++i) {
                    var productInfo = gu_products.getProduct(eval(products.regPrice[i]));
                    var variantId = productInfo.variants[storageIndex].id;
                    variants.regPrice.push(variantId);
                }

                // disc variants
                for (var i = 0; i < products.discPrice.length; ++i) {
                    var productInfo = gu_products.getProduct(eval(products.discPrice[i].split('qd-')[1]));
                    var variantId = productInfo.variants[storageIndex].id;
                    variants.discPrice.push(variantId);
                }

                // get most expensive item
                var highest = 0;
                var highestIndex = 0;
                jQuery.each(products.prices, function (index, value) {
                    if (value > highest) {
                        highest = value;
                        highestIndex = index;
                    }
                });

                // here delete
                // console.log('frag products');
                // console.log('products',products);
                // console.log('variants',variants);
                // console.log('highest index',highestIndex);
                //
                // var testVar = gu_products.getVariant(variants.regPrice[highestIndex]);
                // var testProd = gu_products.getProduct(testVar.product_id);
                //
                // console.log('highest prod ' + testProd.title + ' ' + testVar.title);

                // create frag
                fragment = fragment + variants.regPrice[highestIndex] + ':1,';

                if (totalQty >= 2) {
                    for (var i = 0; i < variants.discPrice.length; ++i) {
                        // get qty
                        var qty = products.qty[i];

                        // exclude most expensive item
                        if (i == highestIndex) {
                            qty = qty - 1;
                        }

                        // add variant to frag
                        if (qty >= 1) {
                            fragment = fragment + variants.discPrice[i] + ':' + qty + ',';
                        }
                    }
                }

                //for reporting
                categorizedProducts['bundle'].name = jQuery('body').attr('data-name');
                categorizedProducts['bundle'].id = jQuery('#offer .offer.selected:visible').attr('id');
                categorizedProducts['bundle'].products = fragment;

                // standard fragment creation
            } else {
                jQuery('#offer .offer.selected:visible').each(function () {
                    var productClasses = '';
                    productClasses += jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('guproduct_') === 0;
                    }).join();
                    productClasses += ",";

                    // Remove Trailing ','
                    if (productClasses.slice(-1) == ',') {
                        productClasses = productClasses.substring(0, productClasses.length - 1);
                    }

                    var products = productClasses.split(',');


                    // discounted quantities
                    //var discQtyProdCounter = 0;
                    var discQtyProd;
                    discQtyProd = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('qd-') === 0;
                    }).join();

                    var bundleQty = jQuery('.quantity input', this).val();
                    var discBundleQty = 0;

                    if (bundleQty == undefined) {
                        bundleQty = 1;
                    }

                    //if qty != undefined
                    //if discQtyProd exists && qty > 1
                    //set discBundleQty bundleQty - 1, bundleQty = 1
                    //run through the bundle products number of bundleQty
                    //run through discounted product number of discBundleQty

                    //qty selector exists on bundle and has a value
                    if (bundleQty > 1) {
                        //check for a discounted product. If none, qty applies to bundle as usual
                        if (discQtyProd) {
                            //make all but one the discounted product
                            discBundleQty = bundleQty - 1;
                            //make bundleQty only one at full price
                            bundleQty = 1;
                        }

                    }

                    //check for accessory prod color overrides (titanium)
                    var accessories = jQuery('#accessories');
                    var activeOverrides = [];
                    if (accessories.length) {
                        var items = [];
                        items = jQuery('.item', accessories);

                        jQuery(items).each(function (index) {

                            jQuery('li', this).each(function (index2) {
                                if (jQuery(this).hasClass('titanium') && jQuery(this).hasClass('active')) {
                                    activeOverrides.push(index);
                                }
                            });

                        });
                    }
                    titaniumOverride = activeOverrides;

                    //run through products in bundle the number of times = qty
                    for (var i = 0; i < bundleQty; i++) {
                        // add bundle to frag
                        jQuery.each(products, function (index, value) {
                            var product = gu_products.getProduct(eval(value));
                            var colors = jQuery('#colors');
                            var sizes = jQuery('#sizes');
                            var storage = jQuery('#storage');

                            // get single variants
                            if (product.variants.length == 1) {
                                jQuery.each(product.variants, function (index, value) {
                                    var variant = value;

                                    // create fragment
                                    fragment = fragment + variant.id + ',';
                                });
                            }

                            // get color variants
                            else if (colors.length) {
                                var color = jQuery('.color.active ', colors).data('color');

                                // variety
                                if (color == 'variety') {
                                    var varietyClasses;
                                    var varietyClasses;
                                    jQuery('#offer .offer.selected:visible').each(function () {
                                        varietyClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                                            return v.indexOf('vp-') === 0;
                                        }).join();
                                    });
                                    var varieties = varietyClasses.split(',');
                                    var color = varieties[index].replace('vp-', '');

                                    if (~color.indexOf('-')) {
                                        color = color.replace(/\-/g, ' ');
                                    }

                                    var variant = jQuery.grep(product.variants, function (item) {
                                        var variantName = item.title.toLowerCase();
                                        return (variantName === color);
                                    });
                                }

                                // standard
                                else {
                                    var variant = jQuery.grep(product.variants, function (item) {
                                        var variantName = item.title.toLowerCase();
                                        if (~color.indexOf('-')) {
                                            color = color.replace(/\-/g, ' ');
                                        }
                                        return (variantName === color);
                                    });
                                }

                                //check for accessory color overrides before adding to frag
                                if (activeOverrides.length) {
                                    jQuery(activeOverrides).each(function (index2) {

                                        //this variant is overriden in acceessories
                                        if (this == index) {
                                            variant = jQuery.grep(product.variants, function (item) {
                                                var variantName = item.title.toLowerCase();
                                                return (variantName === 'titanium');
                                            });
                                        }

                                    });
                                }

                                // create fragment
                                fragment = fragment + variant[0].id + ',';
                            }
                            // get size variants
                            else if (sizes.length) {
                                var size = jQuery('.size.active ', sizes).data('size');


                                var variant = jQuery.grep(product.variants, function (item) {
                                    var variantName = item.title.toLowerCase();
                                    return (variantName === size);
                                });


                                // create fragment
                                fragment = fragment + variant[0].id + ',';
                            }

                            // get size variants
                            else if (storage.length) {
                                var size = jQuery('.size.active ', sizes).data('size');


                                var variant = jQuery.grep(product.variants, function (item) {
                                    var variantName = item.title.toLowerCase();
                                    return (variantName === size);
                                });

                                // create fragment
                                fragment = fragment + variant[0].id + ',';
                            }

                            // choose defined colors
                            else {
                                var definedColors;
                                jQuery('#offer .offer.selected:visible').each(function () {
                                    definedColors = jQuery.grep(this.className.split(" "), function (v, i) {
                                        return v.indexOf('c-') === 0;
                                    }).join();
                                });
                                var colors = definedColors.split(',');
                                var color = '';
                                if (colors.length > 1) { // here
                                    colors[index].replace('c-', '');
                                }

                                // find color in product
                                var variant = jQuery.grep(product.variants, function (item) {
                                    var variantName = item.title.toLowerCase();
                                    return (variantName === color);
                                });

                                // if dechoker
                                if (variant.length === 0) {

                                    var choker = jQuery('#size-select .choker:visible');

                                    var size = choker.eq(index).find('ul li.active').attr('data-size');

                                    // add products
                                    variant = jQuery.grep(product.variants, function (item) {
                                        var variantName = item.title.toLowerCase();
                                        return (variantName === size);
                                    });

                                    fragment = fragment + variant[0].id + ',';

                                    //var optionIndex = choker.eq(index).find('ul li.active').index();
                                    //fragment = fragment + product.variants[optionIndex].id + ',';
                                    //console.log('product added!! ',variant[0].id);

                                    // choker.each(function () {
                                    //     var index = jQuery('li.active',this).index();
                                    //     fragment = fragment + product.variants[index].id + ',';
                                    //     console.log('product added!! ',product.variants[index].id)
                                    // });

                                    // console.log('variant',product.variants);
                                    // console.log('frag',fragment);

                                } else {

                                    // create fragment
                                    fragment = fragment + variant[0].id + ',';
                                }
                            }
                        });
                    }

                    // check for discounted quantities
                    if (discBundleQty) {
                        for (var i = 0; i < discBundleQty; i++) {
                            // add bundle to frag

                            var product = gu_products.getProduct(eval(discQtyProd.replace('qd-', '')));
                            var colors = jQuery('#colors');
                            var sizes = jQuery('#sizes');
                            var storage = jQuery('#storage');

                            // get single variants
                            if (product.variants.length == 1) {
                                jQuery.each(product.variants, function (index, value) {
                                    var variant = value;

                                    // create fragment
                                    fragment = fragment + variant.id + ',';
                                });
                            }

                            // get color variants
                            else if (colors.length) {
                                var color = jQuery('.color.active ', colors).data('color');

                                // variety
                                if (color == 'variety') {
                                    var varietyClasses;
                                    varietyClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                                        return v.indexOf('vp-') === 0;
                                    }).join();
                                    var varieties = varietyClasses.split(',');
                                    var color = varieties[index].replace('vp-', '');
                                    var variant = jQuery.grep(product.variants, function (item) {
                                        var variantName = item.title.toLowerCase();
                                        return (variantName === color);
                                    });
                                }

                                // standard
                                else {
                                    var variant = jQuery.grep(product.variants, function (item) {
                                        var variantName = item.title.toLowerCase();
                                        return (variantName === color);
                                    });
                                }

                                // create fragment
                                fragment = fragment + variant[0].id + ',';
                            }

                            // get size variants
                            else if (sizes.length) {
                                var size = jQuery('.size.active ', sizes).data('size');


                                var variant = jQuery.grep(product.variants, function (item) {
                                    var variantName = item.title.toLowerCase();
                                    return (variantName === size);
                                });


                                // create fragment
                                fragment = fragment + variant[0].id + ',';
                            }

                            // get storage variants
                            else if (storage.length) {
                                var size = jQuery('.size.active ', storage).data('size');


                                var variant = jQuery.grep(product.variants, function (item) {
                                    var variantName = item.title.toLowerCase();
                                    return (variantName === size);
                                });


                                // create fragment
                                fragment = fragment + variant[0].id + ',';
                            }

                        }
                    }

                });

                setTimeout(function() {
                    if (jQuery('#size-select').length && jQuery('#checkout').is(':visible')) {
                        jQuery('html, body').animate({scrollTop: jQuery('#checkout').offset().top}, 1000);
                    }
                }, 500);

                // add premium colors
                if (jQuery('#colors').length && jQuery('#colors .color.active .price').length) {
                    var premiumColorClass;
                    jQuery('#colors').each(function () {
                        premiumColorClass = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var premiumColorObj = gu_products.getProduct(eval(premiumColorClass));
                    var premiumColor = jQuery('#colors .color.active').attr('data-color');
                    var bundleQty = jQuery('#offer .offer.selected .quantity input').val();

                    if (bundleQty == undefined) {
                        bundleQty = 1
                    }

                    // variety
                    if (premiumColor == 'variety') {
                        var premiumVariant = jQuery.grep(premiumColorObj.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            return (variantName === 'variety');
                        });

                        fragment = fragment + premiumVariant[0].id + ':' + bundleQty + ',';
                    }

                    // standard
                    else {
                        var variant = jQuery.grep(premiumColorObj.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            return (variantName === premiumColor);
                        });

                        fragment = fragment + variant[0].id + ':' + bundleQty + ',';
                    }
                }

                // count products (patch)
                var fragArr = [];
                fragArr = fragment.split(',');

                var counts = {};
                jQuery.each(fragArr, function (key, value) {
                    if (!counts.hasOwnProperty(value)) {
                        counts[value] = 1;
                    } else {
                        counts[value]++;
                    }
                });

                jQuery.each(counts, function (key, value) {
                    if (key == '' || key == null) {
                        delete counts[key];
                    }
                });

                // set up the fragment
                fragment = '';
                jQuery.each(counts, function (key, value) {
                    fragment = fragment + key + ':' + value + ',';
                });

                //for reporting
                categorizedProducts['bundle'].name = jQuery('body').attr('data-name');
                categorizedProducts['bundle'].id = jQuery('#offer .offer.selected:visible').attr('id');
                categorizedProducts['bundle'].products = fragment;

                // add accessories
                var accessories = jQuery('#accessories');
                if (accessories.length && accessories.is(':visible')) {
                    var accessoriesClasses;
                    accessories.each(function () {
                        accessoriesClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var accessoriesProducts = accessoriesClasses.split(',');

                    //gather all selected accessories indecies
                    var activeAccessories = jQuery('#accessories .item li.active');
                    var activeAccessoryIds = [];
                    activeAccessories.each(function () {
                        activeAccessoryIds.push(this.id.slice(-1));
                    });

                    //get all accessories products
                    var activeAccessoryProds = [];
                    jQuery.each(activeAccessoryIds, function (index, value) {
                        activeAccessoryProds.push(accessoriesProducts[value]);
                    });

                    // reporting
                    //console.log('!!!! reporting !!!!');
                    var unit = {
                        name: 'Accessories',
                        id: accessories.attr('id'),
                        quantity: 1,
                        type: 'accessories',
                        lineItems: []
                    };

                    // add to fragment
                    jQuery.each(activeAccessoryProds, function (index, value) {
                        var product = gu_products.getProduct(eval(value));
                        fragment = fragment + product.variants[0].id + ',';

                        // check if product is already in reporting
                        var alreadyAdded;
                        jQuery.each(unit.lineItems, function (index, value) {
                            var varId = product.variants[0].id;
                            var lineItemId = value.variantId;
                            if (varId == lineItemId) {
                                alreadyAdded = true;
                            }
                        });

                        // reporting
                        if (alreadyAdded) {
                            // add qty
                            jQuery.each(unit.lineItems, function (index, value) {
                                var varId = product.variants[0].id;
                                var lineItemId = value.variantId;
                                if (varId == lineItemId) {
                                    unit.lineItems[index].quantity = unit.lineItems[index].quantity + 1;
                                }
                            });
                        } else {
                            // add unit
                            var lineItems = {
                                productId: product.variants[0].product_id,
                                variantName: product.variants[0].title,
                                variantId: product.variants[0].id,
                                quantity: 1
                            }
                            // console.log('varitant name',product.variants[0]);
                            unit.lineItems.push(lineItems);
                        }
                    });

                    //gu_products.addSelectedUnit(unit);

                    //reset products string
                    categorizedProducts['accessories'].products = '';

                    for (var i = 0; i <unit.lineItems.length; i++) {
                        categorizedProducts['accessories'].products += unit.lineItems[i].variantId + ':' + unit.lineItems[i].quantity + ',';
                    }

                    // categorizedProducts['accessories'].products = variant[0].id + ':' + presellQty + ',';
                    categorizedProducts['accessories'].id = accessories.attr('id');
                    categorizedProducts['accessories'].name = "Accessories";
                }

                // count products (patch)
                var fragArr = [];
                fragArr = fragment.split(',');

                var counts = {};
                jQuery.each(fragArr, function (key, value) {
                    if (!counts.hasOwnProperty(value)) {
                        counts[value] = 1;
                    } else {
                        counts[value]++;
                    }
                });

                jQuery.each(counts, function (key, value) {
                    if (key == '' || key == null) {
                        delete counts[key];
                    }
                });

                // set up the fragment
                fragment = '';
                jQuery.each(counts, function (key, value) {
                    fragment = fragment + key + ':' + value + ',';
                });



                // add presell
                var presell = findPresell();
                if (presell && presell.hasClass('active')) {
                    var presellClasses;
                    presell.each(function () {
                        presellClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                            return v.indexOf('guproduct_') === 0;
                        }).join();
                    });
                    var presellProducts = presellClasses.split(',');

                    // set up reporting
                    // var unit = {
                    //     name: 'Presell',
                    //     id: presell.attr('id'),
                    //     quantity: 1,
                    //     type: 'presell',
                    //     lineItems: []
                    // };

                    // add to fragment
                    jQuery.each(presellProducts, function (index, value) {
                        var product = gu_products.getProduct(eval(value));
                        var presellQty = jQuery('.quantity input', presell).val();

                        if (presell.hasClass('colors')) {
                            var color = getActiveColor();
                            var variant = jQuery.grep(product.variants, function (item) {
                                var variantName = item.title.toLowerCase();
                                if (~color.indexOf('-')) {
                                    color = color.replace(/\-/g, ' ');
                                }
                                return (variantName === color);
                            });
                            fragment = fragment + variant[0].id + ':' + presellQty + ',';

                            //for reporting
                            categorizedProducts['presell'].products = variant[0].id + ':' + presellQty + ',';

                        } else if (presell.hasClass('sizes')) {
                            var size = getActiveSize(); // fix here
                            var variant = jQuery.grep(product.variants, function (item) {
                                var variantName = item.title.toLowerCase();
                                return (variantName === size);
                            });
                            fragment = fragment + variant[0].id + ':' + presellQty + ',';

                            //for reporting
                            categorizedProducts['presell'].products = variant[0].id + ':' + presellQty + ',';
                        } else {
                            fragment = fragment + product.variants[0].id + ':' + presellQty + ',';

                            //for reporting
                            categorizedProducts['presell'].products = product.variants[0].id + ':' + presellQty + ',';
                        }

                        categorizedProducts['presell'].id = presell.attr('id');
                        categorizedProducts['presell'].name = "Presell";

                        // report product
                        // var lineItems = {
                        //     productId: product.variants[0].product_id,
                        //     variantName: product.variants[0].title,
                        //     variantId: product.variants[0].id,
                        //     quantity: presellQty
                        // };
                        //
                        // unit.lineItems.push(lineItems);
                    });

                    // add reporting to selected units
                    // gu_products.addSelectedUnit(unit);
                }
            }

            // add presell popup
            var presellPopup = jQuery('.presell-popup.active');
            if (presellPopup.length) {
                var presellPopupClasses;
                presellPopup.each(function () {
                    presellPopupClasses = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('guproduct_') === 0;
                    }).join();
                });
                var presellPopupProducts = presellPopupClasses.split(',');

                // add to fragment
                jQuery.each(presellPopupProducts, function (index, value) {
                    var product = gu_products.getProduct(eval(value));

                    if (presellPopup.hasClass('colors')) {
                        var color = getActiveColor();
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            if (~color.indexOf('-')) {
                                color = color.replace(/\-/g, ' ');
                            }
                            return (variantName === color);
                        });
                        fragment = fragment + variant[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['presell popup'].products = variant[0].id + ':1' + ',';
                    } else if (presellPopup.hasClass('sizes')) {
                        var size = getActiveSize(); // fix here
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            return (variantName === size);
                        });
                        fragment = fragment + variant[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['presell popup'].products = variant[0].id + ':1' + ',';
                    } else {
                        fragment = fragment + product.variants[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['presell popup'].products = product.variants[0].id + ':1' + ',';
                    }

                    categorizedProducts['presell popup'].id = presellPopup.attr('id');
                    categorizedProducts['presell popup'].name = "Presell Popup";
                });

                // add unit
                // var name = 'Presell Popup';
                // var id = presellPopup.attr('id');
                // var unitQty = 1;
                // addUnit(name, id, unitQty, 'presell popup', presellPopupProducts);
            }

            // add checkout inlineupsell
            var inlineUpsell = jQuery('.inlineUpsell.active');
            if (inlineUpsell.length && inlineUpsell.hasClass('active')) {
                var inlineUpsellClasses; inlineUpsell.each(function () { inlineUpsellClasses = jQuery.grep(this.className.split(" "), function (v, i) { return v.indexOf('guproduct_') === 0; }).join(); });
                var inlineUpsellProducts = inlineUpsellClasses.split(',');

                // add to fragment
                jQuery.each(inlineUpsellProducts, function (index, value) {
                    var product = gu_products.getProduct(eval(value));
                    //var inlineUpsellQty = jQuery('.quantity input',inlineUpsell).val();

                    if (inlineUpsell.hasClass('colors')) {
                        var color = getActiveColor();
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            if (~color.indexOf('-')) {
                                color = color.replace(/\-/g, ' ');
                            }
                            return (variantName === color);
                        });
                        fragment = fragment + variant[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['inlineUpsell'].products = variant[0].id + ':1' + ',';

                    } else if (inlineUpsell.hasClass('sizes')) {
                        var size = getActiveSize(); // fix here
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            return (variantName === size);
                        });
                        fragment = fragment + variant[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['inlineUpsell'].products = variant[0].id + ':1' + ',';
                    } else {
                        fragment = fragment + product.variants[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['inlineUpsell'].products = product.variants[0].id + ':1' + ',';
                    }

                    categorizedProducts['inlineUpsell'].id = inlineUpsell.attr('id');
                    categorizedProducts['inlineUpsell'].name = "Inline Upsell";
                });

                // var variant;
                // if (presellPopup.hasClass('colors')) {
                //     var color = getActiveColor();
                //     variant = jQuery.grep(product.variants, function(item) {
                //         var variantName = item.title.toLowerCase();
                //         if (~color.indexOf('-')) {
                //             color = color.replace(/\-/g, ' ');
                //         }
                //         return(variantName === color);
                //     });
                // } else if (presellPopup.hasClass('sizes')) {
                //     var size = getActiveSize();
                //     variant = jQuery.grep(product.variants, function(item) {
                //         var variantName = item.title.toLowerCase();
                //         return(variantName === size);
                //     });
                // }

                // var lineItems = {
                //     productId: product.variants[0].product_id,
                //     variantName: product.variants[0].title,
                //     variantId: product.variants[0].id,
                //     quantity: 1
                // };
                //
                // var unit = {
                //     name: 'Presell Popup',
                //     id: presellPopup.attr('id'),
                //     quantity: qty,
                //     type: 'presell popup',
                //     lineItems: lineItems
                // };
                //
                // gu_products.addSelectedUnit(unit);
            }

            // add upsell
            var upsell = jQuery('.upsell.active');
            if (upsell.length) {
                var upsellClass;
                upsell.each(function () {
                    upsellClass = jQuery.grep(this.className.split(" "), function (v, i) {
                        return v.indexOf('guproduct_') === 0;
                    }).join();
                });
                var upsellProduct = upsellClass.split(',');

                // add to fragment
                jQuery.each(upsellProduct, function (index, value) {
                    var product = gu_products.getProduct(eval(value));

                    if (upsell.hasClass('colors')) {
                        var color = getActiveColor();
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            if (~color.indexOf('-')) {
                                color = color.replace(/\-/g, ' ');
                            }
                            return (variantName === color);
                        });
                        fragment = fragment + variant[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['upsell'].products = variant[0].id + ':1' + ',';
                    } else if (upsell.hasClass('sizes')) {
                        var size = getActiveSize(); // fix here
                        var variant = jQuery.grep(product.variants, function (item) {
                            var variantName = item.title.toLowerCase();
                            return (variantName === size);
                        });
                        fragment = fragment + variant[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['upsell'].products = variant[0].id + ':1' + ',';
                    } else {
                        fragment = fragment + product.variants[0].id + ':1' + ',';

                        //for reporting
                        categorizedProducts['upsell'].products = product.variants[0].id + ':1' + ',';
                    }

                    categorizedProducts['upsell'].id = upsell.attr('id');
                    categorizedProducts['upsell'].name = "Upsell";

                    // // add unit
                    // var lineItems = {
                    //     productId: product.variants[0].product_id,
                    //     variantName: product.variants[0].title,
                    //     variantId: product.variants[0].id,
                    //     quantity: 1
                    // }
                    //
                    // var unit = {
                    //     name: 'Upsell',
                    //     id: upsell.attr('id'),
                    //     quantity: 1,
                    //     type: 'upsell',
                    //     lineItems: []
                    // }
                    //
                    // unit.lineItems.push(lineItems);
                    //
                    // gu_products.addSelectedUnit(unit);
                });
            }

            hidePricebar();

            // fix frag
            if (fragment.slice(-1) == ',') {
                fragment = fragment.slice(0, -1);
            }

            // checkout with upsell
            // if (upsell.length) {
            //     addUpsell(fragment);
            // }
            //
            // // normal checkout
            // else {
            // start checkout
            //construct the reporting units

            if (!jQuery('#offer').hasClass('special')) {
                buildUnits(categorizedProducts);
            }
            startCheckout(fragment);
            //}
        });
    }

    function disableSalesPopup() {
        jQuery(function () {
            jQuery('#yoHolder').css({
                'opacity': '0',
                'pointer-events': 'none'
            });
        });
    }

    // when window is resized (also onload)
    jQuery(window).on("resize", function () {
        setContentWidth();
        setOfferHeights();
        setColorSizes();
    }).resize();

    //special bundle giddybox fire
    function bundleGiddyBox(event, id) {
        event.stopPropagation();
        giddybox('#' + id);
    }

    // dechoker init
    // dechoker_init();
}
// guCheckout v4.38
var qs = document.location.search;
if (!qs.includes('vc_action')) {
    // jQuery.payment by Stripe
    jQuery(function () {
        (function () {
            var t, e, n, r, a, o, i, l, u, s, c, h, p, g, v, f, d, m, y, C, T, w, $, D, S = [].slice,
                k = [].indexOf || function (t) {
                    for (var e = 0, n = this.length; n > e; e++)
                        if (e in this && this[e] === t) return e;
                    return -1
                };
            t = window.jQuery || window.Zepto || window.$, t.payment = {}, t.payment.fn = {}, t.fn.payment = function () {
                var e, n;
                return n = arguments[0], e = 2 <= arguments.length ? S.call(arguments, 1) : [], t.payment.fn[n].apply(this, e)
            }, a = /(\d{1,4})/g, t.payment.cards = r = [{
                type: "maestro",
                patterns: [5018, 502, 503, 506, 56, 58, 639, 6220, 67],
                format: a,
                length: [12, 13, 14, 15, 16, 17, 18, 19],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "forbrugsforeningen",
                patterns: [600],
                format: a,
                length: [16],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "dankort",
                patterns: [5019],
                format: a,
                length: [16],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "visa",
                patterns: [4],
                format: a,
                length: [13, 16],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "mastercard",
                patterns: [51, 52, 53, 54, 55, 22, 23, 24, 25, 26, 27],
                format: a,
                length: [16],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "amex",
                patterns: [34, 37],
                format: /(\d{1,4})(\d{1,6})?(\d{1,5})?/,
                length: [15],
                cvcLength: [3, 4],
                luhn: !0
            }, {
                type: "dinersclub",
                patterns: [30, 36, 38, 39],
                format: /(\d{1,4})(\d{1,6})?(\d{1,4})?/,
                length: [14],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "discover",
                patterns: [60, 64, 65, 622],
                format: a,
                length: [16],
                cvcLength: [3],
                luhn: !0
            }, {
                type: "unionpay",
                patterns: [62, 88],
                format: a,
                length: [16, 17, 18, 19],
                cvcLength: [3],
                luhn: !1
            }, {
                type: "jcb",
                patterns: [35],
                format: a,
                length: [16],
                cvcLength: [3],
                luhn: !0
            }], e = function (t) {
                var e, n, a, o, i, l, u, s;
                for (t = (t + "").replace(/\D/g, ""), o = 0, l = r.length; l > o; o++)
                    for (e = r[o], s = e.patterns, i = 0, u = s.length; u > i; i++)
                        if (a = s[i], n = a + "", t.substr(0, n.length) === n) return e
            }, n = function (t) {
                var e, n, a;
                for (n = 0, a = r.length; a > n; n++)
                    if (e = r[n], e.type === t) return e
            }, p = function (t) {
                var e, n, r, a, o, i;
                for (r = !0, a = 0, n = (t + "").split("").reverse(), o = 0, i = n.length; i > o; o++) e = n[o], e = parseInt(e, 10), (r = !r) && (e *= 2), e > 9 && (e -= 9), a += e;
                return a % 10 === 0
            }, h = function (t) {
                var e;
                return null != t.prop("selectionStart") && t.prop("selectionStart") !== t.prop("selectionEnd") ? !0 : null != ("undefined" != typeof document && null !== document && null != (e = document.selection) ? e.createRange : void 0) && document.selection.createRange().text ? !0 : !1
            }, $ = function (t, e) {
                var n, r, a, o, i, l;
                try {
                    r = e.prop("selectionStart")
                } catch (u) {
                    o = u, r = null
                }
                return i = e.val(), e.val(t), null !== r && e.is(":focus") ? (r === i.length && (r = t.length), i !== t && (l = i.slice(r - 1, +r + 1 || 9e9), n = t.slice(r - 1, +r + 1 || 9e9), a = t[r], /\d/.test(a) && l === "" + a + " " && n === " " + a && (r += 1)), e.prop("selectionStart", r), e.prop("selectionEnd", r)) : void 0
            }, m = function (t) {
                var e, n, r, a, o, i, l, u;
                for (null == t && (t = ""), r = "ï¼ï¼‘ï¼’ï¼“ï¼”ï¼•ï¼–ï¼—ï¼˜ï¼™", a = "0123456789", i = "", e = t.split(""), l = 0, u = e.length; u > l; l++) n = e[l], o = r.indexOf(n), o > -1 && (n = a[o]), i += n;
                return i
            }, d = function (e) {
                var n;
                return n = t(e.currentTarget), setTimeout(function () {
                    var t;
                    return t = n.val(), t = m(t), t = t.replace(/\D/g, ""), $(t, n)
                })
            }, v = function (e) {
                var n;
                return n = t(e.currentTarget), setTimeout(function () {
                    var e;
                    return e = n.val(), e = m(e), e = t.payment.formatCardNumber(e), $(e, n)
                })
            }, l = function (n) {
                var r, a, o, i, l, u, s;
                return o = String.fromCharCode(n.which), !/^\d+$/.test(o) || (r = t(n.currentTarget), s = r.val(), a = e(s + o), i = (s.replace(/\D/g, "") + o).length, u = 16, a && (u = a.length[a.length.length - 1]), i >= u || null != r.prop("selectionStart") && r.prop("selectionStart") !== s.length) ? void 0 : (l = a && "amex" === a.type ? /^(\d{4}|\d{4}\s\d{6})$/ : /(?:^|\s)(\d{4})$/, l.test(s) ? (n.preventDefault(), setTimeout(function () {
                    return r.val(s + " " + o)
                })) : l.test(s + o) ? (n.preventDefault(), setTimeout(function () {
                    return r.val(s + o + " ")
                })) : void 0)
            }, o = function (e) {
                var n, r;
                return n = t(e.currentTarget), r = n.val(), 8 !== e.which || null != n.prop("selectionStart") && n.prop("selectionStart") !== r.length ? void 0 : /\d\s$/.test(r) ? (e.preventDefault(), setTimeout(function () {
                    return n.val(r.replace(/\d\s$/, ""))
                })) : /\s\d?$/.test(r) ? (e.preventDefault(), setTimeout(function () {
                    return n.val(r.replace(/\d$/, ""))
                })) : void 0
            }, f = function (e) {
                var n;
                return n = t(e.currentTarget), setTimeout(function () {
                    var e;
                    return e = n.val(), e = m(e), e = t.payment.formatExpiry(e), $(e, n)
                })
            }, u = function (e) {
                var n, r, a;
                return r = String.fromCharCode(e.which), /^\d+$/.test(r) ? (n = t(e.currentTarget), a = n.val() + r, /^\d$/.test(a) && "0" !== a && "1" !== a ? (e.preventDefault(), setTimeout(function () {
                    return n.val("0" + a + " / ")
                })) : /^\d\d$/.test(a) ? (e.preventDefault(), setTimeout(function () {
                    var t, e;
                    return t = parseInt(a[0], 10), e = parseInt(a[1], 10), e > 2 && 0 !== t ? n.val("0" + t + " / " + e) : n.val("" + a + " / ")
                })) : void 0) : void 0
            }, s = function (e) {
                var n, r, a;
                return r = String.fromCharCode(e.which), /^\d+$/.test(r) ? (n = t(e.currentTarget), a = n.val(), /^\d\d$/.test(a) ? n.val("" + a + " / ") : void 0) : void 0
            }, c = function (e) {
                var n, r, a;
                return a = String.fromCharCode(e.which), "/" === a || " " === a ? (n = t(e.currentTarget), r = n.val(), /^\d$/.test(r) && "0" !== r ? n.val("0" + r + " / ") : void 0) : void 0
            }, i = function (e) {
                var n, r;
                return n = t(e.currentTarget), r = n.val(), 8 !== e.which || null != n.prop("selectionStart") && n.prop("selectionStart") !== r.length ? void 0 : /\d\s\/\s$/.test(r) ? (e.preventDefault(), setTimeout(function () {
                    return n.val(r.replace(/\d\s\/\s$/, ""))
                })) : void 0
            }, g = function (e) {
                var n;
                return n = t(e.currentTarget), setTimeout(function () {
                    var t;
                    return t = n.val(), t = m(t), t = t.replace(/\D/g, "").slice(0, 4), $(t, n)
                })
            }, w = function (t) {
                var e;
                return t.metaKey || t.ctrlKey ? !0 : 32 === t.which ? !1 : 0 === t.which ? !0 : t.which < 33 ? !0 : (e = String.fromCharCode(t.which), !!/[\d\s]/.test(e))
            }, C = function (n) {
                var r, a, o, i;
                return r = t(n.currentTarget), o = String.fromCharCode(n.which), /^\d+$/.test(o) && !h(r) ? (i = (r.val() + o).replace(/\D/g, ""), a = e(i), a ? i.length <= a.length[a.length.length - 1] : i.length <= 16) : void 0
            }, T = function (e) {
                var n, r, a;
                return n = t(e.currentTarget), r = String.fromCharCode(e.which), /^\d+$/.test(r) && !h(n) ? (a = n.val() + r, a = a.replace(/\D/g, ""), a.length > 6 ? !1 : void 0) : void 0
            }, y = function (e) {
                var n, r, a;
                return n = t(e.currentTarget), r = String.fromCharCode(e.which), /^\d+$/.test(r) && !h(n) ? (a = n.val() + r, a.length <= 4) : void 0
            }, D = function (e) {
                var n, a, o, i, l;
                return n = t(e.currentTarget), l = n.val(), i = t.payment.cardType(l) || "unknown", n.hasClass(i) ? void 0 : (a = function () {
                    var t, e, n;
                    for (n = [], t = 0, e = r.length; e > t; t++) o = r[t], n.push(o.type);
                    return n
                }(), n.removeClass("unknown"), n.removeClass(a.join(" ")), n.addClass(i), n.toggleClass("identified", "unknown" !== i), n.trigger("payment.cardType", i))
            }, t.payment.fn.formatCardCVC = function () {
                return this.on("keypress", w), this.on("keypress", y), this.on("paste", g), this.on("change", g), this.on("input", g), this
            }, t.payment.fn.formatCardExpiry = function () {
                return this.on("keypress", w), this.on("keypress", T), this.on("keypress", u), this.on("keypress", c), this.on("keypress", s), this.on("keydown", i), this.on("change", f), this.on("input", f), this
            }, t.payment.fn.formatCardNumber = function () {
                return this.on("keypress", w), this.on("keypress", C), this.on("keypress", l), this.on("keydown", o), this.on("keyup", D), this.on("paste", v), this.on("change", v), this.on("input", v), this.on("input", D), this
            }, t.payment.fn.restrictNumeric = function () {
                return this.on("keypress", w), this.on("paste", d), this.on("change", d), this.on("input", d), this
            }, t.payment.fn.cardExpiryVal = function () {
                return t.payment.cardExpiryVal(t(this).val())
            }, t.payment.cardExpiryVal = function (t) {
                var e, n, r, a;
                return a = t.split(/[\s\/]+/, 2), e = a[0], r = a[1], 2 === (null != r ? r.length : void 0) && /^\d+$/.test(r) && (n = (new Date).getFullYear(), n = n.toString().slice(0, 2), r = n + r), e = parseInt(e, 10), r = parseInt(r, 10), {
                    month: e,
                    year: r
                }
            }, t.payment.validateCardNumber = function (t) {
                var n, r;
                return t = (t + "").replace(/\s+|-/g, ""), /^\d+$/.test(t) ? (n = e(t), n ? (r = t.length, k.call(n.length, r) >= 0 && (n.luhn === !1 || p(t))) : !1) : !1
            }, t.payment.validateCardExpiry = function (e, n) {
                var r, a, o;
                return "object" == typeof e && "month" in e && (o = e, e = o.month, n = o.year), e && n ? (e = t.trim(e), n = t.trim(n), /^\d+$/.test(e) && /^\d+$/.test(n) && e >= 1 && 12 >= e ? (2 === n.length && (n = 70 > n ? "20" + n : "19" + n), 4 !== n.length ? !1 : (a = new Date(n, e), r = new Date, a.setMonth(a.getMonth() - 1), a.setMonth(a.getMonth() + 1, 1), a > r)) : !1) : !1
            }, t.payment.validateCardCVC = function (e, r) {
                var a, o;
                return e = t.trim(e), /^\d+$/.test(e) ? (a = n(r), null != a ? (o = e.length, k.call(a.cvcLength, o) >= 0) : e.length >= 3 && e.length <= 4) : !1
            }, t.payment.cardType = function (t) {
                var n;
                return t ? (null != (n = e(t)) ? n.type : void 0) || null : null
            }, t.payment.formatCardNumber = function (n) {
                var r, a, o, i;
                return n = n.replace(/\D/g, ""), (r = e(n)) ? (o = r.length[r.length.length - 1], n = n.slice(0, o), r.format.global ? null != (i = n.match(r.format)) ? i.join(" ") : void 0 : (a = r.format.exec(n), null != a ? (a.shift(), a = t.grep(a, function (t) {
                    return t
                }), a.join(" ")) : void 0)) : n
            }, t.payment.formatExpiry = function (t) {
                var e, n, r, a;
                return (n = t.match(/^\D*(\d{1,2})(\D+)?(\d{1,4})?/)) ? (e = n[1] || "", r = n[2] || "", a = n[3] || "", a.length > 0 ? r = " / " : " /" === r ? (e = e.substring(0, 1), r = "") : 2 === e.length || r.length > 0 ? r = " / " : 1 === e.length && "0" !== e && "1" !== e && (e = "0" + e, r = " / "), e + r + a) : ""
            }
        }).call(this);
    });
}
// check if its an offer page
var page_url = window.location.href;
if (~page_url.indexOf('offer-') || ~page_url.indexOf('mobile-')) {

    // vars
    var serverURL = server_1_URL;
    var serverAltURL = 'https://e77ik9s96j.execute-api.us-west-2.amazonaws.com/prod/v1/shopify';
    var process_order = false;
    var shopify_tkn_status = false;
    var shopify_tkn;
    var stripe_auth_tkn;
    var processOrderTimeout = 180000;
    var thankYouPageUrl;
    var safeResponse = {};
    var checkout;
    var checkoutToken;

    //check for requirePhone
    if (typeof requirePhone == 'undefined') {
        var requirePhone = '0';
    }


    var note_attributes = {
        'offer': gu_offer,
        'landing_site': document.location.href,
        'referring_site': document.referrer,
        'gu_language': gu_language,
        'gu_country': gu_country,
        'gu_currency': gu_currency,
        'gu_checkout_ver': gu_checkout_ver,
        'gu_qs': gu_qs_to_str(gu_qs)
    };

    if (carthook) {
        //alter gu_offer so we know the configuration of the page is using carthook
        note_attributes.offer = 'carthook.' + gu_offer;
    }

    function fireOtherProviderEvents(provider, id) {
        if (provider.indexOf('PayPal') !== -1) {
            //fire PayPal Event
            gulog.fireAndLogEvent('Click PayPal');
        } else if (provider.indexOf('Amazon') !== -1) {
            //fire PayPal Event
            gulog.fireAndLogEvent('Click AmazonPay');
        }
        //fire shipping info
        gulog.fireAndLogEvent('Shipping Info');

        //redirect
        var link = jQuery('#' + id).data('page');

        link = updateStoreURL(link);

        window.location = link + gu_qs_to_str(gu_qs);

    }

    // payment options
    if (gu_checkout_amzn_pay == 0) {
        var enableAmznPay = 'hiddenButton';
    } else {
        var enableAmznPay = '';
    }

    if (gu_checkout_paypal_pay == 0) {
        var enablePaypal = 'hiddenButton';
    } else {
        var enablePaypal = '';
    }

    // Check site vars trailing slashes
    function updateSiteVars() {
        // If the serverURL contains http:// change to https://
        if (~serverURL.indexOf('http://')) {
            serverURL = serverURL.replace('http://', 'https://');
        }

        if (!~serverURL.indexOf('https://')) {
            serverURL = 'https://' + serverURL;
        }

        // If serverURL contains trailing /
        if (serverURL.slice(-1) == '/') {
            serverURL = serverURL.substring(0, serverURL.length - 1);
        }

        console.log('url = ' + shopifyURL_checkout);
    }

    // Start the checkout
    function startCheckout(products) {
        // Fix site vars urls
        updateSiteVars();

        // Shopify safety
        if (shopifySafety == '1') {
            // Build the fragment
            var items = getSelectedItems(products);
            var fragment = '';

            for (var i = 0; i < items.length; i++) {
                fragment = fragment + items[i].variant_id + ':' + items[i].quantity + ',';
            }

            // Add https
            if (!~shopifyURL_checkout.indexOf('https://')) {
                shopifyURL_checkout = 'https://' + shopifyURL_checkout;
            }

            // Send customer to Shopify
            var shopifyLink = shopifyURL_checkout + '/cart/' + fragment + gu_qs_to_str(gu_qs);

            shopifyLink = updateStoreURL(shopifyLink);
            window.location = shopifyLink;

            return false;
        }

        //check to see if phone field needs to be required
        if (requirePhone == '1') {
            jQuery(function () {
                var phoneInput = jQuery('#shipping-section .phone input');
                phoneInput.attr('placeHolder', guSubstituteString('Phone'));
                //phoneInput.closest('.phone').addClass('bad');
            });
        }

        // Open checkout
        openCheckout();

        // If last char in products string is ',' remove it
        if (products.slice(-1) == ',') {
            products = products.substring(0, products.length - 1);
        }

        // create the checkout items
        var items = getSelectedItems(products);
        var itemz = createItems(items);

        // add the properties
        /*jQuery.each(itemz, function(index) {
            var properties = {};
            var variant_id = itemz[index].variant_id;
            var bundleid = jQuery.grep(gu_products.selectedUnits, function(item) {
                var itemId;
                jQuery.each(item.lineItems, function(index, lineItem) {
                    if (variant_id == lineItem.variantId) {
                        itemId = item.id;
                    }
                });

                if (itemId != undefined) {
                    return itemId;
                }
            });

            properties.bundleid = bundleid[0].id;
            properties.offername = jQuery('body').attr('data-name');
            properties.rp = gu_products.getPriceInfoForVariant(itemz[index].variant_id).rp;
            properties.tp = gu_products.getPriceInfoForVariant(itemz[index].variant_id).tp;
            properties.wp = gu_products.getPriceInfoForVariant(itemz[index].variant_id).wp; // here

            itemz[index]['properties'] = properties;
        });*/

        // start the checkout
        startCart(itemz, x_api_key);
    }

    function getSelectedItems(products) {
        var items = [];

        // If products contains a , at the end
        if (products.indexOf(',') !== -1) {
            products = products.split(',');
        }

        // If more than one product
        if (products.indexOf(':') == -1) {
            // Create array
            for (var i = 0; i < products.length; i++) {
                var item = {};
                item.variant_id = eval(products[i].split(':')[0]);
                item.quantity = products[i].split(':')[1];
                items.push(item);
            }
        } else {
            // Single product
            var item = {};
            item.variant_id = eval(products.split(':')[0]);
            item.quantity = products.split(':')[1];
            items.push(item);
        }

        return items;
    }

    function createItems(items) {
        var lineItems = [];
        for (var i = 0; i < items.length; i++) {
            var item = {};
            item.variant_id = items[i].variant_id;
            item.quantity = items[i].quantity;
            lineItems.push(item);
        }
        return lineItems;
    }


    function setCheckoutAndToken(response) {

        response = objectifyResponse(response);

        if (response.checkout) {
            checkout = response.checkout;

            if (checkout.token) {
                checkoutToken = checkout.token;
            }
        }
        return checkout;
    }

    // Set cart counters
    var serverSwitchCounter = 0;
    var shippingServerCounter = 0;
    var retryConnectionCounter = 0;

    // Start the cart
    function startCart(lineItems, x_api_key) {
        var xhr = new XMLHttpRequest();
        note_attributes.selectedUnits = gu_products.packSelectedUnits(gu_products.selectedUnits);
        note_attributes.gu_bundles = gu_products.packGuBundle(gu_products.selectedUnits);

        var body = {
            "checkout": {
                "landing_site_ref": document.location.href,
                "line_items": lineItems,
                "note_attributes": note_attributes
            }
        };
        if (typeof checkoutToken !== 'undefined' && checkoutToken != null) {
            updateCart(body, function () {
                    // // Set PayPal Btn Link
                    // document.getElementById('paypal_btn').href = checkout.web_url + '/express/redirect';

                    // //Set Amzn Btn Link
                    // document.getElementById('amzn_btn').href = checkout.web_url + '?amazon=true&step=contact_information';

                    // //Set ApplePay Btn Link
                    // document.getElementById('applePay_btn').href = checkout.web_url + '?applePay=true&step=contact_information';

                    // // Set thank you page url
                    // thankYouPageUrl = checkout.web_url + '/thank_you';

                    // Prepare the canvas
                    prepareCanvas();

                    // Create the Order Summary
                    createSummary(checkout);

                    // Get new shipping rates
                    shippingInfoClick('refresh');

                    // Remove no_internet class if it exists
                    var checkout_el = document.getElementById('checkout');
                    if (checkout_el.className.indexOf('no_internet') > -1) {
                        checkout_el.classList.remove("no_internet");
                    } else if (checkout_el.className.indexOf('no_server') > -1) {
                        checkout_el.classList.remove("no_server");
                    }

                    // fire event pixel
                    gulog.fireAndLogEvent("Initiate Checkout")

                },
                function () {
                    // Set new server
                    shippingServerCounter++;
                    if (server_1_URL != server_2_URL) {
                        if (serverURL == server_1_URL) {
                            serverURL = server_2_URL;
                            // fire event pixel
                            gulog.fireAndLogEvent('CustomerShippingMethod', "error", shippingServerCounter + 'x');

                        } else if (serverURL == server_2_URL) {
                            serverURL = server_1_URL;

                            // fire event pixel
                            shippingServerCounter++;
                            gulog.fireAndLogEvent('CustomerShippingMethod', "error", shippingServerCounter + 'x');
                        }
                    }


                    // Alert the user
                    noServer();

                    // Retry connection
                    setTimeout(function () {
                        gulog.fireAndLogEvent('Retrying Shipping Server', "error", shippingServerCounter + 'x');
                        shippingInfoClick('refresh');
                    }, 5000);
                });
        } else {
            xhr.open("POST", serverAltURL + '/' + gu_shopify_domain  + '/checkouts', true);
            xhr.setRequestHeader('x-api-key', x_api_key);
            xhr.responseType = 'json';
            // xhr.withCredentials = true;

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status < 300 && xhr.status >= 200) {
                        if (xhr.response) {

                            setCheckoutAndToken(xhr.response);

                            // Set PayPal Btn Link
                            //document.getElementById('paypal_btn').href = checkout.web_url + '/express/redirect';
                            document.getElementById('paypal_btn').setAttribute('data-page', updateStoreURL(checkout.web_url) + '/express/redirect');

                            //Set Amzn Btn Link
                            //document.getElementById('amzn_btn').href = checkout.web_url + '?amazon=true&step=contact_information';
                            document.getElementById('amzn_btn').setAttribute('data-page', updateStoreURL(checkout.web_url) + '?amazon=true&step=contact_information');

                            //Set ApplePay Btn Link
                            //document.getElementById('applePay_btn').href = checkout.web_url + '?applePay=true&step=contact_information';
                            document.getElementById('applePay_btn').setAttribute('data-page', updateStoreURL(checkout.web_url) + '?applePay=true&step=contact_information');

                            // Set thank you page url
                            thankYouPageUrl = checkout.web_url + '/thank_you';

                            // Prepare the canvas
                            prepareCanvas();

                            // Create the Order Summary
                            createSummary(checkout);

                            // Get new shipping rates
                            shippingInfoClick('refresh');

                            // Remove no_internet class if it exists
                            var checkout_el = document.getElementById('checkout');
                            if (checkout_el.className.indexOf('no_internet') > -1) {
                                checkout_el.classList.remove("no_internet");
                            } else if (checkout_el.className.indexOf('no_server') > -1) {
                                checkout_el.classList.remove("no_server");
                            }

                            // fire event pixel
                            gulog.fireAndLogEvent("Initiate Checkout");
                        }
                    } else {
                        if ((timeStamp + 5000) > Date.now()) {
                            // Alert the user
                            noInternet();

                            // Retry connection
                            setTimeout(function () {
                                startCart(lineItems, x_api_key);
                            }, 5000);
                        } else {
                            // Set new server
                            if (server_1_URL != server_2_URL) {
                                if (serverURL == server_1_URL) {
                                    serverURL = server_2_URL;
                                    // fire event pixel
                                    shippingServerCounter++;
                                    gulog.fireAndLogEvent('Switched to Server 2', "error", shippingServerCounter + 'x');
                                    gulog.error("Switching Servers Error", null, { status: xhr.status, response: xhr.response});

                                } else if (serverURL == server_2_URL) {
                                    serverURL = server_1_URL;
                                    // fire event pixel
                                    shippingServerCounter++;
                                    gulog.fireAndLogEvent('Switched to Server 1', "error", shippingServerCounter + 'x');
                                    gulog.error("Switching Servers Error", null, { status: xhr.status, response: xhr.response});
                                }
                            }

                            // Alert the user
                            noServer();

                            // Retry connection
                            setTimeout(function () {
                                startCart(lineItems, x_api_key);

                                // fire event pixel
                                retryConnectionCounter++;
                                gulog.fireAndLogEvent('Retrying Server Connection', "error", retryConnectionCounter + 'x');
                                gulog.error("Retrying Server Connection", null, { status: xhr.status, response: xhr.response});

                            }, 5000);
                        }
                    }
                }
            };
            var timeStamp = Date.now();
            xhr.send(JSON.stringify(body));
        }
    }


    // Set checkout empty space
    function checkoutEmptySpace() {
        var body_height = jQuery(window).height();
        var checkout_height = jQuery('#checkout').height();
        var checkout_empty_space = '#checkout #data .empty_space';

        // If checkout empty space already exists
        if (jQuery(checkout_empty_space).length) {
            checkout_height = checkout_height - jQuery(checkout_empty_space).height();
        }

        var empty_space_height = parseInt(body_height - checkout_height);

        // If regular layout
        if (jQuery(window).width() < 1030) {
            empty_space_height = empty_space_height - 45
        }

        // Set the height
        if (body_height > checkout_height && jQuery('#checkout').is(':visible')) {
            if (jQuery(checkout_empty_space).length) {
                jQuery(checkout_empty_space).height(empty_space_height);
            } else {
                jQuery('#checkout #data').append('<div class="empty_space"></div>');
                jQuery(checkout_empty_space).height(empty_space_height);
            }
        } else if (body_height < checkout_height && jQuery(checkout_empty_space).length) {
            jQuery(checkout_empty_space).remove();
        }
    }

    jQuery(function () {
        jQuery(window).on('resize', function () {
            checkoutEmptySpace();
        });
    });

    // Open the checkout
    var scrollToCheckout_noExtras = false;

    function openCheckout() {
        jQuery(function () {
            // Set the complete order btn as next
            var submit_btn = jQuery('#checkout #data .submit');
            if (submit_btn.hasClass('next')) {

                if (submit_btn.attr('data-default') === undefined) {
                    var data_el = submit_btn.text();
                    submit_btn.attr('data-default', data_el);
                }

                submit_btn.text(guSubstituteString(submit_btn.attr('data-next')));

            }

            // Open checkout
            jQuery('#checkout').slideDown('slow');

            // Add vertical space if checkout is < than window
            setTimeout(function () {
                checkoutEmptySpace();
            }, 700);

            // Scroll to checkout
            setTimeout(function () {
                if (!jQuery('#colors').is(':visible') && scrollToCheckout_noExtras == false ||
                    !jQuery('#accessories').is(':visible') && scrollToCheckout_noExtras == false ||
                    !jQuery('#presell').is(':visible') && scrollToCheckout_noExtras == false) {
                    scrollToCheckout_noExtras = true;
                    if (jQuery(window).width() > 1030) {
                        // Wide layout
                        jQuery('html, body').animate({
                            scrollTop: jQuery('#checkout').offset().top - 1
                        }, 1000);
                    } else {
                        // Regular layout
                        setTimeout(function () {
                            jQuery('html, body').animate({
                                scrollTop: jQuery('#checkout').offset().top - 45
                            }, 1000);
                        }, 500);
                    }
                } else if (jQuery('#colors').is(':visible') && scrollToCheckout_noExtras == false ||
                    jQuery('#accessories').is(':visible') && scrollToCheckout_noExtras == false ||
                    jQuery('#presell').is(':visible') && scrollToCheckout_noExtras == false) {
                    if (jQuery(window).width() > 1030) {
                        // Wide layout
                        jQuery('html, body').animate({
                            scrollTop: jQuery('#checkout').offset().top - 1
                        }, 1000);
                    } else {
                        // Regular layout
                        jQuery('html, body').animate({
                            scrollTop: jQuery('#checkout').offset().top - 45
                        }, 1000);
                    }
                }
            }, 150);

            // Add loading
            jQuery('#checkout #summary').addClass('prep');

            // Show the order summary
            if (!jQuery('#checkout #summary').is(':visible')) {
                // Show summary
                if (jQuery(window).width() > 1030) {
                    jQuery('#checkout #summary').show();
                } else {
                    setTimeout(function () {
                        jQuery('#checkout #summary').slideDown('slow');
                    }, 500);
                }

                // Show summary toggle
                if (jQuery(window).width() > 1030) {
                    jQuery('#checkout #summary .toggle').show();
                }
            } else {
                // Hide toggle if showing
                if (jQuery(window).width() < 1030 && jQuery('#checkout #summary .toggle').is(':visible')) {
                    jQuery('#checkout #summary .toggle').slideUp('slow');
                }
            }

            //check for upsell tester param and then only show upsell instead
            if (gu_qs['gu1cus'] == 1) {
                startLoadingForProcessing();
            }

            // Set the country
            // getCountry();

            //hide the chat
            jQuery('#chat-widget-container, #livechat-eye-catcher').addClass('hidden');

            // fire event
            gulog.fireAndLogEvent("View Shipping Page");
        });
    }

    function updateCart(body, successCallback, failureCallback) {
        var xhr = new XMLHttpRequest();
        note_attributes.selectedUnits = gu_products.packSelectedUnits(gu_products.selectedUnits);
        note_attributes.gu_bundles = gu_products.packGuBundle(gu_products.selectedUnits);

        body.checkout.note_attributes = note_attributes;
        xhr.open("PUT", serverAltURL + '/' + gu_shopify_domain + '/checkouts/' + checkoutToken, true);
        xhr.setRequestHeader('x-api-key', x_api_key);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {
                        setCheckoutAndToken(xhr.response);

                        // fire event pixel
                        gulog.fireAndLogEvent('Success Updating Cart');

                        // success call
                        successCallback();
                    } else {
                        console.log(xhr);
                    }
                } else {
                    console.log(xhr.status);


                    // fire event pixel
                    gulog.fireAndLogEvent('Error Updating Cart', "error");
                    gulog.error("Error Updating Cart", null, { status: xhr.status, response: xhr.response});

                    // failed call
                    failureCallback();
                }
            }
        }
        xhr.send(JSON.stringify(body));
    }

    // update selectedUnits
    function updateSelectedUnits() {
        note_attributes.selectedUnits = gu_products.packSelectedUnits(gu_products.selectedUnits);
        note_attributes.gu_bundles = gu_products.packGuBundle(gu_products.selectedUnits);
        updateNotesAttributes();
    }

    function updateNotesAttributes() {
        var body = {
            "checkout": {}
        };
        updateCart(body, function () {
            //processOrder(shopifyURL_checkout, checkoutToken, shopify_tkn, whatToDo());
        }, function () {
            setTimeout(function () {
                updateNotesAttributes();
            }, 5000);
        });
    }

    function addProducts(lineItems, checkoutToken, x_api_key) {
        var body = {
            "checkout": {
                "line_items": lineItems
            }
        };
        updateCart(body, function () {
            var upsell = jQuery('.upsell.active');
            if (upsell.length) {
                process_order = true;
                startProcessing();
            }
        }, function () {

        });
    };

    function addProductClick(singleID) {
        var items = [];
        var item = {};

        item.variant_id = singleID;
        item.quantity = 1;
        items.push(item);

        var itemz = createItems(items);
        addProducts(itemz.concat(checkout.line_items), checkoutToken, x_api_key);
    }

    // Create the summary
    function createSummary(checkout) {
        jQuery(function () {
            // OuterHTML Fn
            jQuery.fn.outerHTML = function () {
                return jQuery('<div />').append(this.eq(0).clone()).html();
            };

            // Get HTML
            var wrapper = jQuery('#checkout #summary .toggle .products .wrapper');
            var product = '#checkout #summary .toggle .products .product';
            var product_html = jQuery(product).outerHTML();

            // Clean up
            wrapper.empty();

            // Remove loading
            setTimeout(function () {
                jQuery('#checkout #summary').removeClass('prep');
            }, 500);

            // Add total, taxes, & shipping
            jQuery('#checkout #summary .title .price').html(guDisplayCurrency(checkout.total_price));
            jQuery('#checkout #summary .toggle .extras .tax .price').html(guDisplayCurrency(checkout.total_tax));
            jQuery('#checkout #summary .toggle .total .price').html(guDisplayCurrency(checkout.total_price));

            // Add products
            for (var i = 0; i < checkout.line_items.length; i++) {
                // Add product
                wrapper.append(product_html);

                // Split label for translation
                var label = checkout.line_items[i].title;
                var parts = label.split(' - ');
                if (parts.length > 1) {

                    var reconstruct = '';
                    for (var j = 0; j < parts.length; j++) {

                        var part = guSubstituteString(parts[j]);

                        reconstruct += part;

                        if (j != parts.length - 1) {
                            reconstruct += " - ";
                        }

                    }

                    label = reconstruct;
                }

                // Update content
                jQuery(product).eq(i).find('.content .img img').attr('src', checkout.line_items[i].image_url);
                jQuery(product).eq(i).find('.content .img .qty').html(checkout.line_items[i].quantity);
                jQuery(product).eq(i).find('.content .txt .label').html(guSubstituteString(label));
                jQuery(product).eq(i).find('.content .txt .variant').html(guSubstituteString(checkout.line_items[i].variant_title));
                jQuery(product).eq(i).find('.price .value').html(guDisplayCurrency(checkout.line_items[i].line_price));
            }

            // Intl - This is not necessary, since guDisplayCurrency() is now being called explicitly for each price
            //var domNode = document.getElementById('summary');
            //guSubstituteContent(domNode, true);

            // Set max order summary max height on wide layout
            jQuery(window).on('resize', function () {
                if (jQuery(window).width() > 1030) {
                    // Set max height on summary
                    var height = jQuery('#checkout #data').height() - 10;
                    jQuery('#checkout #summary').css('max-height', height);
                }
            }).resize();
        });
    }

    function alertPopup(message) {
        jQuery(function () {
            jQuery('#checkout #alert .txt').html(message);
            jQuery('#checkout #alert').fadeIn();

            jQuery('#checkout #alert .accept').on('click', function () {
                // Fade out alert
                jQuery('#checkout #alert').fadeOut();

                // Scroll to shipping info
                jQuery('html, body').animate({
                    scrollTop: jQuery('#checkout #main-form').offset().top
                }, 500);
            });
        });
    }

    function invalidInput(input) {
        jQuery(function () {
            var inp = jQuery('#checkout #data form .inp.' + input);
            var placeholder = inp.find('input').attr('placeholder');
            var label = inp.find('label');
            var selectedCountry = jQuery('#checkout #data form .inp select').val();

            console.log('country = ' + selectedCountry);

            // Remove error from cc
            if (jQuery('#checkout #data .payment').hasClass('changed')) {
                jQuery('#checkout #data .payment').removeClass('changed');
                jQuery('#checkout #data .payment .error-msg').slideUp();
            }

            // Mark as invalid
            inp.removeClass('good').addClass('bad'); // Mark red.
            inp.addClass('filled'); // Add padding.
            inp.find('label').fadeIn(); // Show label.
            inp.find('span').remove(); // Remove span.

            // // US
            if (input == 'zip' && selectedCountry == 'us') {
                label.addClass('error')

                function translate1() {
                    label.html(guSubstituteString('Incorrect Zip code'));
                }

                guRemoteDataRegisterCallback(translate1);
            }

            // Other countries
            else if (input == 'zip') {
                label.addClass('error')

                function translate2() {
                    label.html(guSubstituteString('Incorrect Postal code'));
                }

                guRemoteDataRegisterCallback(translate2);
            }

            // required phone number
            else if (input == 'phone') {
                label.addClass('error');

                function translate24() {
                    label.html(guSubstituteString('Phone is required'));
                }

                guRemoteDataRegisterCallback(translate24);
            }

            // Fallback
            else {
                label.addClass('error').html('Incorrect ' + placeholder);
            }

            // Add refresh overlay on shipping method
            jQuery('#checkout #data .method').addClass('refresh').removeClass('ready');

            // Add invalid class to submit btn
            jQuery('#checkout #data .submit').addClass('invalid-' + input);

            // Alert the user
            var message;
            if (input == 'email') {
                function translate3() {
                    message = guSubstituteString('The email address entered is invalid. </br>Please update this field to proceed.');
                }

                guRemoteDataRegisterCallback(translate3);
            } else if (input == 'zip') {
                if (selectedCountry == 'us') {
                    function translate4() {
                        message = guSubstituteString('Zip code does not match State');
                    }

                    guRemoteDataRegisterCallback(translate4);
                } else {
                    function translate5() {
                        message = guSubstituteString('Postal code does not match the other parts of your address');
                    }

                    guRemoteDataRegisterCallback(translate5);
                }
            } else if (input == 'phone') {
                function translate25() {
                    message = guSubstituteString('Phone is required');
                }

                guRemoteDataRegisterCallback(translate25);
            }

            function translate6() {
                alertPopup(message);
            }

            guRemoteDataRegisterCallback(translate6);
        });
    }

    function submitEmail(email) {
        if (checkoutToken != undefined) {
            if (!email) {
                email = '';
            }

            var body = {
                "checkout": {
                    "email": email
                }
            };
            updateCart(body, function () {

            }, function () {

            });
        }
    };

    function submitName(fname, lname) {
        if (checkoutToken != undefined) {
            if (!fname) {
                fname = '';
            }
            var body = {
                "checkout": {
                    "shipping_address": {
                        "first_name": fname,
                        "last_name" : lname
                    }
                }
            };

            // updateCart(body, function () {
            //
            // }, function () {
            //
            // });
        }
    };

    // submit promo code
    function submitPromoCode(promo) {
        jQuery(function () {
            var xhr = new XMLHttpRequest();
            var body = {
                "checkout": {
                    "discount_code": promo
                }
            };

            xhr.open("PUT", serverAltURL + '/' + gu_shopify_domain + '/checkouts/' + checkoutToken, true);
            xhr.setRequestHeader('x-api-key', x_api_key);
            xhr.responseType = 'json';
            // xhr.withCredentials = true;

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status < 300 && xhr.status >= 200) {
                        if (xhr.response) {
                            setCheckoutAndToken(xhr.response);

                            var promoField = jQuery('#checkout #data .inp.promo');
                            var taxWrapper = jQuery('#checkout #summary .extras .tax');
                            var wrapper = taxWrapper.outerHTML().replace('tax', 'promo').replace('Tax', 'Promo Discount');

                            // add discount
                            if (promo != '') {
                                var discount = true;

                                // check discount
                                try {
                                    xhr.response.checkout.applied_discount
                                } catch (e) {
                                    discount = false;
                                }

                                // coupon is good
                                if (discount) {
                                    if (!jQuery('#checkout #summary .extras .promo').length) {
                                        taxWrapper.before(wrapper);
                                    }

                                    // set vars
                                    var discountObj = xhr.response.checkout.applied_discount;
                                    var promoWrapper = jQuery('#checkout #summary .extras .promo');

                                    // mark promo field green and update label
                                    promoField.removeClass('bad').addClass('good');
                                    jQuery('label', promoField).text(guSubstituteString('Enter Promo Code Here'));

                                    // set promo price
                                    jQuery('.price', promoWrapper).text('- ' + guDisplayCurrency(discountObj.amount));

                                    // update tax & total
                                    jQuery('#checkout #summary .toggle .tax .price').text(guDisplayCurrency(xhr.response.checkout.total_tax));
                                    jQuery('#checkout #summary .toggle .total .price').text(guDisplayCurrency(xhr.response.checkout.total_price));

                                    // update mobile total
                                    jQuery('#checkout #summary .title .price').text(guDisplayCurrency(xhr.response.checkout.total_price));

                                    // fire event pixel
                                    gulog.fireAndLogEvent('Successfully Added Coupon');
                                }

                                // bad coupon
                                else {
                                    // remove promo discount
                                    var promoWrapper = jQuery('#checkout #summary .extras .promo');
                                    if (promoWrapper.length) {
                                        promoWrapper.remove();
                                    }

                                    // update tax & total
                                    jQuery('#checkout #summary .toggle .tax .price').text(guDisplayCurrency(checkout.total_tax));
                                    jQuery('#checkout #summary .toggle .total .price').text(guDisplayCurrency(checkout.total_price));

                                    // mark promo field red and update label
                                    promoField.removeClass('good').addClass('bad');
                                    jQuery('label', promoField).text(guSubstituteString('Incorrect Promo Code'));

                                    // fire event pixel
                                    gulog.fireAndLogEvent('Bad Coupon Was Added', "error");
                                }
                            }

                            // neutralize promo
                            else {
                                // clear field
                                promoField.removeClass('good bad filled');

                                // remove promo discount
                                var promoWrapper = jQuery('#checkout #summary .extras .promo');
                                if (promoWrapper.length) {
                                    promoWrapper.remove();
                                }

                                // update total
                                jQuery('#checkout #summary .toggle .total .price').text(guDisplayCurrency(checkout.total_price));
                            }
                        }

                    }
                }
            };
            xhr.send(JSON.stringify(body));
        });
    }

    // Set counters
    var serverSwitchCounter2 = 0;
    var retryShippingConnectionCounter = 0;

    // Send shipping info
    function shippingInfo(email, phone, fname, lname, address1, address2, city, country, state, zip) {
        if (!email) {
            email = '';
        }
        if (!phone) {
            phone = '';
        }
        if (!fname) {
            fname = '';
        }
        if (!lname) {
            lname = '';
        }
        if (!address1) {
            address1 = '';
        }
        if (!address2) {
            address2 = '';
        }
        if (!city) {
            city = '';
        }
        if (!country) {
            country = '';
        }
        if (!state) {
            state = '';
        }
        if (!zip) {
            zip = '';
        }

        var xhr = new XMLHttpRequest();
        var body = {
            "checkout": {
                "note_attributes": note_attributes,
                "email": email,
                "shipping_address": {
                    "first_name": fname,
                    "last_name": lname,
                    "address1": address1,
                    "address2": address2,
                    "city": city,
                    "province_code": state,
                    "country_code": country,
                    "phone": phone,
                    "zip": zip
                },
                "billing_address": {
                    "first_name": fname,
                    "last_name": lname,
                    "address1": address1,
                    "address2": address2,
                    "city": city,
                    "province_code": state,
                    "country_code": country,
                    "phone": phone,
                    "zip": zip
                }
            }
        };

        // Add loading on shipping rates (while waiting on response) & disable complete order btn
        jQuery(function () {
            var shippingItem = '#checkout #data .method .item';
            var shippingItemHTML = jQuery(shippingItem).addClass('new fetching').removeClass('active').outerHTML();

            // jQuery('#checkout').addClass('deactivate');
            jQuery(shippingItem).not(':first').remove();
            jQuery(shippingItem).find('input');
            jQuery(shippingItem).find('label').text(guSubstituteString('Fetching shipping options...'));
            jQuery(shippingItem).find('.price').html('<img width="20" src="/wp-content/uploads/checkout-loading.gif" />');
        });

        // timer here

        xhr.open("PUT", serverAltURL + '/' + gu_shopify_domain + '/checkouts/' + checkoutToken, true);
        xhr.setRequestHeader('x-api-key', x_api_key);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {

                        console.log(xhr.response); // here

                        // check for shipping address errors
                        if (typeof (xhr.response.errors) !== 'undefined') {
                            // check zip error
                            if (typeof (xhr.response.errors.shipping_address) !== 'undefined') {
                                invalidInput('zip');
                                return false;

                                // if (typeof(xhr.response.errors.shipping_address.zip[0].code) !== 'undefined' || typeof(xhr.response.errors.shipping_address.province_code[0].code) !== 'undefined') {
                                //
                                // }
                                // var error = xhr.response.errors.shipping_address.zip[0].code;
                                // if (error == 'invalid_for_country_and_province' || error == 'invalid_for_country') {
                                //
                                // }
                            }
                            // check email error
                            else if (typeof (xhr.response.errors.email) !== 'undefined') {
                                invalidInput('email');
                                return false;
                            }
                        }

                        setCheckoutAndToken(xhr.response);

                        if (safeResponse.errors) {
                            var response = JSON.stringify(safeResponse.errors);
                            // console.log(response);
                            if (~response.indexOf('"zip":[{"code":"invalid_for_country_and_province"') || ~response.indexOf('"zip":[{"code":"invalid_for_country"')) {
                                invalidInput('zip');
                            } else if (~response.indexOf('"email":[{"code":"invalid"')) {
                                invalidInput('email');
                            } else {
                                getShippingRates();
                            }
                        } else {
                            // Mark email input as good
                            var input = jQuery('#checkout #data form .inp.email input');
                            var label = input.attr('placeholder');
                            input.parent().find('label').removeClass('error').html(label);
                            input.parent().removeClass('bad').addClass('good'); // Mark green.
                            input.parent().addClass('filled'); // Add padding.

                            // Remove error classes on complete order btn
                            jQuery('#checkout #data .submit').removeClass('invalid-email invalid-zip');

                            // Get shipping rates
                            getShippingRates();

                            // fire cake pixel
                            gulog.fireAndLogEvent("Shipping Info");
                        }
                    }

                } else {
                    // Set new server
                    serverSwitchCounter2++;

                    if (server_1_URL != server_2_URL) {
                        if (serverURL == server_1_URL) {
                            serverURL = server_2_URL;
                            // fire event pixel
                            gulog.fireAndLogEvent('Switched to Server 2', "error",  serverSwitchCounter2 + 'x');
                            gulog.error("Switching Server Error", null, { status: xhr.status, response: xhr.response});

                        } else if (serverURL == server_2_URL) {
                            serverURL = server_1_URL;
                            // fire event pixel
                            gulog.fireAndLogEvent('Switched to Server 1', "error", serverSwitchCounter2 + 'x');
                            gulog.error("Switching Server Error", null, { status: xhr.status, response: xhr.response});
                        }
                    }

                    // Alert the user
                    noServer();

                    // Retry connection
                    setTimeout(function () {
                        shippingInfoClick('refresh');

                        // fire event pixel
                        retryShippingConnectionCounter++;
                        gulog.fireAndLogEvent('Retrying Shipping Connection', "error", retryShippingConnectionCounter + 'x');
                        gulog.error("Retrying Shipping Connection Error", null, { status: xhr.status, response: xhr.response});

                    }, 5000);
                }
            }
        };
        xhr.send(JSON.stringify(body));
    }

    function shippingInfoClick(refresh) {
        jQuery(function () {
            var email = jQuery('#checkout #data form .inp.email input');
            var phone = jQuery('#checkout #data form .inp.phone input');
            var fname = jQuery('#checkout #data form .inp.fname input');
            var lname = jQuery('#checkout #data form .inp.lname input');
            var address1 = jQuery('#checkout #data form .inp.address1 input');
            var address2 = jQuery('#checkout #data form .inp.address2 input');
            var city = jQuery('#checkout #data form .inp.city input');
            var country = jQuery('#checkout #data form .inp.country select');
            var state = jQuery('#checkout #data form .inp.state input');
            var zip = jQuery('#checkout #data form .inp.zip input');

            if (email.parent().hasClass('good') &&
                lname.parent().hasClass('good') &&
                address1.parent().hasClass('good') &&
                city.parent().hasClass('good') &&
                !state.parent().hasClass('bad') &&
                zip.parent().hasClass('good') || zip.parent().hasClass('filled')) {

                var addressChanged = false;
                var firstSend = false;

                if (refresh == 'refresh') {
                    var firstSend = true;
                }

                // Set values
                email_val = email.val();
                phone_val = phone.val();
                fname_val = fname.val();
                lname_val = lname.val();
                city_val = city.val();

                //first run through, auto save as input val
                if (typeof address1_val === 'undefined') {
                    firstSend = true;
                    address1_val = address1.val();
                }
                if (typeof address2_val === 'undefined') {
                    firstSend = true;
                    address2_val = address1.val();
                }
                if (typeof country_val === 'undefined') {
                    firstSend = true;
                    country_val = country.val();
                }
                if (typeof state_val === 'undefined') {
                    firstSend = true;
                    state_val = state.val();
                }
                if (typeof zip_val === 'undefined') {
                    firstSend = true;
                    zip_val = zip.val();
                }

                //for rest, make sure they are different before sending shipping info
                //if old country val is different than last
                if (address1_val != address1.val()) {
                    addressChanged = true;
                    address1_val = address1.val();
                }

                if (address2_val != address2.val()) {
                    addressChanged = true;
                    address2_val = address2.val();
                }

                if (country_val != country.val()) {
                    addressChanged = true;
                    country_val = country.val();
                }

                if (state_val != state.val()) {
                    addressChanged = true;
                    state_val = state.val();
                }

                if (zip_val != zip.val()) {
                    addressChanged = true;
                    zip_val = zip.val();
                }

                // Submit shipping info if address has changed
                if (firstSend) {
                    shippingInfo(email_val, phone_val, fname_val, lname_val, address1_val, address2_val, city_val, country_val, state_val, zip_val);
                } else if (addressChanged) {
                    jQuery('#checkout #data .method').addClass('refresh').removeClass('ready');
                }
            }
        });
    }

    // Get shipping rates
    function getShippingRates() {
        // Get shipping rates
        shippingRates(shopifyURL_checkout, checkoutToken, x_api_key);

        // Remove no_internet/no_server class if it exists
        var checkout_el = document.getElementById('checkout');
        if (checkout_el.className.indexOf('no_internet') > -1) {
            checkout_el.classList.remove("no_internet");
        } else if (checkout_el.className.indexOf('no_server') > -1) {
            checkout_el.classList.remove("no_server");
        }

        // Add remove zip code error on complete order btn
        jQuery('#checkout #data .submit').removeClass('invalid-zip invalid-email');

    }

    // Shipping rates
    function shippingRates(shop, checkoutToken, x_api_key) {
        // Get shipping rates
        var xhr = new XMLHttpRequest();

        xhr.open("GET", serverAltURL + '/' + gu_shopify_domain + '/checkouts/' + checkoutToken + '/shipping_rates/', true);
        xhr.setRequestHeader('x-api-key', x_api_key);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {

                        // check for shipping address errors
                        if (typeof (xhr.response.errors) !== 'undefined') {
                            var error = xhr.response.errors.checkout.shipping_address[0].code;
                            if (error == 'blank') {
                                invalidInput('zip');
                                return false;
                            }
                        }

                        //IE parsing check
                        if (typeof (xhr.response) == "object") {
                            shipping_rates = JSON.parse(JSON.stringify(xhr.response));
                        } else {
                            shipping_rates = JSON.parse(xhr.response);
                        }

                        if (shipping_rates.shipping_rates.length)
                        {
                            // Set rates
                            jQuery(function () {
                                // Reactivate complete order btn
                                // jQuery('#checkout').removeClass('deactivate');
                                jQuery('#checkout #data .method').addClass('ready');

                                // Set zip to good
                                var zip = jQuery('#checkout #data form .inp.zip');
                                var label = zip.find('input').attr('placeholder');
                                if (zip.hasClass('bad') || zip.hasClass('filled')) {
                                    zip.removeClass('bad').addClass('good'); // Mark green.
                                    zip.addClass('filled'); // Add padding.
                                    zip.find('label').removeClass('error').html(label);
                                }

                                // Get shipping rates
                                var shippingWrapper = jQuery('#checkout #data .method .items');
                                var shippingItem = '#checkout #data .method .items .item';
                                var shippingItemHTML = jQuery(shippingItem).removeClass('active new fetching').outerHTML();

                                // Clean up
                                shippingWrapper.find('.item').remove();

                                // Add shipping items
                                for (var i = 0; i < shipping_rates.shipping_rates.length; i++) {
                                    shippingWrapper.append(shippingItemHTML);
                                    jQuery(shippingItem).eq(i).attr('data-handle', shipping_rates.shipping_rates[i].id);
                                    jQuery(shippingItem).eq(i).find('.price').text(guDisplayCurrency(shipping_rates.shipping_rates[i].price));

                                    function translate7() {
                                        jQuery(shippingItem).eq(i).find('label').text(guSubstituteString(shipping_rates.shipping_rates[i].title));
                                    }

                                    guRemoteDataRegisterCallback(translate7);

                                    // Select the first shipping choice
                                    if (i == 0) {
                                        jQuery(shippingItem).eq(i).click();
                                    }
                                }

                                // refresh promo
                                if (gu_qs['gu47'] == 1 && checkout.applied_discount != null) {
                                    jQuery('#checkout #summary .toggle .promo .price').text('- ' + guDisplayCurrency(checkout.applied_discount.amount));

                                }
                            });
                        }
                        else
                        {
                            //we received and empty rates obj, try again
                            console.log("Received an empty rates, try again");
                            setTimeout(function () {
                                shippingRates(shop, checkoutToken, x_api_key);
                            }, 1000);
                        }
                    }
                } else {

                }
            }
        };
        xhr.send(JSON.stringify({}));
    };

    // Chose shipping
    function choseShipping(handle) {
        var body = {
            "checkout": {
                "shipping_line": {
                    "handle": handle
                }
            }
        };
        updateCart(body, function () {
            // Set shipping price in order summary
            jQuery(function () {
                // Update shipping
                jQuery('#checkout #summary .toggle .extras .shipping .price').html(guDisplayCurrency(checkout.shipping_rate.price));

                // Update total & tax
                jQuery('#checkout #summary .title .price').html(guDisplayCurrency(checkout.total_price));
                jQuery('#checkout #summary .toggle .extras .tax .price').html(guDisplayCurrency(checkout.total_tax));
                jQuery('#checkout #summary .toggle .total .price').html(guDisplayCurrency(checkout.total_price));
            });
        }, function () {

        });
    };

    function error(message) {
        jQuery(function () {
            message = 'Weâ€™re sorry but your credit card was declined. Please recheck all information or use an alternative credit card and try submitting again.';
            message = guSubstituteString(message);

            hideLoading();
            jQuery('#checkout #data .payment .error-msg').text(message).slideDown();
            jQuery('html, body').animate({
                scrollTop: jQuery('#checkout #data .payment').offset().top
            }, 500);
        });
    }

    // Set counters
    var failedCounter = 0;
    var ccFailedCounter = 0;

    // Process
    function checkPaymentStatus(shop, checkoutToken, paymentId) {
        var fullURL = checkPaymentStatusURLBase + shop + '/' + checkoutToken + '/' + paymentId

        var xhr = new XMLHttpRequest();

        xhr.open("GET", fullURL, true);
        xhr.setRequestHeader('x-api-key', checkPaymentStatus_x_api_key);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    //https://help.shopify.com/api/storefront-api/reference/enum/transactionstatus

                    //positive example   {"message":"Transaction approved","status":"success"}
                    //negative example  {"message":"Your card's security code is incorrect.","status":"failure"}
                    var response = objectifyResponse(xhr.response);

                    if (response.status.toLowerCase() == 'pending') {
                        setTimeout(function () {
                            checkPaymentStatus(shop, checkoutToken, paymentId);
                        }, 1000);

                    } else {
                        if (response.status.toLowerCase() == 'success' && typeof response.order_id != 'undefined') {
                            //so all good.  you would go down your success path
                            // alert('You finish success path');
                            // much success here
                            checkout.shop = shop;
                            checkout.order_id = response.order_id;
                            checkout.order_name = response.order_name;

                            // fire event pixel
                            //gulog.fireAndLogEvent("Order Conversion", "Order Conversion");

                            gu_qs["gu_fcp"] = 0;
                            setTimeout(function () {
                                thankYouPage();
                            }, 200);

                        } else if (response.status.toLowerCase() == 'success' && typeof response.order_id == 'undefined') {
                            checkPaymentStatus(shop, checkoutToken, paymentId);
                        } else {
                            //processing error.  You surface to user.
                            // alert(response.message);
                            error(response.message);

                            // fire event log
                            ccFailedCounter++;
                            gulog.fireAndLogEvent("Credit Card Failed", "", ccFailedCounter + 'x', "Message", response.message);

                        }
                    }
                } else {
                    //well we have an error.  could be internet connection or shopify down
                    //since we jsut did a lot of interaction with shopify as well as had internet down logic lets just try again.
                    setTimeout(function () {
                        checkPaymentStatus(shop, checkoutToken, paymentId);

                        // fire event pixel
                        failedCounter++;
                        gulog.fireAndLogEvent('Internet or Shopify Down', "error", retryShippingConnectionCounter + 'x');
                        gulog.error("Internet or Shopify Down", null, { status: xhr.status, response: xhr.response,  severity: "critical"});
                    }, 1000);
                }
            }
        }
        xhr.send();
    };

    // set timers
    var orderFailedCounter = 0;

    // process order
    function processOrder(shopifyURL_checkout, checkoutToken, shopify_tkn, process_order) {
        console.log('processing'); // remove
        var body = {};
        body.processNow = process_order;

        var xhr = new XMLHttpRequest();

        xhr.open("POST", serverURL + '/' + shopifyURL_checkout + '/' + checkoutToken + '/processOrder', true);
        xhr.setRequestHeader('x-api-key', x_api_key);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        //add tkn
        xhr.setRequestHeader('shopify_tkn', shopify_tkn);
        //add payment_due
        xhr.setRequestHeader('payment_due', checkout.total_price);

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {
                        var response = objectifyResponse(xhr.response);
                        if (response.errors || response.message.toLowerCase().includes('error')) {

                            error(response.message);

                            // fire event pixel
                            orderFailedCounter++;
                            gulog.fireAndLogEvent('Error Processing Order', "error", orderFailedCounter + 'x');
                            gulog.error("Shopify Processing Order Failed ", null, { status: xhr.status, response: response, severity: "critical"});

                        } else {
                            var response = objectifyResponse(xhr.response);

                            if (response.urlParts && response.urlParts.length >= 5) {
                                checkPaymentStatus(shopifyURL_checkout, checkoutToken, response.urlParts[6].replace('.json', ''));
                            } else {
                                //some horrible error occured. not sure what to do. go to shopify
                                //window.location = checkout.web_url;

                                //check for upsell before we start processing
                                startLoadingForProcessing();

                                //status not ready yet, run through again
                                // process_order = true;
                                // startProcessing();
                            }

                            //console.log('process order status = ' + process_order);
                            //console.log('shopify tkn status = ' + process_order);
                            //if (process_order == true) {
                            //    console.log('processed'); // remove
                            //    thankYouPage();
                            //}
                        }
                    }
                } else {
                    // console.log('info not sent'); // remove
                    // hideLoading();

                    //some horrible error occured. not sure what to do. go to shopify

                    var newURL = updateStoreURL(checkout.web_url);
                    window.location = newURL + gu_qs_to_str(gu_qs);
                }
            }
        }
        xhr.send(JSON.stringify(body));
    };

    // stripe
    function getStripeTKN(card, callback) {
        var xhr = new XMLHttpRequest();
        var formEncoded = 'card[number]=' + card.number + '&card[exp_month]=' + card.exp_mo + '&card[exp_year]=' + card.exp_yr + '&card[cvc]=' + card.cvc;

        xhr.open("POST", 'https://api.stripe.com/v1/tokens', true);
        xhr.responseType = 'json';
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('Authorization', 'Bearer ' + stripePublicAPIKey);


        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {
                        var res = (objectifyResponse(xhr.response));
                        stripe_auth_tkn = res.id;
                        processStripeOrder(stripe_auth_tkn, false);
                    }
                } else {
                    var huh = false;
                }
            }
        };

        xhr.send(formEncoded);
    };

    function processStripeOrder(auth_token, process_order) {
        console.log('processing stripe'); // remove

        var body = {
            "shop": shopifyURL_checkout + '/',
            "auth_source": "stripe",
            "checkout_token": checkoutToken,
            "auth_token": auth_token,
            "processNow": process_order
        };

        var xhr = new XMLHttpRequest();

        xhr.open("POST", 'https://orders.api.giddyup.io/processorder', true);
        xhr.responseType = 'json';

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {
                        var response = objectifyResponse(xhr.response);
                        startLoadingForProcessing();
                    }
                } else {
                    //some horrible error occurred. not sure what to do. go to shopify
                    console.log('stripe processing error');

                    // fire event pixel
                    gulog.fireAndLogEvent('A Horrible Error Occurred', 'error');
                    gulog.error("A Horrible Error Occurred", null, { status: xhr.status, response: xhr.response, severity: "critical"});
                }
            }
        }
        xhr.send(JSON.stringify(body));
    }

    function whatToDo() {
        return (shopify_tkn_status && process_order);
    }

    function objectifyResponse(response) {
        var hld = null;
        if (typeof (response) == "object") {
            hld = response;
        }
        // IE Safety
        else {
            hld = JSON.parse(response);
        }
        return hld;
    }

    // Override initial billing address
    function overrideBillingAddress(fname, lname, ccnumber, ccmonth, ccyear, cccvc) {

        //Re-Capture Billing Vals
        billing_fnameField_val = jQuery('#billing_address_form .billing_fname input').val();
        billing_lnameField_val = jQuery('#billing_address_form .billing_lname input').val();
        billing_address1Field_val = jQuery('#billing_address_form .billing_address1 input').val();
        billing_address2Field_val = jQuery('#billing_address_form .billing_address2 input').val();
        billing_cityField_val = jQuery('#billing_address_form .billing_city input').val();
        billing_countryField_val = jQuery('#billing_address_form .billing_country select').val();
        billing_stateField_val = jQuery('#billing_address_form .billing_state input').val();
        billing_zipField_val = jQuery('#billing_address_form .billing_zip input').val();

        if (!billing_fnameField_val) {
            billing_fnameField_val = '';
        }
        if (!billing_lnameField_val) {
            billing_lnameField_val = '';
        }
        if (!billing_address1Field_val) {
            billing_address1Field_val = '';
        }
        if (!billing_address2Field_val) {
            billing_address2Field_val = '';
        }
        if (!billing_cityField_val) {
            billing_cityField_val = '';
        }
        if (!billing_countryField_val) {
            billing_countryField_val = '';
        }
        if (!billing_stateField_val) {
            billing_stateField_val = '';
        }
        if (!billing_zipField_val) {
            billing_zipField_val = '';
        }

        var xhr = new XMLHttpRequest();
        var body = {
            "checkout": {
                "billing_address": {
                    "first_name": billing_fnameField_val,
                    "last_name": billing_lnameField_val,
                    "address1": billing_address1Field_val,
                    "address2": billing_address2Field_val,
                    "city": billing_cityField_val,
                    "province_code": billing_stateField_val,
                    "country_code": billing_countryField_val,
                    "zip": billing_zipField_val
                }
            }
        };

        xhr.open("PUT", serverAltURL + '/' + gu_shopify_domain + '/checkouts/' + checkoutToken, true);
        xhr.setRequestHeader('x-api-key', x_api_key);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {

                        if (safeResponse.errors) {
                            var response = JSON.stringify(safeResponse.errors);
                            // console.log(response);
                            if (~response.indexOf('"zip":[{"code":"invalid_for_country_and_province"') || ~response.indexOf('"zip":[{"code":"invalid_for_country"')) {
                                invalidInput('zip');
                            } else {
                                if (gu_payment == 1) {
                                    // get the Shopify Token
                                    getShopifyTKN(fname, lname, ccnumber, ccmonth, ccyear, cccvc);
                                } else if (gu_payment == 2) {
                                    var card = {
                                        number: ccnumber.split(' ').join(''),
                                        exp_mo: ccmonth.split(' ').join(''),
                                        exp_yr: ccyear.split(' ').join(''),
                                        cvc: cccvc.split(' ').join('')
                                    };
                                    getStripeTKN(card, getTokenCallback);
                                }
                            }
                        } else {

                            // Remove error classes on complete order btn
                            jQuery('#checkout #data .submit').removeClass('invalid-email invalid-zip');

                            // process order
                            if (gu_payment == 1) {
                                // get the Shopify Token
                                getShopifyTKN(fname, lname, ccnumber, ccmonth, ccyear, cccvc);
                            } else if (gu_payment == 2) {
                                var card = {
                                    number: ccnumber.split(' ').join(''),
                                    exp_mo: ccmonth.split(' ').join(''),
                                    exp_yr: ccyear.split(' ').join(''),
                                    cvc: cccvc.split(' ').join('')
                                };
                                getStripeTKN(card, getTokenCallback);
                            }
                        }

                    }

                }
            }
        };
        xhr.send(JSON.stringify(body));
    }

    // Get CC Token.
    function getShopifyTKN(fname, lname, ccnumber, ccmonth, ccyear, cccvc) {
        if (!fname) {
            fname = '';
        }
        if (!lname) {
            lname = '';
        }
        if (!ccnumber) {
            ccnumber = '';
        }
        if (!ccmonth) {
            ccmonth = '';
        }
        if (!ccyear) {
            ccyear = '';
        }
        if (!cccvc) {
            cccvc = '';
        }
        // if (!cczip) {cczip = ''; }

        var xhr = new XMLHttpRequest();
        var body = {
            "credit_card": {
                "number": ccnumber,
                "first_name": fname,
                "last_name": lname,
                "month": ccmonth,
                "year": ccyear,
                "verification_value": cccvc
            }
        };

        shopify_tkn = null;

        xhr.open("POST", 'https://elb.deposit.shopifycs.com/sessions', true);
        xhr.responseType = 'json';
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {
                        // shopify_tkn_status = true;
                        console.log('went thru'); // remove
                        shopify_tkn = (objectifyResponse(xhr.response)).id;
                        shopify_tkn_status = true;
                        startProcessing();
                    }
                } else {
                    console.log('failed'); // remove

                    // CC verification failed
                    shopify_tkn_status = false;
                    ccFailed();

                    // fire event pixel
                    gulog.fireAndLogEvent('Credit Card Verification Failed', "error");
                }
            }
        }
        xhr.send(JSON.stringify(body));
    };

    function getIESafeResponse(response) {
        var tmp = {};
        if (typeof (response) == "object") {
            tmp = response;
        } else {
            tmp = JSON.parse(response);
        }

        // IE Safety
        return tmp;
    }

    // Get country
    function getCountry() {
        var xhr = new XMLHttpRequest();

        xhr.open("GET", 'https://ulegj23o8l.execute-api.us-west-2.amazonaws.com/production', true);
        xhr.responseType = 'json';
        // xhr.withCredentials = true;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status < 300 && xhr.status >= 200) {
                    if (xhr.response) {
                        var response = getIESafeResponse(xhr.response);

                        country = response.headers['CloudFront-Viewer-Country'];
                        setCountry(country);
                    }
                } else {

                }
            }
        };
        xhr.send();
    };

    function setCountry(country) {
        jQuery(function () {
            var countrySel = jQuery('#checkout #data form .inp.country select');
            countrySel.val(country);
            countrySel.blur();
        });
    }

    // Show loading screen
    function showLoading(item) {
        jQuery(function () {
            // Vars
            var checkout_overlay = jQuery('#checkout_overlay');
            var checkout_overlay_center = checkout_overlay.find('.center .center-anchor');

            var loading = '#checkout_overlay .loading';
            var loadingHTML = jQuery(loading).outerHTML();

            var no_internet = '#checkout_overlay .no_internet';
            var no_internet_HTML = jQuery(no_internet).outerHTML();

            var no_server = '#checkout_overlay .no_server';
            var no_server_HTML = jQuery(no_server).outerHTML();

            // Show loading
            checkout_overlay_center.empty();
            checkout_overlay.fadeIn();

            // Select HTML to append
            if (item == 'no internet') {
                // Prepare canvas
                prepareCanvas();
                checkout_overlay_center.append(no_internet_HTML);

                // Open and scroll to the checkout
                jQuery('#checkout').slideDown('slow');
                setTimeout(function () {
                    jQuery('html, body').animate({
                        scrollTop: jQuery('#checkout').offset().top
                    }, 1000);
                }, 500);
            } else if (item == 'no server') {
                // Prepare canvas
                prepareCanvas();
                checkout_overlay_center.append(no_server_HTML);

                // Open and scroll to the checkout
                jQuery('#checkout').slideDown('slow');
                setTimeout(function () {
                    jQuery('html, body').animate({
                        scrollTop: jQuery('#checkout').offset().top
                    }, 1000);
                }, 500);
            } else {
                checkout_overlay_center.append(loadingHTML);
                checkout_overlay_center.find('.loading').fadeIn();
            }
        });
    };

    function hideLoading(item) {
        var checkout_overlay = jQuery('#checkout_overlay');
        checkout_overlay.fadeOut();
    }

    // No internet connection
    function noInternet() {
        // Set no_internet class
        var checkout_el = document.getElementById('checkout');
        if (checkout_el.className.indexOf('no_internet') == -1) {
            checkout_el.className += " no_internet";
        }

        // Show loading screen
        showLoading('no internet');
    }

    // No server connection
    function noServer() {
        // Set no_server class
        var checkout_el = document.getElementById('checkout');
        if (checkout_el.className.indexOf('no_server') == -1) {
            checkout_el.className += " no_server";
        }

        // Show loading screen
        showLoading('no server');
    }

    function ccFailed() {
        console.log('cc failed'); // remove
        hideLoading();
    }

    function prepShopify() {
        processOrder(shopifyURL_checkout, checkoutToken, shopify_tkn, whatToDo());
    }

    function thankYouPage() {

        // correct bundle id test (temp test)
        var bundleId = jQuery('#offer .offer.selected:visible').attr('id');
        var selectedUnitsBundleId = gu_products.selectedUnits[0].id;

        // if (bundleId != selectedUnitsBundleId) {
        //     console.log('raven error sent');
        //     Raven.captureException(new Error('Wrong bundle submitted for reporting'), {
        //         bundleId: bundleId,
        //         selectedUnits: selectedUnitsBundleId
        //     });
        // }

        // go to thank you page
        setTimeout(function () {

            thankYouPageUrl = updateStoreURL(thankYouPageUrl);

            window.location = thankYouPageUrl + gu_qs_to_str(gu_qs);
        }, 500);
    }

    //*********************************************************************
    // Re-builds the store url to the new shop.offername naming scheme
    //*********************************************************************
    function updateStoreURL (storeurl) {
        var newURL = new URL(storeurl);
        newURL.hostname = "shop." + gu_shopify_name + ".io";

        return newURL;
    }

    function upsellTimer(time, upsell_id) {
        jQuery(function () {

            // create the timer
            if (!jQuery('#' + upsell_id + ' .timer').length) {
                var text = guSubstituteString('Offer expires in');
                jQuery('#' + upsell_id).prepend('<div class="timer"><p>' + text + '<span></span></p></div>');
            }

            // show the timer
            setTimeout(function () {
                jQuery('#' + upsell_id + ' .timer').fadeIn();
            }, 1000);

            // Set interval
            var interval = setInterval(function () {
                // Separate mins and secs
                var timer = time.split(':');

                // By parsing the integer, avoids all extra string processing
                var minutes = parseInt(timer[0], 10);
                var seconds = parseInt(timer[1], 10);
                --seconds;

                // Set correct intervals
                minutes = (seconds < 0) ? --minutes : minutes;
                if (minutes < 0) clearInterval(interval);
                seconds = (seconds < 0) ? 59 : seconds;
                seconds = (seconds < 10) ? '0' + seconds : seconds;

                // Set time
                if (minutes < 0) {
                    if (jQuery('#' + upsell_id).is(':visible')) {
                        upsellClick();
                    }
                } else {
                    jQuery('#' + upsell_id + ' .timer span').html(minutes + ':' + seconds);
                }

                // Adjust time
                time = minutes + ':' + seconds;
            }, 1000);
        });
    }

    function startLoadingForProcessing() {
        // check for upsell
        // var upsellClass;
        // jQuery('#offer .offer.selected:visible').each(function() {
        //     upsellClass = jQuery.grep(this.className.split(" "), function(v, i) {
        //         return v.indexOf('upsell-') === 0;
        //     }).join();
        // });
        // if (upsellClass != '' && !jQuery('#' + upsellClass).hasClass('active')) {
        //     console.log('has upsell');
        //
        //     var upsellHTML = jQuery(upsell).outerHTML();
        //     jQuery(upsell).remove();
        //     //checkout_overlay_center.empty();
        //     jQuery('#main-form').append(upsellHTML);
        //
        //
        //     upsell(upsellClass);
        // }
        //
        // // no upsell
        // else {
        console.log('no upsell');
        showLoading();

        // process
        if (gu_payment == 1) {
            process_order = true;
            startProcessing();
        } else if (gu_payment == 2) {
            processStripeOrder(stripe_auth_tkn, true);
        }
        //}
    }

    // open the upsell
    function upsell(id) {
        jQuery(function () {
            // vars
            var time = '0:60';
            var upsell = '#' + id;
            var checkout_overlay = jQuery('#checkout_overlay');
            var checkout_overlay_center = checkout_overlay.find('.center .center-anchor');

            // show loading
            //showLoading();

            //hide submit button
            jQuery('#main-form .submit').fadeOut();

            // prep upsell
            if (jQuery(upsell).hasClass('colors')) {
                // get the prev color
                if (jQuery(upsell).attr('data-prevColor') == undefined) {
                    if (jQuery('#offer #colors .color:first-child').hasClass('variety')) {
                        var prevColor = jQuery('#offer #colors .color:nth-child(2)').attr('data-color');
                    } else {
                        var prevColor = jQuery('#offer #colors .color:first-child').attr('data-color');
                    }
                } else {
                    var prevColor = jQuery(upsell).attr('data-prevColor');
                }

                var activeColor = jQuery('#offer #colors .color.active').attr('data-color');
                if (activeColor == 'variety') {
                    activeColor = jQuery('#offer #colors .color:first-child').attr('data-color');
                }
                if (activeColor == 'variety') {
                    activeColor = jQuery('#offer #colors .color:nth-child(2)').attr('data-color');
                }

                var img = jQuery('.image img', jQuery(upsell));
                var imgSrc = img.attr('src').replace(prevColor, activeColor);

                // set img
                jQuery('#' + id + ' .image img').attr({
                    'src': imgSrc,
                    'srcset': imgSrc
                });

                // set prev color
                jQuery(upsell).attr('data-prevColor', activeColor);
            }

            // show upsell
            setTimeout(function () {
                jQuery(upsell).show();


                //checkout_overlay.fadeIn();

                // prevent scroll
                //jQuery('body').addClass('prevent-scrolling');

                // start timer
                //upsellTimer(time, id);

                //FIX THIS IF MOVING TO INLINE POST SUBMIT

                // scroll to shipping method
                setTimeout(function () {
                    if (mobileDevice) {
                        var summaryHeight = jQuery('#checkout #summary').height();
                        jQuery('html, body').animate({
                            scrollTop: jQuery('#checkout .upsell').offset().top - summaryHeight
                        }, 1000);
                    }
                    else {
                        jQuery('html, body').animate({
                            scrollTop: jQuery('#checkout .upsell').offset().top
                        }, 1000);
                    }

                }, 150);


            }, 500);

            // fire event
            gulog.fireAndLogEvent("View One Click Upsell", null, null, "Upsell ID", id);
        });
    }

    function upsellClick(status) {
        // set status
        var upsell = jQuery('#' + jQuery('.upsell:visible').attr('id'));
        upsell.addClass('active');

        // close giddybox & reset x btn
        jQuery('#giddybox .giddybox-closeBtn').attr('onclick', '');
        jQuery('#giddybox .giddybox-closeBtn').click();

        // add upsell
        if (status == true) {

            // save upsell
            var upsellHTML = upsell.outerHTML();
            var checkout_overlay = jQuery('#checkout_overlay');
            jQuery('.hidden', checkout_overlay).append(upsellHTML);

            // show confirmation
            jQuery('#checkout #conf').fadeIn();
            setTimeout(function () {
                jQuery('#checkout #conf').fadeOut();
            }, 2000);

            // checkout
            prepCheckout('dtc');

            // here

            // fire event
            gulog.fireAndLogEvent("Complete One Click Upsell", null, null, "Upsell ID", id);
        }

        // cancel upsell
        else {
            process_order = true;
            startProcessing();

            // fire event
            var id = upsell.attr('id');
            gulog.fireAndLogEvent("No To One Click Upsell", null, null, "Upsell ID", id);
        }

        // show Loading
        showLoading();

        //click submit button to finish
        jQuery('#checkout #data .submit').click();

        // bring back scrolling
        //jQuery('body').removeClass('prevent-scrolling');

        //reset submit area, hide upsell
        upsell.fadeOut();
        jQuery('#checkout #data .submit').show();
    }

    function addUpsell(products) {
        // Create line items
        var items = getSelectedItems(products);
        var itemz = createItems(items);

        // add upsell
        addProducts(itemz, checkoutToken, x_api_key);
    }

    function startProcessing() {
        jQuery(function () {
            if (!jQuery('#checkout #data .method').hasClass('ready')) {
                if (jQuery('#checkout #data form .inp.zip').hasClass('bad') || jQuery('#checkout #data form .inp.email').hasClass('bad')) {
                    hideLoading();
                    setTimeout(function () {
                        if (!jQuery('#checkout #alert').is(':visible')) {
                            jQuery('#checkout #data .method .refresh').click();
                        }
                    }, 500);
                } else {
                    setTimeout(function () {
                        if (jQuery('#checkout #data .method .refresh').is(':visible')) {
                            jQuery('#checkout #data .method .refresh').click();
                        }
                        startProcessing();
                    }, 2000);
                }
            } else if (shopify_tkn == undefined || shopify_tkn == null) {
                setTimeout(function () {
                    startProcessing();
                }, processOrderTimeout);
            } else {
                // prep shopify and process order
                prepShopify();
            }
        });
    }

    // Checkout HTML
    var checkout_html = '\
        <main id="checkout" class="">\
            <!-- Data -->\
            <section id="data">\
                <div class="wrapper">\
                    <div class="wrapper"><h1 class="headline">Complete Your Order</h1></div>\
                    <span class="promoWrap">\
                        <!-- <h3>Have a Promo Code?</h3> -->\
                        <span class="inp promo">\
                            <input name="promo" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="Enter Discount Code Here" type="text">\
                            <label for="promo">Enter Discount Code Here</label>\
                            <button>Apply</button>\
                        </span>\
                    </span>\
                    <div id="timer"></div>\
                        <a id="paypal_btn" class="' + enablePaypal + '" onclick="fireOtherProviderEvents(\'PayPal\', this.id);"><img width="70" height="19" src="/wp-content/uploads/checkout-paypal-logo.svg"></a>\
                        <a id="amzn_btn" class="' + enableAmznPay + '" onclick="fireOtherProviderEvents(\'Amazon\', this.id);"><img width="89" height="18" src="/wp-content/uploads/checkout-amazon-logo.png"></a>\
                        <a id="applePay_btn" style="display:none;" onclick="gulog.fireAndLogEvent(\'Click ApplePay\'); gulog.fireAndLogEvent(\'Shipping Info\');">Pay with <img width="65" height="27" src="/wp-content/uploads/checkout-apple-pay-logo.png"></a>\
                    <!-- Form -->\
                    <form id="main-form" method="post" action="" novalidate>\
                        <div class="divider">\
                            <p>Or Pay With Credit Card</p>\
                        </div>\
                        <!-- Shipping -->\
                        <section id="shipping-section" class="shipping">\
                        <h2 class="title ship">Shipping Information</h2>\
                        <span class="inp email">\
                            <input name="email"  autocomplete="email" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="Email" type="email">\
                            <label for="email">Email</label>\
                        </span>\
                        <span class="inp fname">\
                            <input name="fname" autocomplete="given-name" placeholder="First Name" type="text">\
                            <label for="fname">First Name</label>\
                        </span>\
                        <span class="inp lname">\
                            <input name="lname" autocomplete="family-name" placeholder="Last Name" type="text">\
                            <label for="lname">Last Name</label>\
                        </span>\
                        <span class="inp address1">\
                            <input name="address1" autocomplete="address-line1" placeholder="Address" type="text">\
                            <label for="address1">Address</label>\
                        </span>\
                        <span class="inp address2">\
                            <input name="address2" autocomplete="address-line2" placeholder="Apt/Suite (optional)" type="text">\
                            <label for="address2">Apt/Suite (optional)</label>\
                        </span>\
                        <span class="inp city">\
                            <input name="city" autocomplete="address-level2" placeholder="City" type="text">\
                            <label for="city">City</label>\
                        </span>\
                        <span class="inp country">\
                            <select name="country" autocomplete="country" required="">\
                            </select>\
                            <label for="country">Country</label>\
                        </span>\
                        <span class="inp state">\
                            <input name="state" autocomplete="address-level1" autocorrect="off" spellcheck="off" placeholder="State" type="text">\
                            <label for="state">State</label>\
                        </span>\
                        <span class="inp zip">\
                            <input name="zip" autocomplete="postal-code" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="Zip Code" type="text">\
                            <label for="zip">Zip Code</label>\
                        </span>\
                        <span class="inp phone">\
                            <input name="phone" autocomplete="tel" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="Phone (optional)" type="tel" maxlength="100">\
                            <label for="phone">Phone (optional)</label>\
                            <a class="question" data-title="">\
                                <img width="100%" src="/wp-content/uploads/checkout-question.svg" alt="?">\
                            </a>\
                        </span>\
                        </section>\
                        <!-- Shipping Method -->\
                        <section class="method"">\
                            <h2 class="title ship">Shipping Method</h2>\
                            <div class="items">\
                                <div class="item new">\
                                    <span class="radio"><input name="item" type="radio"></span>\
                                    <label>Enter address above to see shipping options</label>\
                                    <p class="price">â€”â€”</p>\
                                </div>\
                                <div class="refresh">\
                                    <div class="center">\
                                        <div class="center-anchor">\
                                            <p>Refresh shipping</p>\
                                            <a>Refresh</a>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </section>\
                        <!-- Payment -->\
                        <section class="payment">\
                            <h2 class="title">Payment Information</h2>\
                            <p class="subtitle">All transactions are secure and encrypted.</p>\
                            <div class="cc">\
                                <img class="visa active" height="30" src="/wp-content/uploads/checkout-visa.svg">\
                                <img class="mastercard active" height="30" src="/wp-content/uploads/checkout-mastercard.svg">\
                                <img class="discover active" height="30" src="/wp-content/uploads/checkout-discover.svg">\
                                <img class="amex active" height="30" src="/wp-content/uploads/checkout-amex.svg">\
                            </div>\
                            <div class="clear"></div>\
                            <p class="error-msg"></p>\
                            <span class="inp cc-number">\
                                <input type="text" pattern="[0-9]*" name="cardNumber" type="tel" autocomplete="off" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="Card Number">\
                                <label for="cc-number">Card Number</label>\
                            </span>\
                            <span class="inp cc-exp">\
                                <input type="text" pattern="[0-9]*" name="cc-exp" autocomplete="off" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="MM / YY" maxlength="7">\
                                <label for="cc-exp">MM / YY</label>\
                            </span>\
                            <span class="inp cc-cvc">\
                                <input type="text" pattern="[0-9]*" name="cc-cvc" autocomplete="off" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="CVC">\
                                <label for="cc-cvc">CVC</label>\
                                <a class="question" data-title="">\
                                    <img width="100%" src="/wp-content/uploads/checkout-question.svg" alt="?">\
                                </a>\
                            </span>\
                            <!-- <span class="inp cc-zip">\
                                <input name="cc-zip" pattern="[0-9]*" autocomplete="off" autocorrect="off" spellcheck="off" autocapitalize="off" maxlength="5" placeholder="Billing Zip Code" type="text">\
                                <label for="cc-zip">Billing Zip Code</label>\
                            </span> -->\
                            <div id="card-errors"></div>\
                        </section>\
                        <!-- Billing Address -->\
                        <section id="billing_address_section" class="billing_address method">\
                        <h2 class="title bill">Billing Address</h2>\
                        <div id="billShipOptions">\
                            <div class="billShip active" id="billing_same">\
                                <span class="radio"><input name="billShip" type="radio"></span>\
                                <label>Same as shipping address</label>\
                            </div>\
                            <div class="billShip" id="billing_other">\
                                <span class="radio"><input name="billShip" type="radio"></span>\
                                <label>Use a different billing address</label>\
                            </div>\
                        </div>\
                        <div class="hidden" id="billing_address_form">\
                            <span class="inp billing_fname">\
                                    <input name="bfname" autocomplete="given-name" placeholder="First Name" type="text">\
                                    <label for="bfname">First Name</label>\
                                </span>\
                            <span class="inp billing_lname">\
                                    <input name="blname" autocomplete="family-name" placeholder="Last Name" type="text">\
                                    <label for="blname">Last Name</label>\
                                </span>\
                            <span class="inp billing_address1">\
                                    <input name="baddress1" autocomplete="address-line1" placeholder="Address" type="text">\
                                    <label for="baddress1">Address</label>\
                                </span>\
                            <span class="inp billing_address2">\
                                    <input name="baddress2" autocomplete="address-line2" placeholder="Apt/Suite (optional)" type="text">\
                                    <label for="baddress2">Apt/Suite (optional)</label>\
                                </span>\
                            <span class="inp billing_city">\
                                    <input name="bcity" autocomplete="address-level2" placeholder="City" type="text">\
                                    <label for="bcity">City</label>\
                                </span>\
                            <span class="inp billing_country">\
                                    <select name="bcountry" autocomplete="country" required="">\
                                    </select>\
                                    <label for="bcountry">Country</label>\
                                </span>\
                            <span class="inp billing_state">\
                                    <input name="bstate" autocomplete="address-level1" autocorrect="off" spellcheck="off" placeholder="State" type="text">\
                                    <label for="bstate">State</label>\
                                </span>\
                            <span class="inp billing_zip">\
                                    <input name="bzip" autocomplete="postal-code" autocorrect="off" spellcheck="off" autocapitalize="off" placeholder="Zip Code" type="text">\
                                    <label for="bzip">Zip Code</label>\
                                </span>\
                        </div>\
                        </section>\
                        <!-- Order Now -->\
                        <a class="submit next" data-next="Next">Complete Order</a>\
                    </form>\
                </div>\
            </section>\
            <!-- Summary -->\
            <section id="summary">\
                <section class="title">\
                    <div class="wrapper">\
                        <a>Order Summary</a>\
                        <p class="price"></p>\
                        <div class="clear"></div>\
                    </div>\
                </section>\
                <div class="toggle">\
                    <section class="products">\
                        <div class="wrapper">\
                            <div class="product">\
                                <!-- Product -->\
                                <div class="content">\
                                    <div class="img">\
                                        <div class="img_wrapper"><img src="" /></div>\
                                        <p class="qty"></p>\
                                    </div>\
                                    <div class="txt">\
                                        <div class="center">\
                                            <div class="center-anchor">\
                                                <p class="label"></p>\
                                                <p class="variant"></p>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="price">\
                                    <div class="center">\
                                        <div class="center-anchor">\
                                            <span class="value"></span>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </section>\
                    <section class="extras">\
                        <div class="wrapper">\
                            <!-- Tax -->\
                            <div class="tax">\
                                <p class="label">Tax</p>\
                                <p class="price"></p>\
                            </div>\
                            <!-- Shipping -->\
                            <div class="shipping">\
                                <p class="label">Shipping</p>\
                                <p class="price">--</p>\
                            </div>\
                        </div>\
                    </section>\
                    <section class="total">\
                        <div class="wrapper">\
                            <p class="label">Total</p>\
                            <p class="price"></p>\
                        </div>\
                    </section>\
                </div>\
            </section>\
            <div class="clear"></div>\
            <!-- Checkout overlay -->\
            <main id="checkout_overlay">\
                <p class="loading_bar">Your order is processing, please wait...</p>\
                <p class="no_internet_bar">No internet connection</p>\
                <p class="no_server_bar">No server connection</p>\
                <div class="hidden"></div>\
                <div class="center">\
                    <div class="center-anchor">\
                    </div>\
                </div>\
                <div class="loading">\
                    <h1>Processing...</h1>\
                    <img width="40" src="/wp-content/uploads/checkout-loading.gif">\
                </div>\
                <div class="no_internet">\
                    <h1>Your internet is disconnected. Trying again...</h1>\
                    <img width="40" src="/wp-content/uploads/checkout-loading.gif">\
                </div>\
                <div class="no_server">\
                    <h1>The server is not responding. Trying again...</h1>\
                    <img width="40" src="/wp-content/uploads/checkout-loading.gif">\
                </div>\
            </main>\
            <!-- Added to order -->\
            <section id="conf">\
                <div class="center">\
                    <div class="center-anchor">\
                        <div class="wrapper">\
                            <img width="60" height="auto" src="/wp-content/uploads/checkout-shipping-green-check.svg">\
                            <p>Item Added To Order</p>\
                        </div>\
                    </div>\
                </div>\
            </section>\
            <!-- Alert -->\
            <section id="alert">\
                <div class="center">\
                    <div class="center-anchor">\
                        <div class="wrapper">\
                            <p class="txt"></p>\
                            <button class="accept">OK</button>\
                        </div>\
                    </div>\
                </div>\
            </section>\
        </main>\
    ';

    // Add the checkout
    function createEl(str) {
        var frag = document.createDocumentFragment(),
            temp = document.createElement('div');

        temp.innerHTML = str;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function insertCheckoutHTML(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
        guSubstituteContent(document.getElementById('checkout'));
    }

    jQuery(function () {
        insertCheckoutHTML(createEl(checkout_html), document.getElementById('offer'));
    });


    jQuery(function () {
        // OuterHTML Fn
        jQuery.fn.outerHTML = function () {
            return jQuery('<div />').append(this.eq(0).clone()).html();
        };

        // Global Vars
        var body = jQuery('body');

        //== Set up ==//

        // Show or hide elements depending on window size
        jQuery(window).on('resize', function () {
            // Wide layout
            if (jQuery(window).width() > 1030) {
                // Show summary
                jQuery('#checkout #summary').show();
                jQuery('#checkout #summary .toggle').show();

                // // Set max height on summary
                // var height = jQuery('#checkout #data').height() - 5;
                // jQuery('#checkout #summary').css('max-height',height);

                // Regular layout
            } else {
                // Hide summary
                jQuery('#checkout #summary .toggle').hide();
            }
        }).resize();

        // Add the promo code
        if (gu_qs['gu47'] == 1) {
            jQuery('#checkout #data .promoWrap').addClass('show');
        }

        // Add the CVC (?) popup text
        function translate8() {
            jQuery('#checkout #data form .inp.cc-cvc .question').attr('data-title', guSubstituteString('3-digit security code usually found on the back of your card. American Express cards have a 4-digit code located on the front.'));
        }

        guRemoteDataRegisterCallback(translate8);

        // Add the phone (?) popup text
        function translate26() {
            jQuery('#checkout #data form .inp.phone .question').attr('data-title', guSubstituteString('In case we need to contact you about your order'));
        }

        guRemoteDataRegisterCallback(translate26);

        //== Form functions ==//

        // Declare shipping field vars
        var shippingTitle = jQuery('#main-form .title');
        var emailField = jQuery('#main-form .email');
        var fnameField = jQuery('#main-form .fname');
        var lnameField = jQuery('#main-form .lname');
        var address1Field = jQuery('#main-form .address1');
        var address2Field = jQuery('#main-form .address2');
        var cityField = jQuery('#main-form .city');
        var countryField = jQuery('#main-form .country');
        var stateField = jQuery('#main-form .state');
        var zipField = jQuery('#main-form .zip');
        var phoneField = jQuery('#main-form .phone');

        //Shipping fields
        var shipping_country = jQuery('#checkout #data form .inp.country');
        var shipping_countryVal /*= jQuery('#checkout #data form .inp.country select').val()*/;
        var shipping_state = jQuery('#checkout #data form .inp.state');
        var shipping_stateInput = jQuery('#checkout #data form .inp.state input');
        var shipping_stateLabel = jQuery('#checkout #data form .inp.state label');
        var shipping_zipInput = jQuery('#checkout #data form .inp.zip input');
        var shipping_zipLabel = jQuery('#checkout #data form .inp.zip label');
        var shipping_zip = jQuery('#checkout #data form .inp .zip');
        var shipping_city = jQuery('#checkout #data form .inp.city');
        var shipping_cityInput = jQuery('#checkout #data form .inp.city input');
        var shipping_cityLabel = jQuery('#checkout #data form .inp.city label');

        // Billing address fields
        var billing_address_selection = '#billShipOptions .billShip';
        var billing_address_form = jQuery('#billing_address_form');
        var billing_fnameField = jQuery('#billing_address_form .billing_fname');
        var billing_lnameField = jQuery('#billing_address_form .billing_lname');
        var billing_address1Field = jQuery('#billing_address_form .billing_address1');
        var billing_address2Field = jQuery('#billing_address_form .billing_address2');
        var billing_cityField = jQuery('#billing_address_form .billing_city');
        var billing_countryField = jQuery('#billing_address_form .billing_country');
        var billing_stateField = jQuery('#billing_address_form .billing_state');
        var billing_zipField = jQuery('#billing_address_form .billing_zip');

        //Billing inputs
        var billing_fnameInput = jQuery('#billing_address_form .billing_fname input');
        var billing_lnameInput = jQuery('#billing_address_form .billing_lname input');
        var billing_address1Input = jQuery('#billing_address_form .billing_address1 input');
        var billing_address2Input = jQuery('#billing_address_form .billing_address2 input');
        var billing_cityInput = jQuery('#billing_address_form .billing_city input');
        var billing_stateInput = jQuery('#billing_address_form .billing_state input');
        var billing_stateLabel = jQuery('#billing_address_form .billing_state label');
        var billing_countryInput = jQuery('#billing_address_form .billing_country select');
        var billing_zipInput = jQuery('#billing_address_form .billing_zip input');
        var billing_zipLabel = jQuery('#billing_address_form .billing_zip label');

        //Billing Vals
        var billing_fnameField_val;
        var billing_lnameField_val;
        var billing_address1Field_val;
        var billing_address2Field_val;
        var billing_cityField_val;
        var billing_countryField_val;
        var billing_stateField_val;
        var billing_zipField_val;


        function BuildShippingForm(country) {
            //reset all fields
            jQuery('#checkout #data form .inp.country').removeClass('noState');
            jQuery('#checkout #data form .inp.country').removeClass('noZip');
            jQuery('#checkout #data form .inp.state').removeClass('noZip');
            jQuery('#checkout #data form .inp.zip').removeClass('noState');
            jQuery('#checkout #data form .inp.state').show();

            //Check for a custom form order
            var customOrder = false;
            var builder = "";
            var validCountry = false;

            // !!make sure current country is a valid shipable country!!
            for (var i = 0; i < gu_remote_data.shippingCountries.length; i++) {
                if (gu_remote_data.shippingCountries[i][0] == country.toUpperCase()) {
                    validCountry = true;
                    break;
                }
            }


            // and pull builder text from remote data if so
            if (validCountry && gu_countries[country.toLowerCase()].formBuilder.length) {
                builder = gu_countries[country.toLowerCase()].formBuilder;
                customOrder = true;
            } else {
                //default val
                if (mobileDevice) {
                    builder = "e-100_f-49s_l-50_a1-49s_a2-50_ci-100_s-49s_z-50_co-100_p-100";
                } else {

                    builder = "e-100_f-49s_l-50_a1-49s_a2-50_ci-100_co-39s_s-29s_z-30_p-100";
                }
            }

            //var builder = "e-100_f-49_l-49_a1-100_a2-100_ci-100_co-40_s-28_z-28_p-100";
            //var builder = "e-100_l-49s_f-49_z-100_co-100_s-49s_ci-49_a1-70s_a2-28_p-100";

            var fields = builder.split('_');
            var fieldTypes = [];
            var fieldWidths = [];


            for (var i = 0; i < fields.length; i++) {
                fieldTypes.push(fields[i].split("-")[0]);
                fieldWidths.push(fields[i].split("-")[1]);
            }

            for (var i = 0; i < fieldTypes.length; i++) {

                //Identify field
                var fieldName = "";
                var theField;

                switch (fieldTypes[i]) {
                    case "e":
                        fieldName = 'email';
                        theField = emailField;
                        break;
                    case "f":
                        fieldName = 'fname';
                        theField = fnameField;
                        break;
                    case "l":
                        fieldName = 'lname';
                        theField = lnameField;
                        break;
                    case "a1":
                        fieldName = 'address1';
                        theField = address1Field;
                        break;
                    case "a2":
                        fieldName = 'address2';
                        theField = address2Field;
                        break;
                    case "ci":
                        fieldName = 'city';
                        theField = cityField;
                        break;
                    case "co":
                        fieldName = 'country';
                        theField = countryField;
                        break;
                    case "s":
                        fieldName = 'state';
                        theField = stateField;
                        break;
                    case "z":
                        fieldName = 'zip';
                        theField = zipField;
                        break;
                    case "p":
                        fieldName = 'phone';
                        theField = phoneField;
                        break;
                    default:
                        console.log("Error: unrecognized field code");
                }

                //check for needed space after field
                var newWidth = fieldWidths[i];

                var needSpace = false;

                if (newWidth.slice(-1) == 's') {
                    //space needed after field
                    needSpace = true;
                    //remove 's' so it's just val
                    newWidth = newWidth.substring(0, newWidth.length - 1);
                }

                jQuery("#shipping-section").append(theField);

                //clear out width classes
                jQuery("#shipping-section ." + fieldName).removeClass("fullWidth");
                jQuery("#shipping-section ." + fieldName).removeClass("formSpace");

                //reset display
                jQuery("#shipping-section ." + fieldName).css('display', '');

                if (newWidth == "100") {
                    jQuery("#shipping-section ." + fieldName).addClass("fullWidth");
                } else if (newWidth == "0") {
                    jQuery("#shipping-section ." + fieldName).css('display', 'none');
                    jQuery("#shipping-section ." + fieldName).removeClass('bad filled');
                    jQuery("#shipping-section ." + fieldName + "input").val('');
                } else {
                    var theWidth = newWidth + '%';
                    jQuery("#shipping-section ." + fieldName).css('width', theWidth);

                    if (needSpace) {
                        jQuery("#shipping-section ." + fieldName).addClass("formSpace");
                    }

                }

            }

            //if using the default form order, use country info for postal & state field visibility
            //otherwise, builder will dictate

            if (validCountry && !customOrder) {

                setCountryFields(shipping_country, country, shipping_state);
            }

            //Set proper labels
            setCountryLabels(country, shipping_stateInput, shipping_stateLabel, shipping_zipInput, shipping_zipLabel, shipping_zip);

            // move promo code
            // if (gu_qs['gu47'] == 1) {
            //     jQuery('#checkout #data form .inp.phone').after(jQuery('#checkout #data .promoWrap'));
            // }
        }


        function BuildBillingAddressForm(country) {

            //reset all fields
            jQuery('#checkout #data form .inp.billing_country').removeClass('noState');
            jQuery('#checkout #data form .inp.billing_country').removeClass('noZip');
            jQuery('#checkout #data form .inp.billing_state').removeClass('noZip');
            jQuery('#checkout #data form .inp.billing_zip').removeClass('noState');
            jQuery('#checkout #data form .inp.billing_state').fadeIn();

            //Check for a custom form order
            var customOrder = false;
            var builder = "";
            var validCountry = false;

            // !!make sure current country is a valid shipable country!!
            for (var i = 0; i < gu_remote_data.allCountries.length; i++) {
                if (gu_remote_data.allCountries[i][0] == country.toUpperCase()) {
                    validCountry = true;
                    break;
                }
            }

            // and pull builder text from remote data if so
            if (validCountry && gu_countries[country.toLowerCase()].formBuilder.length) {
                builder = gu_countries[country.toLowerCase()].formBuilder;
                customOrder = true;
            } else {
                //default val
                if (mobileDevice) {
                    builder = "e-100_f-49s_l-50_a1-49s_a2-50_ci-100_s-49s_z-50_co-100_p-100";
                } else {

                    builder = "e-100_f-49s_l-50_a1-49s_a2-50_ci-100_co-39s_s-29s_z-30_p-100";
                }
            }

            //var builder = "e-100_f-49_l-49_a1-100_a2-100_ci-100_co-40_s-28_z-28_p-100";
            //var builder = "e-100_l-49s_f-49_z-100_co-100_s-49s_ci-49_a1-70s_a2-28_p-100";

            var fields = builder.split('_');
            var fieldTypes = [];
            var fieldWidths = [];


            for (var i = 0; i < fields.length; i++) {
                fieldTypes.push(fields[i].split("-")[0]);
                fieldWidths.push(fields[i].split("-")[1]);
            }

            for (var i = 0; i < fieldTypes.length; i++) {

                //Identify field
                var fieldName = "";
                var theField;

                switch (fieldTypes[i]) {
                    case "e":
                        fieldName = '';
                        theField = undefined;
                        break;
                    case "f":
                        fieldName = 'billing_fname';
                        theField = billing_fnameField;
                        break;
                    case "l":
                        fieldName = 'billing_lname';
                        theField = billing_lnameField;
                        break;
                    case "a1":
                        fieldName = 'billing_address1';
                        theField = billing_address1Field;
                        break;
                    case "a2":
                        fieldName = 'billing_address2';
                        theField = billing_address2Field;
                        break;
                    case "ci":
                        fieldName = 'billing_city';
                        theField = billing_cityField;
                        break;
                    case "co":
                        fieldName = 'billing_country';
                        theField = billing_countryField;
                        break;
                    case "s":
                        fieldName = 'billing_state';
                        theField = billing_stateField;
                        break;
                    case "z":
                        fieldName = 'billing_zip';
                        theField = billing_zipField;
                        break;
                    case "p":
                        fieldName = '';
                        theField = undefined;
                        break;
                    default:
                        console.log("Error: unrecognized field code");
                }

                //check for needed space after field
                var newWidth = fieldWidths[i];

                var needSpace = false;

                if (newWidth.slice(-1) == 's') {
                    //space needed after field
                    needSpace = true;
                    //remove 's' so it's just val
                    newWidth = newWidth.substring(0, newWidth.length - 1);
                }

                //make sure it's not an unneeded field
                if (typeof theField != 'undefined') {

                    jQuery("#billing_address_form").append(theField);

                    //clear out width classes
                    jQuery("#billing_address_form ." + fieldName).removeClass("fullWidth");
                    jQuery("#billing_address_form ." + fieldName).removeClass("formSpace");

                    //reset display
                    jQuery("#billing_address_form ." + fieldName).css('display', '');

                    if (newWidth == "100") {
                        jQuery("#billing_address_form ." + fieldName).addClass("fullWidth");
                    } else if (newWidth == "0") {
                        jQuery("#billing_address_form ." + fieldName).css('display', 'none');
                    } else {
                        var theWidth = newWidth + '%';
                        jQuery("#billing_address_form ." + fieldName).css('width', theWidth);

                        if (needSpace) {
                            jQuery("#billing_address_form ." + fieldName).addClass("formSpace");
                        }

                    }
                }


            }

            //if using the default form order, use country info for postal & state field visibility
            //otherwise, builder will dictate

            if (validCountry && !customOrder) {
                setCountryFields(billing_countryField, country, billing_stateField);
            }


            //Set proper labels
            setCountryLabels(country, billing_stateInput, billing_stateLabel, billing_zipInput, billing_zipLabel, billing_zipField);
        }

        jQuery('.billing_country select').change(function () {
            billing_countryField_val = jQuery('.billing_country select').val();
            BuildBillingAddressForm(billing_countryField_val);
        });


        //build country dropdown
        function BuildCountryList() {
            //build shipping dropdown
            for (var i = 0; i < gu_remote_data.shippingCountries.length; i++) {

                if (gu_country.toUpperCase() === gu_remote_data.shippingCountries[i][0]) {

                    //console.log("matched " + gu_country + " with " + gu_remote_data["shippingCountries"][i][0]);

                    var curValue = gu_remote_data.shippingCountries[i][0].toLowerCase();

                    //add current country as first and selected
                    jQuery('#shipping-section .inp select').prepend('<option value="" disabled="" role="separator">â€”â€”â€”â€”â€”â€”â€”â€”â€”â€”</option>');

                    jQuery('#shipping-section .inp select').prepend(jQuery('<option>', {
                        value: curValue,
                        text: gu_remote_data.shippingCountries[i][1],
                        selected: 'selected'
                    }));


                }

                //console.log("added country: " + gu_remote_data["shippingCountries"][i][0].toLowerCase() + ", " + gu_remote_data["shippingCountries"][i][1]);
                jQuery('#shipping-section .inp select').append(jQuery('<option>', {
                    value: gu_remote_data.shippingCountries[i][0].toLowerCase(),
                    text: gu_remote_data.shippingCountries[i][1]
                }));
            }

            //set country to first option, current country
            jQuery('#shipping-section .inp select').val(jQuery('#shipping-section .inp select option:first').val());
            good(jQuery('#checkout #data form .inp.country select'));

            shipping_countryVal = jQuery('#checkout #data form .inp.country select').val();

            //build billing dropdown
            for (var i = 0; i < gu_remote_data.shippingCountries.length; i++) {

                if (gu_country.toUpperCase() === gu_remote_data.shippingCountries[i][0]) {

                    //console.log("matched " + gu_country + " with " + gu_remote_data["shippingCountries"][i][0]);

                    var curValue = gu_remote_data.shippingCountries[i][0].toLowerCase();

                    //add current country as first and selected
                    jQuery('#billing_address_form .inp select').prepend('<option value="" disabled="" role="separator">â€”â€”â€”â€”â€”â€”â€”â€”â€”â€”</option>');

                    jQuery('#billing_address_form .inp select').prepend(jQuery('<option>', {
                        value: curValue,
                        text: gu_remote_data.shippingCountries[i][1],
                        selected: 'selected'
                    }));

                    //set country to first option, current country
                    jQuery('#billing_address_form .inp select').val(jQuery('#billing_address_form .inp select option:first').val());
                    good(billing_countryInput);
                }

                //console.log("added country: " + gu_remote_data["shippingCountries"][i][0].toLowerCase() + ", " + gu_remote_data["shippingCountries"][i][1]);
                jQuery('#billing_address_form .inp select').append(jQuery('<option>', {
                    value: gu_remote_data.shippingCountries[i][0].toLowerCase(),
                    text: gu_remote_data.shippingCountries[i][1]
                }));
            }

            billing_countryField_val = jQuery('.billing_country select').val();

            BuildShippingForm(gu_country);
            BuildBillingAddressForm(gu_country);

            //when form is initially built, check for phone requirement
            checkPhoneRequirement();

            //setCountryLabels();
            jQuery('#checkout #data form .inp select').blur();
        }

        // Toggle Summary
        jQuery('#checkout #summary:not(.prep) .title').on('click', function () {
            jQuery(this).toggleClass('active');
            jQuery('#checkout #summary .toggle').slideToggle('slow');
        });

        // Form input focus
        jQuery('#checkout #data form .inp input, #checkout #data form .inp select, #checkout #data form .inp textarea').focus(function () {
            jQuery(this).parent(':not(.country)').find('label').fadeIn();
        });
        jQuery('#checkout #data form .inp input, #checkout #data form .inp select, #checkout #data form .inp textarea').blur(function () {
            if (jQuery(this).val() === '') {
                jQuery(this).parent().removeClass('filled');
                jQuery(this).parent(':not(.country)').find('label').fadeOut('fast');
            } else {
                jQuery(this).parent().addClass('filled');
            }
        });

        // Toggle question popup
        var question = jQuery('#checkout .question');
        question.on('click', function () {
            if (question.hasClass('active')) {
                question.removeClass('active');
            } else {
                question.addClass('active');
            }
        });

        // Close question popup if click outside
        jQuery(document).on('mouseup touchstart', function (e) {
            if (!question.is(e.target) && question.has(e.target).length === 0 && question.hasClass('active')) {
                question.removeClass('active');
            }
        });


        // Country
        function setCountryLabels(countryVal, stateInput, stateLabel, zipInput, zipLabel, zip) {
            // Vars
            var theCountryVal = countryVal;
            var theStateInput = stateInput;
            var theStateLabel = stateLabel;
            var theZipInput = zipInput;
            var theZipLabel = zipLabel;
            var theZip = zip;

            // var ccZipInput = jQuery('#checkout #data form .inp.cc-zip input');
            // var ccZipLabel = jQuery('#checkout #data form .inp.cc-zip label');

            //set state to proper label
            function translate9() {
                //shipping
                var shipping_stateLabelText = guSubstituteString(gu_countries[shipping_countryVal.toLowerCase()].provinceLabel);
                shipping_stateLabel.text(shipping_stateLabelText);
                shipping_stateInput.attr('placeholder', shipping_stateLabelText);

                //city label
                var shipping_cityLabelText = guSubstituteString(gu_countries[shipping_countryVal.toLowerCase()].cityLabel);
                shipping_cityLabel.text(shipping_cityLabelText);
                shipping_cityInput.attr('placeholder', shipping_cityLabelText);

                //billing
                var billing_stateLabelText = guSubstituteString(gu_countries[billing_countryField_val.toLowerCase()].provinceLabel);
                billing_stateLabel.text(billing_stateLabelText);
                billing_stateInput.attr('placeholder', billing_stateLabelText);
            }

            guRemoteDataRegisterCallback(translate9);

            //set zip to proper label
            function translate10() {
                //shipping
                var shipping_zipLabelText = guSubstituteString(gu_countries[shipping_countryVal.toLowerCase()].postalLabel);
                shipping_zipLabel.text(shipping_zipLabelText);
                shipping_zipInput.attr('placeholder', shipping_zipLabelText);
                shipping_zip.closest('.zip').fadeIn();
                //billing
                var billing_zipLabelText = guSubstituteString(gu_countries[billing_countryField_val.toLowerCase()].postalLabel);
                billing_zipLabel.text(billing_zipLabelText);
                billing_zipInput.attr('placeholder', billing_zipLabelText);
                billing_zipField.closest('.zip').fadeIn();
            }

            guRemoteDataRegisterCallback(translate10);
        }

        //Show/Hide Postal/State
        function setCountryFields(country, countryVal, state) {
            // Vars
            var theCountry = country;
            var theCountryVal = countryVal;
            var theState = state;

            var hasZip = gu_countries[countryVal.toLowerCase()].postal;
            var hasProvince = gu_countries[countryVal.toLowerCase()].provinces.length;

            if (hasZip == 0 && hasProvince == 0) {
                hideBoth();
            } else if (hasZip == 0 && hasProvince != 0) {
                hideZip();
                showState();
            } else if (hasZip == 1 && hasProvince == 0) {
                hideState();
                showZip();
            } else {
                showAll();
            }

            // Show everything
            function showAll() {
                showState();
                showZip();
            }

            // Show State
            function showState() {
                if (!mobileDevice) {
                    country.removeClass('noState');
                } else {
                    zip.closest('.zip').removeClass('noState');

                }

                state.fadeIn();
            }

            // Show Zip
            function showZip() {
                if (!mobileDevice) {
                    country.removeClass('noZip');
                }

                state.removeClass('noZip');
            }

            // Hide state
            function hideState() {
                if (!mobileDevice) {
                    country.addClass('noState');
                } else {
                    zip.closest('.zip').addClass('noState');
                }
                state.removeClass('bad filled');
                state.hide();
                state.find('input').val('');
            }

            // Hide Zip
            function hideZip() {
                state.addClass('noZip');
                zip.closest('.zip').hide();
            }

            // Hide Both - Country Only
            function hideBoth() {
                if (!mobileDevice) {
                    country.addClass('noState').addClass('noZip');
                }

                zip.closest('.zip').hide();
                state.hide();

            }
        }

        //Check all fields related to country selection
        //to make sure they're still valid
        jQuery('.country select').change(function () {
            shipping_countryVal = jQuery('.country select').val();
            setCountryLabels(shipping_countryVal, shipping_stateInput, shipping_stateLabel, shipping_zipInput, shipping_zipLabel, shipping_zip);
            if (state.is(':visible') && jQuery.trim(state.val()).length > 0) {
                checkState(state, shipping_countryVal);
            }
            checkZip();
            BuildShippingForm(shipping_countryVal);

            //check whether phone is required (non US or CA)
            checkPhoneRequirement();
            checkPhone();
        });

        //== VERIFICATION == //

        function checkState(curState, curCountry) {
            //var input = state;
            //var curCountry = country.val();

            //check for inputted state in country data
            var foundMatch = false;
            for (var i = 0; i < gu_countries[curCountry].provinces.length; i++) {
                //first check for province check exception
                if (gu_countries[curCountry].freeProvince === '1') {
                    //if set to free province, all input will succeed
                    foundMatch = true;
                } else if (curState.val().toLowerCase() === gu_countries[curCountry].provinces[i].name.toLowerCase()) {
                    //console.log("MATCHED!");
                    foundMatch = true;
                } else if (curState.val().toLowerCase() === gu_countries[curCountry].provinces[i].code) {
                    //console.log("MATCHED!");
                    foundMatch = true;
                } else {
                    //foundMatch stays false
                }
            }

            //haven't typed in anything
            if (curState.val() === '') {
                neutral(curState);
            } else if (foundMatch) {
                good(curState);
            } else {
                bad(curState);
            }
        }

        function checkZip() {
            var regex = /[0-9]/;
            var zipVal = jQuery('#checkout #data form .inp.zip input').val();
            var zipInput = jQuery('#checkout #data form .inp.zip input');
            var shipping_method = jQuery('#checkout #data .method');

            // Validate Zip
            if (zipVal === '') {
                neutral(jQuery(zipInput));
            } else /*if (regex.test(zipVal) /* && country.val() === 'US' )*/ {

                //reformat for Canadian postal codes
                var shipping_countryVal = jQuery('#checkout #data form .inp.country select').val();

                if (shipping_countryVal == 'ca') {
                    if (zipVal.length == 7 && zipVal[3] == ' ') {
                        //it's perfect, do nothing
                    }
                    else if (zipVal.length == 7 && zipVal[3] == '-') {
                        zipVal = zipVal.replace('-', ' ');
                        zipInput.val(zipVal);
                    }
                    else if (zipVal.length == 6) {
                        zipVal = zipVal.slice(0, 3) + ' ' + zipVal.slice(3);
                        zipInput.val(zipVal);
                    }

                }

                if (shipping_method.hasClass('refresh') /*&& zip_focused_val != jQuery(this).val()*/) {
                    shipping_method.find('.refresh').click();
                } else {
                    good(jQuery(zipInput));
                }
                // copyToCC();
            }
            // else if (country.val() !== 'US' && jQuery.trim(zipVal).length > 0) {
            //     if (!shipping_method.hasClass('refresh') /*&& zip_focused_val != jQuery(this).val()*/) {
            //         shipping_method.find('.refresh').click();
            //     } else {
            //         good(jQuery(this));
            //     }
            //     // copyToCC();
            // }

            // else {
            //     bad(jQuery(this));
            // }
        }

        function checkEmail() {
            // Email.
            var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var emailInput = email.val();

            if (jQuery('#checkout #data .submit').hasClass('invalid-email')) {
                neutral(email);
            } else if (emailRegex.test(emailInput)) {
                good(email);
            } else {
                bad(email);
                /* return false; */
            }
        }

        function checkPhone() {
            // Phone number.
            var phoneRegex = /[0-9\-\(\)\s]+/;
            var phoneInput = phone.val();

            //check for country requirements here

            if (phoneRegex.test(phoneInput) && jQuery.trim(phoneInput).length > 0) {
                good(phone);
                phone.parent().addClass('acceptable');
            }
            else if (shipping_countryVal != 'us' || requirePhone == '1') {
                bad(phone);
                phone.parent().removeClass('acceptable');
            }
        }

        function checkPhoneRequirement() {
            var shipping_countryVal = jQuery('#checkout #data form .inp.country select').val();
            if (shipping_countryVal != 'us' || requirePhone == '1') {
                //phone is required and not automatically acceptable
                phone.parent().removeClass('acceptable');
                phone.attr('placeholder', guSubstituteString('Phone'));
            }
            else {
                //phone can auto be marked as acceptable to pass check, even if empty
                phone.parent().addClass('acceptable');
                if (phone.val() == '' && phone.parent().hasClass('bad')) {
                    phone.parent().removeClass('bad');
                }
                phone.attr('placeholder', guSubstituteString('Phone (optional)'));
            }

            //checkPhone();
        }

        //== Form validation ==//

        // Vars
        var email = jQuery('#checkout #data form .inp.email input');
        var fname = jQuery('#checkout #data form .inp.fname input');
        var lname = jQuery('#checkout #data form .inp.lname input');
        var address1 = jQuery('#checkout #data form .inp.address1 input');
        var address2 = jQuery('#checkout #data form .inp.address2 input');
        var city = jQuery('#checkout #data form .inp.city input');
        var country = jQuery('#checkout #data form .inp.country select');
        var state = jQuery('#checkout #data form .inp.state input');
        var zip = jQuery('#checkout #data form .inp.zip input');
        var phone = jQuery('#checkout #data form .inp.phone input');

        var ccnumber = jQuery('#checkout #data form .inp.cc-number input');
        var ccexp = jQuery('#checkout #data form .inp.cc-exp input');
        var cccvc = jQuery('#checkout #data form .inp.cc-cvc input');
        // var cczip = jQuery('#checkout #data form .inp.cc-zip input');

        var shippingForm = jQuery('#checkout #data form .shipping');
        var shippingItem = '#checkout #data .method .item';
        var shipping_rates;

        // == REMOTE CALLBACK FOR BUILDING COUNTRY LIST
        guRemoteDataRegisterCallback(BuildCountryList);

        // Show label
        function showLabel(input) {
            if (input.val() === '') {
                input.fadeIn();
            }
        }

        // Reset label.
        function resetLabel(input) {
            var label = input.attr('placeholder');
            input.parent().find('label').removeClass('error').html(label);
        }

        // Neutralize input
        function neutral(input) {
            input.parent().removeClass('bad good'); // Mark neutral.
            resetLabel(input); // Reset label.
        }

        // Good input
        function good(input) {
            input.parent().removeClass('bad').addClass('good'); // Mark green.
            input.parent().addClass('filled'); // Add padding.
            resetLabel(input); // Reset label.
        }

        // Bad input.
        function bad(input, message) {
            input.parent().removeClass('good').addClass('bad'); // Mark red.
            input.parent().addClass('filled'); // Add padding.
            input.parent().find('label').fadeIn(); // Show label.
            input.parent().find('span').remove(); // Remove span.

            // Add required.
            var label = input.parent().find('label');

            if (input.parent().hasClass('email')) {
                if (input.val().indexOf('@') === -1 && input.val() !== '') {
                    label.addClass('error')

                    function translate11() {
                        label.html(guSubstituteString('Email must have an @'));
                    }

                    guRemoteDataRegisterCallback(translate11);
                } else if (jQuery.trim(input.val()).length > 0) {
                    label.addClass('error')

                    function translate12() {
                        label.html(guSubstituteString('Incorrect email address'));
                    }

                    guRemoteDataRegisterCallback(translate12);
                } else {
                    label.addClass('error')

                    function translate13() {
                        label.html(guSubstituteString('Required'));
                    }

                    guRemoteDataRegisterCallback(translate13);
                }
            } else if (input.parent().hasClass('state')) {
                if (jQuery.trim(input.val()).length > 0) {
                    label.addClass('error')

                    function translate14() {
                        label.html(guSubstituteString('Incorrect State name'));
                    }


                    // fire event for typing bad state/province
                    gulog.fireAndLogEvent("Incorrect State Input", null, null, "User Input", input.val());

                    guRemoteDataRegisterCallback(translate14);
                } else {
                    label.addClass('error')

                    function translate15() {
                        label.html(guSubstituteString('Required'));
                    }

                    guRemoteDataRegisterCallback(translate15);
                }
            } else if (message != undefined) {
                label.addClass('error').html(message);
            } else {
                label.addClass('error')

                function translate16() {
                    label.html(guSubstituteString('Required'));
                }

                guRemoteDataRegisterCallback(translate16);
            }
        }


        //== Real-time validation ==//

        // Reset labels
        email.add(phone).add(fname).add(lname).add(address1).add(address2).add(city).add(state).add(zip).focus(function () {
            var input = jQuery(this);
            resetLabel(input);
            neutral(input);
        });

        // Default inputs.
        lname.add(country).add(address1).add(address2).add(city).keyup(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            }
        });

        // First name (optional)
        lname.blur(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            } else if (jQuery.trim(input.val()).length > 0) {
                good(input);
                submitName(fname.val(), input.val());
            }
        });

        /*country.add*/
        (fname).add(lname).add(address1).add(city).blur(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            } else if (jQuery.trim(input.val()).length > 0) {
                good(input);
            } else {
                bad(input);
            }
        });

        // Address 2 (optional)
        address2.blur(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            } else if (jQuery.trim(input.val()).length > 0) {
                good(input);
            }
        });

        // promo
        var promoInput = '#checkout #data .promoWrap .inp.promo input';
        var promoBtn = jQuery('#checkout #data .inp.promo button');

        promoBtn.on('click', function () {
            submitPromoCode(jQuery(promoInput).val());
        });

        jQuery(promoInput).blur(function () {
            submitPromoCode(jQuery(promoInput).val());

            // hide label
            if (jQuery(promoInput).val() == '') {
                jQuery(promoInput).parent().find('label').fadeOut();
            } else {
                jQuery(promoInput).parent().addClass('filled');
            }
        });

        jQuery(promoInput).focus(function () {
            var input = jQuery(this);
            input.parent().removeClass('good bad');
            resetLabel(input);
            // neutral(input);

            // show label
            jQuery(promoInput).parent().find('label').fadeIn();
        });

        // Email.
        email.on('keyup', function () {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var input = jQuery(this);
            if (regex.test(input.val()) && jQuery.trim(input.val()).length > 0) {
                // nothing
            } else {
                neutral(input);
            }
        });

        email.blur(function () {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var input = jQuery(this);
            var shipping_method = jQuery('#checkout #data .method');

            if (input.val() === '') {
                neutral(input);
            } else if (regex.test(input.val()) && jQuery.trim(input.val()).length > 0 && !jQuery('#checkout #data .submit').hasClass('invalid-email')) {
                if (shipping_method.hasClass('refresh')) {
                    neutral(input);
                    shipping_method.find('.refresh').click();
                } else {
                    good(input);
                    submitEmail(input.val());
                }
            } else {
                if (shipping_method.hasClass('refresh')) {
                    neutral(input);
                    shipping_method.find('.refresh').click();
                } else {
                    bad(input);
                }
            }
        });

        // Phone number .
        phone.on('blur', function () {
            // var regex = /[0-9\-\(\)\s]+/;
            // var input = jQuery(this);
            //
            // if (input.val() === '' && requirePhone !== '1') {
            //     neutral(input);
            // } else if (regex.test(input.val()) && jQuery.trim(input.val()).length > 0) {
            //     good(input);
            //     input.parent().addClass('acceptable');
            // } else {
            //     if (requirePhone === '1') {
            //         bad(input);
            //         input.parent().removeClass('acceptable');
            //     }
            // }

            checkPhoneRequirement();
            checkPhone();
        });

        phone.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });

        state.blur(function () {

            //strip out spaces
            var val = state.val();

            //remove any double spaces
            val = val.replace(/  +/g, ' ');
            //remove any whitespace before or after
            val = val.trim();

            state.val(val);

            checkState(state, country.val());

            // Refresh shipping rates
            var shipping_method = jQuery('#checkout #data .method');
            if (shipping_method.hasClass('refresh')) {
                shipping_method.find('.refresh').click();
            }
        });

        state.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });

        // Zip.
        // var zip_focused_val;
        // zip.focus(function() {
        //     zip_focused_val = zip.val();
        // });

        zip.blur(function () {
            checkZip();
        });

        zip.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });

        // Submit shipping info and get shipping rates
        jQuery('#checkout #data .method .refresh').on('click', function () {
            jQuery(this).closest('.method').removeClass('refresh');
            shippingInfoClick('refresh');
        });

        // Show shipping method refresh if input is different
        var shipping_input;
        var shipping_inputs = '#checkout #data form #shipping-section .inp:not(.fname):not(.lname):not(.cc-number):not(.cc-exp):not(.cc-cvc):not(.cc-zip):not(.promo) input, #checkout #data form .inp select';
        var shipping_method = jQuery('#checkout #data .method');

        jQuery(shipping_inputs).focus(function () {
            if (jQuery('#checkout').hasClass('step2') /*&& !shipping_method.hasClass('refresh')*/) {
                shipping_input = jQuery(this).val();
            }
        });

        jQuery(shipping_inputs).keyup(function () {
            if (jQuery('#checkout').hasClass('step2')) {
                // if (shipping_input == jQuery(this).val() && shipping_method.hasClass('refresh')) {
                //     shipping_method.removeClass('refresh');
                // } else {
                //     shipping_method.addClass('refresh').removeClass('ready');
                // }
                shipping_method.addClass('refresh').removeClass('ready');
            }
        });

        jQuery(shipping_inputs).blur(function () {
            if (jQuery('#checkout').hasClass('step2') && shipping_input != jQuery(this).val()) {
                shippingInfoClick();
            }
        });

        // CC input masks.
        ccnumber.payment('formatCardNumber');
        ccexp.payment('formatCardExpiry');
        cccvc.payment('formatCardCVC');

        var brand = jQuery('#checkout #data .payment .cc');

        // CC Number.
        ccnumber.blur(function () {
            // Reset card type.
            brand.find('img').each(function () {
                jQuery(this).addClass('active');
            });

            // Validate
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            } else if (jQuery.payment.validateCardNumber(ccnumber.val())) {
                good(jQuery(this));

                // Card type.
                var cardType = jQuery.payment.cardType(jQuery(this).val());
                brand.find('img').removeClass('active');
                brand.find('.' + cardType + '').addClass('active');


                // fire event
                gulog.fireAndLogEvent("Customer Payment Info", null, null, "Field", "ccnumber");
            } else {
                var error_message = guSubstituteString('Incorrect credit card number');
                bad(jQuery(this), error_message);

                // fire event
                gulog.fireAndLogEvent("Customer Payment Info", null, null, "Field", "ccnumber");
            }


        });

        ccnumber.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            } else {
                // Card type.
                var cardType = jQuery.payment.cardType(jQuery(this).val());

                // Reset card type.
                brand.find('img').each(function () {
                    jQuery(this).addClass('active');
                });

                // Set brand
                if (cardType !== null) {
                    brand.find('img').removeClass('active');
                    brand.find('.' + cardType + '').addClass('active');
                }
            }
        });

        // CC Exp.
        ccexp.blur(function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            } else if (jQuery.payment.validateCardExpiry(jQuery(this).payment('cardExpiryVal'))) {
                good(jQuery(this));

                // fire event
                gulog.fireAndLogEvent("Customer Payment Info", null, null, "Field", "ccexp");

            } else {
                var error_message = guSubstituteString('Incorrect date');
                bad(jQuery(this), error_message);

                // fire event
                gulog.fireAndLogEvent("Customer Payment Info", null, null, "Field", "ccexp");
            }

        });

        ccexp.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });

        // CC CVC.
        cccvc.blur(function () {
            // var cardType = jQuery.payment.cardType(jQuery(this).val());
            var cardType = jQuery.payment.cardType(ccnumber.val());
            var cccvc_size = jQuery.trim(cccvc.val()).length;
            var error_message = '';

            function translate19() {
                error_message = guSubstituteString('Incorrect CVC');
            }

            guRemoteDataRegisterCallback(translate19);

            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            } else if (ccnumber.parent().hasClass('good')) {
                if (cardType == 'visa' || cardType == 'mastercard' || cardType == 'discover') {
                    if (cccvc_size == 3) {
                        good(jQuery(this));
                    } else {
                        function translate20() {
                            bad(jQuery(this), error_message);
                        }

                        guRemoteDataRegisterCallback(translate20);
                    }
                } else if (cardType == 'amex') {
                    if (cccvc_size == 4) {
                        good(jQuery(this));
                    } else {
                        function translate21() {
                            bad(jQuery(this), error_message);
                        }

                        guRemoteDataRegisterCallback(translate21);
                    }
                } else if (cardType != 'visa' || cardType != 'mastercard' || cardType != 'discover' || cardType != 'amex') {
                    good(jQuery(this));
                } else {
                    function translate22() {
                        bad(jQuery(this), error_message);
                    }

                    guRemoteDataRegisterCallback(translate22);
                }

                // fire event
                gulog.fireAndLogEvent("Customer Payment Info", null, null, "Field", "cvc");

            } else {
                function translate23() {
                    bad(jQuery(this), error_message);
                }

                guRemoteDataRegisterCallback(translate23);

                // fire event
                gulog.fireAndLogEvent("Customer Payment Info", null, null, "Field", "cvc");
            }

        });

        cccvc.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });

        //== Billing Address validation ==//

        // Reset labels
        billing_fnameInput.add(billing_lnameInput).add(billing_address1Input).add(billing_address2Input).add(billing_cityInput).add(billing_stateInput).add(billing_zipInput).focus(function () {
            var input = jQuery(this);
            resetLabel(input);
            neutral(input);
        });

        // Default inputs.
        billing_lnameInput.add(billing_countryInput).add(billing_address1Input).add(billing_address2Input).add(billing_cityInput).keyup(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            }
        });

        // First name (optional)
        billing_fnameInput.blur(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            } else if (jQuery.trim(input.val()).length > 0) {
                good(input);
                submitName(input.val());
            }
        });

        /*country.add*/
        (billing_fnameInput).add(billing_lnameInput).add(billing_address1Input).add(billing_cityInput).blur(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            } else if (jQuery.trim(input.val()).length > 0) {
                good(input);
            } else {
                bad(input);
            }
        });

        // Address 2 (optional)
        billing_address2Input.blur(function () {
            var input = jQuery(this);
            if (input.val() === '') {
                neutral(input);
            } else if (jQuery.trim(input.val()).length > 0) {
                good(input);
            }
        });

        billing_stateInput.blur(function () {

            checkState(billing_stateInput, billing_countryInput.val());

        });

        billing_stateInput.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });


        billing_zipInput.blur(function () {
            // Validate Zip
            var regex = /[0-9]/;
            var input = jQuery(this);

            if (input.val() === '') {
                neutral(jQuery(input));
            } else if (regex.test(input.val()) /* && country.val() === 'US'*/) {
                good(jQuery(input));

            } else {
                bad(jQuery(input));
            }
        });

        billing_zipInput.on('keyup', function () {
            if (jQuery(this).val() === '') {
                neutral(jQuery(this));
            }
        });

        // Show shipping method refresh if input is different
        var payment_inputs = '#checkout #data form .inp.cc-number, #checkout #data form .inp.cc-exp, #checkout #data form .inp.cc-cvc';

        jQuery(payment_inputs).keyup(function () {
            if (jQuery('#checkout #data .payment .error-msg').is(':visible')) {
                jQuery('#checkout #data .payment').addClass('changed');
            }
        });

        //== Shipping ==//

        // Shipping method
        jQuery('body').on('click', shippingItem, function () {
            // Set as active
            jQuery(shippingItem + '.active').removeClass('active');
            jQuery(this).addClass('active');

            // Submit handle
            var handle = jQuery(this).attr('data-handle');
            choseShipping(handle);

            //Amplitude Event
            //gu_fire_event('CustomerShippingMethod', 'yes', handle);
        });


        //== Billing Address ==//

        // Billing adress
        jQuery(billing_address_selection).on('click', function () {

            //if not already active
            if (!jQuery(this).hasClass('active')) {

                //get selection
                if (jQuery(this).attr('id') == "billing_other") {

                    //show bill ship fields
                    jQuery(billing_address_form).slideDown();

                } else {

                    //hide bill ship fields
                    jQuery(billing_address_form).slideUp();
                }

            }

            // Toggle active
            jQuery(billing_address_selection + '.active').removeClass('active');
            jQuery(this).addClass('active');

            //Amplitude Event
            //gu_fire_event('CustomerBillingAddressSelection', 'yes');
        });

        //== Upsell ==//

        // Add Upsell Timer
        jQuery('.upsell.timer').each(function () {
            jQuery(this).prepend('<div class="timer"><p>Offer expires in <span>TIME</span></p></div>');
        });

        //== Submit Form ==//

        /*function verifyShippingInfo() {


            checkEmail();

            checkPhone();

            //Make sure there is just valid input in fields

            // First name.
            if (jQuery.trim(fname.val()).length > 0) { good(fname); }
            // else {bad(fname); }

            // Last name.
            if (jQuery.trim(lname.val()).length > 0) { good(lname); }
            else { bad(lname); }

            // Address 1.
            if (jQuery.trim(address1.val()).length > 0) { good(address1); }
            else { bad(address1); }

            // City.
            if (jQuery.trim(city.val()).length > 0) { good(city); }
            else { bad(city); }

            // State.
            if (state.is(':visible')) {
                if (jQuery.trim(state.val()).length > 0) { good(state); }
                else { bad(state); }
            }

            // Country.
            if (country.val() === '') { bad(country); }
            else { good(country); }

            checkZip();

            // check for custom billing address
            if (jQuery('#billing_address_section #billing_other').hasClass('active'))
            {
                verifyBillingInfo();
            }

        }

        function verifyBillingInfo() {

            //Make sure there is just valid input in fields
            //Capture Billing Vals
            billing_fnameField_val = jQuery('#billing_address_form .billing_fname input').val();
            billing_lnameField_val = jQuery('#billing_address_form .billing_lname input').val();
            billing_address1Field_val = jQuery('#billing_address_form .billing_address1 input').val();
            billing_address2Field_val = jQuery('#billing_address_form .billing_address2 input').val();
            billing_cityField_val = jQuery('#billing_address_form .billing_city input').val();
            billing_countryField_val = jQuery('#billing_address_form .billing_country select').val();
            billing_stateField_val = jQuery('#billing_address_form .billing_state input').val();
            billing_zipField_val = jQuery('#billing_address_form .billing_zip input').val();

            // First name.
            if (jQuery.trim(billing_fnameField_val).length > 0) { good(billing_fnameInput); }
            // else {bad(fname); }

            // Last name.
            if (jQuery.trim(billing_lnameField_val).length > 0) { good(billing_lnameInput); }
            else { bad(billing_lnameInput); }

            // Address 1.
            if (jQuery.trim(billing_address1Field_val).length > 0) { good(billing_address1Input); }
            else { bad(billing_address1Input); }

            // City.
            if (jQuery.trim(billing_cityField_val).length > 0) { good(billing_cityInput); }
            else { bad(billing_cityInput); }

            // State.
            if (billing_stateField.is(':visible')) {
                if (jQuery.trim(billing_stateField_val).length > 0) { good(billing_stateInput); }
                else { bad(billing_stateInput); }
            }

            // Country.
            if (billing_countryField_val === '') { bad(billing_countryInput); }
            else { good(billing_countryInput); }

            // Check Zip
            var regex = /[0-9]/;

            // Validate Zip
            if (billing_zipField_val === '') {
                neutral(jQuery(billing_zipField_val));
            }
            else if (regex.test(billing_zipField_val)) {
                good(jQuery(billing_zipInput));

            }
            else {
                bad(jQuery(billing_zipInput));
            }

        }*/

        function phoneIsInvalid() {
            // Phone
            var phoneInput = jQuery('#shipping-section').find('input[name=phone]');
            if (phoneInput.next().hasClass('error')) {
                return true;
            }
        }

        // Next btn
        jQuery('#checkout #data .submit.next').on('click', function () {
            // Verify Shipping Info
            // verifyShippingInfo();

            var submit_btn = jQuery('#checkout #data .submit');
            if (submit_btn.hasClass('showUp')) {

                //only proceed when payment info is valid
                if (ccnumber.parent().hasClass('good') &&
                    ccexp.parent().hasClass('good') &&
                    cccvc.parent().hasClass('good')) {

                    //hide default button and show upsell
                    submit_btn.hide();

                    jQuery('#main-form .upsell.current').show();

                    // Change btn
                    //submit_btn.text(submit_btn.data('default'));
                    setTimeout(function () {
                        submit_btn.removeClass('showUp');
                        submit_btn.text(submit_btn.data('default'));
                        //submit_btn.text(guSubstituteString("No Thanks, Complete Order"));
                        setTimeout(function () {
                            submit_btn.removeClass('next');
                        }, 500);
                    }, 500);

                    // scroll to inline Upsell
                    //adjust the height of the empty space to allow for full scrolling
                    var body_height = jQuery(window).height();
                    var upsell_height = jQuery('#main-form .upsell').height();
                    var checkout_empty_space = '#checkout #data .empty_space';


                    var empty_space_height = parseInt(body_height - upsell_height);

                    jQuery(checkout_empty_space).height(empty_space_height);

                    setTimeout(function () {

                        if (mobileDevice) {
                            var summaryHeight = jQuery('#checkout #summary').height();

                            jQuery('html, body').animate({
                                scrollTop: jQuery('#main-form .upsell').offset().top - summaryHeight
                            }, 1000);

                        }
                        else {
                            jQuery('html, body').animate({
                                scrollTop: jQuery('#main-form .upsell').offset().top
                            }, 1000);
                        }

                    }, 150);
                }
                else {
                    if (!ccnumber.parent().hasClass('good')) {
                        ccnumber.parent().addClass('bad');
                    }

                    if (!ccexp.parent().hasClass('good')) {
                        ccexp.parent().addClass('bad');
                    }

                    if (!cccvc.parent().hasClass('good')) {
                        cccvc.parent().addClass('bad');
                    }
                }


            }
            else {
                // Open the cc section.
                if (email.parent().hasClass('good') &&
                    lname.parent().hasClass('good') &&
                    address1.parent().hasClass('good') &&
                    city.parent().hasClass('good') &&
                    country.parent().hasClass('good') &&
                    !state.parent().hasClass('bad') &&
                    zip.parent().hasClass('good') &&
                    phone.parent().hasClass('acceptable')) {

                    // Add step 2 class to form
                    setTimeout(function () {
                        jQuery('#checkout').addClass('step2');
                    }, 500);

                    // Send shipping info
                    shippingInfoClick();

                    // Slide down shipping & cc
                    jQuery('#checkout #data .method, #checkout #data .payment').slideDown();

                    // Change btn

                    if (submit_btn.hasClass('next')) {

                        // scroll to shipping method

                        //check for upsell
                        if (jQuery('#main-form .upsell.current').length != 0) {
                            submit_btn.addClass('showUp');
                        } else {
                            //check for  inlineupsell
                            if (jQuery('#main-form .inlineUpsell.current').length != 0) {
                                jQuery('#main-form .inlineUpsell.current').show().addClass('shown');
                            }


                            submit_btn.text(submit_btn.data('default'));
                            setTimeout(function () {
                                submit_btn.removeClass('next');
                            }, 500);
                        }

                        setTimeout(function () {
                            var summaryHeight = jQuery('#checkout #summary').height();
                            jQuery('html, body').animate({
                                scrollTop: jQuery('#checkout #data .method').offset().top - summaryHeight
                            }, 1000);
                        }, 150);
                    }



                    // fire pixels

                    // fire event
                    gulog.fireAndLogEvent("View Credit Card Page", null, null, null, null);
                    //}
                } else {
                    if (!email.parent().hasClass('good')) {
                        email.parent().addClass('bad');
                    }

                    if (!lname.parent().hasClass('good')) {
                        lname.parent().addClass('bad');
                    }

                    if (!address1.parent().hasClass('good')) {
                        address1.parent().addClass('bad');
                    }

                    if (!city.parent().hasClass('good')) {
                        city.parent().addClass('bad');
                    }

                    if (!country.parent().hasClass('good')) {
                        country.parent().addClass('bad');
                    }

                    if (!state.parent().hasClass('good')) {
                        state.parent().addClass('bad');
                    }

                    if (!zip.parent().hasClass('good')) {
                        zip.parent().addClass('bad');
                    }
                }

                //if phone is required and not filled out, highlight it now
                if (!phone.parent().hasClass('acceptable')) {
                    phone.parent().addClass('bad');
                }
            }
        });

        // Submit form
        jQuery('body').on('click', '#checkout #data .submit:not(.next)', function (event) {
            event.preventDefault();

            // Verify Shipping Info
            // verifyShippingInfo();

            // if (requirePhone === "1" && phoneIsInvalid()) {
            //     return;
            // }

            // CC Number.
            if (jQuery.payment.validateCardNumber(ccnumber.val())) {
                good(ccnumber);
            } else {
                if (ccnumber.val() == '') {
                    bad(ccnumber);
                } else {
                    var error_message = guSubstituteString('Incorrect credit card number');
                    bad(ccnumber, error_message);
                }
            }

            // CC Exp.
            if (jQuery.payment.validateCardExpiry(ccexp.payment('cardExpiryVal'))) {
                good(ccexp);
            } else {
                if (ccexp.val() == '') {
                    bad(ccexp);
                } else {
                    var error_message = guSubstituteString('Incorrect date');
                    bad(ccexp, error_message);
                }
            }

            // CC CVC.
            var cardType = jQuery.payment.cardType(ccnumber.val());
            var cccvc_size = jQuery.trim(cccvc.val()).length;
            var error_message = guSubstituteString('Incorrect CVC');

            if (cccvc.val() == '') {
                bad(cccvc);
            } else if (ccnumber.parent().hasClass('good')) {
                if (cardType == 'visa' || cardType == 'mastercard' || cardType == 'discover') {
                    if (cccvc_size == 3) {
                        good(cccvc);
                    } else {
                        console.log('bad cvc');
                        bad(cccvc, error_message);
                    }
                } else if (cardType == 'amex') {
                    if (cccvc_size == 4) {
                        good(cccvc);
                    } else {
                        bad(cccvc, error_message);
                    }
                }
            }

            // // CC CVC.
            // var cardType = jQuery.payment.cardType(cccvc.val());

            // if (jQuery.payment.validateCardCVC(cccvc.val(), cardType)) {good(cccvc); }
            // else {
            //     if (cccvc.val() == '') {
            //         bad(cccvc);
            //     }
            //     else {
            //         var error_message = cccvc.data('error');
            //         bad(cccvc, error_message);
            //     }
            // }

            //== Submit Payment info. ==//

            // Submit Payment Info.
            if (ccnumber.parent().hasClass('good') &&
                ccexp.parent().hasClass('good') &&
                cccvc.parent().hasClass('good') && phone.parent().hasClass('acceptable')) {

                // setTimeout(function() {
                // Invalid zip
                // if (jQuery(this).hasClass('invalid-zip') && jQuery('#checkout #data .method').hasClass('refresh')) {

                //     // Set up the alert
                //     var input = zip.attr('placeholder');
                //     var message = 'The ' + input + ', State and/or Country you entered do not match. </br>Please check these fields to proceed.';

                //     // Show alert
                //     alertPopup(message);
                // }

                // // Invalid email
                // if (jQuery(this).hasClass('invalid-email') && jQuery('#checkout #data .method').hasClass('refresh')) {

                //     // Set up the alert
                //     var input = email.attr('placeholder');
                //     var message = 'The email address entered is invalid. </br>Please update this field to proceed.';

                //     // Show alert
                //     alertPopup(message);
                // }

                // Process
                // else {

                // Set values
                var ccnumberVal = ccnumber.val();
                var fnameVal = fname.val();
                var lnameVal = lname.val();
                var countryVal = countryVal = jQuery('#checkout #data form .inp.country select').val();
                var ccmonthVal = ccexp.val().split('/')[0];
                var ccyearVal = ccexp.val().split('/')[1];
                var cccvcVal = cccvc.val();
                var differentBillingAddress = false;


                // fire event
                gulog.fireAndLogEvent("Submit Order");

                // check for custom billing address
                if (jQuery('#billing_address_section #billing_other').hasClass('active')) {
                    differentBillingAddress = true;
                }


                // check for different billing address
                if (differentBillingAddress) {
                    //check billing address fields
                    if (billing_lnameField.hasClass('good') &&
                        billing_address1Field.hasClass('good') &&
                        billing_cityField.hasClass('good') &&
                        !billing_stateField.hasClass('bad') &&
                        billing_countryField.hasClass('good') &&
                        billing_zipField.hasClass('good')) {

                        overrideBillingAddress(fnameVal, lnameVal, ccnumberVal, ccmonthVal, ccyearVal, cccvcVal);
                        // getShopifyTKN is called once billing response returns
                    }
                    //some field is bad, hide loading
                    else {
                        hideLoading();
                    }

                }
                // if not, get tkn
                else {
                    if (gu_payment == 1) {
                        // get the Shopify Token
                        getShopifyTKN(fnameVal, lnameVal, ccnumberVal, ccmonthVal, ccyearVal, cccvcVal);
                    } else if (gu_payment == 2) {
                        var card = {
                            number: ccnumberVal.split(' ').join(''),
                            exp_mo: ccmonthVal.split(' ').join(''),
                            exp_yr: ccyearVal.split(' ').join(''),
                            cvc: cccvcVal.split(' ').join('')
                        };
                        getStripeTKN(card, getTokenCallback);
                    }
                }

            }
            else {

                if (!ccnumber.parent().hasClass('good')) {
                    ccnumber.parent().addClass('bad');
                }

                if (!ccexp.parent().hasClass('good')) {
                    ccexp.parent().addClass('bad');
                }

                if (!cccvc.parent().hasClass('good')) {
                    cccvc.parent().addClass('bad');
                }

                //if phone is required and not filled out, highlight it now
                if (!phone.parent().hasClass('acceptable')) {
                    phone.parent().addClass('bad');
                }
            }

            //update selected units and send out
            buildUnits(true);
        });
    });
}
//Sales Popup Modifier v2.2
function addObserverIfDesiredNodeAvailable() {
    var yoBox = document.querySelectorAll(".yo-notification")[0];
    if (!yoBox) {
        //The node we need does not exist yet.
        //Wait 500ms and try again
        window.setTimeout(addObserverIfDesiredNodeAvailable, 500);
        return;
    }

    // create an observer instance
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            var theText = jQuery(yoBox).find('.yo-message').contents().get(0).nodeValue;
            var justNow = jQuery(yoBox).find('.yo-message').find("p").text();
            //Edward M. from Pierre Part, Louisiana just purchased: 1 BAKBLADE ELITE 2.0

            var name = theText.split(" from");
            name = name[0];
            var firstHalf = theText.split(" just");
            var location = firstHalf[0].split("from ");
            location = location[location.length - 1]
            var secondHalf = theText.split("purchased: ");
            var product = secondHalf[secondHalf.length - 1];
            // console.log("Name: " + name);
            // console.log("Location: " + location);
            // console.log("Product: " + product);
            // console.log(theText);
            // console.log(justNow);

            var finalText = popupText.replace("__name", name);
            finalText = finalText.replace("__location", location);

            //check for product name override
            if (typeof salesPopupProductNameOverride === "undefined") {
                finalText = finalText.replace("__product", product);
            } else {
                finalText = finalText.replace("__product", salesPopupProductNameOverride);
            }

            //theText = "Intercepted";
            jQuery(yoBox).find('.yo-message').contents().get(0).nodeValue = finalText;
            jQuery(yoBox).find('.yo-message').find("p").text(popupTime);

            //Replace the popup image if it exists
            if (typeof salesPopupImage != "undefined") {
                jQuery(yoBox).find('.yo-event-image a').css("background-image", "url(" + salesPopupImage + ")");
            }

        });
    });

    var config = {
        attributes: true,
        childList: true,
        characterData: true
    };
    observer.observe(yoBox, config);
}

//capture popup text on page
var popupText = "Someone  from __location just purchased: __product";
var popupTime = "just now";

// Register our callback for when all remote data is loaded
//guRemoteDataRegisterCallback(PopupTranslation);
PopupTranslation();

//translate popup text
function PopupTranslation() {
    popupText = guSubstituteString(popupText);
    popupTime = guSubstituteString(popupTime);
}

addObserverIfDesiredNodeAvailable();
// LiveChat v1.1
// <!-- Start of LiveChat (www.livechatinc.com) code -->
var hasOffer = urlContainsPath('offer');
var isMobile = urlContainsPath('mobile');
// var splash = urlContainsPath('splash');

if ( hasOffer || isMobile ) {

    console.log('LiveChat On');

    var gu_chat = gu_chat_default;

    if (gu_qs['chat'] == '0' || gu_qs['chat'] == '1') {
        gu_chat = gu_qs['chat'];
    }
    if (gu_qs['guchat'] == '0' || gu_qs['guchat'] == '1') {
        gu_chat = gu_qs['guchat'];
    }
    if (gu_chat == '1') {

        window.__lc = window.__lc || {};
        window.__lc.license = 9973355;

        (function() {
            var lc = document.createElement('script');
            lc.type = 'text/javascript';
            lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(lc, s);
        })();
    }

    // <!-- End of LiveChat code -->

    // <!-- chat css fixes -->

    var gu_chat = gu_chat_default;

    if (gu_qs['chat'] == '0' || gu_qs['chat'] == '1') {
        gu_chat = gu_qs['chat'];
    }
    if (gu_qs['guchat'] == '0' || gu_qs['guchat'] == '1') {
        gu_chat = gu_qs['guchat'];
    }
    if (gu_chat == '1') {
        function waitForChat(elementPath, callBack) {
            window.setTimeout(function() {
                if (jQuery(elementPath).length) {
                    callBack(elementPath, jQuery(elementPath));
                } else {
                    waitForChat(elementPath, callBack);
                }
            }, 500)
        }
        waitForChat("#chat-widget-container", function() {


            jQuery("<style type='text/css'>#chat-widget-container{z-index:214748364799999 !important;}</style>").appendTo('head');
            jQuery("#buy-now").click(function() {
                jQuery("<style type='text/css'>@media only screen and (max-width:50em){#chat-widget-container{bottom:60px !important;}}</style>").appendTo('head');
            });


        });


        function waitForPrice(elementPath, callBack) {
            window.setTimeout(function() {
                if (jQuery(elementPath).length) {
                    callBack(elementPath, jQuery(elementPath));
                } else {
                    waitForPrice(elementPath, callBack);
                }
            }, 500)
        }
        waitForPrice("#offer #price-tab", function() {


            jQuery("#offer #price-tab button").click(function() {
                jQuery("<style type='text/css'>#chat-widget-container, #livechat-eye-catcher{opacity:0 !important}</style>").appendTo('head');
            });


        });


    }

} else {
    console.log('LiveChat off');
}
// Pass Query Params (must be last block) V1.1
// Note: This script block must go after all other script blocks
// Fix up all links on the page with this page's querystring
function gu_linkfix() {
    var links = document.links;
    for (var i = 0; i < links.length; i++) {
        var hashValue = links[i].hash;
        var protocol = links[i].protocol;
        var hostname = links[i].hostname;
        var pathname = links[i].pathname;

        // Merge the link's querystring with querystring parameters in gu_qs[].
        // gu_qs[] wins if they both have the same parameter.
        var originalqs = [];
        if (links[i].search != "") { // Only parse the link's querystring if there's something there
            originalqs = gu_deparam(links[i].search);
        }
        for (var k in gu_qs) {
            if (typeof gu_qs[k] != "undefined") {
                originalqs[k] = gu_qs[k];
            }
        }

        var qs_str = gu_qs_to_str(originalqs);

        // Put humpty dumpty together again
        if (protocol != "mailto:") {
            links[i].href = protocol + "//" + hostname + pathname + qs_str + hashValue;
        }
    }
}

// Do it
gu_linkfix();
if (document.location.href.indexOf('gubanner=0') === -1 && document.getElementById('countdownTimer')) {

    Date.prototype.addHours = function(h) {
        this.setTime(this.getTime() + (h*60*60*1000));
        return this;
    };

    //  TODO: check data attributes for time & provide option to add days
    var countDownDate;

    if (localStorage.getItem('endTime') !== null) {
        countDownDate = new Date(localStorage.getItem('endTime'));
    } else {
        countDownDate = new Date().addHours(1);
        localStorage.setItem('endTime', new Date().addHours(1));
    }

    var countdownTimerWrapper = "<div id='countdownTimerWrapper'></div>";
    var countdownTimerElem = document.getElementById('countdownTimer');
    var clockStructure = '<div id="countdownTimerContainer">\n' +
        '    <div class="timer-column">\n' +
        '        <div class="timer-number hours">00</div>\n' +
        '        <div class="timer-label">HOURS</div>\n' +
        '    </div>\n' +
        '    <div class="timer-column">\n' +
        '        <div class="timer-number mins">00</div>\n' +
        '        <div class="timer-label">MINUTES</div>\n' +
        '    </div>\n' +
        '    <div class="timer-column">\n' +
        '        <div class="timer-number secs">00</div>\n' +
        '        <div class="timer-label">SECONDS</div>\n' +
        '    </div>\n' +
        '</div>';


    countdownTimerElem.innerHTML = countdownTimerWrapper;
    document.getElementById('countdownTimerWrapper').innerHTML = clockStructure;

    var x = setInterval(function() {

        var now = Date.now();
        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (days < 10) { days = '0' + days; }
        if (hours < 10) { hours = '0' + hours; }
        if (minutes < 10) { minutes = '0' + minutes; }
        if (seconds < 10) { seconds = '0' + seconds; }

        document.querySelector('.timer-number.hours').innerHTML = hours.toString();
        document.querySelector('.timer-number.mins').innerHTML = minutes.toString();
        document.querySelector('.timer-number.secs').innerHTML = seconds.toString();

        if (distance < 0) {
            clearInterval(x);
            document.getElementById('countdownTimerWrapper').innerHTML = clockStructure;
        }
    }, 1000);
}

//immediately try loading in the disclaimer if we're on an offer page
var disclaimerLoadAttempts = 0;
if ( hasOffer || isMobile )
{
    getBrandConfig();
    //No longer loading disclaimer
    disclaimerLoader();
}

function getBrandConfig()
{
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "https://9xuzl3lle5.execute-api.us-west-2.amazonaws.com/prod/offers/" + gu_offer + "/web-config", true);
    xhr.responseType = 'json';

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status < 300 && xhr.status >= 200) {
                if (xhr.response) {

                    //if we're in US and on English, we can attempt banner load
                    if (gu_language === "en" && gu_country === "us") {
                        bannerLoader(xhr.response);
                    }
                }
            }
            else {
                console.log("Brand config failed to load");
            }
        }
    };
    xhr.send(JSON.stringify({}));
}

function disclaimerLoader()
{
    //check if opentag vars have been defined
    if (typeof gu_qs.br == "undefined" || typeof gu_qs.imor == "undefined")
    {
        if (disclaimerLoadAttempts < 100)
        {
            setTimeout(function()
            {
                disclaimerLoadAttempts++;

                disclaimerLoader();
            }, 500);
        }
        else
        {
            // Opentag brand name var isn't defined
            //gulog.error("CODE RED: " + gu_offer + " has no definition for brand name.", null, {severity: "critical"});
        }
    }
    else
    {
        //opentag has loaded
        var ppuDisclaimer = '<div id="footer-disclaimer">\
                <em><strong>About This Site</strong></em>\
                <p>GiddyUp is a curator of innovative products and an official __brand retailer. Weâ€™ve partnered with the inventors of __brand to present a special offer that you wonâ€™t find anywhere else. To support these inventors, please consider buying your __brand here, rather than 3rd-Party retailers.</p>\
                </div>';

        var cpaDisclaimer = '<div id="footer-disclaimer">\
                <em><strong>About This Site</strong></em>\
                <p>Purchasing here is buying directly from __brand, the inventors of this innovative solution. __brand has partnered with GiddyUp, a curator of innovative products, to present a special offer that you wonâ€™t find anywhere else. To support these inventors, please consider buying your __brand on this site, rather than 3rd-Party retailers.</p>\
                </div>';

        if (typeof gu_qs.imor == "undefined" || gu_qs.imor === "1") {
            // Opentag MOR var either isn't defined or is set to PPU
            jQuery('#footer .copyright').before(ppuDisclaimer);
        }
        else {
            jQuery('#footer .copyright').before(cpaDisclaimer);
        }

        //substitute element for current language
        guSubstituteContent(jQuery('#footer #footer-disclaimer')[0]);
        //replace the brand token with the current brand
        jQuery('#footer-disclaimer p').text(jQuery('#footer-disclaimer p').text().replace(/__brand/g, gu_qs.br));
        //slide the final disclaimer down
        jQuery('#footer #footer-disclaimer').slideDown();

    }
}

function bannerLoader(config)
{
    //assess qs for overrides
    var gubn = gu_qs['gubn'];
    var gunt = gu_qs['gunt'];

    //get info from config
    var headline = null;
    var headline2 = null;
    //default the colors to black friday because they look nice
    var bgColor = '#2C2C2C';
    var timerBgColor = '#B52E25';

    //check for timer vis
    var timerVis = (gunt === '0') ? 'none' : 'block';

    //default end time to total end time
    //we'll check for a followup and adjust if needed
    var deadline = config.bannerend;

    //check to make sure that the banner is both enabled and that we're after the start date
    if (config.bannerenabled === "enabled" && getTimeRemaining(config.bannerstart).total < 0  && getTimeRemaining(config.bannerend).total > 0 && gubn !== '0')
    {
        //we need to check if there is an upcoming followup and set our end date to the start of that
        if (config.followupbannerstart && getTimeRemaining(config.followupbannerstart).total > 0)
        {
            deadline = config.followupbannerstart;
        }

        //we are now within the window of operation which means we will definitely show something. Now we determine if it's main or secondary (if we have one)
        if (config.followupbannerstart && getTimeRemaining(config.followupbannerstart).total < 0)
        {
            //we have a followup, let's check if we're after its start date. If so, all content comes from it. If not, the main banner
            headline = config.followupbannerheadline1;
            headline2 = config.followupbannerheadline2;

            bgColor = config.followupbannerbgcolor ? bgColor = config.followupbannerbgcolor : bgColor = config.bannerbgcolor;
            timerBgColor = config.followupbannertimerbgcolor ? timerBgColor = config.followupbannertimerbgcolor : timerBgColor = config.bannerbgcolor;

        }
        else
        {
            //we have a followup, let's check if we're after its start date. If so, all content comes from it. If not, the main banner
            headline = config.bannerheadline1;
            headline2 = config.bannerheadline2;

            bgColor = config.bannerbgcolor;
            timerBgColor = config.bannertimerbgcolor;
        }


        var secondLineVis = (headline2 === null || headline2 === 'null') ? 'none' : 'block';

        var holidayBanner = document.createElement('div');
        holidayBanner.innerHTML = "<div id='holidayBanner' style='color: " + lightOrDark(bgColor) + "; text-align: center; padding: 20px 20px 15px 20px !important; background-color: " + bgColor + "; display: none; position: relative; z-index: 1001;'>\
                <span id='bannerHeadline' style='font-size: 28px; font-family: Arial; display: block; padding-right: 5px;'>\
                    <strong>" + headline + "</strong> \
                    <br/>\
                    <strong style='display:" + secondLineVis +"'>" + headline2 + "</strong> \
                </span>\
                <span style='font-size: 16px; display: "+ timerVis +"; padding: 0px 5px 5px 5px; font-weight: bold; max-width: 282px; background: "+ timerBgColor +"; margin: 10px auto !important; border-radius: 2px; -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.35); -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.35); box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.35); color: "+ lightOrDark(timerBgColor) +"; letter-spacing: 1.45px; font-weight: 700;' id='bannerSubHead'>\
                </span>\
		    </div>";

        //check to see if we're displaying countdown instead of text

        var timeRemaining = getTimeRemaining(deadline);
        var bodyElement = document.querySelector('body');

        //check for positive time remaining
        if (timeRemaining.total > 0) {
            bodyElement.insertBefore(holidayBanner, bodyElement.firstChild);
            initializeClock('bannerSubHead', deadline);
        }

        setTimeout(function () {
            jQuery('#holidayBanner').slideDown();
            //reduce astronomical sales popup z-index
            jQuery('#yoHolder').css('z-index', 100);
        }, 1000);

    }
}

function lightOrDark(color)
{
    //nifty little fn for determining the color for text based on the background color -- inspired by Andreas Wik
    var r, g, b, hsp;

    //make sure hex has no hash
    color = color.replace(/[^0-9A-F]/gi, '');
    //convert hex to rgb
    var bigint = parseInt(color, 16);
    r = (bigint >> 16) & 255;
    g = (bigint >> 8) & 255;
    b = bigint & 255;


    // HSP equation from http://alienryderflex.com/hsp.html
    hsp = Math.sqrt(
        0.299 * (r * r) +
        0.587 * (g * g) +
        0.114 * (b * b)
    );

    // Using the HSP value, determine whether the color is light or dark
    if (hsp>127.5) {

        return '#333';
    }
    else {

        return 'white';
    }
}

function initializeClock(id, deadline){
    var clock = document.getElementById(id);
    var timeinterval = setInterval(function(){
        var t = getTimeRemaining(deadline);
        clock.innerHTML = '<div style="font-size:30px; padding: 0px 3px; margin: -5px 0;"><span>'+ t.days + ' : ' +  t.hours + ' : ' + t.minutes + ' : ' + t.seconds + '</span></div>\
	    	          <div style="font-size:13px; letter-spacing: 1.2px;"><span> DAYS <span style="padding:0px 10px;">-</span> HRS <span style="padding:0px 10px;">-</span> MINS <span style="padding:0px 10px;">-</span> SEC</span></div>';
        if(t.total <= 0){
            clearInterval(timeinterval);
        }
    },1000);
}

function getTimeRemaining(endtime){
    var countDownDate = new Date(endtime);
    countDownDate = countDownDate.getTime() + (countDownDate.getTimezoneOffset()*60*1000);

    var t = countDownDate - Date.now();
    var seconds = Math.floor( (t/1000) % 60 );
    var minutes = Math.floor( (t/1000/60) % 60 );
    var hours = Math.floor( (t/(1000*60*60)) % 24 );
    var days = Math.floor( t/(1000*60*60*24) );

    if (days < 10) {
        days = '0' + days;
    }
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }

    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function replaceTokens (rawPhrase)
{
    //This will target words inside [[double square brackets]]
    var regex = /\[\[(.*?)\]\]/;

    //get the var we will need to evaluate
    var token = rawPhrase.match(regex)[1];

    try {
        //get result of token from siteVars
        var result = eval(token);

        //shove the result back into the raw phrase
        var newPhrase = rawPhrase.replace(regex, result);

        return newPhrase;
    }
    catch(e)
    {
        //tried to eval something we don't have defined
        //return false so we can bail out completely
        return false;
    }
}
