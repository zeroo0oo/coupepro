// Variable to hold request
var request;

var url = window.location.href;
jQuery(document).ready(function () {
    jQuery('#url').val(url);
});

// Bind to the submit event of our form
jQuery(document).submit("#sfsdqsubmit", function (event) {
    if (request) {
        request.abort();
    }
    // setup some local variables
    var jQueryform = jQuery(this);

    // Let's select and cache all the fields
    var jQueryinputs = jQueryform.find("input, select, button, textarea");

    // Serialize the data in the form
    var serializedData = jQueryform.serialize();

    console.log(serializedData);
    // Let's disable the inputs for the duration of the Ajax request.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    jQueryinputs.prop("disabled", true);

    // Fire off the request to /form.php
    request = jQuery.ajax({
        url: 'https://docs.google.com/spreadsheets/d/14nrORrQrTBsGevpV7ZGTJkxqnLwjdrAYOw0LmhtjUBI/edit?usp=sharing',
        // url: "https://script.google.com/macros/s/1h_959rKk9Kdvc88Dh0UxvXAaZeuCdFVwoswK5ftmHU4/exec",
        type: "post",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
        // Log a message to the console
        console.log("Hooray, it worked!");
        console.log(response);
        console.log(textStatus);
        console.log(jqXHR);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        // Log the error to the console
        console.error(
            "The following error occurred: " +
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        jQueryinputs.prop("disabled", false);
        console.log("It's running");
        // window.location.href = 'success';
    });

    // Prevent default posting of form
    event.preventDefault();
});
