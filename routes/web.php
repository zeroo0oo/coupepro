<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'core'], function(){
    Route::get('/', 'HomeController@product');

    Route::get('/privacy.htm', function(){
        return view('privacy');
    });


    Route::get('/terms.htm', function(){
        return view('terms');
    });


    Route::get('/about.htm', function(){
        return view('about');
    });

    Route::post('/payment', [
        'uses'  => 'HomeController@strip',
        'as'    => 'strip'
    ]);

    Route::post('paypal', [
        'uses'  => 'HomeController@paypal',
        'as'    => 'paypal'
    ]);

    Route::post('/variant', [
        'uses'  => 'HomeController@variants',
        'as'    => 'variant'
    ]);

    Route::post('/postal', [
        'uses'  => 'HomeController@postal',
        'as'    => 'postal'
    ]);

    Route::post('/users', [
        'uses'  => 'HomeController@users',
        'as'    => 'users'
    ]);

    Route::post('/create/order', [
        'uses'  => 'HomeController@createOrder',
        'as'    => 'create.order'
    ]);

    Route::get('success', [
        'uses'  => 'HomeController@success',
        'as'    => 'paypal.success'
    ]);

    Route::get('paypal/cancel/{id}', [
        'uses'  => 'HomeController@cancel',
        'as'    => 'paypal.cancel'
    ]);

//Route::get('/{product}', 'HomeController@product')->name('find');

});
